function mensagem (tipo, texto) {
	var m;
	switch (tipo) {
		case 'error':
			m = '<div class="activity-item"> <i class="fa fa-times text-danger"></i><div class="activity"> '+texto+'</div></div>';
			break;
		case 'success':
			m = '<div class="activity-item"> <i class="fa fa-check text-success"></i><div class="activity"> '+texto+'</div></div>';
			break;
		case 'warning':
			m = '<div class="activity-item"> <i class="fa fa-exclamation-circle text-warning"></i><div class="activity"> '+texto+'</div></div>';
			break;
		case 'information':
			m = '<div class="activity-item"> <i class="fa fa-exclamation text-info"></i><div class="activity"> '+texto+'</div></div>';
			break;	
		default:
			alert('Erro ao carregar mensagem!');
			break;
	}
	var n = noty({
	    text        : m,
	    type        : tipo,
	    dismissQueue: true,
	    layout      : 'topRight',
	    closeWith   : ['click'],
	    theme       : 'relax',
	    maxVisible  : 10,
	    timeout     : 5000,
	    animation   : {
	        open  : 'animated flipInX',
	        close : 'animated flipOutX',
	        easing: 'swing',
	        speed : 500
	    }	    
	});
}

function base64Encode(str) {
    var CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    var out = "", i = 0, len = str.length, c1, c2, c3;
    while (i < len) {
        c1 = str.charCodeAt(i++) & 0xff;
        if (i == len) {
            out += CHARS.charAt(c1 >> 2);
            out += CHARS.charAt((c1 & 0x3) << 4);
            out += "==";
            break;
        }
        c2 = str.charCodeAt(i++);
        if (i == len) {
            out += CHARS.charAt(c1 >> 2);
            out += CHARS.charAt(((c1 & 0x3)<< 4) | ((c2 & 0xF0) >> 4));
            out += CHARS.charAt((c2 & 0xF) << 2);
            out += "=";
            break;
        }
        c3 = str.charCodeAt(i++);
        out += CHARS.charAt(c1 >> 2);
        out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
        out += CHARS.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
        out += CHARS.charAt(c3 & 0x3F);
    }
    return out;
}
 function mascara () {
 	jQuery(function($){
 	   $("[tipo='data']").mask("00/00/0000", {placeholder: "__/__/____"}); 	  
 	   $("[tipo='hora']").mask("00:00", {placeholder: "__:__"}); 
 	   $("[tipo='telefone']").mask("(00) 0000-0000",{placeholder:"(__) ____-____"});
 	   $("[tipo='celular']").mask("(99) 99999-9999",{placeholder:"(__) _____-____"}).focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(99) 99999-9999");  
            } else {  
                element.mask("(99) 9999-99999");  
            }  
        });
       $("[tipo='celular']").focus(function (event){
       	    var target,element;
       	    target = (event.currentTarget) ? event.currentTarget : event.srcElement;
       		element=$(target);
       		element.unmask();  
       		element.mask("(99) 99999-9999");
       });
 	   $("[tipo='cpf']").mask("000.000.000-00",{placeholder:"___.___.___-__"});
 	   $("[tipo='cep']").mask("00000-000",{placeholder:"_____-___"}); 
 	   $("[tipo='numero']").mask('0#');
       $("[tipo='real']").mask("#0,00", {reverse: true});
 	}); 	  
 }

 function data(hoje){
    if(hoje==true){
        $("[tipo='data']").datepicker({
        todayBtn: "linked",
        language: "pt-BR",
        todayHighlight: true,
        autoclose: true
        }); 
    }else{
        $("[tipo='data']").datepicker({
        todayBtn: "linked",
        language: "pt-BR",
        autoclose: true
        });
    }
    $("[tipo='hora']").datetimepicker({
        format: 'HH:ss'
    });
}



function tabela (tabela, orderable, order) {
    if(orderable==null){
        if(order==null){
            return $('#'+tabela).DataTable({            
                "info" : false,
                "searching": false,
                "lengthChange": false,
                "pageLength": 5,
                "language":{
                    "emptyTable": "Nenhum dado encontrado.",
                    "paginate": {                                        
                            "first":      "Primeiro",
                            "last":       "Último",
                            "next":       "Próximo",
                            "previous":   "Anterior"
                        }
                }
            });
        }else{
            return $('#'+tabela).DataTable({            
                "info" : false,
                "searching": false,
                "lengthChange": false,
                "pageLength": 5,
                "order": [[ order, "desc" ]],
                "language":{
                    "emptyTable": "Nenhum dado encontrado.",
                    "paginate": {                                        
                            "first":      "Primeiro",
                            "last":       "Último",
                            "next":       "Próximo",
                            "previous":   "Anterior"
                        }
                }
            });
        }
         
    }else{
        if(order==null){
            return $('#'+tabela).DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": orderable }
                  ],
                "info" : false,
                "searching": false,
                "lengthChange": false,
                "pageLength": 5,
                "language":{
                    "emptyTable": "Nenhum dado encontrado.",
                    "paginate": {                                        
                            "first":      "Primeiro",
                            "last":       "Último",
                            "next":       "Próximo",
                            "previous":   "Anterior"
                        }
                }
            });
        }else{
            return $('#'+tabela).DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": orderable }
                  ],
                "info" : false,
                "searching": false,
                "lengthChange": false,
                "pageLength": 5,
                "order": [[ order, "asc" ]],
                "language":{
                    "emptyTable": "Nenhum dado encontrado.",
                    "paginate": {                                        
                            "first":      "Primeiro",
                            "last":       "Último",
                            "next":       "Próximo",
                            "previous":   "Anterior"
                        }
                }
            });
        }

         

    }
    
}
