<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Template {
    function show($view, $data = array())
    {
        // Get current CI Instance
        $CI = & get_instance();
        
        $data['view'] = $CI->session->userdata('view');
 
        // Load template views
        $CI->load->view('template/header', $data);      
        $CI->load->view($view, $data);
        $CI->load->view('template/footer', $data);
    }
 
    function menu($view)
    {
        // Get current CI Instance
        $CI = & get_instance();
 
        // Load menu template
        $CI->load->view('template/menu', array('view' => $view));
    }
    
    
    /*
     * Método loadCabecalho carrega os dados:
     * 
     *  ->nome_usuario
     *  ->nome_unidade
     *  ->nome_faculdade
     * 
     * tais dados sempre estão presentes nas telas do sistema
     */
    function loadCabecalho($title)
    {
        $CI = & get_instance();
        
        $data['page_title'] = $title;
        $CI->load->model('unidade_model');
        $CI->load->model('user_model');
        $CI->load->model('faculdade_model');
        $id_user = $CI->session->userdata('user');
        $id_unidade = $CI->session->userdata('unidade');  
        
        $usuario = $CI->user_model->get($id_user);   
        $data['user'] = $usuario;
        $unidade = $CI->unidade_model->get($id_unidade);
        $data['unidade'] = $unidade;
        $faculdade = $CI->faculdade_model->get($unidade['unidade_idfaculdade']);
        $data['faculdade'] = $faculdade;
        
        return $data;
    }
    
    /*
     * Método validacaoUsuario()
     * seta as regras de validação para usuário, sempre que um 
     * usuário é cadastrado tais verificações devem ser feitas. 
     *
     */
    
    function validacaoUsuario(){
        $CI = & get_instance();
        $CI->load->model('estado_model');
        if ($CI->session->userdata('view')==6)
            $CI->form_validation->set_rules('nome_faculdade','Faculdade','required|is_natural|contains[faculdade.idfaculdade]');
        $CI->form_validation->set_rules('nome_usuario','Nome','required|alpha_brazilian');
        $CI->form_validation->set_rules('rg_usuario','RG','required'); //verificar
        $CI->form_validation->set_rules('data_nascimento_usuario','Data de Nascimento',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_age[15]','valid_date'));
        if ($CI->input->post('telefone_usuario')){
            $CI->form_validation->set_rules('telefone_usuario','Telefone','required|exact_length[14]|regex_match[/\([1-9]{2}\)\ [2-9]\d{3}\-\d{4}$/]');
            $CI->form_validation->set_rules('celular_usuario','Celular',array('min_length[14]','max_length[15]','regex_match[/\([1-9]{2}\)\ [2-9]\d{3,4}\-\d{4}$/]'));
        }else{
            $CI->form_validation->set_rules('celular_usuario','Celular',array('required','min_length[14]','max_length[15]','regex_match[/\([1-9]{2}\)\ [2-9]\d{3,4}\-\d{4}$/]'));
        }
        $CI->form_validation->set_rules('numero_usuario','Numero','is_natural');
        $CI->form_validation->set_rules('cep_usuario','CEP','regex_match[/\d{5}\-\d{3}$/]');
        $CI->form_validation->set_rules('estado_usuario','UF','required|is_natural|contains[estado.id]');
        $CI->form_validation->set_rules('cidade_usuario','Cidade','required|contains[cidade.nome,cidade.estado,estado_usuario]');
        //$CI->form_validation->set_rules('logradouro_usuario','Logradouro','required'); //verificar 
        //$CI->form_validation->set_rules('bairro_usuario','Bairro','required'); //verificar
        
    }

    /*
     * Método updateUsuario()
     * valida as entradas e atualiza o registro de um usuário no banco de dados 
     * Pode-se fazer regras antes da chamada para validação extras
     * Caso haja o $id_usuario, atualiza de acordo com o $id caso contrário, pelo CPF
     */
    public function loadUsuario()
    {
        $CI = & get_instance();
        $this->validacaoUsuario();
        if ($CI->form_validation->run()){
            $CI->load->model('user_model');
            
            $sql_data = array(
                'nome_usuario' => $CI->input->post('nome_usuario'),
                'nome_social_usuario' => $CI->input->post('nome_social_usuario')!=null?$CI->input->post('nome_social_usuario'):NULL,
                'rg_usuario' => $CI->input->post('rg_usuario'),
                'data_nascimento_usuario' => DateTime::createFromFormat('d/m/Y', $CI->input->post('data_nascimento_usuario'))->format('Y-m-d'),
                'cpf_usuario' => $CI->input->post('cpf_usuario'),
                'logradouro_usuario' => $CI->input->post('logradouro_usuario'),
                'numero_usuario' => $CI->input->post('numero_usuario'),
                'bairro_usuario' => $CI->input->post('bairro_usuario'),
                'complemento_endereco_usuario' => $CI->input->post('complemento_endereco_usuario'),
                'cep_usuario' => $CI->input->post('cep_usuario'),
                'cidade_usuario' => $CI->input->post('cidade_usuario'),
                'estado_usuario' => $CI->estado_model->getUF($CI->input->post('estado_usuario')),
                'email_usuario' => $CI->input->post('email_usuario'),
                'telefone_usuario' => $CI->input->post('telefone_usuario'),
                'celular_usuario' => $CI->input->post('celular_usuario'),
                'status'=>1
            );
            return $sql_data;
        }
        return FALSE;
    }


    public function controle_acesso($nome,$classe)
    {
        $CI = & get_instance();
        
        if ( $nome=='get_data'){
            return;
        }
        if (!$CI->session->userdata('unidade')) {
            $CI->session->sess_destroy();
            $this->session->set_flashdata('acesso_temp', uri_string());
            redirect('login');
        }
        //----------------------------------------------------------------- Verificação de Email -----------------
        $qry_email = $CI->db->where('idusuario',$CI->session->userdata('user'))->get('usuario');
        $user = $qry_email->row_array();
        if ((!isset($user['email_usuario']) || $user['email_usuario']=='') && ($nome!='cadastro_incompleto' && $nome!='conclui_cadastro'))
            redirect('inicio/cadastro_incompleto');
        //-------------------------------------------------------------------------------------------------------
        
        $qry_aluno = $CI->db->where('usuario_idusuario',$CI->session->userdata('user'))->get('aluno');
        $user = $qry_aluno->row_array();
        if ($user['realizacao_ensino_fund_aluno']==null && $CI->session->userdata('level')==1 && ($nome!='socio' && $nome!='questionario'))
            redirect('aluno/socio');
        
        //-----------------------------------------------------------------Verificação de nível de acesso--------
        
        $sql_acesso = array('classe_metodo' => $classe , 
                        'nome_metodo' => $nome
                        );
        $qry_metodo = $CI->db->where($sql_acesso)->get('metodo');
        $metodo = $qry_metodo->row_array();
        if (count($metodo)==0){
            redirect('inicio');
        }
        else{
            if ($metodo['nivel_metodo']<=$CI->session->userdata('level')){
                if ($CI->session->userdata('view')<$metodo['nivel_metodo']){
                    $CI->session->set_userdata(array('view' => $metodo['nivel_metodo'])) ;
                }    
            }
            else {
                redirect("inicio");
            }
        }
        
    }


    public function gerar_senha()
    {
        $tamanho = 10;
        
        $lmin = 'abcdefghijklmnopqrstuvwxyz';
        $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $num = '1234567890';
        $simb = '!@#$%*-';
        
        $retorno = '';
        $caracteres = '';
        
        $caracteres .= $lmin;
        $caracteres .= $lmai;
        $caracteres .= $num;
        $caracteres .= $simb;
        $len = strlen($caracteres);
        for ($n = 1; $n <= $tamanho; $n++) {
            $rand = mt_rand(1, $len);
            $retorno .= $caracteres[$rand-1];
        }
        return $retorno;        
    }

    public function redirect($local)
    {
        $CI = & get_instance();
 
        $times = $CI->db->query_times;                   // Get execution time of all the queries executed by controller
        foreach ($CI->db->queries as $key => $query) {
            if ((strpos($query, 'imagem_evento')===FALSE)&&((strpos($query, 'SELECT')===FALSE)&&((!(strpos($query, 'ci_sessions')===FALSE)&&(strpos($query, 'DELETE')===FALSE))||(strpos($query, 'ci_sessions')===FALSE)))){ 
                $sql_log['query_log'] = $query;
                $sql_log['time_log'] = $times[$key]; // Generating SQL file alongwith execution time
                $sql_log['usuario_idusuario'] = $CI->session->userdata('user');
                $sql_log['data_hora_log'] = date("Y-m-d H:i:s");
                $CI->db->insert('log_execucao',$sql_log);  
            }
        }
        
        redirect($local);
    }
    
    
    /*
     * Método Encrypt
     * Realiza a criptografia definida em algum dado
     * 
     * 
     */
    public function encrypt($data){
        $key = md5('U2N0E2P');
        return rtrim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256,$key, $data,MCRYPT_MODE_ECB)));
    }
    
    /*
     * Método Decrypt
     * Realiza a criptografia definida em algum dado
     * 
     * 
     */
    public function decrypt($data){
        $key = md5('U2N0E2P');
        return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256,$key, base64_decode($data),MCRYPT_MODE_ECB));
    }
    
}
?>
