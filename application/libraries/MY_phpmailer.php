<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class My_PHPMailer {
    //MY_ para informar ao framework de que se trata de uma classe customizada, ou seja, não faz parte do framework. Pode ser alterada no arquivo config.php em application/config/
    public function My_PHPMailer() {
        require_once('PHPMailer-master/PHPMailerAutoload.php');
    }
    
    
    
    public function enviarEmailCadastro($sql_data)
    {
        
        
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->IsSMTP(); //Definimos que usaremos o protocolo SMTP para envio.
        $mail->SMTPAuth = true; //Habilitamos a autenticação do SMTP. (true ou false)
        
        $mail->SMTPSecure = "tls"; //Estabelecemos qual protocolo de segurança será usado.
        //$mail->Host = gethostbyname("smtp.gmail.com");
        $mail->Host = "smtp.gmail.com";
        
        $mail->Port = 587; //Estabelecemos a porta utilizada pelo servidor do gMail.
        $mail->Username = "noreply.sgc.unesp@gmail.com"; //Usuário do gMail
        $mail->Password = "unesp123"; //Senha do gMail
        $mail->setFrom('noreply.sgc.unesp@gmail.com', 'Sistema de Gestão de Cursinhos da Unesp'); //Quem está enviando o e-mail.
        //$mail->AddReplyTo("response@email.com","Nome Completo"); //Para que a resposta será enviada.
        $mail->Subject = '[SisCursinhos]'."Cadastro - SGC"; //Assunto do e-mail.
        
        
        $mail->Body = '
        
                <!DOCTYPE html>
                <html lang="en">
                <body>
                    <h3>'.$sql_data['nome_usuario'].'</h3>
                    <p>Seu cadastro foi realizado com sucesso no Sistema de Gestão de Cursinhos da UNESP - SGC.</p>
                    <p>Os seu dados para o <strong>primeiro acesso</strong> são:</p>
                    <ul >
                        <li> Email: '.$sql_data['email_usuario'].' ou seu CPF;</li>
                        <li> Senha: '.$sql_data['senha_usuario'].'
                    </ul>
                    <a href="'.base_url().'index.php/login">Ir para o portal</a>
                    
                
                </body>
                </html>';
                              
        $mail->isHTML(true);
        $mail->addAddress($sql_data['email_usuario'], $sql_data['nome_usuario']);
      
        if (!$mail->Send()){
            echo "Não foi possível enviar o e-mail.";
            echo "<b>Informações do erro:</b> " . $mail->ErrorInfo;
            return FALSE;
        }
        return TRUE;
                    
    }

    public function enviarEmailEsqueci($sql_data)
    {
        
        
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->IsSMTP(); //Definimos que usaremos o protocolo SMTP para envio.
        $mail->SMTPAuth = true; //Habilitamos a autenticação do SMTP. (true ou false)
        $mail->SMTPSecure = "tls"; //Estabelecemos qual protocolo de segurança será usado.
        //$mail->Host = gethostbyname("smtp.gmail.com"); //Podemos usar o servidor do gMail para enviar.
        $mail->Host = "smtp.gmail.com";
        
        $mail->Port = 587; //Estabelecemos a porta utilizada pelo servidor do gMail.
        $mail->Username = "noreply.sgc.unesp@gmail.com"; //Usuário do gMail
        $mail->Password = "unesp123"; //Senha do gMail
        $mail->setFrom('noreply.sgc.unesp@gmail.com', 'Sistema de Gestão de Cursinhos da Unesp'); //Quem está enviando o e-mail.
        //$mail->AddReplyTo("response@email.com","Nome Completo"); //Para que a resposta será enviada.
        $mail->Subject = '[SisCursinhos]'."Alteração de Senha - SGC"; //Assunto do e-mail.
        
        
        $mail->Body = '
        
                <!DOCTYPE html>
                <html lang="en">
                <body>
                    <h3>'.$sql_data['nome_usuario'].'</h3>
                    <p>Sua Senha foi modificada.</p>
                    <p>Os seu dados para o <strong>acesso</strong> são:</p>
                    <ul >
                        <li> Email: '.$sql_data['email_usuario'].' ou seu CPF;</li>
                        <li> Senha: '.$sql_data['senha_usuario'].'
                    </ul>
                    <a href="'.base_url().'index.php/login">Ir para o portal</a>
                    
                
                </body>
                </html>';
        $mail->isHTML(true);
        $mail->addAddress($sql_data['email_usuario'], $sql_data['nome_usuario']);
      
        if (!$mail->Send()){
            echo "Não foi possível enviar o e-mail.";
            echo "<b>Informações do erro:</b> " . $mail->ErrorInfo;
            return FALSE;
        }
        return TRUE;
                    
                    
    }

 public function enviarEmailMensagem($sql_data)
    {
        
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->IsSMTP(); //Definimos que usaremos o protocolo SMTP para envio.
        $mail->SMTPAuth = true; //Habilitamos a autenticação do SMTP. (true ou false)
        $mail->SMTPSecure = "tls"; //Estabelecemos qual protocolo de segurança será usado.
        //$mail->Host = gethostbyname("smtp.gmail.com"); //Podemos usar o servidor do gMail para enviar.
        $mail->Host = "smtp.gmail.com";
        
        $mail->Port = 587; //Estabelecemos a porta utilizada pelo servidor do gMail.
        $mail->Username = "noreply.sgc.unesp@gmail.com"; //Usuário do gMail
        $mail->Password = "unesp123"; //Senha do gMail
        $mail->setFrom('noreply.sgc.unesp@gmail.com', 'Sistema de Gestão de Cursinhos da Unesp'); //Quem está enviando o e-mail.
        //$mail->AddReplyTo("response@email.com","Nome Completo"); //Para que a resposta será enviada.
        $mail->Subject = '[SisCursinhos]'.$sql_data['remetente'].': '.$sql_data['assunto']; //Assunto do e-mail.
        
        
        $mail->Body = '
        
                <!DOCTYPE html>
                <html lang="en">
                <body>
                    <h3>'.$sql_data['nome_usuario'].'</h3>
                    <p>Você acaba de receber a seguinte mensagem no SisCursinhos:</p>
                    <p>'.$sql_data['mensagem_enviada'].'</p>
                    <br>
                    <a href="'.$sql_data['caminho'].'">Ver no Portal.</a>
                </body>
                </html>';
        $mail->isHTML(true);
        $mail->addAddress($sql_data['email_usuario'], $sql_data['nome_usuario']);
      
        if (!$mail->Send()){
            echo "Não foi possível enviar o e-mail.";
            echo "<b>Informações do erro:</b> " . $mail->ErrorInfo;
            return FALSE;
        }
        return TRUE;
                    
                    
    }

 public function enviarEmailOuvidoria($sql_data)
    {
        
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->IsSMTP(); //Definimos que usaremos o protocolo SMTP para envio.
        $mail->SMTPAuth = true; //Habilitamos a autenticação do SMTP. (true ou false)
        $mail->SMTPSecure = "tls"; //Estabelecemos qual protocolo de segurança será usado.
        //$mail->Host = gethostbyname("smtp.gmail.com"); //Podemos usar o servidor do gMail para enviar.
        $mail->Host = "smtp.gmail.com";
        
        $mail->Port = 587; //Estabelecemos a porta utilizada pelo servidor do gMail.
        $mail->Username = "noreply.sgc.unesp@gmail.com"; //Usuário do gMail
        $mail->Password = "unesp123"; //Senha do gMail
        $mail->setFrom('noreply.sgc.unesp@gmail.com', 'Sistema de Gestão de Cursinhos da Unesp'); //Quem está enviando o e-mail.
        //$mail->AddReplyTo("response@email.com","Nome Completo"); //Para que a resposta será enviada.
        $mail->Subject = '[SisCursinhos] Ouvidoria'; //Assunto do e-mail.
        
        
        $mail->Body = '
        
                <!DOCTYPE html>
                <html lang="en">
                <body>
                    <h3>'.$sql_data['nome_usuario'].'</h3>
                    <p>Você acaba de receber a seguinte manifestação no SisCursinhos:</p>
                    <p>'.$sql_data['mensagem_enviada'].'</p>
                    <br>
                    a href="'.base_url().'index.php/login">Ir para o Portal.</a>
                </body>
                </html>';
        $mail->isHTML(true);
        $mail->addAddress($sql_data['email_usuario'], $sql_data['nome_usuario']);
      
        if (!$mail->Send()){
            echo "Não foi possível enviar o e-mail.";
            echo "<b>Informações do erro:</b> " . $mail->ErrorInfo;
            return FALSE;
        }
        return TRUE;
                    
                    
    }


}
?>