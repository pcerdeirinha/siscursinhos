<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * MY_Form_validation Class
 *
 * Extends Form_Validation library
 *
 */
class MY_Form_validation extends CI_Form_validation {
    function __construct()
    {
        parent::__construct();
    }

    
    
    // --------------------------------------------------------------------
    /**
     *
     * contains
     *
     * Verifica se a tabela contém o índice
     * @access  public
     * @param   string
     * @return  bool
     */
     function contains($str = '', $field = '')
    {
        
        $CI =& get_instance();
        $campos = explode(',', $field);
        $res = explode('.', $campos[0]);
        $table  = $res[0];
        $column = $res[1];
        $CI->form_validation->set_message('contains', 'O(A) %s informado(a) não existe.'); 
        $CI->db->select('COUNT(*) as total');
        $CI->db->where($campos[0], $str);
        $i = 3;
        $j=0;
        while ($i<=count($campos)){
            $resjoin = explode('.', $campos[$i-2]);    
            $tbjoin = $resjoin[0];
            $cljoin = $resjoin[1];           
            if ($campos[$i-1]{0}=='#'){
                $valor=explode('#',$campos[$i-1]);
            }
            else if ((strpos($campos[$i-1], '.') === false)){
              $valor[1]=$this->_field_data[$campos[$i-1]]['postdata'];  
            } 
            else {
              $valor[1] = $campos[$i-1];
            }
            if (($tbjoin!=$table)&&(!in_array($tbjoin, $tables_joins))){
                $CI->db->join($tbjoin,$campos[$i-2].'='.$valor[1]);   
                $tables_joins[$j++] = $tbjoin;
            }
            else{
                $CI->db->where($campos[$i-2],$valor[1]);
            }
            $i+=2;
            
        }
        //echo $this->db->get_compiled_select();
        $total = $CI->db->get($table)->row()->total;
        return ($total == 0) ? FALSE : TRUE;
        
    }
/**
     *
     * not_contains
     *
     * Verifica se a tabela não contém o índice
     * @access  public
     * @param   string
     * @return  bool
     */
     function not_contains($str = '', $field = '')
    {
        $CI =& get_instance();
        $campos = explode(',', $field);
        $res = explode('.', $campos[0]);
        $table  = $res[0];
        $column = $res[1];
        $CI->form_validation->set_message('not_contains', 'O(A) %s informado(a) já existe.'); 
        $CI->db->select('COUNT(*) as total');
        $CI->db->where($campos[0], $str);
        $i = 3;
        $j=0;
        while ($i<=count($campos)){
            $resjoin = explode('.', $campos[$i-2]);    
            $tbjoin = $resjoin[0];
            $cljoin = $resjoin[1];           
            if ($campos[$i-1]{0}=='#'){
                $valor=explode('#',$campos[$i-1]);
            }
            else if ((strpos($campos[$i-1], '.') === false)){
              $valor[1]=$this->_field_data[$campos[$i-1]]['postdata'];  
            } 
            else {
              $valor[1] = $campos[$i-1];
            }
            if (($tbjoin!=$table)&&(!in_array($tbjoin, $tables_joins))){
                $CI->db->join($tbjoin,$campos[$i-2].'='.$valor[1]);   
                $tables_joins[$j++] = $tbjoin;
            }
            else{
                $CI->db->where($campos[$i-2],$valor[1]);
            }
            $i+=2;
            
        }
        $total = $CI->db->get($table)->row()->total;
        return ($total == 0) ? TRUE : FALSE;
        
    }
    /**
     *
     * decimar_br
     *
     * Verifica se é decimal, mas com virgula no lugar de .
     * @access  public
     * @param   string
     * @return  bool
     */
    public function decimal_br($str)
    {
        $CI =& get_instance();
        $CI->form_validation->set_message('decimal_br', 'O campo %s não contem um valor decimal válido.');
        return (bool) preg_match('/^[\-+]?[0-9]+\,[0-9]+$/', $str);
    }
    /**
     *
     * valid_cpf
     *
     * Verifica CPF é válido
     * @access  public
     * @param   string
     * @return  bool
     */
    function valid_cpf($cpf)
    {
        $CI =& get_instance();
        
        $CI->form_validation->set_message('valid_cpf', 'O %s informado não é válido.');
        $cpf = preg_replace('/[^0-9]/','',$cpf);
        if(strlen($cpf) != 11 || preg_match('/^([0-9])\1+$/', $cpf))
        {
            return false;
        }
        // 9 primeiros digitos do cpf
        $digit = substr($cpf, 0, 9);
        // calculo dos 2 digitos verificadores
        for($j=10; $j <= 11; $j++)
        {
            $sum = 0;
            for($i=0; $i< $j-1; $i++)
            {
                $sum += ($j-$i) * ((int) $digit[$i]);
            }
            $summod11 = $sum % 11;
            $digit[$j-1] = $summod11 < 2 ? 0 : 11 - $summod11;
        }
        
        return $digit[9] == ((int)$cpf[9]) && $digit[10] == ((int)$cpf[10]);
    }
    /**
     * valid_date
     *
     * valida data no pradrao brasileiro
     * 
     * @access  public
     * @param   string
     * @return  bool
     */
    function valid_date($data)
    {
        $CI =& get_instance();
        $CI->form_validation->set_message('valid_date', 'O campo %s não contém uma data válida.');
        $padrao = explode('/', $data);
        return checkdate($padrao[1], $padrao[0], $padrao[2]);
    }
    /**
     * valid_age
     *
     * ídade mínima a partir de uma data
     * 
     * @access  public
     * @param   string
     * @return  bool
     */
    function valid_age($data, $val)
    {
        $CI =& get_instance();
        $CI->form_validation->set_message('valid_age', 'O campo possui idade inferior a '.$val.' anos');
        $date = DateTime::createFromFormat('d/m/Y', $data);
        $dataStr = $date->format('Y-m-d');
        if (strtotime($dataStr)<=strtotime(date('d-m-Y',strtotime('-'.$val.' years'))))
            return TRUE;
        return FALSE;
    }
    /**
     * valid_cep
     *
     * Verifica se CEP é válido
     * 
     * @access  public
     * @param   string
     * @return  bool
     */
    function valid_cep($cep)
    {
        $CI =& get_instance();
        $CI->form_validation->set_message('valid_cep', 'O campo %s não contém um CEP válido.');
        $cep = str_replace('.', '', $cep);
        $cep = str_replace('-', '', $cep);
        $url = 'http://republicavirtual.com.br/web_cep.php?cep='.urlencode($cep).'&formato=query_string';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 0);
        $resultado = curl_exec($ch);
        curl_close($ch);
        if( ! $resultado)
            $resultado = "&resultado=0&resultado_txt=erro+ao+buscar+cep";
        $resultado = urldecode($resultado);
        $resultado = utf8_encode($resultado);
        parse_str( $resultado, $retorno);
        if($retorno['resultado'] == 1 || $retorno['resultado'] == 2)
            return TRUE;
        else
            return FALSE;
    }
    /**
     * valid_phone
     *
     * validação simples de telefone
     *
     * @access  public
     * @param   string
     * @return  bool
     */
    function valid_phone($fone)
    {
        $CI =& get_instance();
        $CI->form_validation->set_message('valid_fone', 'O campo %s não contém um Telefone válido.');
        $fone = preg_replace('/[^0-9]/','',$fone);
        $fone = (string) $fone;
        if( strlen($fone) >= 10)
            return TRUE;
        else
            return FALSE;
    }
    
    
    public function alpha_brazilian($str)
    {
        $CI =& get_instance();
        $CI->form_validation->set_message('alpha_brazilian', 'O campo %s possui caracteres inválidos.');
        return (!preg_match("/^([A-Za-záàâãéèêíïóôõöúüçñÁÀÂÃÉÈÊÍÏÓÔÕÖÚÜÇÑ.' ])+$/i", $str)) ? FALSE : TRUE;   
    }
    
    
    	
}
