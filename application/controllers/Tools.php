<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tools extends CI_Controller
{
    
    public function enviaemail()
    {
        $this->load->library("MY_phpmailer");
    
        $param = $this->input->post('lista');
        foreach ($param as  $item) {
            $array['nome_usuario'] = $item['nome_usuario'];
            $array['email_usuario'] = $item['email_usuario'];
            $array['senha_usuario'] = $item['senha_usuario'];   
            
            $this->my_phpmailer->enviarEmailCadastro($array); 
            
        }
        
    }
    
    public function enviaemailmensagem()
    {
        $this->load->library("MY_phpmailer");
    
        $param = $this->input->post('lista');
        foreach ($param as  $item) {
            $array['nome_usuario'] = $item['nome_usuario'];
            $array['email_usuario'] = $item['email_usuario'];
            $array['mensagem_enviada'] = $item['mensagem_enviada'];
            $array['remetente'] = $item['remetente'];
            $array['assunto'] = $item['assunto'];
            $array['caminho'] = $item['caminho'];
            
            $this->my_phpmailer->enviarEmailMensagem($array); 
            
        }
    }
}

?>