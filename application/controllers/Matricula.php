<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Matricula extends CI_Controller {
	
	function Matricula() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }
     public function index(){
        $this->template->show('matricula');
     }
/*
     public function edita($id){
     	
        $idmatricula = $id;
        
        $data = $this->template->loadCabecalho('Editar Matricula');

        $this->load->model('matricula_model');
        $this->load->model('curso_model');

     	$matricula = $this->matricula_model->get($idmatricula);

     	 //seleciona os cursos da unidade
        $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);

        foreach ($cursos as $curso) {            
            $dropcursos[$curso['idcurso']] = $curso['nome_curso'];
        }
        
        $data['cursos_drop'] = $dropcursos;
        $data['cursos'] = $cursos;
        //Join entre Turma e Curso da unidade para o dropdown de escolha da turma 
        $cursoturmas = $this->curso_model->cursoturma($data['unidade']['idunidade']);
        foreach ($cursoturmas as $cursoturma) {
            $turma_drop[$cursoturma['idturma']] = $cursoturma['nome_curso'].' / '.$cursoturma['nome_turma'].' - '.DateTime::createFromFormat("Y-m-d", $cursoturma['data_inicio_turma'])->format('Y');
            $turmacurso[$cursoturma['idturma']] = $cursoturma['curso_idcurso'];
            $turma[$cursoturma['idturma']] = $cursoturma['nome_turma'].' - '.DateTime::createFromFormat("Y-m-d", $cursoturma['data_inicio_turma'])->format('Y');
        }
        $aluno = $this->user_model->get($matricula['matricula_idaluno']);
        $matricula['aluno'] = $aluno['nome_usuario'];
        $data['matricula'] = $matricula;
        $data['turmas'] = $turma;
        $data['turmas_drop'] = $turma_drop;
        $data['turmacurso'] = $turmacurso;

     	$this->template->show('editamatricula', $data);
     }
 * 
 */

     public function cancelar($id,$flag){
        $idmatricula = base64_decode($id);

        $this->load->model('matricula_model');
        $this->load->model('turma_model');
        
        $matricula = $this->matricula_model->get($idmatricula);
		if (count($matricula)>0){
		
			$turma = $this->turma_model->getTurma($matricula['matricula_idturma']);
			if ($turma['curso_unidade_idunidade']==$this->session->userdata('unidade')){
				
	            $this->load->helper('url');
	            $this->load->helper('form');
	            $this->load->library('form_validation');
				
				$this->form_validation->set_rules('data_cancelamento','D. Cancelamento',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_date','callback_validaDataCancelamento['.$matricula['data_matricula'].']'));
				$this->form_validation->set_rules('justificativa','Justificativa','required|is_natural|contains[justificativa.idjustificativa]');
				$this->form_validation->set_rules('tipos','Tipo','required|is_natural|less_than[5]');
				
				if ($this->form_validation->run()){
				
			        $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_cancelamento'));
	        		$data_cancelamento = $date->format('Y-m-d');
					
			        $sql_data = array(
			        	'data_trancamento' => $data_cancelamento,
			        	'status' => $this->input->post('tipos') ,
			            'idjustificativa' => $this->input->post('justificativa'),
			        	'matricula_observacoes' => $this->input->post('matricula_observacoes')
			        );
			
			        $this->matricula_model->update($matricula['idmatricula'], $sql_data);
			        if (isset($flag)) {
			            switch ($flag) {
			                case 'true':
			                    if ($sql_data['status']==2)
			                        $this->template->redirect('aprovacao/novo/'.base64_encode($matricula['matricula_idaluno']));
			                    $this->template->redirect('matricula/desmatricularAlunos/cancelada');                    
			                    break;
			                
			                default:
			                    break;
			            }
			        }else{
			            $idaluno = base64_encode($matricula['matricula_idaluno']);
			            if ($sql_data['status']==2)
			                $this->template->redirect('aprovacao/novo/'.base64_encode($matricula['matricula_idaluno']));
			            $this->template->redirect('aluno/matricula/'.$idaluno);
			        }
			   }
			   $erros = $this->form_validation->error_array();
			   $this->session->set_userdata('erros',$erros);
			   if (isset($flag)){
					$this->template->redirect('matricula/desmatricularAlunos');
			   }
			   else{
	   				$this->template->redirect('aluno/matricula/'.base64_encode($matricula['matricula_idaluno']).'/aluno');
			   }
			   
	        }
			$this->template->redirect('inicio');
	     }
		 $this->template->redirect('inicio');
     }

    public function validaDataCancelamento($data_cancelamento, $data_matricula){
    	        
        $this->form_validation->set_message('validaDataCancelamento', 'A Data de Cancelamento deve vir após a de Matrícula'); 
        $data_c = DateTime::createFromFormat('d/m/Y', $data_cancelamento);
		
        $data_m = DateTime::createFromFormat('Y-m-d H:i:s', $data_matricula);
		 
        if ($data_c>=$data_m)                                
            return TRUE;
        else                     
            return FALSE;             
    }


     public function infoMatricula(){
        $idmatricula = base64_decode($this->input->post('matricula'));
        $this->load->model('matricula_model');
        $matricula = $this->matricula_model->get($idmatricula);
        $this->load->model('justificativa_model');
        $justificativa = $this->justificativa_model->get($matricula['idjustificativa']);
        $data['observacao'] = $matricula['matricula_observacoes'];
        $data['justificativa'] = $justificativa['descricao_justificativa'];
        echo json_encode($data); 
     }

     public function matricularAlunos(){
        $data = $this->template->loadCabecalho('Matricular Alunos');
        $this->template->show('matricular_alunos', $data);
     }

     public function desmatricularAlunos($flag){
            $data = $this->template->loadCabecalho('Desmatricular Alunos');
            
        if (isset($flag)) {
            switch ($flag) {
                case 'cancelada':
                    $data['msg'] = 'Inscrição cancelada com sucesso!';
                    break;
                default:
                    break;
            }
        }
		
		if ($this->session->userdata('erros')){
			$erros = $this->session->userdata('erros');
			
			foreach ($erros as $erro) {
				$data['err'] .= $erro;
			}
			
			$this->session->unset_userdata('erros');
		}
        $this->load->model('justificativa_model');
        $justificativas = $this->justificativa_model->getJustMatricula();
        foreach ($justificativas as $jus ) {
            $justificativa[$jus['idjustificativa']] = $jus['descricao_justificativa'];           
        }
        $data['justificativa'] = $justificativa;
        $tipos  = array(0 => 'Cancelamento',
        				1 => 'Desligamento', 
                        2 => 'Aprovação' ,
                        3 => 'Desistência',
                        4 => 'Abandono'
        );
        $data['tipos'] = $tipos;
        $data['data_cancelamento']= date('d/m/Y');//data atual do servidor                
        $this->template->show('desmatricular_alunos', $data);
     }

     public function buscaMatricula(){        
        $this->load->model('unidade_model');
        $this->load->model('user_model');
		$this->load->model('aluno_model');
        $id = $this->session->userdata('user');
        
        $cpf = $this->input->post('cpf');
        $nome = $this->input->post('nome');
        $tipo = $this->input->post('tipo');
		$concluinte = $this->input->post('concluinte');
        $usuarios = array();        
        if(empty($nome)&&(!empty($cpf))){//busca apenas com cpf
            $useralunos = $this->user_model->getUsuariosCPF($cpf,$this->session->userdata('unidade'),1);
        }else if (empty($cpf)&&(!empty($nome))) {//busca apenas com o nome
            $useralunos = $this->user_model->getUsuariosNome($nome,$this->session->userdata('unidade'),1);           
        }else if ((!empty($cpf))&&(!empty($nome))){//busca com ambos
            $useralunos = $this->user_model->getUsuariosNomeCPF($nome,$cpf,$this->session->userdata('unidade'),1); 
        }else{//todos os alunos da unidade
            $useralunos = $this->user_model->getAlunos($this->session->userdata('unidade')); 
        }
        if(!(empty($useralunos))){
        	if (!empty($concluinte))
				$concluintes = $this->aluno_model->getConcluintes();                           
            foreach ($useralunos as $aluno) {
                    $alunos[$aluno['idusuario']] = base64_encode($aluno['idusuario']);
                    $this->load->model('matricula_model');
                	if($this->matricula_model->matriculaAtiva($aluno['idusuario'])){
                    	if($tipo==1){
                            $usuarios[] = $aluno;//tela de cancelamento de matrícula
                            $aux_matriculas = $this->matricula_model->getUltimaMatricula($aluno['idusuario']);
                            $matricula = $aux_matriculas[0];
                            //$aux = base64_encode($matricula['idmatricula']); 
                            $matriculas[$aluno['idusuario']] = base64_encode($matricula['idmatricula']);                            
                        } 
                    }else{
                        if($tipo==0){
                        	if (!empty($concluinte)){
								if (array_search($aluno['idusuario'], array_column($concluintes, 'usuario_idusuario'))){
									$usuarios[]=$aluno;
								}
                        	}
							else{
                           		$usuarios[]=$aluno;//tela de matrícula 
							}
                        } 
                    }
                    
                } 
            $data['alunos'] = $alunos;
            $data['usuarios'] = $usuarios;
            $data['curso'] = $curso;
            $data['turma'] = $turma;
            $data['matriculas'] = $matriculas;
            $data['err'] = 'ok';            
            echo json_encode($data); 
        }else{
            $data['err'] = 'Não foram encontrados alunos com estes dados!';
            echo json_encode($data);
        }
        
     }

     public function qtdMatriculas(){
        $this->load->model('matricula_model');
        $total = $this->matricula_model->qtdMatriculas($this->input->post('turma'));
        $this->load->model('turma_model');
        $turma = $this->turma_model->getTurma($this->input->post('turma'));
        $data['vagas'] = $turma['num_vagas'];
        $data['matriculas'] = $total;
        echo json_encode($data);        
     }

     public function getJustificativa(){
        $this->load->model('justificativa_model');
        $tipo = $this->input->post('tipo');
        switch ($tipo) {
            case 0://cancelamento - Justificativa: Cadastro Indevido
                $jus = $this->justificativa_model->get(4);
                $justificativa[$jus['idjustificativa']] = $jus['descricao_justificativa']; 
                break;
			case 1:
                $jus = $this->justificativa_model->get(6);
                $justificativa[$jus['idjustificativa']] = $jus['descricao_justificativa']; 
                break;				
            case 2://aprovação - Justificativa: Aprovação em Vestibular
                $jus = $this->justificativa_model->get(3);
                $justificativa[$jus['idjustificativa']] = $jus['descricao_justificativa']; 
                break;
             
            default:
                $justificativas = $this->justificativa_model->getJustMatricula();
                foreach ($justificativas as $jus ) {
                    $justificativa[$jus['idjustificativa']] = $jus['descricao_justificativa'];           
                }
                break;
        }
        $data['justificativa'] = $justificativa;
        echo json_encode($data); 
     }
 }