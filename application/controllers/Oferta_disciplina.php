<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Oferta_disciplina extends CI_Controller {
	
	function Oferta_disciplina() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }

     public function index(){ //mostra as opções de disciplina
        $data = $this->template->loadCabecalho('Opções de Ofertas de Disciplina');

     	$this->load->model('user_model');
        $this->load->model('faculdade_model');
        $this->load->model('curso_model');

        $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);
        foreach ($cursos as $curso){
            $drop_curso[$curso['idcurso']] = $curso['nome_curso'];
        }
        
        $monitores = $this->user_model->getFuncionariosTipo($data['unidade']['idunidade'],'2');
        foreach ($monitores as $monitor) {
            $drop_monitor[$monitor['idusuario']] = $monitor['nome_usuario'];
        }
        $monitores = $this->user_model->getFuncionariosTipo($data['unidade']['idunidade'],'3');
        foreach ($monitores as $monitor) {
            $drop_monitor[$monitor['idusuario']] = $monitor['nome_usuario'];
        }
        $monitores = $this->user_model->getFuncionariosTipo($data['unidade']['idunidade'],'4');
        foreach ($monitores as $monitor) {
            $drop_monitor[$monitor['idusuario']] = $monitor['nome_usuario'];
        }
        $data['nome_monitor'] = $drop_monitor;
        
        $data['nome_curso'] = $drop_curso;

        $this->template->show('oferta_opcoes', $data);
     }

    public function getOfertasTurma()
    {
        

        $this->load->model('oferta_model');
        $this->load->model('user_model');
        $this->load->model('turma_model');
        $this->load->model('disciplina_model');
        
        $idTurma = $this->input->post('idTurma');
        
        if ($this->turma_model->UnidadePossuiTurma($idTurma,$this->session->userdata('unidade'))!=0){
            $disciplinasLivres = $this->oferta_model->getDiscLiv($idTurma);
            
            $i = 0;
            foreach ($disciplinasLivres as $disc) {
                $drop_ofertas[$i]['id_disciplina'] = $disc['iddisciplina'];            
                $drop_ofertas[$i]['id_oferta'] = '';
                $drop_ofertas[$i]['nome_disciplina'] = $disc['nome_disciplina'];
                $drop_ofertas[$i]['nome_monitor'] = '--';
                $i++;
            }
            
            $ofertas = $this->oferta_model->getOferTur($idTurma);
            
            foreach ($ofertas as $oferta) {
                $disciplina = $this->disciplina_model->getDisciplina($oferta['iddisciplina']);
                $monitor = $this->user_model->get($oferta['monitor_idusuario']);
                $drop_ofertas[$i]['id_disciplina'] = $disciplina['iddisciplina']; 
                $drop_ofertas[$i]['id_oferta'] = $oferta['id_oferta'];
                $drop_ofertas[$i]['nome_disciplina'] = $disciplina['nome_disciplina'];
                $drop_ofertas[$i]['nome_monitor'] = $monitor['nome_usuario'];
                $i++;
            }
            
            $data['disciplinas'] = $drop_ofertas;
            $data['err'] = "ok";
        }
        else $data['err'] = "ERRO: turma indisponível";
            
        echo json_encode($data);
        
    }

      public function cria(){
        $this->load->model('oferta_model');
        $this->load->model('turma_model');
        $this->load->model('user_model');
        $this->load->model('curso_model');
        
        $data = $this->template->loadCabecalho('Opções de Ofertas de Disciplina');
            
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $sql_oferta['iddisciplina'] = $this->input->post('iddisciplina');
        $sql_oferta['idturma'] = $this->input->post('idturma');
        $sql_oferta['monitor_idusuario'] = $this->input->post('nome_monitor');
        $sql_oferta['status'] = 1;
        
        $turmaAt = $this->turma_model->getTurma($sql_oferta['idturma']);
        
        $this->form_validation->set_rules('nome_monitor','Monitor ','required|contains[monitor.usuario_idusuario]|contains[unidade_usuario.usuario_idusuario,unidade_usuario.unidade_idunidade,#'.$data['unidade']['idunidade'].'#]');
        $this->form_validation->set_rules('data_inicio_oferta','Data de Início',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_date'));
        $this->form_validation->set_rules('iddisciplina','iddisciplina','required|contains[disciplina.iddisciplina,disciplina.disciplina_idcurso,#'.$turmaAt['curso_idcurso'].'#,disciplina.status,#1#]');
        $this->form_validation->set_rules('idturma','idturma','required|contains[turma.idturma,turma.curso_unidade_idunidade,#'.$data['unidade']['idunidade'].'#]');
        
        //VERIFICAR DATA DE INICIO DE ACORDO COM A TURMA
        
        if ($this->form_validation->run()){
            if ($this->oferta_model->possuiMatricula($sql_oferta)==0){
                $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_inicio_oferta'));
                $sql_oferta['data_inicio_oferta']=$date->format('Y-m-d');
                $this->oferta_model->create($sql_oferta);
                $this->template->redirect('oferta_disciplina');
            }
            $this->template->redirect('oferta_disciplina');
        }
        else {

            $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);
            foreach ($cursos as $curso){
                $drop_curso[$curso['idcurso']] = $curso['nome_curso'];
            }
            
            $monitores = $this->user_model->getFuncionariosTipo($data['unidade']['idunidade'],'2');
            foreach ($monitores as $monitor) {
                $drop_monitor[$monitor['idusuario']] = $monitor['nome_usuario'];
            }
            $monitores = $this->user_model->getFuncionariosTipo($data['unidade']['idunidade'],'3');
            foreach ($monitores as $monitor) {
                $drop_monitor[$monitor['idusuario']] = $monitor['nome_usuario'];
            }
            $monitores = $this->user_model->getFuncionariosTipo($data['unidade']['idunidade'],'4');
            foreach ($monitores as $monitor) {
                $drop_monitor[$monitor['idusuario']] = $monitor['nome_usuario'];
            }
            $data['nome_monitor'] = $drop_monitor;
            
            $data['nome_curso'] = $drop_curso;
            
            $data['nome_turma_sel'] = $sql_oferta['idturma'];
            
            $turmas = $this->turma_model->get($turmaAt['curso_idcurso']);
            
            foreach ($turmas as $turma) {
                $drop_turma[$turma['idturma']] = $turma['nome_turma'];
            }
           $data['nome_turma'] = $drop_turma;
           
           $this->template->show('oferta_opcoes', $data);
                
        }
     }

    public function remove(){
        
        $data = $this->template->loadCabecalho('Opções de Ofertas de Disciplina');
        
        $this->load->model('oferta_model');
        $this->load->model('turma_model');
        $this->load->model('user_model');
        $this->load->model('curso_model');
            
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $sql_oferta['iddisciplina'] = $this->input->post('iddisciplinaD');
        $sql_oferta['idturma'] = $this->input->post('idturmaD');
        $sql_oferta['status'] = 1;
        
        $turmaAt = $this->turma_model->getTurma($sql_oferta['idturma']);
        
        $this->form_validation->set_rules('data_fim_oferta','Data de Início',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_date'));
         $this->form_validation->set_rules('iddisciplinaD','iddisciplina','required|contains[disciplina.iddisciplina,disciplina.disciplina_idcurso,#'.$turmaAt['curso_idcurso'].'#]');
        $this->form_validation->set_rules('idturmaD','idturma','required|contains[turma.idturma,turma.curso_unidade_idunidade,#'.$data['unidade']['idunidade'].'#]');
        
        //VERIFICAR DATA DE ACORDO COM A DATA_INICIO_OFERTA
        
        if ($this->form_validation->run()){
            if ($this->oferta_model->possuiMatricula($sql_oferta)==1){
                $oferta = $this->oferta_model->getWhere($sql_oferta);
                $sql_oferta['status'] = 0;
                $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_fim_oferta'));
                $sql_oferta['data_fim_oferta']=$date->format('Y-m-d');
                $this->oferta_model->delete($oferta['id_oferta'],$sql_oferta);
                $this->template->redirect('oferta_disciplina');
            }
            $this->template->redirect('oferta_disciplina');
        }
        else {
            $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);
            foreach ($cursos as $curso){
                $drop_curso[$curso['idcurso']] = $curso['nome_curso'];
            }
            
            $monitores = $this->user_model->getFuncionariosTipo($data['unidade']['idunidade'],'2');
            foreach ($monitores as $monitor) {
                $drop_monitor[$monitor['idusuario']] = $monitor['nome_usuario'];
            }
            $monitores = $this->user_model->getFuncionariosTipo($data['unidade']['idunidade'],'3');
            foreach ($monitores as $monitor) {
                $drop_monitor[$monitor['idusuario']] = $monitor['nome_usuario'];
            }
            $monitores = $this->user_model->getFuncionariosTipo($data['unidade']['idunidade'],'4');
            foreach ($monitores as $monitor) {
                $drop_monitor[$monitor['idusuario']] = $monitor['nome_usuario'];
            }
            $data['nome_monitor'] = $drop_monitor;
            
            $data['nome_curso'] = $drop_curso;
            
            $data['nome_turma_sel'] = $sql_oferta['idturma'];
            
            $turmas = $this->turma_model->get($turmaAt['curso_idcurso']);
            
            foreach ($turmas as $turma) {
                $drop_turma[$turma['idturma']] = $turma['nome_turma'];
            }
           
           $data['nome_turma'] = $drop_turma;
    
           $this->template->show('oferta_opcoes', $data);
   
        }
    }



}