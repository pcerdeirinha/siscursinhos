<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Evento extends CI_Controller {
    
     function Evento() {
        parent::__construct();
     }

     public function calendario(){ //mostra o calendário de eventos
        if(!$this->session->userdata('logged'))
            $this->template->redirect('login');
        
        $this->load->model('evento_model');
        $this->load->model('turma_model');
        
        $data = $this->template->loadCabecalho('Eventos');
        $data['eventos'] = $this->evento_model->getAll();
        
        if($this->session->userdata('level')>2){
            $turmas = $this->turma_model->getTurmas($this->session->userdata('unidade'));
            foreach ($turmas as $key => $turma) 
                $data['turmas'][$turma['idturma']] = $turma['nome_turma'];
        }
        
        $this->template->show('eventos_calendario', $data);
     }
     
     public function obter_informacoes(){
        if(!$this->session->userdata('logged'))
            $this->template->redirect('login');
        
        $this->load->model('evento_model');
         
        $evento = $this->evento_model->get($this->input->post('idevento'));
        
        echo json_encode($evento);
     }
     
     public function pagina($id_evento)
     {
         $this->load->model('evento_model');
         $this->load->model('participacao_model');
         $this->load->model('user_model');
         
         $imagens = $this->evento_model->get_imagens($id_evento);
         $data['imagens'] = $imagens;
         
         $evento = $this->evento_model->get($id_evento);
         $data['evento'] = $evento;
         
         $pagina_evento = $this->evento_model->get_pagina($id_evento);
         $data['pagina_evento'] = $pagina_evento;
         
         if ($this->session->userdata('logged')){
                 
             $data['user'] = $this->user_model->get($this->session->userdata('user'));
             
             $data['confirmado'] = $this->participacao_model->participacao_confirmada($this->session->userdata('user'),$id_evento);
         } 
         $this->load->view('pagina_evento', $data);
     }

     

     public function imagem($id_imagem)
     {
         $this->load->model('evento_model');
         
         $imagem = $this->evento_model->get_imagem($id_imagem);
          if(empty($imagem))
             header("HTTP/1.0 404 Not Found");
          else {
             header('Content-type: image/jpeg');
             echo $imagem['imagem_evento_src'];
         }         
     }

    public function opcoes()
    {
        if(!$this->session->userdata('logged') || ($this->session->userdata('level')<2))
            $this->template->redirect('login');
        $data = $this->template->loadCabecalho('Opções de Evento');
        $this->template->show('evento_opcoes', $data);
    }

    public function edita($id)
    {
        if(!$this->session->userdata('logged') || ($this->session->userdata('level')<2))
            $this->template->redirect('login');
        
        $this->load->model('estado_model');
        $this->load->model('evento_model');
        $this->load->model('cidade_model');
        
        $data = $this->template->loadCabecalho('Edita Evento');
        $data['id'] = $id;
        
        $evento = $this->evento_model->get($id);
        $data['nome_evento'] = $evento['nome_evento'];
        $data['data_inicio_evento'] = DateTime::createFromFormat('Y-m-d', $evento['data_inicio_evento'])->format('d/m/Y');
        if (isset($evento['data_fim_evento']))
            $data['data_fim_evento'] = DateTime::createFromFormat('Y-m-d', $evento['data_fim_evento'])->format('d/m/Y');
        if (isset($evento['horario_inicio_evento']))
            $data['hora_inicio_evento'] = DateTime::createFromFormat('G:i:s', $evento['horario_inicio_evento'])->format('G:i');
        if (isset($evento['horario_fim_evento']))
            $data['hora_fim_evento'] = DateTime::createFromFormat('G:i:s', $evento['horario_fim_evento'])->format('G:i');
        $data['descricao_evento'] = $evento['descricao_evento'];
        
        $pagina_evento = $this->evento_model->get_pagina($id);
        $data['telefone_evento'] = $pagina_evento['telefone_evento'];
        $data['email_evento'] = $pagina_evento['email_evento'];
        $data['logradouro_evento'] = $pagina_evento['endereco_evento'];
        $endereco = explode(',', $pagina_evento['endereco_evento']);
        foreach ($endereco as $end) {
            if (strpos($end, '-') !== false) {
                list($cidad,$est) = explode('-', $end);
                
                if ($id_est = $this->estado_model->getID($est)){
                    $data['estado_sel'] = $id_est;
                    $data['cidade_sel'] = trim($cidad);
                    $data['logradouro_evento'] = str_replace(','.$end, '', $data['logradouro_evento']);
                    break;                    
                }
                    
            }
        }
        $data['cor_principal_evento'] ='#'.$pagina_evento['cor_primaria_evento'];
        $data['cor_secundaria_evento'] = '#'.$pagina_evento['cor_secundaria_evento'];
        
        $imagens = $this->evento_model->get_imagens($id);
        foreach ($imagens as $key => $imagem) {
            $data['imagem'][$key]['titulo_imagem'] = $imagem['imagem_evento_titulo'];
            $data['imagem'][$key]['descricao_imagem'] = $imagem['imagem_evento_descricao'];
        }
        $estados = $this->estado_model->get();
        foreach ($estados as $estado) {
            $uf[$estado['id']] = $estado['uf'];
        }
        $data['estados'] = $uf;
        $data['estado_sel'] = $data['estado_sel']!=null?$data['estado_sel']:1;
        //Dropdown de cidades        
        $cidades = $this->cidade_model->get($data['estado_sel']);
        foreach ($cidades as $cidade) {
            $city[$cidade['nome']] = $cidade['nome'];
        }
        $data['cidades'] = $city;
        
        $this->template->show('novoevento', $data);
    }
    
    public function novo()
    {
        if(!$this->session->userdata('logged') || ($this->session->userdata('level')<2))
            $this->template->redirect('login');
        
        $this->load->model('estado_model');
        $this->load->model('cidade_model');
        
        $data = $this->template->loadCabecalho('Novo Evento');
        
        $estados = $this->estado_model->get();
        foreach ($estados as $estado) {
            $uf[$estado['id']] = $estado['uf'];
        }
        $data['estados'] = $uf;
        $data['estado_sel'] = 1;
        //Dropdown de cidades        
        $this->load->model('cidade_model');
        $cidades = $this->cidade_model->get(1);
        foreach ($cidades as $cidade) {
            $city[$cidade['nome']] = $cidade['nome'];
        }
        
        $data['cidades'] = $city;
        
        $data['cor_principal_evento'] = "#555555";
        $data['cor_secundaria_evento'] = "#FFFFFF";
        
        $this->template->show('novoevento', $data);
    }
    
    public function cria()
    {
        if(!$this->session->userdata('logged') || ($this->session->userdata('level')<2))
            $this->template->redirect('login');
        $this->load->model('estado_model');
        $this->load->model('evento_model');
        
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nome_evento','Nome do Evento','required|max_length[45]');
        $this->form_validation->set_rules('descricao_evento','Descrição do Evento','required|max_length[150]');
        $this->form_validation->set_rules('data_inicio_evento','Data de Início',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_date'));
        $this->form_validation->set_rules('data_fim_evento','Data de Fim',array('regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_date'));
        $this->form_validation->set_rules('hora_inicio_evento','Horário de Início',array('regex_match[/([01]?[0-9]|2[0-3]):[0-5][0-9]/]','exact_length[5]'));
        $this->form_validation->set_rules('hora_fim_evento','Horário de Fim',array('regex_match[/([01]?[0-9]|2[0-3]):[0-5][0-9]/]','exact_length[5]'));
        $this->form_validation->set_rules('telefone_evento','Telefone de Contato',array('required','min_length[14]','max_length[15]','regex_match[/\([1-9]{2}\)\ [2-9]\d{3,4}\-\d{4}$/]'));
        $this->form_validation->set_rules('email_evento','Email de Contato',array('required','valid_email'));
        $this->form_validation->set_rules('numero_evento','Numero','is_natural');
        $this->form_validation->set_rules('cep_evento','CEP','regex_match[/\d{5}\-\d{3}$/]');
        $this->form_validation->set_rules('estado_evento','UF','required|is_natural|contains[estado.id]');
        $this->form_validation->set_rules('cidade_evento','Cidade','required|contains[cidade.nome,cidade.estado,estado_evento]');
        $this->form_validation->set_rules('logradouro_evento','Logradouro','required'); 
        $this->form_validation->set_rules('cor_principal_evento','Cor Principal',array('required','regex_match[/#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/]')); 
        $this->form_validation->set_rules('cor_secundaria_evento','Cor Secundária',array('required','regex_match[/#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/]')); 
        $this->form_validation->set_rules('descricao_imagem[0]','Descrição da Imagem Obrigatória',array('required')); 
        $this->form_validation->set_rules('titulo_imagem[0]','Titulo da Imagem Obrigatória',array('required')); 
        if (empty($_FILES['imagem_0']['name']) && (!$this->input->post('id')))
            $this->form_validation->set_rules('imagem_0','Imagem Obrigatória','required');
        if ($this->input->post('id'))
            $this->form_validation->set_rules('id','Nome do Evento','required|contains[evento.idevento]');     
        if ($this->form_validation->run()){
            $sql_evento = array('nome_evento' => $this->input->post('nome_evento'),
                                'descricao_evento' => $this->input->post('descricao_evento'),
                                'unidade_idunidade' => $this->session->userdata('unidade')
            );
            if ($this->input->post('hora_inicio_evento'))
                $sql_evento['horario_inicio_evento'] = DateTime::createFromFormat('G:i', $this->input->post('hora_inicio_evento'))->format('G:i:s');
            if ($this->input->post('hora_fim_evento'))
                $sql_evento['horario_fim_evento'] = DateTime::createFromFormat('G:i', $this->input->post('hora_fim_evento'))->format('G:i:s');
            $sql_evento['data_inicio_evento'] = DateTime::createFromFormat('d/m/Y', $this->input->post('data_inicio_evento'))->format('Y-m-d');
            if ($this->input->post('data_fim_evento'))
                $sql_evento['data_fim_evento'] = DateTime::createFromFormat('d/m/Y', $this->input->post('data_fim_evento'))->format('Y-m-d');
            $endereco = '';
            $endereco .= $this->input->post('logradouro_evento');
            if ($this->input->post('numero_evento'))
                $endereco .= ', '.$this->input->post('numero_evento');
            if ($this->input->post('bairro_evento'))
                $endereco .= ', '.$this->input->post('bairro_evento');
            if ($this->input->post('cidade_evento'))
                $endereco .= ', '.$this->input->post('cidade_evento');
            if ($this->input->post('estado_evento'))
                $endereco .= '-'.$this->estado_model->getUF($this->input->post('estado_evento'));
            if ($this->input->post('cep_evento'))
                $endereco .= ', '.$this->input->post('cep_evento');
            $sql_pagina = array('endereco_evento' => $endereco,
                                'confirmar_presenca_evento' => 1,
                                'cor_primaria_evento' => str_replace('#', '', $this->input->post('cor_principal_evento')),
                                'cor_secundaria_evento' => str_replace('#', '', $this->input->post('cor_secundaria_evento')),
                                'telefone_evento' => $this->input->post('telefone_evento'),
                                'email_evento' => $this->input->post('email_evento'),
             );
             if ($this->input->post('id')){
                 $imagens = $this->evento_model->get_imagens($this->input->post('id'));
             }
            $titulos = $this->input->post('titulo_imagem[]');
            foreach ($titulos as $key => $titulo){
                if ($titulo!=null){
                    $this->form_validation->set_rules('descricao_imagem['.$key.']','Descrição da Imagem Obrigatória',array('required')); 
                    $this->form_validation->set_rules('titulo_imagem['.$key.']','Titulo da Imagem Obrigatória',array('required')); 
                    if (empty($_FILES['imagem_'.$key.'']['name']) && (!$this->input->post('id')))
                        $this->form_validation->set_rules('imagem_'.$key.'','Imagem Adicional','required'); 
                    if ($this->form_validation->run()){
                        if (!$this->input->post('id') || !(empty($_FILES['imagem_'.$key.'']['name'])))
                            $sql_imagem[$key]['imagem_evento_src'] = file_get_contents($_FILES['imagem_'.$key.'']['tmp_name']);
                        $sql_imagem[$key]['imagem_evento_titulo'] = $titulo;
                        $sql_imagem[$key]['imagem_evento_descricao'] = $this->input->post('descricao_imagem['.$key.']');
                        if ($this->input->post('id')){
                            $sql_imagem[$key]['idimagem_evento'] = $imagens[$key]['idimagem_evento'];
                            unset($imagens[$key]);
                        }
                    }
                    else{
                        $this->load->model('cidade_model');
                
                        $data = $this->template->loadCabecalho('Novo Evento');
                        
                        $estados = $this->estado_model->get();
                        foreach ($estados as $estado) {
                            $uf[$estado['id']] = $estado['uf'];
                        }
                        $data['estados'] = $uf;
                        //Dropdown de cidades        
                        $this->load->model('cidade_model');
                        $cidades = $this->cidade_model->get($this->input->post('estado_evento'));
                        foreach ($cidades as $cidade) {
                            $city[$cidade['nome']] = $cidade['nome'];
                        }
                        $data['cidades'] = $city;
                        
                        $this->template->show('novoevento', $data);
                        return;
                    }
                }
            }

            if (!$this->input->post('id')){
                $id_evento = $this->evento_model->inserir_evento($sql_evento);
                $sql_pagina['evento_idevento'] = $id_evento;
                $this->evento_model->inserir_pagina($sql_pagina);
                foreach ($sql_imagem as $img) {
                    $img['evento_idevento'] = $id_evento;
                    $this->evento_model->inserir_imagem($img);
                }
            }
            else{
                $this->evento_model->update_evento($this->input->post('id'),$sql_evento);
                $this->evento_model->update_pagina($this->input->post('id'),$sql_pagina);
                $id_evento= $this->input->post('id');
                foreach ($sql_imagem as $img) {
                    $img['evento_idevento'] = $this->input->post('id');
                    if ($img['idimagem_evento']!=null)
                        $this->evento_model->update_imagem($img['idimagem_evento'],$img);
                    else
                        $this->evento_model->inserir_imagem($img);
                }
                foreach ($imagens as $imagem) {
                    $this->evento_model->remover_imagem($imagem['idimagem_evento']);
                }
            }
            $this->template->redirect('evento/pagina/'.$id_evento);
        }
        else{
            $this->load->model('cidade_model');
        
            $data = $this->template->loadCabecalho('Novo Evento');
            
            $estados = $this->estado_model->get();
            foreach ($estados as $estado) {
                $uf[$estado['id']] = $estado['uf'];
            }
            $data['estados'] = $uf;
            //Dropdown de cidades        
            $this->load->model('cidade_model');
            $cidades = $this->cidade_model->get($this->input->post('estado_evento'));
            foreach ($cidades as $cidade) {
                $city[$cidade['nome']] = $cidade['nome'];
            }
            $data['cidades'] = $city;
            
            
            $this->template->show('novoevento', $data);
        }
    }

    public function remove($id_evento)
    {
        if(!$this->session->userdata('logged') || ($this->session->userdata('level')<2))
            $this->template->redirect('login');
            
        $this->load->model('evento_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_data(array('id_evento'=>$id_evento));
        $this->form_validation->set_rules('id_evento','Evento','required|contains[evento.idevento,evento.unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
        if ($this->form_validation->run()){
        
            $this->evento_model->remove_evento($id_evento);
        }
        $this->template->redirect('evento/busca');        
    }

    public function busca()
    {
        if(!$this->session->userdata('logged') || ($this->session->userdata('level')<2))
            $this->template->redirect('login');
        
        $this->load->model('turma_model');
        $this->load->model('evento_model');
        $this->load->model('participacao_model');
        
        $data = $this->template->loadCabecalho('Busca de Eventos');
        $eventos = $this->evento_model->getAll($this->session->userdata('unidade'));
        foreach ($eventos as $key => $evento) {
            $data['eventos'][$evento['idevento']]['nome_evento'] = $evento['nome_evento'];
            $data['eventos'][$evento['idevento']]['data_inicio'] = $evento['data_inicio_evento'];
            $data['eventos'][$evento['idevento']]['qtd'] = $this->participacao_model->qtd_participacoes($evento['idevento']);
        }
        $turmas = $this->turma_model->getTurmas($this->session->userdata('unidade'));
        foreach ($turmas as $key => $turma) 
            $data['turmas'][$turma['idturma']] = $turma['nome_turma'];
        
        $this->template->show('lista_evento', $data);
    }

    public function confirmar_usuario()
    { 
         $this->load->model('participacao_model');
         $this->load->helper('url');
         $this->load->helper('form');
         $this->load->library('form_validation');
         $this->form_validation->set_rules('id_evento','id_evento','required|contains[evento.idevento]');
         if ($this->form_validation->run()){
             if ($this->session->userdata('logged')){
                 $participante = $this->participacao_model->get_participante_interno($this->session->userdata('user'));
                 if (!isset($participante)){
                     $participante['participante_idparticipante'] = $this->participacao_model->inserir_participante();
                     $participante['usuario_idusuario'] = $this->session->userdata('user');
                     $this->participacao_model->inserir_participante_interno($participante);
                 }
                 $this->form_validation->set_rules('id_evento','id_evento','required|not_contains[participacao.evento_idevento,participacao.participante_idparticipante,#'.$participante['participante_idparticipante'].'#]');
                 if ($this->form_validation->run()){
                     $sql_participacao['evento_idevento'] = $this->input->post('id_evento');
                     $sql_participacao['participante_idparticipante'] = $participante['participante_idparticipante'];
                     $sql_participacao['recebe_email_participacao'] = 1;
                     $this->participacao_model->inserir_participacao($sql_participacao);
                 }   
                 echo "ok";
             }else{
                 $this->form_validation->set_rules('nome_usuario','nome','required|alpha_brazilian');
                 $this->form_validation->set_rules('email_usuario','email','required|valid_email');
                 if ($this->form_validation->run()){
                     $participante = $this->participacao_model->get_participante_externo($this->input->post('email_usuario'));
                     if (!isset($participante)){
                        $participante['participante_idparticipante'] = $this->participacao_model->inserir_participante();
                        $participante['email_participante'] = $this->input->post('email_usuario');
                        $participante['nome_participante'] = $this->input->post('nome_usuario');
                        $this->participacao_model->inserir_participante_externo($participante);
                     }
                     $this->form_validation->set_rules('id_evento','id_evento','required|not_contains[participacao.evento_idevento,participacao.participante_idparticipante,#'.$participante['participante_idparticipante'].'#]');
                     if ($this->form_validation->run()){
                         $sql_participacao['evento_idevento'] = $this->input->post('id_evento');
                         $sql_participacao['participante_idparticipante'] = $participante['participante_idparticipante'];
                         $sql_participacao['recebe_email_participacao'] = 1;
                         $this->participacao_model->inserir_participacao($sql_participacao);
                     }                         
                     echo "ok";
                 }
                 else{
                     echo "error";
                 }
             }
         }
         else{
             echo "error";
         }      
    }

    public function convidar_turmas()
    {
        if(!$this->session->userdata('logged') || ($this->session->userdata('level')<2)){
            echo "error";
            return;
        }
        $this->load->model('evento_model');
        $this->load->model('turma_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('evento','evento','required|contains[evento.idevento]');
        if ($this->form_validation->run()){
            $turmas = $this->input->post('turmas[]');
            $evento = $this->input->post('evento');
            foreach ($turmas as $key => $turma){
                if ($this->turma_model->UnidadePossuiTurma($turma['value'],$this->session->userdata('unidade'))>0)
                    $this->evento_model->convidar_turma(array('turma_idturma' => $turma['value'], 'evento_idevento' => $evento));
            }
            echo json_encode("ok");
        }
    }
    
    public function busca_alunos()
    {
        if(!$this->session->userdata('logged') || ($this->session->userdata('level')<2)){
            echo "error";
            return;
        }
        
        $this->load->model('evento_model');
        $this->load->model('aluno_model');
        
        $id_evento = $this->input->post('idevento');
        $evento = $this->evento_model->get($id_evento);
        $turmas = $this->evento_model->get_turmas_convidadas($id_evento);
        $alunos = array();
        foreach ($turmas as $key => $turma) {
            $alunos = array_merge($alunos, $this->aluno_model->obterAlunoTurma($turma['turma_idturma'],$evento['data_inicio_evento']));
        }
        
        $frequencia_evento = $this->evento_model->get_frequencia($id_evento);
        foreach ($alunos as $aluno) {
            $data['alunos'][$aluno['idusuario']]['nome_aluno'] = $aluno['nome_usuario'];
            $data['alunos'][$aluno['idusuario']]['falta_aluno'] = array_search($aluno['idusuario'], array_column($frequencia_evento, 'usuario_idusuario'))===false?0:1;
            $data['alunos'][$aluno['idusuario']]['idusuario'] = $aluno['idusuario'];
            $alunos_array[$aluno['idusuario']] = $aluno['nome_usuario'];
        }
        array_multisort($alunos_array,SORT_ASC,$data['alunos']);
        $data['err'] = "ok";
        echo json_encode($data);
    }
    
    public function frequencia()
    {
        if(!$this->session->userdata('logged') || ($this->session->userdata('level')<2))
            $this->template->redirect('login');
        
            
        $this->load->model('evento_model');
        $this->load->model('aluno_model');
        
        $id_evento = $this->input->post('id_evento_frequencia');
        $evento = $this->evento_model->get($id_evento);
        $turmas = $this->evento_model->get_turmas_convidadas($id_evento);
        $alunos = array();
        foreach ($turmas as $key => $turma) {
            $alunos = array_merge($alunos, $this->aluno_model->obterAlunoTurma($turma['turma_idturma'],$evento['data_inicio_evento']));
        }
                   
        foreach ($alunos as $aluno) {
            $sql_falta['usuario_idusuario'] = $aluno['idusuario'];
            $sql_falta['evento_idevento'] = $id_evento;
            if ($this->input->post("Presenca".$aluno['idusuario'])=='Nao')
                $this->evento_model->adicionar_falta($sql_falta);
            else  $this->evento_model->remover_falta($sql_falta);
        }
        $this->template->redirect('evento/busca');
    }

    public function desconfirmar_usuario()
    {
         $this->load->model('participacao_model');
         $this->load->helper('url');
         $this->load->helper('form');
         $this->load->library('form_validation');
         $this->form_validation->set_rules('id_evento','id_evento','required|contains[evento.idevento]');
         if ($this->form_validation->run()){
             if ($this->session->userdata('logged')){
                 $participante = $this->participacao_model->get_participante_interno($this->session->userdata('user'));
                 if (isset($participante)){
                     $this->form_validation->set_rules('id_evento','id_evento','required|not_contains[participacao.evento_idevento,participacao.participante_idparticipante,#'.$participante['participante_idparticipante'].'#]');
                     if ($this->form_validation->run()){
                         $sql_participacao = array(
                                             'participante_idparticipante' => $participante['participante_idparticipante'],
                                             'evento_idevento' => $this->input->post('id_evento')
                                             );
                         $this->participacao_model->remover_participacao($sql_participacao);
                     }
                     echo "ok";
                 }
                 else{
                     echo "error";
                 }
             } else {echo "error";}
         } else {echo "error";}
    }
}