<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inicio extends CI_Controller {
	
	function Inicio() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }

     public function index(){
            
        
        $data = $this->template->loadCabecalho('Inicio');
        
        $this->load->model('user_model');
        $this->load->model('mensagem_model');
        
        $recados = $this->mensagem_model->obterNumeroRecadosNaoLidos($this->session->userdata('user'));
        $respostas = $this->mensagem_model->obterNumeroRespostasNaoLidas($this->session->userdata('user'));
        $total = 0;
        foreach ($recados as $recado) {
            $total += $recado['count'];
        }
        foreach ($respostas as $recado) {
            $total += $recado['count'];
        }
        $data['numero_mensagens'] = $total;
        
        $unidades = $this->user_model->unidades_usuario($this->session->userdata('user'));
        if (count($unidades)>1)
        $data['unidades'] = $unidades;
        
       	$this->template->show('inicio', $data);
     }
     
     public function cadastro_incompleto()
     {
        $data = $this->template->loadCabecalho('Finalização do Cadastro'); 
        $this->template->show('cadastro_incompleto',$data);  
     }
 }