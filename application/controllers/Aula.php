<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Aula extends CI_Controller {
    
    function Aula() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
    }
    
        
        public function novo($idOferta)
        {
            $data = $this->template->loadCabecalho('Nova Aula');
            $data['view']='coordenador';
            
            $this->load->model('user_model');
            $this->load->model('monitor_model');
            $this->load->model('oferta_model');
            $this->load->model('disciplina_model');
            $this->load->model('turma_model');
            
            if ($this->session->userdata('level')>2){
                $monitores = $this->monitor_model->getMonitoresComOfertas($data['unidade']['idunidade']);
                $drop_monitor[0] = 'Todos';
                foreach ($monitores as $monitor) {
                    $drop_monitor[$monitor['idusuario']]=$monitor['nome_usuario'];
                }
            }
            else if ($this->session->userdata('level')==2){
                $monitor = $this->user_model->get($this->session->userdata('user'));
                $drop_monitor[$monitor['idusuario']]=$monitor['nome_usuario'];
            }
            
            if (isset($idOferta)){
                $oferta = $this->oferta_model->get($idOferta);
                    
                $data['disciplina_oferta_sel'] = $idOferta;
                
                $data['$nome_monitor_sel'] = $oferta['monitor_idusuario'];
                
                $ofertas = $this->oferta_model->getOferMon($oferta['monitor_idusuario']);   
            
                foreach ($ofertas as $oferta) {
                    $disciplina = $this->disciplina_model->getDisciplina($oferta['iddisciplina']);
                    $turma = $this->turma_model->getTurma($oferta['idturma']);
                    $drop_ofertas[$oferta['id_oferta']] = $disciplina['nome_disciplina'].' - '.$turma['nome_turma'];
                }
                
                $data['disciplina_oferta'] = $drop_ofertas;
            }
            $data['nome_monitor'] = $drop_monitor;
            $this->template->show('busca_aulas',$data);
        }
        
        
        public function buscaOfertas()
        {
            $this->load->model('oferta_model');
            $this->load->model('disciplina_model');
            $this->load->model('turma_model');    
            
            $idMonitor = $this->input->post('idMonitor');
            
            if (($idMonitor==0)&&($this->session->userdata('level')>2)){
                $ofertas = $this->oferta_model->getOferUni($this->session->userdata('unidade'));
            }
            else if (($this->session->userdata('level')>2)||(($this->session->userdata('level')==2)&&($idMonitor==$this->session->userdata('user')))){
                $ofertas = $this->oferta_model->getOferMonUni($idMonitor,$this->session->userdata('unidade'));   
            }
            
                            
            foreach ($ofertas as $oferta) {
                $disciplina = $this->disciplina_model->getDisciplina($oferta['iddisciplina']);
                $turma = $this->turma_model->getTurma($oferta['idturma']);
                $drop_ofertas[$oferta['id_oferta']] = $disciplina['nome_disciplina'].' - '.$turma['nome_turma'];
            }
            
            $data['ofertas_monitor'] = $drop_ofertas;
            
            echo json_encode($data);
        }
        
        public function busca()
        {
                
            $this->load->model('aula_model');
            $this->load->model('oferta_model');
            $this->load->model('user_model');
            
            $idOferta = $this->input->post('idOferta');
            $oferta = $this->oferta_model->get($idOferta);
            if (
                ($this->session->userdata('level')>2)&&
                    ($this->oferta_model->UnidPossuiOfer($this->session->userdata('unidade'),$idOferta))
                ||
                (($this->session->userdata('level')==2)&&
                    ($oferta['monitor_idusuario']==$this->session->userdata('user')))
            ){
                $aulas = $this->aula_model->getAulasOfer($idOferta);
                foreach ($aulas as $aula) {
                    $dados['descricao_aula'] = $aula['descricao_aula'];
                    $date = DateTime::createFromFormat('Y-m-d', $aula['data_aula']);
                    $dados['data_aula'] = $date->format('d/m/Y'); 
                    $dados['frequencia_lancada'] = $aula['frequencia_lancada'];
                    $dados['id_oferta'] = $aula['aula_id_oferta'];
                    $list_aula[$aula['idaula']] = $dados;
                }
                
                $data['aulas_ofertadas'] = $list_aula;
                $data['err'] = "ok";
                echo json_encode($data);
            
            }
        else{    
           
            $data['err'] = "Ação Indisponível";
            echo json_encode($data);
            }
        
        }

        
        
        public function buscaAlunos()
        {
            $this->load->model('oferta_model');
            $this->load->model('aluno_model');
            $this->load->model('aula_model');
            $this->load->model('frequencia_model');
            
            
            $idAula = $this->input->post('idAula');
            
            $aula = $this->aula_model->get($idAula);
            
            $oferta = $this->oferta_model->get($aula['aula_id_oferta']);
            
            if (
                ($this->session->userdata('level')>2)&&
                    ($this->oferta_model->UnidPossuiOfer($this->session->userdata('unidade'),$oferta['id_oferta']))
                ||
                (($this->session->userdata('level')==2)&&
                    ($oferta['monitor_idusuario']==$this->session->userdata('user')))
            ){
                
                
                
                $alunos = $this->aluno_model->obterAlunoTurma($oferta['idturma'],$aula['data_aula']);
                
                foreach ($alunos as $aluno) {
                    $aluno_data['nome_aluno'] = $aluno['nome_social_usuario']==null?$aluno['nome_usuario']:$aluno['nome_social_usuario'];
                    if ($this->frequencia_model->possuiFalta(array('aula_idaula' => $idAula, 'aluno_idaluno' => $aluno['idusuario'])))
                    $aluno_data['falta_aluno'] = 1;
                    else $aluno_data['falta_aluno'] = 0;
                    $drop_aluno[$aluno['idusuario']] = $aluno_data;
                }
                
                $alunos = $this->aluno_model->obterAlunoTurmaTrans($oferta['idturma'],$aula['data_aula']);
                
                foreach ($alunos as $aluno) {
                    $aluno_data['nome_aluno'] = $aluno['nome_social_usuario']==null?$aluno['nome_usuario']:$aluno['nome_social_usuario'];
                    if ($this->frequencia_model->possuiFalta(array('aula_idaula' => $idAula, 'aluno_idaluno' => $aluno['idusuario'])))
                    $aluno_data['falta_aluno'] = 1;
                    else $aluno_data['falta_aluno'] = 0;
                    $drop_aluno[$aluno['idusuario']] = $aluno_data;
                }
                
                $data['alunos'] = $drop_aluno;
                $data['err'] = "ok";
            }
            else {
                $data['alunos'] = '';
                $data['err'] = "Ação Indisponível";
            } 
            
            echo json_encode($data);
        }
        
        public function cria()
        {
            $data = $this->template->loadCabecalho('Cadastro de Aula');
            
            $data['view'] = "coordenador";
                        
            $this->load->model('aula_model');
            $this->load->model('user_model');
            $this->load->model('oferta_model');
            
            
            $sql_data = array(
                'aula_id_oferta' => $this->input->post('disciplina_oferta'),
                'descricao_aula' => $this->input->post('conteudo_aula'),
                'qtd_aulas' => 1
            );
                
            $this->load->helper('url');
            $this->load->helper('form');
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('disciplina_oferta','Oferta','required|is_natural|contains[oferta_disciplina.id_oferta,oferta_disciplina.status,#1#]');
            $this->form_validation->set_rules('data_aula','Data da Aula',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_date'));
            //$this->form_validation->set_rules('qtd_aulas','Quantidade de Aulas','required|is_natural');
            
            
            if ($this->form_validation->run()==TRUE){
                    
                $id = $this->session->userdata('user');
                $user = $this->user_model->get($id); 
                
                $oferta = $this->oferta_model->get($sql_data['aula_id_oferta']);
                
                
                
                if (
                    ($this->session->userdata('level') > 2) &&
                        ($this->aula_model->UnidadeContemOferta($this->input->post('disciplina_oferta'),$this->session->userdata('unidade')))
                    ||
                    (($this->session->userdata('level') == 2) &&
                        ($oferta['monitor_idusuario'] == $id))
                ){
                    
                    $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_aula'));
                    $sql_data['data_aula']=$date->format('Y-m-d');
                    
                    if (!$this->input->post('id_aula')){
                        $this->aula_model->create($sql_data);
                    }else {
                        $this->form_validation->set_rules('id_aula','aula','required|is_natural|contains[aula.idaula,oferta_disciplina.id_oferta,aula.aula_id_oferta,turma.idturma,oferta_disciplina.idturma,turma.curso_unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
                        if ($this->form_validation->run()){
                            $this->aula_model->update($this->input->post('id_aula'),$sql_data);
                        }
                        else{
                            $this->template->redirect('inicio');
                        }
                    }
                    
                    $this->template->redirect('aula/novo/'.$sql_data['aula_id_oferta']);
                    return;
                }
            }
                        
            
            $data = $this->template->loadCabecalho('Nova Aula');
            $data['view']='coordenador';
            
            $this->load->model('disciplina_model');
            $this->load->model('turma_model');
            $this->load->model('monitor_model');
            
            
            $monitores = $this->monitor_model->getMonitoresComOfertas($data['unidade']['idunidade']);
            $drop_monitor[0] = 'Todos';
            foreach ($monitores as $monitor) {
                $drop_monitor[$monitor['idusuario']]=$monitor['nome_usuario'];
            }

            $oferta = $this->oferta_model->get($this->input->post('disciplina_oferta'));
                
            $data['disciplina_oferta_sel'] = $this->input->post('disciplina_oferta');
            
            $data['$nome_monitor_sel'] = $oferta['monitor_idusuario'];
            
            $ofertas = $this->oferta_model->getOferMonUni($oferta['monitor_idusuario'],$this->session->userdata('unidade'));   
        
            foreach ($ofertas as $oferta) {
                $disciplina = $this->disciplina_model->getDisciplina($oferta['iddisciplina']);
                $turma = $this->turma_model->getTurma($oferta['idturma']);
                $drop_ofertas[$oferta['id_oferta']] = $disciplina['nome_disciplina'].' - '.$turma['nome_turma'];
            }
            
            $data['disciplina_oferta'] = $drop_ofertas;
            $data['exibir'] = true;
            
            $data['nome_monitor'] = $drop_monitor;
            $this->template->show('busca_aulas',$data);
        }

        public function frequencia()
        {
            $this->load->model('oferta_model');
            $this->load->model('aula_model');
            $this->load->model('aluno_model');
            $this->load->model('frequencia_model');
            $this->load->model('user_model');
            
            
            $idAula = $this->input->post('idAula');
            
            
            
            $aula = $this->aula_model->get($idAula);
            
            
            $oferta = $this->oferta_model->get($aula['aula_id_oferta']);

            $id = $this->session->userdata('user');
            $user = $this->user_model->get($id);             
            
            
            if (
                ($this->session->userdata('level') > 2)&&
                    ($this->aula_model->UnidadeContemOferta($aula['aula_id_oferta'],$this->session->userdata('unidade')))
                ||
                (($this->session->userdata('level') == 2) &&
                        ($oferta['monitor_idusuario'] == $id))
            ){            
            
                if ($aula['frequencia_lancada'] == 0){
                    $sql_aula['frequencia_lancada'] = 1;
                    $this->aula_model->update($aula['idaula'],$sql_aula);
                }
                
                $alunos = $this->aluno_model->obterAlunoTurma($oferta['idturma'],$aula['data_aula']);
                
                
                foreach ($alunos as $aluno) {
                    $sql_falta['aluno_idaluno'] = $aluno['idusuario'];
                    $sql_falta['aula_idaula'] = $idAula;
                    if ($this->input->post("Presenca".$aluno['idusuario'])=='Nao')
                        $this->frequencia_model->adicionarFalta($sql_falta);
                    else  $this->frequencia_model->removerFalta($sql_falta);
                }
                
                $alunos = $this->aluno_model->obterAlunoTurmaTrans($oferta['idturma'],$aula['data_aula']);
                
                foreach ($alunos as $aluno) {
                    $sql_falta['aluno_idaluno'] = $aluno['idusuario'];
                    $sql_falta['aula_idaula'] = $idAula;
                    if ($this->input->post("Presenca".$aluno['idusuario'])=='Nao')
                        $this->frequencia_model->adicionarFalta($sql_falta);
                    else  $this->frequencia_model->removerFalta($sql_falta);
                }
                
                $this->template->redirect('aula/novo/'.$aula['aula_id_oferta']);
                return;            
            }
            $this->template->redirect('aula/novo');             
    }

   public function getAulasFrequentadasTurmaDia(){
       $this->load->model('aula_model');
       $this->load->model('oferta_model');

       $oferta = $this->oferta_model->get($this->input->post('id_oferta'));
        
       if (
            ($this->session->userdata('level') > 2)&&
                ($this->aula_model->UnidadeContemOferta($this->input->post('id_oferta'),$this->session->userdata('unidade')))
            ||
            (($this->session->userdata('level') == 2) &&
                    ($oferta['monitor_idusuario'] == $this->session->userdata('user')))
        ){            

           
           $id_oferta = $this->input->post('id_oferta');
           $data_aula = $this->input->post('data_aula');
    
           $date = DateTime::createFromFormat('d/m/Y', $data_aula);
           $data_aula=$date->format('Y-m-d');
    
           $data['aulas'] = $this->aula_model->getAulasFrequentadasOfertaDia($id_oferta,$data_aula);
        
           echo json_encode($data);
        }
   }
   
   public function copia_frequencia(){
       $this->load->model('aula_model');
       $this->load->model('frequencia_model');
       $this->load->model('oferta_model');
        
       $this->load->helper('url');
       $this->load->helper('form');
       $this->load->library('form_validation');
       
       $this->form_validation->set_rules('aula_copia','disciplina1','required|is_natural|contains[aula.idaula,oferta_disciplina.id_oferta,aula.aula_id_oferta,turma.idturma,oferta_disciplina.idturma,turma.curso_unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
       $this->form_validation->set_rules('aula_copiada','disciplina2','required|is_natural|contains[aula.idaula,oferta_disciplina.id_oferta,aula.aula_id_oferta,turma.idturma,oferta_disciplina.idturma,turma.curso_unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
       if ($this->session->userdata('level')==2)
            $this->form_validation->set_rules('aula_copiada','disciplina','contains[aula.idaula,oferta_disciplina.id_oferta,aula.aula_id_oferta,oferta_disciplina.monitor_idusuario,#'.$this->session->userdata('user').'#]');
       if ($this->form_validation->run()){
           $id_aula_original = $this->input->post('aula_copia');
           $id_aula_copiada = $this->input->post('aula_copiada');
           $aula_original = $this->aula_model->get($id_aula_original);
           $aula_copiada = $this->aula_model->get($id_aula_copiada);
           $oferta = $this->oferta_model->get($aula_original['aula_id_oferta']);
           $this->form_validation->set_rules('aula_copiada','disciplina','contains[aula.idaula,oferta_disciplina.id_oferta,aula.aula_id_oferta,oferta_disciplina.idturma,#'.$oferta['idturma'].'#]');
           if ($this->form_validation->run()){
               $faltas = $this->frequencia_model->getFrequenciAula($id_aula_original);
               $this->frequencia_model->removerFaltasAula($id_aula_copiada);
               foreach ($faltas as $falta) {
                   
                   $this->frequencia_model->adicionarFalta(array('aula_idaula' => $id_aula_copiada, 'aluno_idaluno' => $falta['aluno_idaluno']));
               }
               $aula['frequencia_lancada'] = 1;
               $this->aula_model->update($id_aula_copiada,$aula);
               $this->template->redirect('aula/novo/'.$aula_copiada['aula_id_oferta']);
           }
                   
       }else{
           echo validation_errors();
       }
       
   } 


   public function remove($id){

        $this->load->model('aula_model');
        $this->load->model('oferta_model');
        $this->load->model('user_model');
        $this->load->model('turma_model');
        
        $aula = $this->aula_model->get($id);
        
        $user = $this->user_model->get($this->session->userdata('user'));
                
        $oferta = $this->oferta_model->get($aula['aula_id_oferta']);
                
        $turma = $this->turma_model->getTurma($oferta['idturma']);
        
        if ((($this->session->userdata('level')==2)&&($oferta['monitor_idusuario']==$this->session->userdata('user')))||
            (($this->session->userdata('level')>2)&& ($turma['curso_unidade_idunidade']==$this->session->userdata('unidade')))){
                
            $this->aula_model->delete($id);
        }
            
        $this->template->redirect('aula/novo');
    }
   
       public function relatorio_frequencia_disciplina()
    {

       $this->load->model('aula_model');
       $this->load->model('oferta_model');
       $this->load->library('excel');
       $this->load->library('pdf');
       $this->load->model('aluno_model');
       $this->load->model('disciplina_model');
       $this->load->model('turma_model');
       
       
       $tipo = $this->input->post('btnRelatorio');
       
       
       $idDisciplina = $this->input->post('disciplina_oferta');
       $idTurma = $this->input->post('nome_turma');
       
       $turma = $this->turma_model->getTurma($idTurma);
       
       $disciplinas = $this->disciplina_model->getDisciplinasCurso($turma['curso_idcurso']);
       
       $data = $this->template->loadCabecalho('Olá Mundo');
       
       $sheet = $this->excel->getActiveSheet();
       $n = 0;
       $index = 0;
       foreach ($disciplinas as $disciplina) {
           $qtdAulas = 0; 
           
           $idDisciplina = $disciplina['iddisciplina'];
                      
           $aulas = $this->aula_model->getAulasTurmaDisc($idTurma,$idDisciplina);
           $alunos = $this->aluno_model->obterAlunoTurma($idTurma,date('Y-m-d'));
           
           if (count($aulas)>0){
                if ($n!=0)
                    $sheet = $this->excel->createSheet($n++);
                else $n++;    
               $array = array();
               $array[0][0] = 'Disciplina:';
               $array[0][1] = $disciplina['nome_disciplina'];
               $i = 1;
               $j = 0;
               $array[$i][$j] = 'Alunos';  
               foreach ($alunos as $aluno) {
                   $sheet->getColumnDimension('A')->setAutoSize(true);
                   $array[++$i][0] = $aluno['nome_social_usuario']==null?$aluno['nome_usuario']:$aluno['nome_social_usuario'];
               }   
               $array[++$i][0] = '% Frequencia Aula';
               foreach ($aulas as $aula) {
                   $qtdAulas += $aula['qtd_aulas'];
                   $i = 1;
                   $date = DateTime::createFromFormat('Y-m-d', $aula['data_aula']);
                   $array[$i][++$j] = $date->format('d/m'); 
                   $sheet->getColumnDimension(chr(65+$j))->setAutoSize(true);
                    foreach ($alunos as $aluno) {
                        if ($this->aula_model->possuiFalta(array('aluno_idaluno' => $aluno['idusuario'], 'aula_idaula' => $aula['idaula']))){
                            $array[++$i][$j] = '0';
                        }
                        else{
                            $array[++$i][$j] = $aula['qtd_aulas'];
                        }
                    }
                    $array[++$i][$j] = "=SUM(".chr(65+$j)."2:".chr(65+$j).$i.")/(COUNT(".chr(65+$j)."2:".chr(65+$j).$i.')*'.$aula['qtd_aulas'].')';
                    $sheet->getStyle(chr(65+$j).($i+1))->getNumberFormat()->applyFromArray( 
                    array( 
                            'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00
                        )
                    );
                    
               }
        
               
               $i = 1;
               
               
               $array[$i][$j+1]= "% Presença";
               $fim = chr(65+$j);
               $j = ++$j;
               $sheet->getColumnDimension(chr(65+$j))->setAutoSize(true);
               foreach ($alunos as $aluno) {
                   $sheet->getStyle(chr(65+$j).($i+2))->getNumberFormat()->applyFromArray( 
                    array( 
                            'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00
                        )
                    );
                   $array[++$i][$j] = "=SUM(B".($i+1).':'.$fim.($i+1).')/'.$qtdAulas;
               }
               
               
        
                //name the worksheet
                $sheet->setTitle($disciplina['nome_disciplina']);
        
        
                // read data to active sheet
                $sheet->fromArray($array,null,'A1');
                
                $data['disciplinas'][$index++]=$array;   
            }
            
        
        }
     
     if ($tipo=='csv'){
     
        $date = DateTime::createFromFormat("Y-m-d",$turma['data_inicio_turma']);
    
        $filename=$turma['nome_turma'].'-'.$date->format("Y").'.csv'; //save our workbook as this file name
     
        header('Content-Type: application/vnd.ms-excel'); //mime type
     
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
     
        header('Cache-Control: max-age=0'); //no cache
                    
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
     
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel,"Excel5");
        
         $objWriter->save('php://output');
    
     }
    
     else {      
    
        $pdfFilePath = "/downloads/reports/$filename.pdf";  
        if (file_exists($pdfFilePath) == FALSE)
        {
            $date = DateTime::createFromFormat("Y-m-d",$turma['data_inicio_turma']);
    
            $filename=$turma['nome_turma'].'-'.$date->format("Y").'.pdf'; //save our workbook as this file name
            
            $data['turma'] = $turma['nome_turma'].'-'.$date->format("Y");
            
            $html = $this->load->view('relatorios/frequencia_disciplina', $data, true); // render the view into HTML
             
            $this->load->library('pdf');
            
            $pdf = $this->pdf->load();
            
            $pdf->AddPage('L');
            $pdf->SetFooter('Página {PAGENO} de {nb}| |'.date("d/m/Y")); // Add a footer for good measure <img class="emoji" draggable="false" alt="😉" src="https://s.w.org/images/core/emoji/72x72/1f609.png">
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($filename,"D"); // save to file because we can
        }
         
        $this->template->redirect("");
    
    }
        

        
    }
   
}
    