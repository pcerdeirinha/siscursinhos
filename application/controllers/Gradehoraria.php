<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gradehoraria extends CI_Controller {
	
	function Gradehoraria() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }

     public function index(){
        $data = $this->template->loadCabecalho('Grade horária');
         
        $this->load->model('curso_model');
        $this->load->model('turma_model');
        
        $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);

        foreach ($cursos as $curso) {            
            $dropcursos[$curso['idcurso']] = $curso['nome_curso'];
        }
        $data['cursos_drop'] = $dropcursos;
        $data['cursos'] = $cursos;
        $turmas[0]='Selecionar curso';
        $data['turmas_drop']=$turmas;

       	$this->template->show('gradehoraria', $data);
     }

     public function exibir($id_grade){
        $this->load->model('horario_model');
        $this->load->model('turma_model');
        $this->load->model('oferta_model');
        $this->load->model('user_model');
        $this->load->model('disciplina_model');
		$this->load->model('matricula_model');
		
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
		
        
		if ($this->session->userdata('level')==1){
			$matricula = $this->matricula_model->getMatriculaAtiva($this->session->userdata('user'));
            if (!$matricula) {$this->template->redirect("aluno");}
			$idturma = $matricula[0]['matricula_idturma'];
		}
        
        if (isset($id_grade)){
            $grade = $this->horario_model->getGrade($id_grade);
            if (isset($idturma) && ($idturma!=$grade['turma_idturma']))
                $this->template->redirect("aluno");
            else{
                $this->form_validation->set_data(array('turmas'=>$grade['turma_idturma']));
                $_POST['turmas'] = $grade['turma_idturma'];
            }
        }
		
		$data = $this->template->loadCabecalho('Horários das Aulas');

		$this->form_validation->set_rules('turmas','Turma','required|contains[turma.idturma,turma.curso_unidade_idunidade,#'.$data['unidade']['idunidade'].'#]');
		
		if (isset($idturma)||($this->form_validation->run())){
			if ($this->session->userdata('level')>1)
	        	$idturma = $this->input->post('turmas');
	        $grades = $this->horario_model->getGrade_Horaria($idturma);
            if (count($grades)==0){
                $idgrade = $this->horario_model->createGrade_Horaria(array('nome_grade'=>'Padrão', 'turma_idturma'=>$idturma));
                $data['grades'][$idgrade] = 'Padrão';
                $data['grade_at'] = 'Padrão';
                $data['idgrade_at'] = $idgrade;
            }else{
	           $horarios = $this->horario_model->get(isset($grade)?$id_grade:$grades[0]['idgrade_horaria']);
               $data['grade_at'] = isset($grade)?$grade['nome_grade']:$grades[0]['nome_grade'];
               $data['idgrade_at'] = isset($grade)?$id_grade:$grades[0]['idgrade_horaria'];
               foreach ($grades as $grade) 
                   $data['grades'][$grade['idgrade_horaria']] = $grade['nome_grade'];
            }
	        $turma = $this->turma_model->getTurma($idturma);
	
	        $data['id_turma'] = $idturma;
	        $data['turma'] = $turma;
	        
	        foreach ($horarios as $horario) {
	            $drop_horario[$horario['idhorario']]['pos_horario'] = $horario['pos_horario'];
	            $drop_horario[$horario['idhorario']]['dia_horario'] = $horario['dia_horario'];
	            $oferta = $this->oferta_model->get($horario['id_oferta']);
	            $professor = $this->user_model->get($oferta['monitor_idusuario']);
	            $disciplina = $this->disciplina_model->getDisciplina($oferta['iddisciplina']);
	            $drop_horario[$horario['idhorario']]['nome_professor'] = $professor['nome_usuario'];
	            $drop_horario[$horario['idhorario']]['nome_disciplina'] = $disciplina['nome_disciplina'];
	        }
	        $data['horarios'] = $drop_horario;
			$data['nome_turma'] = $turma['nome_turma'];
	
	        $this->template->show('gradehorariaview', $data);
        }
		else{
			$this->template->redirect('gradehoraria');
		}
     }

     public function novo(){
        $this->load->model('user_model');
        $this->load->model('oferta_model');
        $this->load->model('disciplina_model');
		$this->load->model('horario_model');
        
		$this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
		
		$data = $this->template->loadCabecalho('Confirmar Horário');
		
		$this->form_validation->set_rules('novohorario[]','Novo Horário','required|regex_match[/[a-z]{2}\d{2}$/]');
        $this->form_validation->set_rules('nome_grade','Grade Horária Selecionada','required|contains[grade_horaria.idgrade_horaria,turma.idturma,grade_horaria.turma_idturma,turma.curso_unidade_idunidade,#'.$data['unidade']['idunidade'].'#]');
        		
        $data['id_grade'] = $this->input->post('nome_grade');
		$grade = $this->horario_model->getGrade($data['id_grade']);
        if ($this->form_validation->run()){
	        
	        $ofertas = $this->oferta_model->getOferTur($grade['turma_idturma']);
	        
	        foreach ($ofertas as $oferta) {
	            $disciplina = $this->disciplina_model->getDisciplina($oferta['iddisciplina']);
	            $monitor = $this->user_model->get($oferta['monitor_idusuario']);
	            $drop_oferta[$oferta['id_oferta']] = $disciplina['nome_disciplina'] .' - '. $monitor['nome_usuario'];
	        }
	        
	        $data['disciplina_oferta'] = $drop_oferta;
	        $data['dias_semana'] = $diasDasemana = array (1 => "Segunda-Feira",2 => "Terça-Feira",3 => "Quarta-Feira",4 => "Quinta-Feira",5 => "Sexta-Feira",6 => "Sábado",7 => "Domingo");
	        $poshorario = $this->input->post('novohorario[]');
	        foreach ($poshorario as $key => $poshor) {
			    $data['tpos'][$key]=substr($poshor, 2,1);
                $data['tdia'][$key]=substr($poshor, 3);	
			}
	        $this->template->show('novohorario', $data);
        }
		else{
			$this->template->redirect('gradehoraria');
		}
    }

    public function novoHorario(){
        $data = $_SESSION['dados'];
        unset($_SESSION['dados']);
        $data['unidade'] = $this->input->post('unidade');

        $this->template->show('novohorario', $data);   
    }


    public function checked($data){

        $result = isset($_POST['novohorario']);
        if ($result){
            $retorno['tag']="ok";
            $retorno['dados']=$data;
            $retorno = json_encode($retorno);
            echo $retorno;
        } 
        else {
            $data['error']=true;
            $retorno['tag']="error";
            $retorno['dados']=$data;
            $retorno = json_encode($retorno);
            echo $retorno;
        }
    }

        
        public function buscaTurma(){
            $this->load->model('turma_model'); 
            $curso_sel = $this->input->post('cursos'); 

            if ($this->session->userdata('level')>2){
    
                $turmas = $this->turma_model->getTurmas($this->session->userdata('idunidade'));
                foreach ($turmas as $turma) {            
                    $dropturmas[$turma['idturma']] = $turma['nome_turma'].' / '.$turma['periodo_turma'].' / '.$turma['sala_turma'];
                }
                $data['turmas_drop'] = $dropturmas;
                $data['turmas'] = $turmas;
    
                $turmas = $this->turma_model->get($curso_sel);
            }
            else if ($this->session->userdata('level')==2){
                $this->load->model('oferta_model'); 
                $ofertas = $this->oferta_model->getOferMon($this->session->userdata('user'));
                
                $i = 0;
                foreach ($ofertas as $oferta) {
                    $turma = $this->turma_model->getTurma($oferta['idturma']);
                    
                    if ($turma['curso_idcurso']==$curso_sel){
                        $turmas[$i] = $turma;
                        $i++;
                    }
                }
            }
            
            $data['turmas'] = $turmas;
            
            echo json_encode($data);
        }

     public function cria(){
            $this->load->model('horario_model');
            
            $this->load->helper('url');
            $this->load->helper('form');
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('id_grade','Grade Horária Selecionada','required|contains[grade_horaria.idgrade_horaria,turma.idturma,grade_horaria.turma_idturma,turma.curso_unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
            $this->form_validation->set_rules('disciplina_oferta[]','Oferta de Disciplinas','required|contains[oferta_disciplina.id_oferta,grade_horaria.turma_idturma,oferta_disciplina.idturma,grade_horaria.idgrade_horaria,id_grade]');
			$this->form_validation->set_rules('dia_horario[]','Dia da Semana','required|is_natural_no_zero|less_than[8]');
            $posicoes_horario = $this->input->post('pos_horario[]');
            $dias_horario = $this->input->post('dia_horario[]');
            $disciplinas_oferta = $this->input->post('disciplina_oferta[]');
            foreach ($dias_horario as $key => $value) 
                $this->form_validation->set_rules('pos_horario['.$key.']','Posição da Aula','required|is_natural_no_zero|callback_verificaPendencias['.$this->input->post('dia_horario['.$key.']').','.$this->input->post('id_grade').','.$this->input->post('disciplina_oferta['.$key.']').']'); //Fazer verificação de horário já existente para a turma
                        
            if ($this->form_validation->run()){
                $sql_horario['grade_horaria_idgrade'] = $this->input->post('id_grade');
                foreach ($disciplinas_oferta as $key => $disciplina_oferta) {
                    $sql_horario['pos_horario'] = $posicoes_horario[$key];
                    $sql_horario['dia_horario'] = $dias_horario[$key];
                    $horario = $this->horario_model->getHorario_array($sql_horario);
                    if (isset($horario)){
                        $sql_horario['id_oferta'] = $disciplina_oferta;
                        $this->horario_model->update($horario['idhorario'],$sql_horario);
                    }
                    else{
                        $sql_horario['id_oferta'] = $disciplina_oferta;
                        $this->horario_model->create($sql_horario);
                    }
                    unset ($horario);
                    unset ($sql_horario['id_oferta']);
                }
                $this->template->redirect('gradehoraria/exibir/'.$this->input->post('id_grade'));
            }
            else{
                $this->load->model('user_model');
                $this->load->model('oferta_model');
                $this->load->model('disciplina_model');
                    
                $data = $this->template->loadCabecalho('Novo Horário');
                $grade = $this->horario_model->getGrade($this->input->post('id_grade'));
                
                $ofertas = $this->oferta_model->getOferTur($grade['turma_idturma']);

                foreach ($ofertas as $oferta) {
                    $disciplina = $this->disciplina_model->getDisciplina($oferta['iddisciplina']);
                    $monitor = $this->user_model->get($oferta['monitor_idusuario']);
                    $drop_oferta[$oferta['id_oferta']] = $disciplina['nome_disciplina'] .' - '. $monitor['nome_usuario'];
                }
                $poshorario = $this->input->post('novohorario[]');
                foreach ($dias_horario as $key => $dia_horario) {
                    $data['tpos'][$key]=$posicoes_horario[$key];
                    $data['tdia'][$key]=$dia_horario; 
                    $data['disciplina_oferta_sel'][$key] = $disciplinas_oferta[$key];
                }
                $data['disciplina_oferta'] = $drop_oferta;
                $data['dias_semana'] = $diasDasemana = array (1 => "Segunda-Feira",2 => "Terça-Feira",3 => "Quarta-Feira",4 => "Quinta-Feira",5 => "Sexta-Feira",6 => "Sábado",7 => "Domingo");
                
                $this->template->show('novohorario', $data);                
            }
     }

	public function verificaPendencias($pos,$params)
	{
		$this->load->model('horario_model');
		$this->form_validation->set_message('verificaPendencias','Horário coincidentes nesta turma');
		list($dia, $idgrade, $idoferta) = split(',', $params);
		if ($this->horario_model->horarioLivreTurma($idgrade,$pos,$dia)){
			$this->form_validation->set_message('verificaPendencias','Horário coincidentes para este professor');
			if ($this->horario_model->horarioLivreProfessor($idoferta,$pos,$dia)){
				return TRUE;
			}
		}
		return FALSE;
	}

     public function altera($idhorario){

     }

     public function remove($idhorario){
        $this->load->model('horario_model');
		$this->load->model('user_model');
		$this->load->model('oferta_model');
		$horario = $this->horario_model->getHorario($idhorario);
		if (isset($horario)){
			$user = $this->user_model->get($this->session->userdata('user'));
			$oferta = $this->oferta_model->get($horario['id_oferta']);
			$monitor = $this->user_model->get($oferta['monitor_idusuario']);
			if ($user['unidade_idunidade']==$monitor['unidade_idunidade']){
				$this->horario_model->delete($idhorario);
                $this->template->redirect('gradehoraria/exibir/'.$horario['grade_horaria_idgrade']);
			}
		}
		$this->template->redirect('gradehoraria');
     }
     
     public function cria_grade()
     {
         $this->load->model('horario_model');
        
         $this->load->helper('url');
         $this->load->helper('form');
         $this->load->library('form_validation');
            
         $this->form_validation->set_rules('id_turma','Turma','required|contains[turma.idturma,turma.curso_unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
        
         if ($this->form_validation->run()){
        
             $sql_grade['nome_grade'] = $this->input->post('nome_nova_grade');
             $sql_grade['turma_idturma'] = $this->input->post('id_turma');
             
             $id_grade = $this->horario_model->createGrade_Horaria($sql_grade);
             $this->template->redirect('gradehoraria/exibir/'.$id_grade);    
         }
         $this->template->redirect('gradehoraria');
     }
	 
     public function edita_grade()
     {
         $this->load->model('horario_model');
        
         $this->load->helper('url');
         $this->load->helper('form');
         $this->load->library('form_validation');
            
         $this->form_validation->set_rules('id_grade','Grade','required|contains[grade_horaria.idgrade_horaria,turma.idturma,grade_horaria.turma_idturma,turma.curso_unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
        
         if ($this->form_validation->run()){
        
             $sql_grade['nome_grade'] = $this->input->post('nome_nova_grade');
             
             $this->horario_model->updateGrade_Horaria($this->input->post('id_grade'),$sql_grade);
             $this->template->redirect('gradehoraria/exibir/'.$this->input->post('id_grade'));    
         }
         $this->template->redirect('gradehoraria');
     }

     public function remove_grade()
     {
         $this->load->model('horario_model');
        
         $this->load->helper('url');
         $this->load->helper('form');
         $this->load->library('form_validation');
            
         $this->form_validation->set_rules('id_grade','Grade','required|contains[grade_horaria.idgrade_horaria,turma.idturma,grade_horaria.turma_idturma,turma.curso_unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
         
         $grade = $this->horario_model->getGrade($this->input->post('id_grade'));
         $grades_da_turma = $this->horario_model->getGrade_Horaria($grade['turma_idturma']);
        
         if ($this->form_validation->run() && count($grades_da_turma)>1){
             
             $this->horario_model->deleteGrade_Horaria($this->input->post('id_grade'));
             $grades_da_turma = $this->horario_model->getGrade_Horaria($grade['turma_idturma']);
             end($grades_da_turma);
             $this->template->redirect('gradehoraria/exibir/'.$grades_da_turma[key($grades_da_turma)]['idgrade_horaria']);
         }
         $this->template->redirect('gradehoraria');
     }
     
     public function obter_horarios()
     {
         $this->load->model('horario_model');
         $this->load->model('oferta_model');
         $this->load->model('user_model');
         $this->load->model('disciplina_model');
         
         $this->load->helper('url');
         $this->load->helper('form');
         $this->load->library('form_validation');
            
         $this->form_validation->set_rules('idgrade_sel','Grade','required|contains[grade_horaria.idgrade_horaria,turma.idturma,grade_horaria.turma_idturma,turma.curso_unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
         
         if ($this->form_validation->run()){
             $grade = $this->horario_model->getGrade($this->input->post('idgrade_sel'));
             $horarios = $this->horario_model->get($this->input->post('idgrade_sel'));
             $data['grade_at'] = $grade['nome_grade'];
             $data['idgrade_at'] = $grade['idgrade_horaria'];
             $data['horario'] = null;
             foreach ($horarios as $horario) {
                 $data['horario'][$horario['pos_horario']][$horario['dia_horario']]['idhorario'] = $horario['idhorario'];
                 $oferta = $this->oferta_model->get($horario['id_oferta']);
                 $professor = $this->user_model->get($oferta['monitor_idusuario']);
                 $disciplina = $this->disciplina_model->getDisciplina($oferta['iddisciplina']);
                 $data['horario'][$horario['pos_horario']][$horario['dia_horario']]['nome_professor'] = $professor['nome_usuario'];
                 $data['horario'][$horario['pos_horario']][$horario['dia_horario']]['nome_disciplina'] = $disciplina['nome_disciplina'];
             }
             
             echo json_encode($data);
         }
               
     }
     
          
	 public function relatorio()
	 {
        $this->load->model('horario_model');
        $this->load->model('turma_model');
        $this->load->model('oferta_model');
        $this->load->model('user_model');
        $this->load->model('disciplina_model');
		$this->load->model('matricula_model');
		
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');

		$data = $this->template->loadCabecalho('Relatório');
		
    	$idturma = $this->input->post('nome_turma');
        $horarios = $this->horario_model->get($idturma);
        $turma = $this->turma_model->getTurma($idturma);

		if ($turma['curso_unidade_idunidade']==$data['unidade']['idunidade']){
	        $data['id_turma'] = $idturma;
	        $data['turma'] = $turma;
	        
	        foreach ($horarios as $horario) {
	            $drop_horario[$horario['idhorario']]['pos_horario'] = $horario['pos_horario'];
	            $drop_horario[$horario['idhorario']]['dia_horario'] = $horario['dia_horario'];
	            $oferta = $this->oferta_model->get($horario['id_oferta']);
	            $professor = $this->user_model->get($oferta['monitor_idusuario']);
	            $disciplina = $this->disciplina_model->getDisciplina($oferta['iddisciplina']);
	            $drop_horario[$horario['idhorario']]['nome_professor'] = $professor['nome_usuario'];
	            $drop_horario[$horario['idhorario']]['nome_disciplina'] = $disciplina['nome_disciplina'];
	        }
	        $data['horarios'] = $drop_horario;
			$data['nome_turma'] = $turma['nome_turma'];
				
				
	        $pdfFilePath = "/downloads/reports/$filename.pdf";  
	        if (file_exists($pdfFilePath) == FALSE)
	        {
	        	
	            $date = DateTime::createFromFormat("Y-m-d",$turma['data_inicio_turma']);
	    
	            $filename=$turma['nome_turma'].'-'.$date->format("Y").'.pdf'; //save our workbook as this file name
	            
	            $data['turma'] = $turma['nome_turma'].'-'.$date->format("Y");
	            $html = $this->load->view('relatorios/grade_horaria', $data, true); // render the view into HTML
	             
	            $this->load->library('pdf');
				
	            $pdf = $this->pdf->load();
	            $pdf->AddPage('L');
				
	            $pdf->SetFooter('Página {PAGENO} de {nb}| |'.date("d/m/Y")); // Add a footer for good measure <img class="emoji" draggable="false" alt="😉" src="https://s.w.org/images/core/emoji/72x72/1f609.png">
	            
	            $pdf->WriteHTML($html); // write the HTML into the PDF
	            $pdf->Output($filename,"D"); // save to file because we can
	            
	        }
	         
	        $this->template->redirect("");
        }
	
	 }
 }