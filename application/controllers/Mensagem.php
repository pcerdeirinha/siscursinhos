<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mensagem extends CI_Controller {
    
    function Mensagem() {
        parent::__construct();
        if(!$this->session->userdata('logged')){
            $this->session->set_flashdata('acesso_temp', uri_string());
        	$this->template->redirect('login');	
		}
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }
     public function index(){
         $data = $this->template->loadCabecalho('Caixa de Entrada');
         
         $this->load->model('mensagem_model');
         $data['mensagens'] = $this->mensagem_model->obterUltimasMensagensUsuario($this->session->userdata('user'));
         $contador_recados = $this->mensagem_model->obterNumeroRecadosNaoLidos($this->session->userdata('user'));
         foreach ($contador_recados as $key => $cont) {
             $key_r = array_search($cont['idmensagem'], array_column($data['mensagens'], 'idmensagem'));
             $data['mensagens'][$key_r]['count'] = $cont['count'];
         }
         $contador_respostas = $this->mensagem_model->obterNumeroRespostasNaoLidas($this->session->userdata('user'));
         foreach ($contador_respostas as $key => $cont) {
             $key_r = array_search($cont['idmensagem'], array_column($data['mensagens'], 'idmensagem'));
             $data['mensagens'][$key_r]['count'] += $cont['count'];
         }
         
         foreach ($data['mensagens'] as $key => $mensagem) {
            $data['mensagens'][$key]['assunto_mensagem'] = $this->template->decrypt($mensagem['assunto_mensagem']);    
         }         
         $this->template->show('caixa_entrada',$data);
     }
     
     public function novo(){
         $data = $this->template->loadCabecalho('Nova Mensagem');
         $this->load->model('grupo_model');
         $gruposPadroes = $this->grupo_model->getGruposPadroes($this->session->userdata('level'));
         foreach ($gruposPadroes as $grupoPadrao) {
             if ($grupoPadrao['model_obtencao']!=NULL){
                $this->load->model($grupoPadrao['model_obtencao']);
                $res = $this->$grupoPadrao['model_obtencao']->$grupoPadrao['metodo_obtencao']($this->session->userdata($grupoPadrao['parametro_obtencao']));
                foreach ($res as $resp) {
                    $data['grupos_padroes'][$grupoPadrao['idgrupo']][$resp[$grupoPadrao['indice_resultado']]] = $resp[$grupoPadrao['nome_resultado']];
                }
             }
             else{
                 $data['grupos_padroes'][$grupoPadrao['idgrupo']]=NULL;
             }
             $data['gruposNome'][$grupoPadrao['idgrupo']] = $grupoPadrao['nome_grupo'];
         }
           
         $gruposPersonalizados = $this->grupo_model->getGruposPersonalizados($this->session->userdata('user'));
         foreach ($gruposPersonalizados as  $grupoPersonalizado) {
             $data['grupos_padroes'][$grupoPersonalizado['idgrupo']]=NULL;
             $data['gruposNome'][$grupoPersonalizado['idgrupo']] = $grupoPersonalizado['nome_grupo'];
         }
         $this->template->show('novamensagem',$data);
     }
     
     public function cria(){
        $this->load->model('mensagem_model');
        $this->load->model('grupo_model');
        $this->load->model('user_model');
		 
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        if ($this->session->userdata('level')<6)
            $this->form_validation->set_rules('usuario[]','usuarios','contains[usuario.idusuario,unidade_usuario.usuario_idusuario,usuario.idusuario,unidade_usuario.unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
        else if ($this->session->userdata('level')==6)
            $this->form_validation->set_rules('usuario[]','usuarios','contains[usuario.idusuario,usuario.status,#1#]');
        
        $this->form_validation->set_rules('mensagem','mensagem','required');
        if (!$this->input->post('usuario[]'))
            $this->form_validation->set_rules('idgrupo[]','idgrupo','required');
        $this->form_validation->set_rules('idgrupo[]','idgrupo','contains[grupo.idgrupo]');
        
        $grupos = $this->input->post('grupos[]');
        sort($grupos);
        foreach ($grupos as $key=>$grupo) {
            list($_POST['idgrupo'][$key],$_POST['idobter'][$key]) = explode('-', $grupo);
        }
        
        if (!$this->form_validation->run()){
            $this->form_validation->reset_validation();
            if ($this->session->userdata('level')==4)
                $this->form_validation->set_rules('usuario[]','usuarios','contains[usuario.idusuario,unidade_usuario.usuario_idusuario,usuario.idusuario,unidade_usuario.tipo_usuario,#4#]');
            else if ($this->session->userdata('level')==5)
                $this->form_validation->set_rules('usuario[]','usuarios','contains[usuario.idusuario,unidade_usuario.usuario_idusuario,usuario.idusuario,unidade_usuario.tipo_usuario,#5#]');
            else 
                $this->form_validation->set_rules('usuario[]','usuarios','contains[usuario.idusuario,unidade_usuario.usuario_idusuario,usuario.idusuario,unidade_usuario.unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
            
            $this->form_validation->set_rules('mensagem','mensagem','required');
            if (!$this->input->post('usuario[]'))
                $this->form_validation->set_rules('idgrupo[]','idgrupo','required');
            $this->form_validation->set_rules('idgrupo[]','idgrupo','contains[grupo.idgrupo]');
        }
        if ($this->form_validation->run()){
            $sql_mensagem['assunto_mensagem'] = $this->template->encrypt($this->input->post('assunto'));
            $msg = $this->input->post('resposta[]');
            if (!$msg)
                $sql_mensagem['resposta_mensagem'] = 0;
            else if ((count($msg)==1) && ($msg[0]=='publica'))
                $sql_mensagem['resposta_mensagem'] = 1;
            else if ((count($msg)==1) && ($msg[0]=='privada'))
                $sql_mensagem['resposta_mensagem'] = 2;
            else    
                $sql_mensagem['resposta_mensagem'] = 3;
            $idmensagem = $this->mensagem_model->novaMensagem($sql_mensagem);
            
            $sql_recado = array(
                            'mensagem_idmensagem' => $idmensagem,
                            'corpo_recado' => $this->template->encrypt($this->input->post('mensagem')),
                            'remetente_idusuario' => $this->session->userdata('user')
                          );
            $this->mensagem_model->novoRecado($sql_recado);
            $sql_destinatario['mensagem_idmensagem'] = $idmensagem;
            
            $usuarios = $this->input->post('usuario[]');
            
            foreach ($usuarios as $usuario) {
                $user = $this->user_model->get($usuario);
                $i = count($lst_mensagem);
                $lst_mensagem[$i]['nome_usuario'] = $user['nome_usuario'];
                $lst_mensagem[$i]['email_usuario'] = $user['email_usuario'];
                $lst_mensagem[$i]['mensagem_enviada'] = $this->input->post('mensagem');
                $remetente = $this->user_model->get($this->session->userdata('user'));
                $lst_mensagem[$i]['remetente'] = $remetente['nome_usuario'];
                $lst_mensagem[$i]['assunto'] = $this->input->post('assunto');
                $lst_mensagem[$i]['caminho'] = site_url('mensagem/ver/'.$idmensagem);
            }
            
            $grupos_id = $this->grupo_model->getGruposPadraoInArray($this->input->post('idgrupo[]'),$this->session->userdata('level'));
            $obter_id = $this->input->post('idobter[]');
            foreach ($grupos_id as $key => $grupo_id) {
                if ($grupo_id!=$id_anterior){
                    $id_anterior = $grupo_id;
                    $grupo = $this->grupo_model->getGrupoArmazenamento($grupo_id);
                    if (!in_array($grupo['model_armazenamento'],$models)){
                        $this->load->model($grupo['model_armazenamento']);
                        $models[count($models)] = $grupo['model_armazenamento'];
                    }
                }
                $users = $this->$grupo['model_armazenamento']->$grupo['metodo_armazenamento']($obter_id[$key]);
                foreach ($users as $user){
                    if ($this->session->userdata('user')!=$user['idusuario'])
                        $usuarios[count($usuarios)] = $user['idusuario'];
                        $i = count($lst_mensagem);
                        $lst_mensagem[$i]['nome_usuario'] = $user['nome_usuario'];
                        $lst_mensagem[$i]['email_usuario'] = $user['email_usuario'];
                        $lst_mensagem[$i]['mensagem_enviada'] = $this->input->post('mensagem');
                        $remetente = $this->user_model->get($this->session->userdata('user'));
                        $lst_mensagem[$i]['remetente'] = $remetente['nome_usuario'];
                        $lst_mensagem[$i]['assunto'] = $this->input->post('assunto');
                        $lst_mensagem[$i]['caminho'] = site_url('mensagem/ver/'.$idmensagem);
                }
            }
            $grupos_id = $this->grupo_model->getGruposPersonalizadoInArray($this->input->post('idgrupo[]'),$this->session->userdata('user'));
            foreach ($grupos_id as $grupo_id) {
                $users = $this->grupo_model->getUsersInGrupoPersonalizado($grupo_id);
                foreach ($users as $user) {
                    $usuarios[count($usuarios)] = $user['usuario_idusuario'];
					$i = count($lst_mensagem);
                    $lst_mensagem[$i]['nome_usuario'] = $user['nome_usuario'];
                    $lst_mensagem[$i]['email_usuario'] = $user['email_usuario'];
                    $lst_mensagem[$i]['mensagem_enviada'] = $this->input->post('mensagem');
                    $remetente = $this->user_model->get($this->session->userdata('user'));
                    $lst_mensagem[$i]['remetente'] = $remetente['nome_usuario'];
                    $lst_mensagem[$i]['assunto'] = $this->input->post('assunto');
                    $lst_mensagem[$i]['caminho'] = site_url('mensagem/ver/'.$idmensagem);
                }
            }
            $usuarios = array_unique($usuarios);
            foreach ($usuarios as $usuario) {
                $sql_destinatario['usuario_idusuario'] = $usuario;
                $this->mensagem_model->novoDestinatario($sql_destinatario);
            }
            $parametro['lista'] = $lst_mensagem;
            $param = http_build_query($parametro);
            $fp = fsockopen(gethostname(), 80, $errno, $errstr, 30);
            //echo gethostname();
            if (!$fp) {
                echo "$errstr ($errno)<br />\n";
            } else {
                    
                $out = "POST /projetos/sgc/index.php/tools/enviaemailmensagem HTTP/1.1\r\n";
                
                //$out = "POST /versoes/12.07.2/SGC/index.php/tools/enviaemailmensagem HTTP/1.1\r\n";
                $out .= "Host: ".gethostname()."\r\n";
                $out .= "Content-Type: application/x-www-form-urlencoded\r\n";
                $out .= "Content-Length: ".strlen($param)."\r\n";
                $out .= "Connection: Close\r\n\r\n";
                fwrite($fp, $out);
                fwrite($fp, $param);
                
            }
            $this->template->redirect('mensagem');
        }
        else{
            $this->novo();
        }
     }

     public function ver($id_mensagem){
         $data = $this->template->loadCabecalho('Vizualizar Mensagem');
         $this->load->model('mensagem_model');
         $ultima = 0;
         $data['recados'] = $this->mensagem_model->getAllRecados($id_mensagem,$this->session->userdata('user'));
         foreach ($data['recados'] as $key => $recado) {
             $data['recados'][$key]['corpo_recado'] = $this->template->decrypt($recado['corpo_recado']);
             $data['respostas'][$key] = $this->mensagem_model->getAllRespostas($recado['idrecado'],$this->session->userdata('user'));
            foreach ($data['respostas'][$key] as $key_r => $resposta) {
                if ($resposta['idresposta_recado']>$ultima)
                    $ultima = $resposta['idresposta_recado'];
                $data['respostas'][$key][$key_r]['recado_resposta'] = $this->template->decrypt($resposta['recado_resposta']);
            }
         }
         if ($data['recados']!=array()){
             $data['usuarios'] = $this->mensagem_model->obterMembrosMensagem($id_mensagem);
         }
         $this->mensagem_model->updateRecadoVisualizado($id_mensagem,$this->session->userdata('user'),array('recado_visualizado_idrecado' => $data['recados'][count($data['recados'])-1]['idrecado'], 'resposta_visualizada_idresposta' => $ultima==0?NULL:$ultima));
         $this->template->show('mensagem',$data);
     }
     
     public function resposta($id_mensagem){
         $this->load->model('mensagem_model');
         
         $recado = $this->mensagem_model->getPrimeiroRecado($id_mensagem,$this->session->userdata('user'));
         if ($recado){
             $this->mensagem_model->novoDestinatario(array('usuario_idusuario'=>$recado['usuario_idusuario'], 'mensagem_idmensagem'=>$id_mensagem, 'recado_visualizado_idrecado'=>$recado['idrecado']));
                
             $id_recado = $this->mensagem_model->novoRecado(array('mensagem_idmensagem' => $id_mensagem, 'remetente_idusuario' => $this->session->userdata('user'), 'corpo_recado' => $this->template->encrypt($this->input->post('resposta'))));         
         
             $this->mensagem_model->exibirMensagens($id_mensagem);
             
             $this->mensagem_model->updateRecadoVisualizado($id_mensagem,$this->session->userdata('user'),array('recado_visualizado_idrecado' => $id_recado));
         }

         $this->template->redirect('mensagem/ver/'.$recado['mensagem_idmensagem']);
     }
     
     public function resposta_recado(){
        $this->load->model('mensagem_model');
        
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('idRecado','recado','contains[recado_mensagem.idrecado]');
        $this->form_validation->set_rules('resposta','resposta','required');
        $this->form_validation->set_rules('tipo_resposta','tipo','required|is_natural|less_than[2]');
        $recado = $this->mensagem_model->getRecado($this->input->post('idRecado'),$this->session->userdata('user'));
        if ($this->form_validation->run() && recado){
            $sql_resp = array(
                            'recado_idrecado'=>$this->input->post('idRecado'),
                            'remetente_idusuario'=>$this->session->userdata('user'),
                            'recado_resposta' => $this->template->encrypt($this->input->post('resposta')),
                            'publica_resposta' => $this->input->post('tipo_resposta')
            );
            $this->mensagem_model->novoDestinatario(array('usuario_idusuario'=>$recado['remetente_idusuario'], 'mensagem_idmensagem'=>$recado['mensagem_idmensagem'], 'recado_visualizado_idrecado'=>$recado['idrecado']));
            $idresposta = $this->mensagem_model->novaRespostaRecado($sql_resp);
            $this->mensagem_model->updateRecadoVisualizado($recado['mensagem_idmensagem'],$this->session->userdata('user'),array('resposta_visualizada_idresposta' => $idresposta));
        }
        $this->template->redirect('mensagem/ver/'.$recado['mensagem_idmensagem']);
     }
     
     public function enviado(){
         $data = $this->template->loadCabecalho('Enviados');
 
         $this->load->model('mensagem_model');
         $data['mensagens'] = $this->mensagem_model->obterUltimasMensagensEnviadas($this->session->userdata('user'));
         foreach ($data['mensagens'] as $key => $mensagem) {
            $data['mensagens'][$key]['assunto_mensagem'] = $this->template->decrypt($mensagem['assunto_mensagem']);    
         }
         
         $this->template->show('enviados',$data);
     }
     
    public function grupo(){
         $data = $this->template->loadCabecalho('Grupos');
         $this->load->model('grupo_model');
         $gruposPadroes = $this->grupo_model->getGruposPadroes($this->session->userdata('level'));
         foreach ($gruposPadroes as $grupoPadrao) {
             if ($grupoPadrao['model_obtencao']!=NULL){
                $this->load->model($grupoPadrao['model_obtencao']);
                $res = $this->$grupoPadrao['model_obtencao']->$grupoPadrao['metodo_obtencao']($this->session->userdata($grupoPadrao['parametro_obtencao']));
                foreach ($res as $resp) {
                    $data['grupos_padroes'][$grupoPadrao['idgrupo']][$resp[$grupoPadrao['indice_resultado']]] = $resp[$grupoPadrao['nome_resultado']];
                }
             }
             else{
                 $data['grupos_padroes'][$grupoPadrao['idgrupo']]=NULL;
             }
             $data['gruposNome'][$grupoPadrao['idgrupo']] = $grupoPadrao['nome_grupo'];
         }
           
         $gruposPersonalizados = $this->grupo_model->getGruposPersonalizados($this->session->userdata('user'));
         foreach ($gruposPersonalizados as  $grupoPersonalizado) {
             $data['grupos_personalizados'][$grupoPersonalizado['idgrupo']]=NULL;
             $data['gruposNome'][$grupoPersonalizado['idgrupo']] = $grupoPersonalizado['nome_grupo'];
         }
        
         $this->template->show('grupos',$data);
    }

    public function mensagem_grupo($idgrupo,$idparametro){
        $data = $this->template->loadCabecalho('Mensagens do Grupo');
        
        $this->load->model('grupo_model');
        $this->load->model('mensagem_model');
            
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_data(array('idgrupo'=>$idgrupo));
        $this->form_validation->set_rules('idgrupo','grupo','contains[grupo_padrao.grupo_idgrupo,grupo_padrao.nivel_minimo <=,#'.$this->session->userdata('level').'#,grupo_padrao.nivel_maximo >=,#'.$this->session->userdata('level').'#]');
        
        if ($this->form_validation->run()){
            $grupo = $this->grupo_model->getGrupoArmazenamento($idgrupo);
            $this->load->model($grupo['model_armazenamento']);
            $usuarios = $this->$grupo['model_armazenamento']->$grupo['metodo_armazenamento']($idparametro);
            $data['personalizado'] = false;    
        }else{
            $this->form_validation->reset_validation();
            $this->form_validation->set_data(array('idgrupo'=>$idgrupo));
            $this->form_validation->set_rules('idgrupo','grupo','contains[grupo_personalizado.grupo_idgrupo,grupo_personalizado.usuario_idusuario,#'.$this->session->userdata('user').'#]');
            if ($this->form_validation->run()){
                $usuarios = $this->grupo_model->getUsersInGrupoPersonalizado($idgrupo);
                $data['personalizado'] = true;
                $data['idgrupo'] = $idgrupo;
            }
            else $this->template->redirect('mensagem');
        }
        if (count(array_diff(array_column($usuarios, 'idusuario'), array($this->session->userdata('user'))))>0)
            $data['mensagens'] = $this->mensagem_model->obterMensagensUsuarios(array_diff(array_column($usuarios, 'idusuario'), array($this->session->userdata('user'))),$this->session->userdata('user'));
        else
            $data['mensagens'] = array();     
        foreach ($data['mensagens'] as $key => $mensagem) {
            $data['mensagens'][$key]['assunto_mensagem'] = $this->template->decrypt($mensagem['assunto_mensagem']);
        }
        
        $data['usuarios'] = $usuarios;
        
        $this->template->show('grupo_mensagem',$data);
    }

    public function remove_usuario($idgrupo,$idusuario){
        $this->load->model('grupo_model');
            
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
         
           
        $this->form_validation->set_data(array('idgrupo'=>$idgrupo));
        $this->form_validation->set_rules('idgrupo','grupo','required|contains[grupo_personalizado.grupo_idgrupo,grupo_personalizado.usuario_idusuario,#'.$this->session->userdata('user').'#]');
        if ($this->form_validation->run()){
            $this->grupo_model->remove_usuario(array('grupo_personalizado_idgrupo' => $idgrupo, 'usuario_idusuario' => $idusuario));
        }
        $this->template->redirect('mensagem/mensagem_grupo/'.$idgrupo);
    }

    public function cria_grupo(){
        $this->load->model('grupo_model');
            
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        if ($this->session->userdata('level')<6)
            $this->form_validation->set_rules('usuario[]','usuarios','contains[usuario.idusuario,unidade_usuario.usuario_idusuario,usuario.idusuario,unidade_usuario.unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
        else if ($this->session->userdata('level')==6)
            $this->form_validation->set_rules('usuario[]','usuarios','contains[usuario.idusuario,usuario.status,#1#]');
        if (!$this->form_validation->run()){
            $this->form_validation->reset_validation();
            if ($this->session->userdata('level')==4)
                $this->form_validation->set_rules('usuario[]','usuarios','contains[usuario.idusuario,unidade_usuario.usuario_idusuario,usuario.idusuario,unidade_usuario.tipo_usuario,#4#]');
            else if ($this->session->userdata('level')==5)
                $this->form_validation->set_rules('usuario[]','usuarios','contains[usuario.idusuario,unidade_usuario.usuario_idusuario,usuario.idusuario,unidade_usuario.tipo_usuario,#5#]');
            else 
                $this->form_validation->set_rules('usuario[]','usuarios','contains[usuario.idusuario,unidade_usuario.usuario_idusuario,usuario.idusuario,unidade_usuario.unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
        }       
        
        if ($this->input->post('idgrupo')){
            $this->form_validation->set_rules('idgrupo','grupo','required|contains[grupo_personalizado.grupo_idgrupo,grupo_personalizado.usuario_idusuario,#'.$this->session->userdata('user').'#]');
            $this->form_validation->set_rules('usuario[]','usuarios','not_contains[grupo_usuario.usuario_idusuario,grupo_usuario.grupo_personalizado_idgrupo,#'.$this->input->post('idgrupo').'#]');
            if ($this->form_validation->run()){
                $usuarios = $this->input->post('usuario[]');
                $sql_grupo['grupo_personalizado_idgrupo'] = $this->input->post('idgrupo');
                foreach ($usuarios as $key => $usuario) {
                    $sql_grupo['usuario_idusuario'] = $usuario;
                    $this->grupo_model->adicionaMembro($sql_grupo);
                }
            }
            $this->template->redirect('mensagem/mensagem_grupo/'.$this->input->post('idgrupo'));               
        }
        else{            
            $this->form_validation->set_rules('nome_grupo','Nome','required');
            if ($this->form_validation->run()){
                $idgrupo = $this->grupo_model->cria_grupo(array('nome_grupo' => $this->input->post('nome_grupo')));
                $this->grupo_model->cria_grupo_personalizado(array('grupo_idgrupo' => $idgrupo, 'usuario_idusuario' => $this->session->userdata('user')));
                $usuarios = $this->input->post('usuario[]');
                $sql_grupo['grupo_personalizado_idgrupo'] = $idgrupo;
                foreach ($usuarios as $key => $usuario) {
                    $sql_grupo['usuario_idusuario'] = $usuario;
                    $this->grupo_model->adicionaMembro($sql_grupo);
                }
                $this->template->redirect('mensagem/mensagem_grupo/'.$idgrupo);
            }
            else {
                $this->grupo();
            }
        }
    }

    public function remove_grupo($idgrupo){
        $this->load->model('grupo_model');
          
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_data(array('idgrupo'=>$idgrupo));
        $this->form_validation->set_rules('idgrupo','grupo','required|contains[grupo_personalizado.grupo_idgrupo,grupo_personalizado.usuario_idusuario,#'.$this->session->userdata('user').'#]');
            
        if ($this->form_validation->run()){
            $this->grupo_model->removerGrupo($idgrupo);
        }
        $this->template->redirect('mensagem/grupo');
        
    }

    public function remove($idmensagem){
        $this->load->model('mensagem_model');
        
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_data(array('idmensagem'=>$idmensagem));
        $this->form_validation->set_rules('idmensagem','mensagem','required|contains[destinatario_mensagem.mensagem_idmensagem,destinatario_mensagem.usuario_idusuario,#'.$this->session->userdata('user').'#]');
            
        if ($this->form_validation->run()){
            $this->mensagem_model->desativa_destinatario(array('mensagem_idmensagem' => $idmensagem, 'usuario_idusuario' => $this->session->userdata('user')));
        }
        $this->template->redirect('mensagem');
    }
}
?>