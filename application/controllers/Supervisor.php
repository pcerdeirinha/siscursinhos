<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Supervisor extends CI_Controller {
    
    function Supervisor() {
        parent::__construct();
        if(!$this->session->userdata('logged')){
            $this->template->redirect('login');
        }
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }
    
    function index()
    {
        $data = $this->template->loadCabecalho('Módulo Supervisor');  
        $this->session->set_userdata(array('view' => 6));
        $this->template->show('supervisor', $data); 
    }
}
?>