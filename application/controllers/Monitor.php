<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Monitor extends CI_Controller {
	
	function Monitor() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }
     
     
     public function bolsas($id_monitor)
     {
         $this->load->model('bolsa_model');
         $this->load->model('user_model');
         $user = $this->user_model->userEstaNaUnidade($id_monitor,$this->session->userdata('unidade'));
         if ($user['tipo_usuario']>1 && $user['tipo_usuario']<5){
             
             $data = $this->template->loadCabecalho('Lista de Bolsas');
             
             $data['monitor'] = $user;
             $bolsas = $this->bolsa_model->get($id_monitor);
             
             $data['bolsas'] = $bolsas;
             
             $this->template->show('lista_bolsa',$data);
         }
         else $this->template->redirect('inicio');
     }
     
    public function adicionarbolsa($idusuario)
    {
         $this->load->model('bolsa_model');
         $this->load->model('user_model');
         
         $this->load->helper('url');
         $this->load->helper('form');
         $this->load->library('form_validation');
        
         $user = $this->user_model->userEstaNaUnidade($idusuario,$this->session->userdata('unidade'));
         if ($user['tipo_usuario']>1 && $user['tipo_usuario']<5 && $user!=FALSE){
                $this->form_validation->set_rules('data_inicio_bolsa','Data de Início',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_date', 'callback_validaDataBolsa['.$this->input->post('data_inicio_bolsa').'#'.$idusuario.']'));
                $this->form_validation->set_rules('data_fim_bolsa','Data de Fim',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_date','callback_validaDataBolsa['.$this->input->post('data_fim_bolsa').'#'.$idusuario.']','callback_validaDataSequencia['.$this->input->post('data_fim_bolsa').']'));
                $this->form_validation->set_rules('tipo_bolsa','Tipo','required|is_natural|less_than[2]');
                
                if ($this->form_validation->run()){
                    $sql_bolsa = array(
                                    'monitor_idusuario' => $idusuario,
                                    'tipo_bolsa' => $this->input->post('tipo_bolsa')
                    );
                    $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_inicio_bolsa'));
                    $sql_bolsa['data_inicio_bolsa'] = $date->format('Y-m-d');
                 
                    $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_fim_bolsa'));
                    $sql_bolsa['data_fim_bolsa'] = $date->format('Y-m-d');
                    
                    $this->bolsa_model->create($sql_bolsa);
                    
                    $this->template->redirect("monitor/bolsas/".$idusuario);
                }
                else {
                     $data = $this->template->loadCabecalho('Lista de Bolsas');
             
                     $data['monitor'] = $user;
                     $bolsas = $this->bolsa_model->get($idusuario);
                     
                     $data['bolsas'] = $bolsas;
                     $data['erro']="adicionar";
                     $this->template->show('lista_bolsa',$data);
                }
         }
         else $this->template->redirect('inicio');
    }

    public function validaDataBolsa($data_p, $dataid)
    {
        $dados = split('#', $dataid);
        
        
        $data1 = DateTime::createFromFormat('d/m/Y', $data_p);
        //echo $data1;
        $data2 = DateTime::createFromFormat('d/m/Y', $dados[0]);
        
        $this->form_validation->set_message('validaDataBolsa', 'As Datas de Início e de Fim não devem coincidir com outras bolsas'); 
        if ($this->bolsa_model->get_interseccao($data1->format('Y-m-d'),$data2->format('Y-m-d'),$dados[1])==0){
            return TRUE;
        }
        else return FALSE;
    }
    
    public function validaDataSequencia($data_final, $data_inicial){
                
        $this->form_validation->set_message('validaDataSequencia', 'A Data Final deve vir após a Data Inicial'); 
        $data_i = DateTime::createFromFormat('d/m/Y', $data_inicial);
        
        $data_f = DateTime::createFromFormat('d/m/Y', $data_final);
         
        if ($data_f>=$data_i)                                
            return TRUE;
        else                     
            return FALSE;             
    }
        
        public function removerbolsa()
        {
            $this->load->model('bolsa_model');
            $this->load->model('user_model');
 
            $this->load->helper('url');
            $this->load->helper('form');
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('idbolsa','idbolsa','required|contains[bolsa.id_bolsa]');
            $this->form_validation->set_rules('tipos','Tipos','required|less_than[2]|is_natural');
            if ($this->form_validation->run()){    
                
                $idbolsa = $this->input->post('idbolsa');
                
                $bolsa = $this->bolsa_model->get_bolsa($idbolsa);
                $monitor = $this->user_model->userEstaNaUnidade($bolsa['monitor_idusuario'],$this->session->userdata('unidade'));
                if ($monitor!=FALSE){
                    if ($this->input->post('tipos')==1){
                        $this->bolsa_model->delete($idbolsa); 
                    }else{
                        $this->form_validation->set_rules('data_cancelamento','Data de Cancelamento',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_date','callback_validaDataSequencia['.$bolsa['data_inicio_bolsa'].']'));
                        
                        if ($this->form_validation->run()){
                            $sql_bolsa['id_bolsa'] = $idbolsa;
                                
                            $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_cancelamento'));
                            $sql_bolsa['data_fim_bolsa'] = $date->format('Y-m-d');
                            
                            $this->bolsa_model->update($sql_bolsa);
                        }
                    }
                    $this->template->redirect("monitor/bolsas/".$monitor['idusuario']);    
                }
                else{
                    $this->template->redirect("inicio");
                }
            }
            $this->template->redirect("monitor/bolsas/".$monitor['idusuario']);      
        }


        public function mudarAtividade()
        {
            $this->load->model('user_model');
            
            $this->load->helper('url');
            $this->load->helper('form');
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('id_monitor','idmonitor','required|contains[monitor.usuario_idusuario,unidade_usuario.usuario_idusuario,id_monitor,unidade_usuario.unidade_idunidade,#'.$this->session->userdata('unidade').'#,unidade_usuario.tipo_usuario <,#5#,unidade_usuario.tipo_usuario >,#1#]');
            $this->form_validation->set_rules('func','func','required|less_than[5]|is_natural|greater_than[1]');
            if ($this->form_validation->run()){    
                $sql_unidade_usuario['unidade_idunidade'] = $this->session->userdata('unidade');
                $sql_unidade_usuario['usuario_idusuario'] = $this->input->post('id_monitor');
                $sql_unidade_usuario['tipo_usuario'] = $this->input->post('func');
                
                $this->user_model->createUnidadeUsuario($sql_unidade_usuario);
                $this->template->redirect('usuario/busca_funcionario/'.$this->input->post('func'));
            }
            else{
                $this->template->redirect('inicio');
            }
        }

        public function buscar_monitores()
        {
             $this->load->model('user_model');
             $cpf = $this->input->post('cpf');
             $nome = $this->input->post('nome');
             $userfunc = array();
             for ($tipo=2;$tipo<5;$tipo++){
                 if(empty($nome)&&(!empty($cpf))){//busca apenas com cpf
                     $userfunc = array_merge($this->user_model->getUsuariosCPF($cpf,$this->session->userdata('unidade'),$tipo),$userfunc);
                 }else if (empty($cpf)&&(!empty($nome))) {//busca apenas com o nome
                     $userfunc = array_merge($this->user_model->getUsuariosNome($nome,$this->session->userdata('unidade'),$tipo),$userfunc);           
                 }else if ((!empty($cpf))&&(!empty($nome))){//busca com ambos
                     $userfunc = array_merge($this->user_model->getUsuariosNomeCPF($nome,$this->session->userdata('unidade'),$tipo),$userfunc); 
                 }else{//todos os funcionários
                     $userfunc = array_merge($this->user_model->getFuncionariosTipo($this->session->userdata('unidade'),$tipo),$userfunc); 
                 }
             }
             if(!(empty($userfunc))){
                 $data['userfunc']= $userfunc;
                 $data['err'] = 'ok';
                 echo json_encode($data);
             }else{
                 $data['err'] = 'Não foram encontrados monitores com estes dados!';
                 echo json_encode($data);
             }
        }

        public function atividade()
        {
            $data = $this->template->loadCabecalho('Busca de Monitores');
            $this->template->show('busca_monitores_atividade',$data);
        }
    
 }