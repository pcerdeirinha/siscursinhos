<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Aprovacao extends CI_Controller {
    
    function Aprovacao() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
            $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }
    
    
    public function novo($id)
    {
        $idAluno = base64_decode($id);
        
        $data = $this->template->loadCabecalho('Nova Aprovação');
        $this->load->model('curso_vestibular_model');
        $this->load->model('faculdade_vestibular_model');
        $this->load->model('estado_model');
        $this->load->model('user_model');
        
        $estados = $this->estado_model->get();
        $faculdades = $this->faculdade_vestibular_model->getFaculdades();
        $cursos = $this->curso_vestibular_model->getCursos();
        
        foreach ($faculdades as $faculdade) {
            $drop_faculdade[$faculdade['idFaculdade']] = $faculdade['nomeFaculdade_vestibular'];
        }
        
        foreach ($cursos as $curso) {
            $drop_curso[$curso['idCurso']] = $curso['nomeCurso'];            
        }
        foreach ($estados as $estado) {
            $drop_estados[$estado['id']] = $estado['uf'];
        }
        
        $data['cursos_faculdade_drop'] = $drop_curso;
        $data['faculdade_drop'] = $drop_faculdade;
        $data['estados'] = $drop_estados;
        $data['tipos_faculdade'] = array('Estadual'=>'Estadual','Federal'=>'Federal','Municipal'=>'Municipal','Privada'=>'Privada');
        $data['idaluno'] = $idAluno;
        
        $aluno = $this->user_model->get($idAluno);
        $data['nome_aluno'] = $aluno['nome_usuario'];
        $this->template->show('novaaprovacao',$data);
    }
    
    public function cria()
    {
        $this->load->model('aprovacao_model');
        
        $data = $this->template->loadCabecalho('Nova Aprovação');
        
        $sql_aprovacao = array('aluno_usuario_idusuario' => $this->input->post('idaluno'),
                               'idCurso' => $this->input->post('curso_faculdade'),
                               'idFaculdade' => $this->input->post('faculdade_aprovada'),
                              );
                              
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        
        $this->form_validation->set_rules('curso_faculdade','Curso Aprovado','required|contains[curso_vestibular.idCurso]');
        $this->form_validation->set_rules('idaluno','aluno','required|contains[aluno.usuario_idusuario]');  
        $this->form_validation->set_rules('faculdade_aprovada','Faculdade Aprovada','required|contains[faculdade_vestibular.idFaculdade]'); 
        $this->form_validation->set_rules('data_aprovacao','Data de Aprovação',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_date')); 
        
        
        if ($this->form_validation->run()){
                $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_aprovacao'));
                $aprov =  $date->format('Y-m-d');
                $sql_aprovacao['data_aprovacao']=$aprov;
                $this->aprovacao_model->create($sql_aprovacao);
                
                $this->template->redirect('coordenador');
                
        }
        else{
            $this->load->model('curso_vestibular_model');
            $this->load->model('faculdade_vestibular_model');
            $this->load->model('estado_model');
            
            $estados = $this->estado_model->get();
            $faculdades = $this->faculdade_vestibular_model->getFaculdades();
            $cursos = $this->curso_vestibular_model->getCursos();
            
            foreach ($faculdades as $faculdade) {
                $drop_faculdade[$faculdade['idFaculdade']] = $faculdade['nomeFaculdade'];
            }
            
            foreach ($cursos as $curso) {
                $drop_curso[$curso['idCurso']] = $curso['nomeCurso'];            
            }
            foreach ($estados as $estado) {
                $drop_estados[$estado['id']] = $estado['uf'];
            }
            $data['cursos_faculdade_drop'] = $drop_curso;
            $data['faculdade_drop'] = $drop_faculdade;
            $data['estados'] = $drop_estados;
            $data['tipos_faculdade'] = array('Estadual'=>'Estadual','Federal'=>'Federal','Municipal'=>'Municipal','Privada'=>'Privada');
        
            
            $this->template->show('novaaprovacao',$data);
            
            
        }
               
    }
    
    
}