<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Usuario extends CI_Controller {

    function Usuario() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
    }

    public function get_data(){
        $this->load->model('user_model');
        $id = $this->session->userdata('user');        
        $usuario = $this->user_model->get($id);
        
        echo json_encode($usuario['nome_usuario']);
    }

    public function buscaCidade(){

        $uf = $this->input->post('uf');        
        $this->load->model('cidade_model'); 
        
        $cidades = $this->cidade_model->get($uf);
        foreach ($cidades as $cidade) {
            $city[$cidade['id']] = $cidade['nome'];
        }
        $data['cidades'] = $city;
        echo json_encode($data);
    }

    public function trocasenha(){
        $data = $this->template->loadCabecalho('Trocar Senha');
        $this->template->show('trocasenha', $data);
    }

    public function updatesenha(){
        $this->load->model('user_model');
        $id = $this->session->userdata('user');        
        $usuario = $this->user_model->get($id);
        $senhaAtual = $this->input->post('senha_atual');
        $senhaNova = $this->input->post('senha_nova');
        $senhaNovaRepete = $this->input->post('senha_redigite');

        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('senha_atual','Senha atual','required|callback_validaSenhaAtual['.$id.']');
        $this->form_validation->set_rules('senha_nova','Nova senha','required');
        $this->form_validation->set_rules('senha_redigite','Redigitar senha','required|matches[senha_nova]');

        if($this->form_validation->run()==FALSE){
            $data = $this->template->loadCabecalho('Trocar Senha');
            $data['view']  = 'inicio';
            $data['senha_atual'] = $senhaAtual;       //senha atual digitada
            $data['senha_nova'] = $senhaNova;       //nova senha
            $data['senha_redigite'] = $senhaNovaRepete;       //confirmação nova senha
            $data['err'] = 'Erro no preenchimento das informações!';
            $this->template->show('trocasenha', $data);   
                        
        }else{
            $sql_data = array('senha_usuario' => $senhaNova);
            $this->user_model->update($usuario['idusuario'],$sql_data);
            $this->template->redirect('inicio');
        }        
        
    }
    public function validaSenhaAtual($senhaAtual, $id){        
        $this->load->model('user_model');
        $CI = & get_instance();
        $CI->form_validation->set_message('validaSenhaAtual', 'A senha digitada não confere com a sua senha atual!'); 
        $usuario = $this->user_model->get($id);
        $result = $this->user_model->validate($usuario['email_usuario'], $senhaAtual);             
        if($result){                       
            return TRUE;
        }else{                     
            return FALSE;
        }                
    }

  public function busca_funcionario($tipoFuncionario){
    $data = $this->template->loadCabecalho('');
     
     switch ($tipoFuncionario) {
         case 2://professor
             $data['page_title'] = 'Busca de Professores-Monitor';
             $data['tipoFuncionario'] = 2;
             $data['voltar'] = 'index.php/professor/opcoes';
             $data['monitor'] = 'Monitor';
             $this->template->show('busca_funcionario', $data);
             break;
         case 3://secretário
             $data['page_title'] = 'Busca de Secretários-Monitor';
             $data['tipoFuncionario'] = 3;
             $data['voltar'] = 'index.php/secretario/opcoes';
             $data['monitor'] = 'Secretário';
             $this->template->show('busca_funcionario', $data);
             break;
         case 4://coordenador
             $data['page_title'] = 'Busca de Coordenadores';
             $data['tipoFuncionario'] = 4;
             $data['voltar'] = 'index.php/coordenador/opcoes';
             $data['monitor'] = 'Coordenador Discente';
             $this->template->show('busca_funcionario', $data);
             break; 
         case 5://coordenador Docente  
             $data['page_title'] = 'Busca de Coordenadores Docentes';
             $data['tipoFuncionario'] = 5;
             $data['voltar'] = 'index.php/docente/opcoes';
             $data['monitor'] = 'Coordenador Docente';
             $this->template->show('busca_funcionario', $data);
             break;         
         
         default:
             $data['page_title'] = 'Busca de Professores-Monitor';
             $data['tipoFuncionario'] = 2;
             $data['voltar'] = 'index.php/professor/opcoes';
             $this->template->show('busca_funcionario', $data);
             break;
        }         
    }
      public function busca(){
         $this->load->model('user_model');
         $cpf = $this->input->post('cpf');
         $nome = $this->input->post('nome');
         $tipo = $this->input->post('tipo');        
         if(empty($nome)&&(!empty($cpf))){//busca apenas com cpf
             $userfunc = $this->user_model->getUsuariosCPF($cpf,$this->session->userdata('unidade'),$tipo);
         }else if (empty($cpf)&&(!empty($nome))) {//busca apenas com o nome
             $userfunc = $this->user_model->getUsuariosNome($nome,$this->session->userdata('unidade'),$tipo);           
         }else if ((!empty($cpf))&&(!empty($nome))){//busca com ambos
             $userfunc = $this->user_model->getUsuariosNomeCPF($nome,$this->session->userdata('unidade'),$tipo); 
         }else{//todos os funcionários
             $userfunc = $this->user_model->getFuncionariosTipo($this->session->userdata('unidade'),$tipo); 
         }
         if(!(empty($userfunc))){
             $data['userfunc']= $userfunc;
             $data['err'] = 'ok';
             echo json_encode($data);
         }else{
             $data['err'] = 'Não foram encontrados funcionários com estes dados!';
             echo json_encode($data);
         }
     }

     public function buscaNomeEmail(){
         $this->load->model('user_model');
         $nome = $this->input->post('nome');
         $usuarios = $this->input->post('usuarios');
         $data['users'] = $this->user_model->getUsuariosNomeEmail($nome,$this->session->userdata('unidade'),$usuarios==null?$usuarios:array(0),$this->session->userdata('level'));
         echo json_encode($data);
     }

    public function obterUsuario()
    {
        $cpf = $this->input->post('cpf');
        $this->load->model('user_model'); 
        $usuario = $this->user_model->getUser($cpf);
        echo json_encode($usuario);
    }
    
    public function obterUsuarioValido()
    {
        $cpf = $this->input->post('cpf');
        $this->load->model('user_model'); 
        $usuario = $this->user_model->getUserValid($cpf,$this->session->userdata('unidade'));
            if (count($usuario)==0){
                $usuario = $this->user_model->getUser($cpf);
                if (count($usuario)>0)
                    $usuario[0]['info'] = "false";
            }
        echo json_encode($usuario);
        //else json_encode(FALSE);
        
    }
    
    public function remove($id){
        //$idcurso = base64_decode(urldecode($id));
        $this->load->model('user_model');
        $unidade_usuario = $this->user_model->userEstaNaUnidade($id,$this->session->userdata('unidade'));
        if (($unidade_usuario['tipo_usuario'] == 5)||($unidade_usuario['tipo_usuario'] == 1)){
            echo "aa";
            $this->user_model->removeUnidadesUser($id);
            $this->user_model->delete($id);
        }else{
            $this->user_model->delete_unidade($id,$this->session->userdata('unidade'));
            $unidades = $this->user_model->unidades_usuario($id);
            if (count($unidades)==0)
                $this->user_model->delete($id);    
        }
        $this->template->redirect('usuario/busca_funcionario/'.$unidade_usuario['tipo_usuario']);
    }
    
    public function conclui_cadastro()
    {
        $this->load->model('user_model');
        
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email_usuario','Email','required|is_unique[usuario.email_usuario]|valid_email');                    
        if ($this->form_validation->run()){
            $sql_user['email_usuario'] = $this->input->post('email_usuario');
            $this->user_model->update($this->session->userdata('user'),$sql_user);
            $this->template->redirect('inicio');
        }
        else {
            $data = $this->template->loadCabecalho('Finalização do Cadastro');
            $this->template->show('cadastro_incompleto',$data);
        }
    }
	
	public function reset_senha_usuario()
	{
			
		$this->load->model('user_model');
		
		$user = $this->input->post('id_usuario');
		
		if ($this->user_model->userEstaNaUnidade($user,$this->session->userdata('unidade'))){
			$sql_data = $this->user_model->get($user);
				
			$sql_data['senha_usuario'] = $this->template->gerar_senha();
            
            
            $this->user_model->update($user,$sql_data);
            $this->load->library("MY_phpmailer");
                            
            if ($this->my_phpmailer->enviarEmailEsqueci($sql_data)){
            	echo json_encode("ok");
			}
			
		}
		else{
			echo json_encode("error");
		}
		
	}
}

?>