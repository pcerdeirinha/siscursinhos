<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Relatorio extends CI_Controller {
    
    function Relatorio() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }
    
    public function index()
    {
        $this->load->model('curso_model');
        
        $data = $this->template->loadCabecalho('Relatorios');
        $data['view'] = 'coordenador';
        
        $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);
        foreach ($cursos as $curso){
            $drop_curso[$curso['idcurso']] = $curso['nome_curso'];
        }
        $data['nome_curso'] = $drop_curso;
        
        $this->template->show('lista_relatorios',$data);
    }
    
}

 ?>