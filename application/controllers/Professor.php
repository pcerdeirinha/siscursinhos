<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Professor extends CI_Controller {
	
	function Professor() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }

     public function index(){
        $data = $this->template->loadCabecalho('Módulo Professor');
        $this->session->set_userdata(array('view' => 2));
        $this->template->show('professor', $data);
     }

     public function opcoes(){
        $data = $this->template->loadCabecalho('Cadastro de Professores');
        $this->template->show('professor_opcoes', $data);
     }  

     public function novo(){
        $data = $this->template->loadCabecalho('Novo Professor');

        $this->load->model('estado_model');
        $this->load->model('faculdade_model');
        $this->load->model('cidade_model');
        $this->load->model('departamento_model');
        
        //dados do usuário
        $data['nome_usuario'] = '';
        $data['nome_social_usuario'] = '';
        $data['rg_usuario'] = '';
        $data['cpf_usuario'] = '';
        $data['data_nascimento_usuario'] = '';
        $data['logradouro_usuario'] = '';
        $data['bairro_usuario'] = '';
        $data['complemento_endereco_usuario'] = '';
        $data['cep_usuario'] = '';
        $data['cidade_usuario'] = '';
        $data['estado_usuario'] = '';
        $data['email_usuario'] = '';
        $data['telefone_usuario'] = '';
        $data['celular_usuario'] = '';
        $data['periodo_curso_monitor'] = array('Matutino' => 'Matutino','Vespertino' => 'Vespertino','Noturno' => 'Noturno','Integral'=>'Integral', 'Vespertino/Noturno' => 'Vespertino/Noturno');        

        $estados = $this->estado_model->get();
        foreach ($estados as $estado) {
            $uf[$estado['id']] = $estado['uf'];
        }
        $data['estados'] = $uf;
        $city[0] ='Selecionar Estado'; 
        //Dropdown de Departamentos da Faculdade
        
        $faculdades = $this->faculdade_model->getFaculdadeCidade($data['faculdade']['cidade_faculdade']);
        foreach ($faculdades as $faculdade) {
            $departamentos = $this->departamento_model->get($faculdade['idfaculdade']);
            foreach ($departamentos as $departamento) {
                $drop_dep[$departamento['iddepartamento']] = $departamento['nome_departamento'];
            }    
        }  
        $data['departamento_monitor'] = $drop_dep;
        $data['monitor'] = 'Professor';
        $data['funcao_monitor'] = 'Professor-Monitor';
        
        $data['cidades'] = $city ;
        $this->template->show('novomonitor', $data);//chama a view novoaluno, com o vetor de dados.
     }

     public function edita($idprofessor){
            
        $data = $this->template->loadCabecalho('Edita Professor');
        
        $data['id'] = $idprofessor;
        $this->load->model('user_model');
        $this->load->model('faculdade_model');
        $this->load->model('estado_model');
        $this->load->model('cidade_model');
        $this->load->model('departamento_model');
        $this->load->model('cursofaculdade_model');
        $this->load->model('monitor_model');
        
        // Dados do Professor
        $professor = $this->monitor_model->getMonitor($idprofessor);
        //Dados do usuário
        $userProf = $this->user_model->get($idprofessor);
        
        $date = DateTime::createFromFormat('Y-m-d', $userProf['data_nascimento_usuario']);
        $nasc =  $date->format('d/m/Y'); 
        $date = DateTime::createFromFormat('Y-m-d', $professor['data_admissao_monitor']);     
        $adm =  $date->format('d/m/Y'); 

        $cursofaculdade = $this->cursofaculdade_model->getCurso($professor['monitor_idcurso_faculdade']);
        //pega o departamento do curso do monitor
        $dep = $this->departamento_model->getDep($cursofaculdade['curso_iddepartamento']);

        //Valores dos Dados Gerais
        $data['nome_usuario'] = $userProf['nome_usuario'];
        $data['nome_social_usuario'] = $userProf['nome_social_usuario'];
        $data['email_usuario'] = $userProf['email_usuario'];
        $data['rg_usuario'] = $userProf['rg_usuario'];
        $data['cpf_usuario'] = $userProf['cpf_usuario'];
        $data['data_nascimento_usuario'] = $nasc; 
        $data['telefone_usuario'] = $userProf['telefone_usuario'];
        $data['celular_usuario'] = $userProf['celular_usuario'];
        //Valores de endereço
        $data['logradouro_usuario'] = $userProf['logradouro_usuario'];
        $data['numero_usuario'] = $userProf['numero_usuario'];
        $data['bairro_usuario'] = $userProf['bairro_usuario'];
        $data['complemento_endereco_usuario'] = $userProf['complemento_endereco_usuario'];
        $data['estado_sel'] = $this->estado_model->getID($userProf['estado_usuario']);
        $data['cidade_sel'] = $userProf['cidade_usuario'];
        $data['cep_usuario'] = $userProf['cep_usuario'];
        //valores academicos
        $data['departamento_monitor_sel'] = $dep['iddepartamento'];
        $data['curso_monitor_sel'] = $professor['monitor_idcurso_faculdade'];
        $data['periodo_sel'] = $professor['periodo_curso'];
        $data['ano_curso_monitor'] = $professor['ano_ingresso_curso'];
        $data['ra_monitor'] = $professor['ra_monitor'];
        //valores do projeto
        $data['funcao_monitor'] = 'Professor-Monitor';
        $data['data_admissao_monitor'] = $adm; 
        $data['monitor_voluntario'] = $professor['monitor_voluntario'];
        //valores bancários
        $data['banco_monitor'] = $professor['banco_monitor'];
        $data['agencia_monitor'] = $professor['agencia_monitor'];
        $data['conta_monitor'] = $professor['conta_monitor'];
        $data['periodo_curso_monitor'] = array('Matutino' => 'Matutino','Vespertino' => 'Vespertino','Noturno' => 'Noturno','Integral'=>'Integral', 'Vespertino/Noturno' => 'Vespertino/Noturno');        
               
        
        //Dropdown de estados
        $estados = $this->estado_model->get();
        foreach ($estados as $estado) {
            $uf[$estado['id']] = $estado['uf'];
        }
        $data['estados'] = $uf;
        //Dropdown de cidades        
        $cidades = $this->cidade_model->get($userProf['estado_usuario']);
        foreach ($cidades as $cidade) {
            $city[$cidade['nome']] = $cidade['nome'];
        }
        //Dropdown de Departamentos da Faculdade 
        $faculdades = $this->faculdade_model->getFaculdadeCidade($data['faculdade']['cidade_faculdade']);
        foreach ($faculdades as $faculdade) {
            $departamentos = $this->departamento_model->get($faculdade['idfaculdade']);
            foreach ($departamentos as $departamento) {
                $drop_dep[$departamento['iddepartamento']] = $departamento['nome_departamento'];
            }    
        }
        $data['departamento_monitor'] = $drop_dep;
        //Dropdown de Curso
        $cursos = $this->cursofaculdade_model->get($dep['iddepartamento']);
        foreach ($cursos as $curso) {
            $drop_curso[$curso['idcurso_faculdade']] = $curso['nome_curso'];
        }
        $data['curso_monitor'] = $drop_curso;
        $data['monitor'] = 'professor';
        $data['funcao_monitor'] = 'Professor-Monitor';
        
        $data['cidades'] = $city;
        
        $this->template->show('novomonitor', $data);
     }
     public function buscaCursos(){
        $dep = $this->input->post('dep');
        $this->load->model('cursofaculdade_model');
        $cursos = $this->cursofaculdade_model->get($dep);
        foreach ($cursos as $curso) {
            $drop_curso[$curso['idcurso_faculdade']] = $curso['nome_curso'];
        }
        $data['curso_monitor'] = $drop_curso;

        echo json_encode($data);
     }

     public function cria(){
          
         // carrega o cabeçalho com o título 'Cadastro de Monitor'
        $data = $this->template->loadCabecalho('Cadastro de Monitor');
        
        $this->load->model('monitor_model');

        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        if ($this->input->post('id')){
            $this->form_validation->set_rules('email_usuario','Email','required|not_contains[usuario.email_usuario,usuario.idusuario !=,#'.$this->input->post('id').']|valid_email');
            $this->form_validation->set_rules('cpf_usuario','CPF','required|not_contains[usuario.cpf_usuario,usuario.idusuario !=,#'.$this->input->post('id').']|exact_length[14]|valid_cpf|regex_match[/\d{3}\.\d{3}\.\d{3}\-\d{2}$/]');
            $this->form_validation->set_rules('id','id','contains[monitor.usuario_idusuario]');
            
            //CARREGAR VALIDAÇÕES EXTRAS
            
            $this->form_validation->set_rules('data_admissao_monitor','Data de Admissão',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_age[0]','valid_date'));
            $this->form_validation->set_rules('ano_curso_monitor','Ano de Ingresso no Curso','required|is_natural|exact_length[4]');
            //verificar se data é maior que data de inicio
            if ($sql_data = $this->template->loadUsuario()){
                /********************************** ATUALIZA O USUÁRIO **********************************/
                $this->user_model->update($this->input->post('id'),$sql_data);
                
                $sql_monitor = array(
                    'monitor_idcurso_faculdade' =>  $this->input->post('curso_monitor'),
                    'ano_ingresso_curso' => $this->input->post('ano_curso_monitor'),
                    'periodo_curso'  => $this->input->post('periodo_curso_monitor'),
                    'ra_monitor' => $this->input->post('ra_monitor'),
                    'banco_monitor' => $this->input->post('banco_monitor'),
                    'data_admissao_monitor' => DateTime::createFromFormat('d/m/Y', $this->input->post('data_admissao_monitor'))->format('Y-m-d'),
                    'agencia_monitor' => $this->input->post('agencia_monitor'),
                    'conta_monitor' => $this->input->post('conta_monitor'),
                    'desligamento_data_monitor' => NULL,
                    'desligamento_observacoes_monitor' => NULL
                    );
                /********************************** ATUALIZA COORDENADOR **********************************/
       
                $this->monitor_model->update($this->input->post('id'),$sql_monitor); 
                
                $this->template->redirect('usuario/busca_funcionario/2');
                return;
            }
        }
        else{
            //VERIFICA SE O USUARIO ESTÁ SENDO REATIVADO. 
            $this->form_validation->set_rules('email_usuario','Email','required|valid_email|not_contains[usuario.email_usuario,usuario.cpf_usuario !=,cpf_usuario]');
            $this->form_validation->set_rules('cpf_usuario','CPF','required|contains[usuario.cpf_usuario,usuario.status,#0#]|exact_length[14]|valid_cpf|regex_match[/\d{3}\.\d{3}\.\d{3}\-\d{2}$/]');
            
            $verifUserInativo = $this->form_validation->run();
            //VERIFICA SE O USUARIO ESTÁ SENDO CADASTRADO EM OUTRA UNIDADE.
            if (!$verifUserInativo){
                $this->form_validation->reset_validation();
                $this->form_validation->set_rules('email_usuario','Email','required|valid_email|not_contains[usuario.email_usuario,usuario.cpf_usuario !=,cpf_usuario]');
                $this->form_validation->set_rules('cpf_usuario','CPF','required|contains[usuario.cpf_usuario,usuario.status,#1#,unidade_usuario.usuario_idusuario,usuario.idusuario,unidade_usuario.unidade_idunidade !=,#'.$this->session->userdata('unidade').'#]|exact_length[14]|valid_cpf|regex_match[/\d{3}\.\d{3}\.\d{3}\-\d{2}$/]');
                
                $verifUserMaisUnidades = $this->form_validation->run();
            }
            //VERIFICA SE O USUÁRIO ESTÁ SENDO CADASTRADO PELA PRIMEIRA VEZ
            if (!$verifUserMaisUnidades && !$verifUserInativo){
                
                $this->form_validation->reset_validation();
                $this->form_validation->set_rules('email_usuario','Email','required|is_unique[usuario.email_usuario]|valid_email');
                $this->form_validation->set_rules('cpf_usuario','CPF','required|is_unique[usuario.cpf_usuario]|exact_length[14]|valid_cpf|regex_match[/\d{3}\.\d{3}\.\d{3}\-\d{2}$/]');
                        
                $verifUserNaoCadastrado = $this->form_validation->run();
            }
            
            //VALIDAÇÕES EXTRAS
            $this->form_validation->set_rules('data_admissao_monitor','Data de Admissão',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_age[0]','valid_date'));
            $this->form_validation->set_rules('ano_curso_monitor','Ano de Ingresso no Curso','required|is_natural|exact_length[4]');
            if ($sql_data = $this->template->loadUsuario()){
                $sql_monitor = array(
                    'monitor_idcurso_faculdade' =>  $this->input->post('curso_monitor'),
                    'ano_ingresso_curso' => $this->input->post('ano_curso_monitor'),
                    'periodo_curso'  => $this->input->post('periodo_curso_monitor'),
                    'ra_monitor' => $this->input->post('ra_monitor'),
                    'banco_monitor' => $this->input->post('banco_monitor'),
                    'data_admissao_monitor' => DateTime::createFromFormat('d/m/Y', $this->input->post('data_admissao_monitor'))->format('Y-m-d'),
                    'agencia_monitor' => $this->input->post('agencia_monitor'),
                    'conta_monitor' => $this->input->post('conta_monitor'),
                    'desligamento_data_monitor' => NULL,
                    'desligamento_observacoes_monitor' => NULL
                    );
                $sql_unidade= array(
                    'unidade_idunidade' => $this->session->userdata('unidade'),
                    'tipo_usuario' => 2,//pois é professor      
                    );
                              
                if ($verifUserMaisUnidades || $verifUserInativo){
                    
                    if ($verifUserInativo) 
                        $sql_data['senha_usuario'] = $this->template->gerar_senha();
                    /********************************** ATUALIZA O USUÁRIO **********************************/
                    $iduser = $this->user_model->updateCPF($sql_data);
                    
                    $sql_unidade['usuario_idusuario'] = $iduser;
                    $this->user_model->createUnidadeUsuario($sql_unidade);
                        
                    /********************************** ATUALIZA OU CRIA MONITOR **********************************/
                    
                    $monitor = $this->monitor_model->getMonitor($iduser);
                    
                    if(empty($monitor)) {
                        $sql_monitor['usuario_idusuario']=$iduser;
                        $this->monitor_model->create($sql_monitor); 
                    }
                    else {
                        $this->monitor_model->update($iduser,$sql_monitor);
                    }  
                    
                    if (!$verifUserMaisUnidades){
                        $this->load->library("MY_phpmailer");
                        
                        $this->my_phpmailer->enviarEmailCadastro($sql_data);
                    }
                    $this->template->redirect('usuario/busca_funcionario/2');
                    return;
                }
                else if ($verifUserNaoCadastrado){
                    $sql_data['senha_usuario'] = $this->template->gerar_senha();
                    /********************************** CRIA O USUÁRIO **********************************/
                    $iduser = $this->user_model->create($sql_data); 
                    
                    $sql_unidade['usuario_idusuario'] = $iduser;
                    $this->user_model->createUnidadeUsuario($sql_unidade);
            
                    /**************************************CRIA MONITOR **********************************/
           
                    $sql_monitor['usuario_idusuario']=$iduser;
                    $this->monitor_model->create($sql_monitor);       
                    
                    $this->load->library("MY_phpmailer");
            
                    $this->my_phpmailer->enviarEmailCadastro($sql_data);
                    
                    $this->template->redirect('usuario/busca_funcionario/2');
                    return;
                }
            }
            //echo validation_errors();
        }
        //USUÁRIO EXISTE E ENCONTRA-SE ATIVO OU HÁ ERROS DE VALIDAÇÃO
        // retorna os dados preenchidos para a página de cadastro
        
        $data['page_title'] = 'Novo Professor';
  
        $data['err'] = "O formulário possui erros de validação!";
        //carrega cidades e estados
                
        $this->load->model('cidade_model');
        $this->load->model('departamento_model');
        $this->load->model('faculdade_model');
        
        $estados = $this->estado_model->get();
        foreach ($estados as $estado) {
            $uf[$estado['id']] = $estado['uf'];
        }
        $data['estados'] = $uf;
        $city[0] ='Selecionar Estado'; 
        $data['cidades'] = $city ;
        
        $faculdades = $this->faculdade_model->getFaculdadeCidade($data['faculdade']['cidade_faculdade']);
        foreach ($faculdades as $faculdade) {
            $departamentos = $this->departamento_model->get($faculdade['idfaculdade']);
            foreach ($departamentos as $departamento) {
                $drop_dep[$departamento['iddepartamento']] = $departamento['nome_departamento'];
            }    
        }
        $data['departamento_monitor'] = $drop_dep;
        $data['monitor'] = 'professor';
        $data['funcao_monitor'] = $this->input->post('funcao_monitor');
        $data['periodo_curso_monitor'] = array('Matutino' => 'Matutino','Vespertino' => 'Vespertino','Noturno' => 'Noturno','Integral'=>'Integral', 'Vespertino/Noturno' => 'Vespertino/Noturno');        
        //volta para o template
        $this->template->show('novomonitor', $data);
        return;  
    } 
 }