<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Advertencia extends CI_Controller {

		function Advertencia() {
	        parent::__construct();
	        if(!$this->session->userdata('logged'))
	        redirect('login');
            $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
	    }

	   public function index()
	    {
        	$this->load->model('advertencia_model');
               
                
        	$data = $this->template->loadCabecalho('Advertências');    
        	if(isset($flag)) $data['msg'] = 'Sucesso';  
        	$data['view']  = 'coordenador';

        	$data['advertencias'] = $this->advertencia_model->getAdvertencias($data['user']['idusuario']);

	        $this->template->show('lista_advertencia', $data);
	    }
        
	    public function cria()
		{
			$this->load->model('advertencia_model');
            
            $data = $this->template->loadCabecalho('Nova Advertencia');
            
            $sql_advertencia =  array('usuario_idusuario' => $this->input->post('idusuario'),
                                      'pontos_advertencia' => $this->input->post('pontos_advertencia'),
                                      'descricao_advertencia' => $this->input->post('descricao_advertencia')
                                     );
            $this->load->helper('url');
            $this->load->helper('form');
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('data_advertencia','Data da Advertência',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_date')); 
            $this->form_validation->set_rules('idusuario','usuario','required|contains[usuario.idusuario]');  
            $this->form_validation->set_rules('pontos_advertencia','Pontos','required|is_natural');
            $this->form_validation->set_rules('descricao_advertencia','Descrição da Advertência','required');    
            
            if ($this->form_validation->run()){
                                             
                $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_advertencia'));
                $adv =  $date->format('Y-m-d');
                $sql_advertencia['data_advertencia'] = $adv;
                
                $this->advertencia_model->create($sql_advertencia);
            
                $this->template->redirect("advertencia");
            }
            else{
                $this->load->model('user_model');
                    
                //$idAdvertido = base64_decode($id);
                $idAdvertido = $id;
            
                $data = $this->template->loadCabecalho('Nova Aprovação');
                
                $data['idusuario'] = $idAdvertido;
            
                $advertido = $this->user_model->get($idAdvertido);
                $data['nome_usuario'] = $advertido['nome_usuario'];
                $data['err'] = "Ocorreram Problemas de Validação";
                $this->template->show('lista_advertencia_adm', $data);
            }
        
		}

        public function lista($id)
        {
            $this->load->model('advertencia_model');
            
            $data = $this->template->loadCabecalho('Advertências');    
            if(isset($flag)) $data['msg'] = 'Sucesso';  
            $data['view']  = 'coordenador';
            $data['idusuario'] = $id;
            $data['advertencias'] = $this->advertencia_model->getAdvertencias($id);

            $this->template->show('lista_advertencia_adm', $data);        	
        }
        
        public function remove($id)
        {
            $this->load->model('advertencia_model');
            
            $advertencia = $this->advertencia_model->get($id);
            $this->advertencia_model->delete($id);
            
            if (isset($advertencia)){
                $this->template->redirect('advertencia/lista/'.$advertencia['usuario_idusuario']);
            } 
            else{
                $this->template->redirect('inicio');
            } 
        }
        
        

}





 ?>