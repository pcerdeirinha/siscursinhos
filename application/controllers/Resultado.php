<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Resultado extends CI_Controller {
    
    function Resultado() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        //$this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }
    
    public function index()
    {
        $data = $this->template->loadCabecalho('Resultados');
        $this->load->model('resultado_model');
        $resultados = $this->resultado_model->get_resultados();
        foreach ($resultados as $resultado) {
            $data['resultados'][$resultado['idresultado']] = $resultado['resultado_descricao'];
        }
        $data['result_sel'] = key($data['resultados']);
        $filtros = $this->resultado_model->get_filtros($data['result_sel']);
        $data['filtros'][null]= "Selecione";
        foreach ($filtros as $filtro) {
            $data['filtros'][$filtro['idresultado_filtro']] = $filtro['resultado_filtro_descricao'];
        }
        $agrupamentos = $this->resultado_model->get_agrupamentos($data['result_sel']);
        $data['agrupamentos'][null]= "Selecione";
        foreach ($agrupamentos as $agrupamento) {
            $data['agrupamentos'][$agrupamento['idresultado_agrupamento']] = $agrupamento['resultado_agrupamento_descricao'];
        }
        $socios = $this->resultado_model->get_socios($data['result_sel']);
        $data['socios'][null]= "Selecione";
        foreach ($socios as $socio) {
            $data['socios'][$socio['idresultado_socio']] = $socio['resultado_socio_desc'];
        }
        
        $this->template->show('resultados',$data);
    }    

    public function obter_resultado()
    {
        $this->load->model('resultado_model');
        $id_resultado = $this->input->post('id_resultado');
        $id_filtro = $this->input->post('id_filtro');
        $id_agrupamento = $this->input->post('id_agrupamento');
        if ($this->input->post('id_socio'))
            $ids_socio[0] = $this->input->post('id_socio');
        //echo json_encode($id_agrupamento); return;
        
        $resultado = $this->resultado_model->gerar_resultado($id_resultado,$id_filtro,$id_agrupamento,$ids_socio, $this->session->userdata('unidade'), 2017 );
        echo json_encode($resultado);
        
    }
    
}
?>    