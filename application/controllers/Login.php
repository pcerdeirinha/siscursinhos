<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$data['page_title']  = "Login"; 
        $data['email'] = '';
        $data['password'] = '';
        $local = $this->session->flashdata('acesso_temp');
        $data['local'] = $local?$local:"inicio";
        
        if($this->session->userdata('logged'))
        {
            $this->template->redirect('inicio');
        }
            
        $this->template->show('login', $data);
	}
    
    
	public function validate(){
	    $this->load->model('user_model');
	    $captcha = "";
        if ($this->input->post('recaptcha'))
            $captcha = $this->input->post('recaptcha');
         if (!$captcha){
            echo "error1";
         }else{
             $secret = "6Lf9NiYUAAAAADtEQ-J8hiYqGsYwocB9HCaQydDl";
             $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$captcha), true);
             if ($response["success"]!=false){
        	    $result = $this->user_model->validate($this->input->post('email'),$this->input->post('password'));
        	    if($result) {
        	        $unidade_usuario = $this->user_model->unidades_usuario($result['idusuario']);
                    
                    if (count($unidade_usuario)==1){
        	           $this->session->set_userdata(array(
        	               'logged' => true,
        	               'user'  => $result['idusuario'],
        	               'level' => $unidade_usuario[0]['tipo_usuario'], //FIXO DURANTE TODA A EXECUÇÃO
        	               'view' => $unidade_usuario[0]['tipo_usuario'], // ALTERA DE ACORDO COM A VIEW
        	               'unidade' => $unidade_usuario[0]['unidade_idunidade']
        	           ));
        	           echo  'ok' ;
        	        }
                    else {
                        $this->session->set_userdata(array(
                           'logged' => true,
                           'user'  => $result['idusuario'],
                           ));
                        
                        $rows = '<table id="alunos" align="center" class="table table-hover table-bordered">';
                        foreach ($unidade_usuario as $unidade) {
                            $rows .= '<tr><td align="center"><a href="'.base_url().'index.php/login/unidade/'.$unidade['idunidade'].'" style="text-decoration: none""><div style="height:100%;width:100%">'.$unidade['nome_cursinho_unidade'].' - '.$unidade['cidade_unidade'].'</div></a></td></tr>';
                        }
                       $rows.='</table>';
                       echo $rows;
                       
                    }   
        	    } else {
        	        // Load View
        	        $data['page_title']  = "Login";
        	 
        	        $data['email'] = $this->input->post('email');
        	        $data['password'] = $this->input->post('password');
        	 
        	        $data['error'] = true;
        
        	        echo "error2";
        	    }
             }
             else{
                 echo "error3";
             }         	    
        }
	}
	
	//Deve ser deletado quando for para o servidor
	public function validate2(){
        $this->load->model('user_model');
        $result = $this->user_model->validate($this->input->post('email'),$this->input->post('password'));
        
        if($result) {
            $unidade_usuario = $this->user_model->unidades_usuario($result['idusuario']);
            if (count($unidade_usuario)==1){
               $this->session->set_userdata(array(
                   'logged' => true,
                   'user'  => $result['idusuario'],
                   'level' => $unidade_usuario[0]['tipo_usuario'], //FIXO DURANTE TODA A EXECUÇÃO
                   'view' => $unidade_usuario[0]['tipo_usuario'], // ALTERA DE ACORDO COM A VIEW
                   'unidade' => $unidade_usuario[0]['unidade_idunidade']
               ));
               echo  'ok' ;
            }
            else {
                $this->session->set_userdata(array(
                   'logged' => true,
                   'user'  => $result['idusuario'],
                   ));
                
                $rows = '<table id="alunos" align="center" class="table table-hover table-bordered">';
                foreach ($unidade_usuario as $unidade) {
                    $rows .= '<tr><td align="center"><a href="'.base_url().'index.php/login/unidade/'.$unidade['idunidade'].'" style="text-decoration: none""><div style="height:100%;width:100%">'.$unidade['nome_cursinho_unidade'].'</div></a></td></tr>';
                }
               $rows.='</table>';
                
                
               //$data['error'] = true;
               //$rows = $unidade_usuario[1]['nome_cursinho_unidade'];

               echo $rows;
               
            }
            
        } else {
            // Load View
            $data['page_title']  = "Login";
     
            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');
     
            $data['error'] = true;

            echo "error";
            
            //$this->template->show('login', $data);
        }
    }
    
	
    public function unidade($id_unidade)
     {
         $this->load->model('user_model');
         if ($unidade = $this->user_model->monitorNaUnidade($this->session->userdata('user'),$id_unidade)){
             $this->session->set_userdata(array('unidade' => $id_unidade,
                                                 'level' => $unidade['tipo_usuario'], //FIXO DURANTE TODA A EXECUÇÃO
                                                 'view' => $unidade['tipo_usuario'] // ALTERA DE ACORDO COM A VIEW
                                          ));
             $this->template->redirect('inicio');
         }
         else{
             $this->session->sess_destroy();
             $this->template->redirect('login');
         }
            
     }
    
    
    public function esquecisenha()
    {

        if($this->session->userdata('logged'))
        {
            $this->template->redirect('inicio');
        }
        $data['page_title']  = "Esqueci a Senha"; 
        $data['email'] = '';
            
        $this->template->show('esqueci_senha', $data);
    }
    
    public function enviar_senha()
    {
        if ($this->input->post('g-recaptcha-response'))
            $captcha = $this->input->post('g-recaptcha-response');
         if (!$captcha){
            $this->template->redirect('login');
         }else{
             $secret = "6Lf9NiYUAAAAADtEQ-J8hiYqGsYwocB9HCaQydDl";
             $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$captcha), true);
             if ($response["success"]!=false){
                $this->load->model('user_model');
                
                $this->load->helper('url');
                $this->load->helper('form');
                $this->load->library('form_validation'); 
                
                $this->form_validation->set_rules('email','Email','required|valid_email|contains[usuario.email_usuario]');
                if ($this->form_validation->run()){
                    $user = $this->user_model->getUserEmail($this->input->post('email'));
                    $user['senha_usuario'] = $this->template->gerar_senha();
                    $this->user_model->updateCpf($user); 
                    $this->load->library("MY_phpmailer");
                            
                    if ($this->my_phpmailer->enviarEmailEsqueci($user))
                        $this->template->redirect('login');                
                }
                else{
                    $data['page_title']  = "Esqueci a Senha"; 
                    $this->template->show('esqueci_senha',$data);
                }
            }   
        }       
    }
    
	public function logout(){
	    //$this->session->unset_userdata('logged');
        $this->session->sess_destroy();
	 
	    $this->template->redirect('login');
	}

}
?>
