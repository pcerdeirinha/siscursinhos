<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Unidade extends CI_Controller {
    
    function Unidade() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }
    
    public function opcoes(){
        $data = $this->template->loadCabecalho('Unidades');
        $this->template->show('unidade_opcoes', $data);
     }
     
     public function novo()
     {
        $data = $this->template->loadCabecalho('Nova Unidade');    
            
        $this->load->model('cidade_model'); 
        $this->load->model('faculdade_model'); 
        
        $cidades = $this->cidade_model->get(26);//Estado de São Paulo
        foreach ($cidades as $cidade) {
            $city[$cidade['nome']] = $cidade['nome'];
        }
        
        $faculdades = $this->faculdade_model->getFaculdades();
        foreach ($faculdades as $faculdade) {
            $fac[$faculdade['idfaculdade']] = $faculdade['nome_faculdade'].", ". $faculdade['cidade_faculdade'];
        }
        $data['faculdades'] = $fac;
        $data['cidades'] = $city;
        $data['nome_unidade'] = "";
        $this->template->show('novaunidade', $data);
        
     }

    public function edita($idunidade)
    {
        $data = $this->template->loadCabecalho('Edita Unidade');
        
        $this->load->model('cidade_model'); 
        $this->load->model('faculdade_model');
        $this->load->model('unidade_model');
        
        $data['id'] = $idunidade;
        $unidade = $this->unidade_model->get($idunidade);
        $data['nome_unidade'] = $unidade['nome_cursinho_unidade'];
        $data['cidade_unidade'] = $unidade['cidade_unidade'];
        $data['faculdade_unidade'] = $unidade['unidade_idfaculdade'];
        
        $cidades = $this->cidade_model->get(26);//Estado de São Paulo
        foreach ($cidades as $cidade) {
            $city[$cidade['nome']] = $cidade['nome'];
        }
        $data['cidades'] = $city;
        
        $faculdades = $this->faculdade_model->getFaculdades();
        foreach ($faculdades as $faculdade) {
            $fac[$faculdade['idfaculdade']] = $faculdade['nome_faculdade'].", ". $faculdade['cidade_faculdade'];
        }
        $data['faculdades'] = $fac;
        
        $this->template->show('novaunidade', $data);
    }

    public function cria()
    {
        $this->load->model('faculdade_model');
        $this->load->model('cidade_model');
        $this->load->model('unidade_model');
        
        
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $sql_data = array('nome_cursinho_unidade' => $this->input->post('nome_unidade'),
                          'cidade_unidade' => $this->input->post('cidade_unidade'),
                          'unidade_idfaculdade' => $this->input->post('faculdade_unidade')
                            );
                            
       if ($this->input->post('id')){ // UPDATE
           $this->form_validation->set_rules('nome_unidade','Nome','required');
           $this->form_validation->set_rules('cidade_unidade','Cidade','required|contains[cidade.nome,cidade.estado,#26#]');
           $this->form_validation->set_rules('faculdade_unidade','Faculdade','required|contains[faculdade.idfaculdade]');
           
           if ($this->form_validation->run()){
               $this->unidade_model->update($this->input->post('id'),$sql_data);
               $this->template->redirect('unidade/busca');
           }        
       }  
       else { // INSERT
           $this->form_validation->set_rules('nome_unidade','Nome','required');
           $this->form_validation->set_rules('cidade_unidade','Cidade','required|contains[cidade.nome,cidade.estado,#26#]');
           $this->form_validation->set_rules('faculdade_unidade','Faculdade','required|is_natural|contains[faculdade.idfaculdade]');
           
           if ($this->form_validation->run()){
               $this->load->model('user_model');
               $idunidade = $this->unidade_model->create($sql_data);
               $docentes = $this->user_model->getDocentesFaculdade($sql_data['unidade_idfaculdade']);
               $sql_unidade = array(
                                    'unidade_idunidade' => $idunidade,
                                    'tipo_usuario' => 5
                                  );
               foreach ($docentes as $docente) {
                   $sql_unidade['usuario_idusuario'] = $docente['idusuario'];
                   $this->user_model->createUnidadeUsuario($sql_unidade);
               }
               $this->template->redirect('unidade/busca');
           }
       }
       
       // Caso haja algum erro no formulário
       $data = $this->template->loadCabecalho('Nova Unidade');  
       $data['err'] = "O formulário possui erros de validação!"; 
            
       $this->load->model('cidade_model'); 
        
       $cidades = $this->cidade_model->get(26);//Estado de São Paulo
       foreach ($cidades as $cidade) {
            $city[$cidade['nome']] = $cidade['nome'];
       }
       $data['cidades'] = $city;
       
       $faculdades = $this->faculdade_model->getFaculdades();
        foreach ($faculdades as $faculdade) {
            $fac[$faculdade['idfaculdade']] = $faculdade['nome_faculdade'].", ". $faculdade['cidade_faculdade'];
        }
        $data['faculdades'] = $fac;
       
       
       $this->template->show('novaunidade', $data);                   
    }


    public function busca()
    {
        $data = $this->template->loadCabecalho('Lista de Unidade');
        $this->load->model('unidade_model');
        $this->load->model('faculdade_model');
        $unidades = $this->unidade_model->getUnidades();
        foreach ($unidades as $key => $unidade) {
            $faculdade = $this->faculdade_model->get($unidade['unidade_idfaculdade']);
            $unidades[$key]['nome_faculdade'] = $faculdade['nome_faculdade'];
        }
        $data['unidades']=$unidades;
        $this->template->show('lista_unidades', $data); 
    }
    
    public function remove($id){
        $this->load->model('unidade_model');
        $this->unidade_model->delete($id);
        $this->template->redirect('unidade/busca');
    }

    
}
?>
    