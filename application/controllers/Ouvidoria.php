<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ouvidoria extends CI_Controller {
	
	function Ouvidoria() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        	$this->template->redirect('login');
        //$this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }
	
	
	public function index()
	{
		
	}
	
	public function novo()
	{
		$data = $this->template->loadCabecalho('Nova Cobrança');
		$this->load->model('ouvidoria_model');
		$representantes = $this->ouvidoria_model->get_representantes($this->session->userdata('level'));
		foreach ($representantes as $representante) {
			$drop_representates[$representante['idrepresentante']] = $representante['descricao_representante'];
		}
		$data['representantes']= $drop_representates;
		
		$this->load->model('monitor_model');
		if ($this->session->userdata('level')>1){
			$monitores = $this->monitor_model->getMonitoresComOfertas($this->session->userdata('unidade'));
		}
		else{
			$monitores = $this->monitor_model->getMonitoresAluno($this->session->userdata('user'));
		}
		$drop_monitores[null] = "Todos";
		foreach ($monitores as $monitor) {
			$drop_monitores[$monitor['idusuario']] = $monitor['nome_usuario'];
		}
		$data['individuos']= $drop_monitores;
		
		
		$tipos = $this->ouvidoria_model->get_tipos();
		foreach ($tipos as $tipo) {
			$drop_tipos[$tipo['idtipo']] = $tipo['descricao_tipo'];
		}
		$data['tipos'] = $drop_tipos;
		
		$ambientes = $this->ouvidoria_model->get_ambientes($this->session->userdata('level'));
		$drop_ambientes[null] = 'Não desejo informar';
		foreach ($ambientes as $ambiente) {
			$drop_ambientes[$ambiente['idambiente']] = $ambiente['descricao_ambiente'];
		}
		$data['ambientes']= $drop_ambientes;
		
		$drop_assuntos[null] = 'Não desejo informar';
		$data['assuntos']= $drop_assuntos;
		
		
		$this->template->show('novacobranca',$data);	
	}
	
	public function get_assuntos(){
		$id_ambiente = $this->input->post('idambiente');
		$this->load->model('ouvidoria_model');
		
		$assuntos = $this->ouvidoria_model->get_assuntos($id_ambiente);
		$drop_assuntos[null] = 'Não desejo informar';
		foreach ($assuntos as $assunto) {
			$drop_assuntos[$assunto['idassunto']] = $assunto['descricao_assunto'];
		}
		$data['assuntos']= $drop_assuntos;
		echo json_encode($data);	
		
	}
	
	public function cria()
	{
		$this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
		$this->load->model('ouvidoria_model');
        if ($this->input->post('idmanifestacao'))
			$this->form_validation->set_rules('idmanifestacao','Manifestação','contains[ouvidoria_manifestacao.idmanifestacao,ouvidoria_manifestacao.usuario_idusuario_remetente,#'.$this->session->userdata('user').'#]');
       	$this->form_validation->set_rules('representantes_ouvidoria','Representantes','required|contains[ouvidoria_representante.idrepresentante]');
        if ($this->session->userdata('level')==1)
        	$this->form_validation->set_rules('individuos_ouvidoria','Indivíduo','contains[oferta_disciplina.monitor_idusuario,matricula.matricula_idturma,oferta_disciplina.idturma,matricula.matricula_idaluno,#'.$this->session->userdata('user').'#]');
		else
			$this->form_validation->set_rules('individuos_ouvidoria','Indivíduo','contains[unidade_usuario.usuario_idusuario,unidade_usuario.tipo_usuario >=,#2#,unidade_usuario.tipo_usuario <=,#4#,unidade_usuario.unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
		$this->form_validation->set_rules('identificacao','Identificação','required|is_natural');
		$this->form_validation->set_rules('tipo_ouvidoria','Tipo de Manifestação','required|contains[ouvidoria_tipo.idtipo]');
		$this->form_validation->set_rules('assunto_ouvidoria','Assunto da Manifestação','contains[ouvidoria_assunto.idassunto,ouvidoria_ambiente.idambiente,ouvidoria_assunto.ouvidoria_ambiente_idambiente,ouvidoria_ambiente.nivel_minimo_ambiente <=,#'.$this->session->userdata('level').'#]');
		$this->form_validation->set_rules('descricao','Descrição da Manifestação','required|max_length[500]');
		if ($this->form_validation->run()){
			$sql_manifestacao = array(
				'descricao_manifestacao' => $this->template->encrypt($this->input->post('descricao')),
				'ouvidoria_representante_idrepresentante' => $this->input->post('representantes_ouvidoria'),
				'ouvidoria_assunto_idassunto' => $this->input->post('assunto_ouvidoria'),
				'usuario_idusuario_destinatario' => $this->input->post('individuos_ouvidoria'),
				'usuario_idusuario_remetente' => $this->session->userdata('user'),
				'ouvidoria_tipo_idtipo' => $this->input->post('tipo_ouvidoria')
			);
			if($this->input->post('identificacao')<2)
				$sql_manifestacao['nivel_usuario'] = $this->session->userdata('level');
			if($this->input->post('identificacao')>0)
				$sql_manifestacao['anonimo_manifestacao'] = 1;
			if (!$this->input->post('idmanifestacao')){
				$this->load->library("MY_phpmailer");
                    
				$sql_manifestacao['data_criacao_manifestacao'] = date('Y-m-d H:i:s');
				$this->ouvidoria_model->create_manifestacao($sql_manifestacao);
				
				if($sql_manifestacao['ouvidoria_representante_idrepresentante']==1){
					$this->load->model('monitor_model');
					$destinatarios = $this->monitor_model->getCoordenadoresUnidade($this->session->userdata('unidade'));
				}
				else if ($sql_manifestacao['ouvidoria_representante_idrepresentante']==2){
					$this->load->model('docente_model');
					$destinatarios = $this->docente_model->getDocentesUnidade($this->session->userdata('unidade'));
				}
				else if ($sql_manifestacao['ouvidoria_representante_idrepresentante']==4){
					$this->load->model('user_model');
					$destinatarios = $this->user_model->getAllSupervisores();
				}
				else if ($sql_manifestacao['ouvidoria_representante_idrepresentante']==5){
					$this->load->model('monitor_model');
					if ($sql_manifestacao['usuario_idusuario_destinatario']==null){
						if($this->session->userdata('level')==1)
							$destinatarios = $this->monitor_model->getMonitoresAluno($this->session->userdata('user'));
						else
							$destinatarios = $this->monitor_model->getMonitoresComOfertas($this->session->userdata('unidade'));
							
					}
					else{
						$this->load->model('user_model');
						$destinatarios[0] = $this->user_model->get($sql_manifestacao['usuario_idusuario_destinatario']);
					}
				}
				foreach ($destinatarios as $destinatario) 
					$this->my_phpmailer->enviarEmailOuvidoria($destinatario);
                
				$this->template->redirect('inicio');
				
			}else{
				$sql_manifestacao['data_alteracao_manifestacao'] = date('Y-m-d H:i:s');
				$this->ouvidoria_model->update_manifestacao($this->input->post('idmanifestacao'),$sql_manifestacao);
				$this->template->redirect('inicio');
			}
		}
		else{
			$data=$this->template->loadCabecalho('Nova Manifestação'); 
			$data['err'] = "O formulário possui erros de validação!";
			
			$representantes = $this->ouvidoria_model->get_representantes($this->session->userdata('level'));
			foreach ($representantes as $representante) {
				$drop_representates[$representante['idrepresentante']] = $representante['descricao_representante'];
			}
			$data['representantes']= $drop_representates;
			
			$this->load->model('monitor_model');
			if ($this->session->userdata('level')>1){
				$monitores = $this->monitor_model->getMonitoresComOfertas($this->session->userdata('unidade'));
			}
			else{
				$monitores = $this->monitor_model->getMonitoresAluno($this->session->userdata('user'));
			}
			$drop_monitores[null] = "Todos";
			foreach ($monitores as $monitor) {
				$drop_monitores[$monitor['idusuario']] = $monitor['nome_usuario'];
			}
			$data['individuos']= $drop_monitores;
			
			
			$tipos = $this->ouvidoria_model->get_tipos();
			foreach ($tipos as $tipo) {
				$drop_tipos[$tipo['idtipo']] = $tipo['descricao_tipo'];
			}
			$data['tipos'] = $drop_tipos;
			
			$ambientes = $this->ouvidoria_model->get_ambientes($this->session->userdata('level'));
			$drop_ambientes[null] = 'Não desejo informar';
			foreach ($ambientes as $ambiente) {
				$drop_ambientes[$ambiente['idambiente']] = $ambiente['descricao_ambiente'];
			}
			$data['ambientes']= $drop_ambientes;
			
			$id_ambiente = $this->input->post('ambiente_ouvidoria');
			$assuntos = $this->ouvidoria_model->get_assuntos($id_ambiente);
			$drop_assuntos[null] = 'Não desejo informar';
			foreach ($assuntos as $assunto) {
				$drop_assuntos[$assunto['idassunto']] = $assunto['descricao_assunto'];
			}
			$data['assuntos']= $drop_assuntos;
			//echo (validation_errors());			
			$this->template->show('novacobranca',$data);
		}
		
	}
	
	public function lista()
	{
		$data = $this->template->loadCabecalho('Lista de Manifestações');
		$this->load->model('ouvidoria_model');
		$manifestacoes = $this->ouvidoria_model->get_manifestacoes_enviadas($this->session->userdata('user'));
		$data['manifestacoes'] = $manifestacoes;
		$this->template->show('lista_manifestacoes_enviados',$data);
	}
	
	public function ver($id_ouvidoria)
	{
		$data = $this->template->loadCabecalho('Visualizar Manifestação');
		$this->template->show('ouvidoria_resposta',$data);
	}
	
	
	public function edita($id_ouvidoria)
	{
		$data = $this->template->loadCabecalho('Editar Manifestação');
		$this->load->model('ouvidoria_model');
		
		$manifestacao = $this->ouvidoria_model->get_manifestacao($id_ouvidoria);
		if ($manifestacao['usuario_idusuario_remetente'] != $this->session->userdata('user'))
			$this->template->redirect('inicio');
		$data['idouvidoria'] = $id_ouvidoria;
		$data['representates_ouvidoria'] = $manifestacao['ouvidoria_representante_idrepresentante'];
		$data['individuos_ouvidoria'] = $manifestacao['usuario_idusuario_destinatario'];
		if($manifestacao['anonimo_manifestacao']==1 && $manifestacao['nivel_usuario']==NULL)
			$data['identificacao'] = 2;
		else if ($manifestacao['nivel_usuario']!=NULL)
			$data['identificacao'] = 1;
		else
			$data['identificacao'] = 0;
		
		$data['tipo_ouvidoria'] = $manifestacao['ouvidoria_tipo_idtipo'];
		$data['ambiente_ouvidorira'] = $manifestacao['ouvidoria_ambiente_idambiente'];
		$data['assunto_ouvidorira'] = $manifestacao['idassunto'];
		$data['descricao'] = $this->template->decrypt($manifestacao['descricao_manifestacao']);
		
		$representantes = $this->ouvidoria_model->get_representantes($this->session->userdata('level'));
		foreach ($representantes as $representante) {
			$drop_representates[$representante['idrepresentante']] = $representante['descricao_representante'];
		}
		$data['representantes']= $drop_representates;
		
		$this->load->model('monitor_model');
		if ($this->session->userdata('level')>1){
			$monitores = $this->monitor_model->getMonitoresComOfertas($this->session->userdata('unidade'));
		}
		else{
			$monitores = $this->monitor_model->getMonitoresAluno($this->session->userdata('user'));
		}
		$drop_monitores[null] = "Todos";
		foreach ($monitores as $monitor) {
			$drop_monitores[$monitor['idusuario']] = $monitor['nome_usuario'];
		}
		$data['individuos']= $drop_monitores;
		
		
		$tipos = $this->ouvidoria_model->get_tipos();
		foreach ($tipos as $tipo) {
			$drop_tipos[$tipo['idtipo']] = $tipo['descricao_tipo'];
		}
		$data['tipos'] = $drop_tipos;
		
		$ambientes = $this->ouvidoria_model->get_ambientes($this->session->userdata('level'));
		$drop_ambientes[null] = 'Não desejo informar';
		foreach ($ambientes as $ambiente) {
			$drop_ambientes[$ambiente['idambiente']] = $ambiente['descricao_ambiente'];
		}
		$data['ambientes']= $drop_ambientes;
		
		$assuntos = $this->ouvidoria_model->get_assuntos($manifestacao['ouvidoria_ambiente_idambiente']);
		$drop_assuntos[null] = 'Não desejo informar';
		foreach ($assuntos as $assunto) {
			$drop_assuntos[$assunto['idassunto']] = $assunto['descricao_assunto'];
		}
		$data['assuntos']= $drop_assuntos;
				
		$this->template->show('novacobranca',$data);	
	}
	
}
?>