<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Curso_faculdade extends CI_Controller {
    
    function Curso_faculdade() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }

     
     public function novo($idfaculdade)
     {
        $data = $this->template->loadCabecalho('Novo Curso');    
        $this->load->model('departamento_model'); 
        $this->load->model('cursofaculdade_model'); 
        $departamentos = $this->departamento_model->get($idfaculdade);
        foreach ($departamentos as $departamento) {
            $drop_dep[$departamento['iddepartamento']] = $departamento['nome_departamento'];
        }
        $data['departamentos'] = $drop_dep;
        $data['periodos'] = $this->cursofaculdade_model->getPeriodos();

        $this->template->show('novocurso_faculdade', $data);
        
     }

    public function cria()
    {
        $this->load->model('cursofaculdade_model');
        $this->load->model('departamento_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $sql_data = array('nome_curso' => $this->input->post('nome_curso'),
                          'curso_iddepartamento' => $this->input->post('departamento_curso')
                            );
       $periodos = $this->input->post('periodo');
       $this->form_validation->set_rules('nome_curso','Nome','required');
       $this->form_validation->set_rules('departamento_curso','Departamentos','required|contains[departamento.iddepartamento]');
       $this->form_validation->set_rules('periodo[]','Períodos','required|contains[periodo.idperiodo]');
       $dep = $this->departamento_model->getDep($this->input->post('departamento_curso'));
       if ($this->form_validation->run()){
               
           $sql_periodo['curso_faculdade_idcurso_faculdade'] = $this->cursofaculdade_model->create($sql_data);
           foreach ($periodos as $periodo) {
               $sql_periodo['periodo_idperiodo'] = $periodo;
               $this->cursofaculdade_model->createPeriodo($sql_periodo);
           }
           
           $this->template->redirect('curso_faculdade/busca/'.$dep['depto_idfaculdade']);
           
           
       }
       echo validation_errors();
       // Caso haja algum erro no formulário
        $data = $this->template->loadCabecalho('Novo Curso');   
        $data['err'] = "O formulário possui erros de validação!"; 

        $departamentos = $this->departamento_model->get($dep['depto_idfaculdade']);
        foreach ($departamentos as $departamento) {
            $drop_dep[$departamento['iddepartamento']] = $departamento['nome_departamento'];
        }
        $data['departamentos'] = $drop_dep;
        $data['periodos'] = $this->cursofaculdade_model->getPeriodos();

        $this->template->show('novocurso_faculdade', $data);              
    }


    public function busca($idfaculdade)
    {
        $data = $this->template->loadCabecalho('Lista de Cursos');
        $this->load->model('cursofaculdade_model');
        $data['cursos_faculdade'] = $this->cursofaculdade_model->getFaculdade($idfaculdade);
        $data['idfaculdade'] = $idfaculdade;
        $this->template->show('lista_cursos_faculdade', $data); 
    }
    
    public function remove($id){
    }
     
    
    
}
?>
    