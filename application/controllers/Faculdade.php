<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Faculdade extends CI_Controller {
    
    function Faculdade() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }
    
    public function opcoes(){
        $data = $this->template->loadCabecalho('Faculdades');
        $this->template->show('faculdade_opcoes', $data);
     }
     
     public function novo()
     {
        $data = $this->template->loadCabecalho('Nova Faculdade');    
            
        $this->load->model('cidade_model'); 
        
        $cidades = $this->cidade_model->get(26);//Estado de São Paulo
        foreach ($cidades as $cidade) {
            $city[$cidade['nome']] = $cidade['nome'];
        }
        $data['cidades'] = $city;
        $data['nome_faculdade'] = "";
        $this->template->show('novafaculdade', $data);
        
     }

    public function edita($idfaculdade)
    {
        $data = $this->template->loadCabecalho('Edita Faculdade');
        
        $this->load->model('cidade_model'); 
        $this->load->model('faculdade_model');
        
        $data['id'] = $idfaculdade;
        $faculdade = $this->faculdade_model->get($idfaculdade);
        $data['nome_faculdade'] = $faculdade['nome_faculdade'];
        $data['cidade_faculdade'] = $faculdade['cidade_faculdade'];
        
        $cidades = $this->cidade_model->get(26);//Estado de São Paulo
        foreach ($cidades as $cidade) {
            $city[$cidade['nome']] = $cidade['nome'];
        }
        $data['cidades'] = $city;
        $this->template->show('novafaculdade', $data);
    }

    public function cria()
    {
        $this->load->model('faculdade_model');
        $this->load->model('cidade_model');
        
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $sql_data = array('nome_faculdade' => $this->input->post('nome_faculdade'),
                          'cidade_faculdade' => $this->input->post('cidade_faculdade')
                            );
                            
       if ($this->input->post('id')){ // UPDATE
           $this->form_validation->set_rules('nome_faculdade','Nome','required|not_contains[faculdade.nome_faculdade,faculdade.idfaculdade !=,#'.$this->input->post('id').']');
           $this->form_validation->set_rules('cidade_faculdade','Cidade','required|contains[cidade.nome,cidade.estado,#26#]');
           
           if ($this->form_validation->run()){
               $this->faculdade_model->update($this->input->post('id'),$sql_data);
               $this->template->redirect('faculdade/busca');
           }        
       }  
       else { // INSERT
           $this->form_validation->set_rules('nome_faculdade','Nome','required|is_unique[faculdade.nome_faculdade]');
           $this->form_validation->set_rules('cidade_faculdade','Cidade','required|contains[cidade.nome,cidade.estado,#26#]');
           
           if ($this->form_validation->run()){
               
               $this->faculdade_model->create($sql_data);
               $this->template->redirect('faculdade/busca');
           }
       }
       
       // Caso haja algum erro no formulário
       $data = $this->template->loadCabecalho('Nova Faculdade');  
       $data['err'] = "O formulário possui erros de validação!";
  
            
       $this->load->model('cidade_model'); 
        
       $cidades = $this->cidade_model->get(26);//Estado de São Paulo
       foreach ($cidades as $cidade) {
            $city[$cidade['nome']] = $cidade['nome'];
       }
       $data['cidades'] = $city;
       
       $this->template->show('novafaculdade', $data);                   
    }


    public function busca()
    {
        $data = $this->template->loadCabecalho('Lista de Faculdades');
        $this->load->model('faculdade_model');
        $data['faculdades'] = $this->faculdade_model->getFaculdades();
        $this->template->show('lista_faculdade', $data); 
    }
    
    public function remove($id){
        $this->load->model('faculdade_model');
        $this->faculdade_model->delete($id);
        $this->template->redirect('faculdade/busca');
    }
}
?>
    