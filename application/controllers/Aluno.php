

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Aluno extends CI_Controller {
	
	function Aluno() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        	$this->template->redirect('login');
         $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }
     public function index(){
        $data = $this->template->loadCabecalho('Aluno');
        $this->session->set_userdata(array('view' => 1));
        $this->template->show('aluno',$data);
     }

     public function busca_aluno(){
         
        $data = $this->template->loadCabecalho('Busca de Alunos'); 
        $this->template->show('busca_aluno', $data);
     }

    public function busca_aprovacao(){ 
        $data=$this->template->loadCabecalho("Busca de Alunos para Aprovação");         
        $this->template->show('busca_aprovacao', $data);
     }

    public function edita($idaluno){
        $data= $this->template->loadCabecalho('Edita Aluno');
        
        $data['id'] = $idaluno;
        $this->load->model('user_model');
        $this->load->model('faculdade_model');
        $this->load->model('estado_model');
        $this->load->model('departamento_model');
        $this->load->model('cursofaculdade_model');
        $this->load->model('socio_model');
        $this->load->model('faculdade_vestibular_model');
                // Dados do Aluno
        $this->load->model('aluno_model');
        $aluno = $this->aluno_model->getAluno($idaluno);
        //Dados do usuário
        $userAlu = $this->user_model->get($idaluno);
        if (isset ($userAlu['data_nascimento_usuario']) && $userAlu['data_nascimento_usuario']!=''){
            $date = DateTime::createFromFormat('Y-m-d', $userAlu['data_nascimento_usuario']);
            $nasc =  $date->format('d/m/Y'); 
        }
        else $nasc= ''; 
        //Valores dos Dados Gerais
        $data['nome_usuario'] = $userAlu['nome_usuario'];
        $data['nome_social_usuario'] = $userAlu['nome_social_usuario'];
        $data['email_usuario'] = $userAlu['email_usuario'];
        $data['rg_usuario'] = $userAlu['rg_usuario'];
        $data['cpf_usuario'] = $userAlu['cpf_usuario'];
        $data['data_nascimento_usuario'] = $nasc; 
        $data['telefone_usuario'] = $userAlu['telefone_usuario'];
        $data['celular_usuario'] = $userAlu['celular_usuario'];
        //Valores de endereço
        $data['logradouro_usuario'] = $userAlu['logradouro_usuario'];
        $data['numero_usuario'] = $userAlu['numero_usuario'];
        $data['bairro_usuario'] = $userAlu['bairro_usuario'];
        $data['complemento_endereco_usuario'] = $userAlu['complemento_endereco_usuario'];
        $data['estado_sel'] = $this->estado_model->getID($userAlu['estado_usuario']);
        $data['cidade_sel'] = $userAlu['cidade_usuario'];
        $data['cep_usuario'] = $userAlu['cep_usuario'];
        
        //$data['ano_conclusao_em'] = $aluno['ano_conclusao_em'];
        
        $_POST = $aluno;
        /*
        $data['vestibular'] = array_column($this->socio_model->obterPretensao($idaluno),'faculdade_vestibular_idfaculdade');
        $data['info_atual'] = array_column($this->socio_model->obterAtualidade($idaluno),'atualidade_idatualidade');
        $data['habito_cultural'] =  array_column($this->socio_model->obterHabito($idaluno),'habito_idhabito');
        
        */
        $this->load->model('estado_model');
        //Dropdown de estados
        $estados = $this->estado_model->get();
        foreach ($estados as $estado) {
            $uf[$estado['id']] = $estado['uf'];
        }
        $data['estados'] = $uf;
        //Dropdown de cidades        
        $this->load->model('cidade_model');
        $cidades = $this->cidade_model->get($userAlu['estado_usuario']);
        foreach ($cidades as $cidade) {
            $city[$cidade['nome']] = $cidade['nome'];
        }
        
        $data['cidades'] = $city;
        /*
        $res = $this->socio_model->getAll();
        foreach ($res['etnia'] as $re) {
            $data['etnia'][$re['idsocio_etnia']]=$re['descricao_etnia'];
        }
        $data['etnia'][null] = "Prefiro não declarar";
        foreach ($res['ensino'] as $re) {
            $data['ensino'][$re['idsocio_ensino']]=$re['descricao_ensino'];
        }
        foreach ($res['escolaridade'] as $re) {
            $data['escolaridade'][$re['idsocio_escolaridade_resp']]=$re['descricao_escolaridade'];
        }
        $data['remunerada'][null]='Não';
        foreach ($res['remunerada'] as $re) {
            $data['remunerada'][$re['idsocio_atividade_remunerada']]='Sim, '.$re['descricao_atividade_remunerada'];
        }
        $data['habito_cult'] = $res['habito_cult'];
        $data['info_atualidades'] = $res['info_atualidades'];
        foreach ($res['vez'] as $re) {
            $data['vez'][$re['idsocio_vezes_cursinho']]=$re['descricao_vezes_cursinho'];
        }
        $data['conclusao'][date("Y")] = date("Y");
        $data['conclusao'][date("Y")-1] = date("Y")-1;
        $data['conclusao'][date("Y")-2] = date("Y")-2;
        $data['conclusao'][null] = 'Antes de '.(date("Y")-2);
        $data['renda']["0-1"] = "Menos que 1 salário mínimo";
        for ($i=1;$i<10;$i++){
            $data['renda']["$i-".($i+1)] = "De $i a ".($i+1)." salários mínimos";
        }
        $data['renda']["10-"]= "Mais de dez salários mínimos";
        $data['renda']["null"] = "Prefiro não declarar";
        
        $faculdades = $this->faculdade_vestibular_model->getFaculdades();
        
        $data['vestibulares'][null] = "Selecione";
        foreach ($faculdades as $faculdade) {
            $data['vestibulares'][$faculdade['idFaculdade']] = $faculdade['nomeFaculdade_vestibular'];
        } 
        
        unset($data['vestibulares'][1]);
        unset($data['vestibulares'][2]);
        unset($data['vestibulares'][3]);
        
        $data['tipos_faculdade'] = array('Estadual'=>'Estadual','Federal'=>'Federal','Municipal'=>'Municipal','Privada'=>'Privada');
        */
        $this->template->show('novoaluno', $data);
     }
    
     

     public function busca(){
        $this->load->model('unidade_model');
        $this->load->model('user_model');
        $id = $this->session->userdata('user');
        $usuario = $this->user_model->get($id);        
        $unidade = $this->session->userdata('unidade');        
        $cpf = $this->input->post('cpf');
        $nome = $this->input->post('nome');        
        if(empty($nome)&&(!empty($cpf))){//busca apenas com cpf
            $useralunos = $this->user_model->getUsuariosCPF($cpf,$unidade['idunidade'],1);
        }else if (empty($cpf)&&(!empty($nome))) {//busca apenas com o nome
            $useralunos = $this->user_model->getUsuariosNome($nome,$unidade['idunidade'],1);           
        }else if ((!empty($cpf))&&(!empty($nome))){//busca com ambos
            $useralunos = $this->user_model->getUsuariosNomeCPF($nome,$cpf,$unidade['idunidade'],1); 
        }else{//todos os alunos da unidade
            $useralunos = $this->user_model->getAlunos($unidade['idunidade']); 
        }
        if(!(empty($useralunos))){
            foreach ($useralunos as $aluno) {
                $alunos[$aluno['idusuario']] = base64_encode($aluno['idusuario']);
                $this->load->model('matricula_model');
                $matriculas = $this->matricula_model->getUltimaMatricula($aluno['idusuario']);
                $curso[$aluno['idusuario']] = '-----';
                $turma[$aluno['idusuario']] = '-----';
                if(empty($matriculas)){
                    $matricula[$aluno['idusuario']]  = 'Sem Matricula';
                }else{
                    $matricula_atual = $matriculas[0];//matricula atual
                    switch ($matricula_atual['status']) {
                        case 0:
                            $matricula[$aluno['idusuario']]  = 'Cancelada';
                            break;
                        case 1:
                            $this->load->model('turma_model');
                            $this->load->model('curso_model'); 
                            $turma_matricula = $this->turma_model->getTurma($matricula_atual['matricula_idturma']);
                            $curso_matricula = $this->curso_model->get($turma_matricula['curso_idcurso']);
                            $matricula[$aluno['idusuario']]  = 'Ativa';
                            $curso[$aluno['idusuario']] = $curso_matricula['nome_curso'];
                            $turma[$aluno['idusuario']] = $turma_matricula['nome_turma'];
                            break;
                        case 2:
                            $matricula[$aluno['idusuario']]  = 'Aprovação';
                            break;
                        case 3:
                            $matricula[$aluno['idusuario']]  = 'Desistência';
                            break;
                        case 4:
                            $matricula[$aluno['idusuario']]  = 'Abandono';
                            break;
                        default:
                            $matricula[$aluno['idusuario']]  = 'Cancelada';
                            break;
                    }                    
                }
            }
            $data['alunos'] = $alunos;
            $data['useralunos'] = $useralunos;
            $data['curso'] = $curso;
            $data['turma'] = $turma;
            $data['matricula'] = $matricula;
            $data['err'] = 'ok';
            echo json_encode($data); 
        }else{
            $data['err'] = 'Não foram encontrados alunos com estes dados!';
            echo json_encode($data);
        }
    }

     //Mostra Tela com opções para aluno (listagem e criação)
     public function opcoes(){
         
        $data = $this->template->loadCabecalho('Cadastro de Alunos');
        $this->template->show('aluno_opcoes', $data);
     }

   
     public function update(){
          $this->load->model('user_model');

        //Confere se a senha é igual nos dois campos
        if($this->input->post('senha1') == $this->input->post('senha2')){

            $sql_data = array(
                'nome_usuario' => $this->input->post('nome_usuario'),
                'nome_social_usuario' => $this->input->post('nome_social_usuario'),
                'senha_usuario' => $this->input->post('senha1'),
                'rg_usuario' => $this->input->post('rg_usuario'),
                'cpf_usuario' => $this->input->post('cpf_usuario'),
                'data_nascimento_usuario' => $this->input->post('data_nascimento_usuario'),
                'logradouro_usuario' => $this->input->post('logradouro_usuario'),
                'bairro_usuario' => $this->input->post('bairro_usuario'),
                'complemento_endereco_usuario' => $this->input->post('complemento_endereco_usuario'),
                'cep_usuario' => $this->input->post('cep_usuario'),
                'cidade_usuario' => $this->input->post('cidade_usuario'),
                'estado_usuario' => $this->input->post('estado_usuario'),
                'email_usuario' => $this->input->post('email_usuario'),
                'telefone_usuario' => $this->input->post('telefone_usuario'),
                'celular_usuario' => $this->input->post('celular_usuario')
            );
            $this->user_model->update($this->input->post('idusuario'),$sql_data);
            $this->template->redirect('inicio');
        }else{
                
            $data = $this->template->loadCabecalho('Atualiza');
            $this->load->model('unidade_model');
            $this->load->model('user_model');

            //Dados do usuário logado
            $data['senha1'] = $this->input->post('senha1');
            $data['senha2'] = $this->input->post('senha2');
            //mostra um erro
            $data['err'] = "OS DOIS CAMPOS DEVEM SER IGUAIS";
            //volta para o template
            $this->template->show('trocasenha', $data);
        }
        
     }
     public function socio(){
        $data = $this->template->loadCabecalho('Questionário Socioeconômico');
        $this->load->model('socio_model');
        $this->load->model('faculdade_vestibular_model');
        
        $res = $this->socio_model->getAll();
        $data['etnia'][-1]='Selecione';
        foreach ($res['etnia'] as $re) {
            $data['etnia'][$re['idsocio_etnia']]=$re['descricao_etnia'];
        }
        $data['etnia'][null] = "Prefiro não declarar";
        $data['ensino'][-1]='Selecione';
        foreach ($res['ensino'] as $re) {
            $data['ensino'][$re['idsocio_ensino']]=$re['descricao_ensino'];
        }
        $data['escolaridade'][-1]='Selecione';
        foreach ($res['escolaridade'] as $re) {
            $data['escolaridade'][$re['idsocio_escolaridade_resp']]=$re['descricao_escolaridade'];
        }
        $data['remunerada'][-1]='Selecione';
        $data['remunerada'][null]='Não';
        foreach ($res['remunerada'] as $re) {
            $data['remunerada'][$re['idsocio_atividade_remunerada']]='Sim, '.$re['descricao_atividade_remunerada'];
        }
        $data['habito_cult'] = $res['habito_cult'];
        $data['info_atualidades'] = $res['info_atualidades'];
        $data['vez'][-1]='Selecione';
        foreach ($res['vez'] as $re) {
            $data['vez'][$re['idsocio_vezes_cursinho']]=$re['descricao_vezes_cursinho'];
        }
        $data['conclusao'][-1]='Selecione';
        $data['conclusao'][date("Y")] = date("Y");
        $data['conclusao'][date("Y")-1] = date("Y")-1;
        $data['conclusao'][date("Y")-2] = date("Y")-2;
        $data['conclusao'][null] = 'Antes de '.(date("Y")-2);
        $data['renda'][-1]='Selecione';
        $data['renda']["0-1"] = "Menos que 1 salário mínimo";
        
        for ($i=1;$i<10;$i++){
            $data['renda']["$i-".($i+1)] = "De $i a ".($i+1)." salários mínimos";
        }
        $data['renda']["10-"]= "Mais de dez salários mínimos";
        $data['renda'][null] = "Prefiro não declarar";
        
        $faculdades = $this->faculdade_vestibular_model->getFaculdades();
        
        $data['vestibulares'][null] = "Selecione";
        foreach ($faculdades as $faculdade) {
            $data['vestibulares'][$faculdade['idFaculdade']] = $faculdade['nomeFaculdade_vestibular'];
        } 
        
        unset($data['vestibulares'][1]);
        unset($data['vestibulares'][2]);
        unset($data['vestibulares'][3]);
        
        $data['tipos_faculdade'] = array('Estadual'=>'Estadual','Federal'=>'Federal','Municipal'=>'Municipal','Privada'=>'Privada');
        
        $this->template->show('questionario_socio', $data);//chama a view novoaluno, com o vetor de dados.
    
    }
    
    public function questionario()
    {
         $this->load->model('aluno_model');
         $this->load->model('socio_model');
            
        
         $this->load->helper('url');
         $this->load->helper('form');
         $this->load->library('form_validation');
  
         $sql_aluno = array(
            'sexo_aluno'=>$this->input->post('sexo_aluno'),
            'etnia_aluno'=>$this->input->post('etnia_aluno')==''?null:$this->input->post('etnia_aluno'),
            'realizacao_ensino_fund_aluno'=>$this->input->post('realizacao_ensino_fund_aluno'),
            'realizacao_ensino_medio_aluno'=>$this->input->post('realizacao_ensino_medio_aluno'),
            'vez_cursinho_aluno'=>$this->input->post('vez_cursinho_aluno'),
            'nome_cursinho_anterior_aluno'=>$this->input->post('nome_cursinho_anterior_aluno'),
            'atividade_remunerada_aluno'=>$this->input->post('atividade_remunerada_aluno')==''?null:$this->input->post('atividade_remunerada_aluno'),
            'concurso_aluno'=>$this->input->post('concurso'),
            'nome_concurso_aluno'=>$this->input->post('nome_concurso_aluno'),
            'escolaridade_resp1_aluno'=>$this->input->post('escolaridade_resp1_aluno'),
            'escolaridade_resp2_aluno'=>$this->input->post('escolaridade_resp2_aluno'),
            'info_atualidades_aluno'=>$this->input->post('info_atualidades_aluno'),
            'organizacao_cultura_aluno' => $this->input->post('organizacao_cultura_aluno'),
            'outro_sexo_aluno' => $this->input->post('outro_sexo_aluno'),
            'escola_ensino_medio' => $this->input->post('escola_ensino_medio'),
            'habitos_aluno' => $this->input->post('habitos_aluno'),
            'ano_conclusao_em' => $this->input->post('ano_conclusao_em')==''?null:$this->input->post('ano_conclusao_em'),
        );
        $salario = explode("-",$this->input->post('renda_familiar_aluno'));
        $sql_aluno['renda_minima_aluno'] = $salario[0]!='null'?$salario[0]:null;
        $sql_aluno['renda_maxima_aluno'] = $salario[1]!=''?$salario[1]:null;
        
         //CARREGAR VALIDAÇÕES 
        $this->form_validation->set_rules('ano_conclusao_em','Ano de Conclusão','is_natural|exact_length[4]');
        $this->form_validation->set_rules('sexo_aluno','Sexo','is_natural|greater_than[-1]|less_than[2]');
        $this->form_validation->set_rules('etnia_aluno','Etnia','contains[socio_etnia.idsocio_etnia]');
        $this->form_validation->set_rules('renda_familiar_aluno','Renda Familiar','regex_match[/\d\-\d$/]');
        $this->form_validation->set_rules('realizacao_ensino_fund_aluno','Realização do ensino fundamental','required|contains[socio_ensino.idsocio_ensino]');
        $this->form_validation->set_rules('realizacao_ensino_medio_aluno','Realização do ensino médio','required|contains[socio_ensino.idsocio_ensino]');
        $this->form_validation->set_rules('escola_ensino_medio','Realização do ensino médio','required');
        $this->form_validation->set_rules('vez_cursinho_aluno','Quanto ao cursinho pré-vestibular','required|contains[socio_vezes_cursinho.idsocio_vezes_cursinho]');
        $this->form_validation->set_rules('atividade_remunerada_aluno','Exerce atividade remunerada','contains[socio_atividade_remunerada.idsocio_atividade_remunerada]');
        $this->form_validation->set_rules('vestibular[]','Vestibular','contains[faculdade_vestibular.idFaculdade]');
        $this->form_validation->set_rules('vestibular_aluno','Vestibular','contains[faculdade_vestibular.idFaculdade]');
        $this->form_validation->set_rules('concurso','Pretende prestar outros concursos?','is_natural|less_than[2]');
        $this->form_validation->set_rules('escolaridade_resp1_aluno','Qual o grau de escolaridade da sua mãe?','required|contains[socio_escolaridade_resp.idsocio_escolaridade_resp]');
        $this->form_validation->set_rules('escolaridade_resp2_aluno','Qual o grau de escolaridade da seu pai?','required|contains[socio_escolaridade_resp.idsocio_escolaridade_resp]');
        $this->form_validation->set_rules('info_atual[]','Como você se informar sobre atualidades?','contains[socio_info_atualidades.idsocio_atualidades]');
        $this->form_validation->set_rules('habito_cultural[]','Quais são seus hábitos culturais mais frequentes?','contains[socio_habito.idsocio_habito]');
        if ($this->form_validation->run()==TRUE){

            $this->aluno_model->update($this->session->userdata('user'),$sql_aluno);
            $sql_atualidade['aluno_idusuario'] = $this->session->userdata('user');
            $atualidades = $this->input->post('info_atual[]');                    
            foreach ($atualidades as $atualidade) {
                $sql_atualidade['atualidade_idatualidade'] = $atualidade;
                $this->socio_model->criaAtualidade($sql_atualidade);
            }
            $sql_pretensao['aluno_idaluno'] = $this->session->userdata('user');
            $pretensoes = $this->input->post('vestibular[]');
            foreach ($pretensoes as  $pretensao) {
                $sql_pretensao['faculdade_vestibular_idfaculdade'] = $pretensao;
                $this->socio_model->criaPretensao($sql_pretensao);
            }
            if ($this->input->post('vestibular_aluno')){
                $sql_pretensao['faculdade_vestibular_idfaculdade'] = $this->input->post('vestibular_aluno');
                $this->socio_model->criaPretensao($sql_pretensao);
            }
            $sql_habito['aluno_idusuario'] = $this->session->userdata('user');
            $habitos = $this->input->post('habito_cultural[]');
            foreach ($habitos as $habito) {
                $sql_habito['habito_idhabito'] = $habito;
                $this->socio_model->criaHabito($sql_habito);
            }
            $this->template->redirect('inicio');
        }
        else{
            $data = $this->template->loadCabecalho('Questionário Socioeconômico');
            $this->load->model('faculdade_vestibular_model');
            
            $res = $this->socio_model->getAll();
            $data['etnia'][-1]='Selecione';
            foreach ($res['etnia'] as $re) {
                $data['etnia'][$re['idsocio_etnia']]=$re['descricao_etnia'];
            }
            $data['etnia'][null] = "Prefiro não declarar";
            $data['ensino'][-1]='Selecione';
            foreach ($res['ensino'] as $re) {
                $data['ensino'][$re['idsocio_ensino']]=$re['descricao_ensino'];
            }
            $data['escolaridade'][-1]='Selecione';
            foreach ($res['escolaridade'] as $re) {
                $data['escolaridade'][$re['idsocio_escolaridade_resp']]=$re['descricao_escolaridade'];
            }
            $data['remunerada'][-1]='Selecione';
            $data['remunerada'][null]='Não';
            foreach ($res['remunerada'] as $re) {
                $data['remunerada'][$re['idsocio_atividade_remunerada']]='Sim, '.$re['descricao_atividade_remunerada'];
            }
            $data['habito_cult'] = $res['habito_cult'];
            $data['info_atualidades'] = $res['info_atualidades'];
            $data['vez'][-1]='Selecione';
            foreach ($res['vez'] as $re) {
                $data['vez'][$re['idsocio_vezes_cursinho']]=$re['descricao_vezes_cursinho'];
            }
            $data['conclusao'][-1]='Selecione';
            $data['conclusao'][date("Y")] = date("Y");
            $data['conclusao'][date("Y")-1] = date("Y")-1;
            $data['conclusao'][date("Y")-2] = date("Y")-2;
            $data['conclusao'][null] = 'Antes de '.(date("Y")-2);
            $data['renda'][-1]='Selecione';
            $data['renda']["0-1"] = "Menos que 1 salário mínimo";
            
            for ($i=1;$i<10;$i++){
                $data['renda']["$i-".($i+1)] = "De $i a ".($i+1)." salários mínimos";
            }
            $data['renda']["10-"]= "Mais de dez salários mínimos";
            $data['renda'][null] = "Prefiro não declarar";
            
            $faculdades = $this->faculdade_vestibular_model->getFaculdades();
            
            $data['vestibulares'][null] = "Selecione";
            foreach ($faculdades as $faculdade) {
                $data['vestibulares'][$faculdade['idFaculdade']] = $faculdade['nomeFaculdade_vestibular'];
            } 
            
            unset($data['vestibulares'][1]);
            unset($data['vestibulares'][2]);
            unset($data['vestibulares'][3]);
            
            $data['tipos_faculdade'] = array('Estadual'=>'Estadual','Federal'=>'Federal','Municipal'=>'Municipal','Privada'=>'Privada');
            
            $this->template->show('questionario_socio', $data);//chama a view novoaluno, com o vetor de dados.
        }
}

     public function novo(){
        $data = $this->template->loadCabecalho('Novo Aluno');
        $this->load->model('socio_model');
        $this->load->model('faculdade_vestibular_model');
        
        //Dados do novo usuário 
        //vão em branco do controller para a view
        $data['unidade_idunidade'] = '';  
        $data['nome_usuario'] = '';
        $data['nome_social_usuario'] = '';
        $data['rg_usuario'] = '';
        $data['cpf_usuario'] = '';
        $data['data_nascimento_usuario'] = '';
        $data['logradouro_usuario'] = '';
        $data['numero_usuario'] = '';
        $data['bairro_usuario'] = '';
        $data['complemento_endereco_usuario'] = '';
        $data['cep_usuario'] = '';
        $data['cidade_usuario'] = '';
        $data['estado_usuario'] = '';
        $data['email_usuario'] = '';
        $data['telefone_usuario'] = '';
        $data['celular_usuario'] = '';

        $this->load->model('estado_model');
        $this->load->model('cidade_model');
        $estados = $this->estado_model->get();
        foreach ($estados as $estado) {
            $uf[$estado['id']] = $estado['uf'];
        }
        $data['estados'] = $uf;
        $city[0] ='Selecionar Estado'; 
        
        $data['cidades'] = $city ;        
        $this->template->show('novoaluno', $data);//chama a view novoaluno, com o vetor de dados.
     }

     

     public function cria(){
         // carrega o cabeçalho com o título 'Cadastro de Alunos'
        $data = $this->template->loadCabecalho('Cadastro de Alunos');
         
        $this->load->model('estado_model'); 
        $this->load->model('user_model');               
        $this->load->model('aluno_model');
        //$this->load->model('socio_model');
            
        
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $data['view']  = 'coordenador';
        
        //CARREGA OS DADOS
        

        $sql_data = array(
            'nome_usuario' => $this->input->post('nome_usuario'),
            'nome_social_usuario' => $this->input->post('nome_social_usuario')!=null?$this->input->post('nome_social_usuario'):NULL,
            'rg_usuario' => $this->input->post('rg_usuario'),
            'cpf_usuario' => $this->input->post('cpf_usuario'),
            'logradouro_usuario' => $this->input->post('logradouro_usuario'),
            'numero_usuario' => $this->input->post('numero_usuario'),
            'bairro_usuario' => $this->input->post('bairro_usuario'),
            'complemento_endereco_usuario' => $this->input->post('complemento_endereco_usuario'),
            'cep_usuario' => $this->input->post('cep_usuario'),
            'cidade_usuario' => $this->input->post('cidade_usuario'),
            'estado_usuario' => $this->estado_model->getUF($this->input->post('estado_usuario')),
            'email_usuario' => $this->input->post('email_usuario'),
            'telefone_usuario' => $this->input->post('telefone_usuario'),
            'celular_usuario' => $this->input->post('celular_usuario'),
            'status'=>1
        );
        
        $sql_unidade= array(
            'unidade_idunidade' => $this->session->userdata('unidade'),
            'tipo_usuario' => 1,//pois é aluno       
        );
        
        if (!$this->input->post('id')){
            $sql_data['senha_usuario'] = $this->template->gerar_senha();
        }
        
        if ($this->input->post('id')){
                
            $this->form_validation->set_rules('email_usuario','Email','required|not_contains[usuario.email_usuario,usuario.idusuario !=,#'.$this->input->post('id').']|valid_email');
            $this->form_validation->set_rules('cpf_usuario','CPF','required|not_contains[usuario.cpf_usuario,usuario.idusuario !=,#'.$this->input->post('id').']|exact_length[14]|valid_cpf|regex_match[/\d{3}\.\d{3}\.\d{3}\-\d{2}$/]');
            $this->form_validation->set_rules('id','id','contains[aluno.usuario_idusuario]');
            $this->template->validacaoUsuario();
            
            //CARREGAR VALIDAÇÕES EXTRAS
            if ($this->form_validation->run()==TRUE){
                
                $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_nascimento_usuario'));
                $nasc =  $date->format('Y-m-d');
                $sql_data['data_nascimento_usuario']=$nasc;
                
                /***************************************ATUALIZA USUÁRIO***********************************/
                
                $iduser = $this->user_model->updateCPF($sql_data); 
                
                /********************************** ATUALIZA ALUNO **********************************/
                /*
                $this->aluno_model->update($iduser,$sql_aluno);    
                
                $this->socio_model->limpaAtualidades($iduser);
                $sql_atualidade['aluno_idusuario'] = $iduser;
                $atualidades = $this->input->post('info_atual[]');                    
                foreach ($atualidades as $atualidade) {
                    $sql_atualidade['atualidade_idatualidade'] = $atualidade;
                    $this->socio_model->criaAtualidade($sql_atualidade);
                } 
                $this->socio_model->limpaPretensoes($iduser);       
                $sql_pretensao['aluno_idaluno'] = $iduser;
                $pretensoes = $this->input->post('vestibular[]');
                foreach ($pretensoes as  $pretensao) {
                    $sql_pretensao['faculdade_vestibular_idfaculdade'] = $pretensao;
                    $this->socio_model->criaPretensao($sql_pretensao);
                }
                if ($this->input->post('vestibular_aluno')){
                    $sql_pretensao['faculdade_vestibular_idfaculdade'] = $this->input->post('vestibular_aluno');
                    $this->socio_model->criaPretensao($sql_pretensao);
                }
                $this->socio_model->limpaHabitos($iduser);       
                $sql_habito['aluno_idusuario'] = $iduser;
                $habitos = $this->input->post('habito_cultural[]');
                foreach ($habitos as $habito) {
                    $sql_habito['habito_idhabito'] = $habito;
                    $this->socio_model->criaHabito($sql_habito);
                }
                */
                $data['msg'] = 'Aluno cadastrado com sucesso!';
                $this->template->show('aluno_opcoes', $data);
                return;
            }
        }
        else{ 
            
            $this->form_validation->set_rules('email_usuario','Email','required|valid_email');
            $this->form_validation->set_rules('cpf_usuario','CPF','required|contains[usuario.cpf_usuario,usuario.status,#0#,usuario.email_usuario,email_usuario]|exact_length[14]|valid_cpf|regex_match[/\d{3}\.\d{3}\.\d{3}\-\d{2}$/]');
            if ($this->form_validation->run()){
                
                $this->template->validacaoUsuario();
                
                if ($this->form_validation->run()==TRUE){
                    
                    $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_nascimento_usuario'));
                    $nasc =  $date->format('Y-m-d');
                    $sql_data['data_nascimento_usuario']=$nasc;
                    
                    /***************************************ATUALIZA USUÁRIO***********************************/
                    
                    $iduser = $this->user_model->updateCPF($sql_data); 
                    $sql_unidade['usuario_idusuario'] = $iduser;
                    $this->user_model->createUnidadeUsuario($sql_unidade);
                    
                    
                    /********************************** ATUALIZA OU CRIA ALUNO **********************************/
                    
                    $aluno = $this->aluno_model->getAluno($iduser);
                    
                    if(empty($aluno)) {
                        $sql_aluno['usuario_idusuario']=$iduser;
                        $this->aluno_model->create($sql_aluno);       
                    }
                    else {
                        //$this->aluno_model->update($iduser,$sql_aluno);
                        $this->socio_model->limpaAtualidades($iduser);
                        $this->socio_model->limpaPretensoes($iduser);
                        $this->socio_model->limpaHabitos($iduser);       
                    } 
                    /* 
                    $sql_atualidade['aluno_idusuario'] = $iduser;
                    $atualidades = $this->input->post('info_atual[]');                    
                    foreach ($atualidades as $atualidade) {
                        $sql_atualidade['atualidade_idatualidade'] = $atualidade;
                        $this->socio_model->criaAtualidade($sql_atualidade);
                    }
                    $sql_pretensao['aluno_idaluno'] = $iduser;
                    $pretensoes = $this->input->post('vestibular[]');
                    foreach ($pretensoes as  $pretensao) {
                        $sql_pretensao['faculdade_vestibular_idfaculdade'] = $pretensao;
                        $this->socio_model->criaPretensao($sql_pretensao);
                    }
                    if ($this->input->post('vestibular_aluno')){
                        $sql_pretensao['faculdade_vestibular_idfaculdade'] = $this->input->post('vestibular_aluno');
                        $this->socio_model->criaPretensao($sql_pretensao);
                    }
                    $sql_habito['aluno_idusuario'] = $iduser;
                    $habitos = $this->input->post('habito_cultural[]');
                    foreach ($habitos as $habito) {
                        $sql_habito['habito_idhabito'] = $habito;
                        $this->socio_model->criaHabito($sql_habito);
                    }*/
                    $this->load->library("MY_phpmailer");
                    
                    $this->my_phpmailer->enviarEmailCadastro($sql_data);
                    
                    $data['msg'] = 'Aluno cadastrado com sucesso!';
                    $this->template->show('aluno_opcoes', $data);
                    return;
                }
            }
            else{
                $this->form_validation->reset_validation();
                $this->form_validation->set_rules('email_usuario','Email','required|is_unique[usuario.email_usuario],valid_email');
                $this->form_validation->set_rules('cpf_usuario','CPF','required|contains[usuario.cpf_usuario,usuario.status,#0#]|exact_length[14]|valid_cpf|regex_match[/\d{3}\.\d{3}\.\d{3}\-\d{2}$/]');
                if ($this->form_validation->run()){
                    $this->template->validacaoUsuario();
                    
                    if ($this->form_validation->run()==TRUE){
                        
                        $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_nascimento_usuario'));
                        $nasc =  $date->format('Y-m-d');
                        $sql_data['data_nascimento_usuario']=$nasc;
                        
                         /***************************************ATUALIZA USUÁRIO***********************************/
                        
                        $iduser = $this->user_model->updateCPF($sql_data);
                        $sql_unidade['usuario_idusuario'] = $iduser;
                        $this->user_model->createUnidadeUsuario($sql_unidade); 
                        
                        /********************************** ATUALIZA OU CRIA ALUNO **********************************/
                        
                        $aluno = $this->aluno_model->getAluno($iduser);
                        
                        if(empty($aluno)) {
                            $sql_aluno['usuario_idusuario']=$iduser;
                            $this->aluno_model->create($sql_aluno);
                        }
                        else {
                            //$this->aluno_model->update($iduser,$sql_aluno);
                            $this->socio_model->limpaAtualidades($iduser);
                            $this->socio_model->limpaPretensoes($iduser);
                            $this->socio_model->limpaHabitos($iduser);       
                        }
                        /*
                        $sql_atualidade['aluno_idusuario'] = $iduser;
                        $atualidades = $this->input->post('info_atual[]');                    
                        foreach ($atualidades as $atualidade) {
                            $sql_atualidade['atualidade_idatualidade'] = $atualidade;
                            $this->socio_model->criaAtualidade($sql_atualidade);
                        } 
                        $sql_pretensao['aluno_idaluno'] = $iduser;
                        $pretensoes = $this->input->post('vestibular[]');
                        foreach ($pretensoes as  $pretensao) {
                            $sql_pretensao['faculdade_vestibular_idfaculdade'] = $pretensao;
                            $this->socio_model->criaPretensao($sql_pretensao);
                        }
                        if ($this->input->post('vestibular_aluno')){
                            $sql_pretensao['faculdade_vestibular_idfaculdade'] = $this->input->post('vestibular_aluno');
                            $this->socio_model->criaPretensao($sql_pretensao);
                        }
                        $sql_habito['aluno_idusuario'] = $iduser;
                        $habitos = $this->input->post('habito_cultural[]');
                        foreach ($habitos as $habito) {
                            $sql_habito['habito_idhabito'] = $habito;
                            $this->socio_model->criaHabito($sql_habito);
                        }*/
                        $this->load->library("MY_phpmailer");
                    
                        $this->my_phpmailer->enviarEmailCadastro($sql_data);
                        
                        $data['msg'] = 'Aluno cadastrado com sucesso!';
                        $this->template->show('aluno_opcoes', $data);
                        return;
                    }
                }
                else{
                    
                    $this->form_validation->reset_validation();
                    $this->form_validation->set_rules('email_usuario','Email','required|is_unique[usuario.email_usuario]|valid_email');
                    $this->form_validation->set_rules('cpf_usuario','CPF','required|is_unique[usuario.cpf_usuario]|exact_length[14]|valid_cpf|regex_match[/\d{3}\.\d{3}\.\d{3}\-\d{2}$/]');
                    if ($this->form_validation->run()){
                        
                        $this->template->validacaoUsuario();
                        //CASO USUÁRIO NÃO EXISTA
                        if ($this->form_validation->run()==TRUE){
                            
                            $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_nascimento_usuario'));
                            $nasc =  $date->format('Y-m-d');
                            $sql_data['data_nascimento_usuario']=$nasc;
                            
                            /***************************************CRIA USUÁRIO***********************************/
        
                            $iduser = $this->user_model->create($sql_data); 
                            $sql_unidade['usuario_idusuario'] = $iduser;
                            $this->user_model->createUnidadeUsuario($sql_unidade);
                            
                            /********************************** CRIA ALUNO **********************************/
                            $sql_aluno['usuario_idusuario']=$iduser;
                            $this->aluno_model->create($sql_aluno);
                            /*
                            $sql_atualidade['aluno_idusuario'] = $iduser;
                            $atualidades = $this->input->post('info_atual[]');                    
                            foreach ($atualidades as $atualidade) {
                                $sql_atualidade['atualidade_idatualidade'] = $atualidade;
                                $this->socio_model->criaAtualidade($sql_atualidade);
                            }
                            $sql_pretensao['aluno_idaluno'] = $iduser;
                            $pretensoes = $this->input->post('vestibular[]');
                            foreach ($pretensoes as  $pretensao) {
                                $sql_pretensao['faculdade_vestibular_idfaculdade'] = $pretensao;
                                $this->socio_model->criaPretensao($sql_pretensao);
                            }
                            if ($this->input->post('vestibular_aluno')){
                                $sql_pretensao['faculdade_vestibular_idfaculdade'] = $this->input->post('vestibular_aluno');
                                $this->socio_model->criaPretensao($sql_pretensao);
                            }
                            $sql_habito['aluno_idusuario'] = $iduser;
                            $habitos = $this->input->post('habito_cultural[]');
                            foreach ($habitos as $habito) {
                                $sql_habito['habito_idhabito'] = $habito;
                                $this->socio_model->criaHabito($sql_habito);
                            }*/
                            $this->load->library("MY_phpmailer");
                    
                            $this->my_phpmailer->enviarEmailCadastro($sql_data);
                           
                            $data['msg'] = 'Aluno cadastrado com sucesso!';
                            $this->template->show('aluno_opcoes', $data);
                            return;
                        }
                    } 
                }
            }
        }
        //USUÁRIO EXISTE E ENCONTRA-SE ATIVO
        // retorna os dados preenchidos para a página de cadastro
        
        $this->template->validacaoUsuario();
        
        /* carrega validações extras
        $this->form_validation->set_rules('ano_conclusao_em','Ano de Conclusão','is_natural|exact_length[4]');
        $this->form_validation->set_rules('sexo_aluno','Sexo','is_natural|less_than[2]');
        $this->form_validation->set_rules('etnia_aluno','Etnia','contains[socio_etnia.idsocio_etnia]');
        $this->form_validation->set_rules('realizacao_ensino_fund_aluno','Realização do ensino fundamental','required|contains[socio_ensino.idsocio_ensino]');
        $this->form_validation->set_rules('realizacao_ensino_medio_aluno','Realização do ensino médio','required|contains[socio_ensino.idsocio_ensino]');
        $this->form_validation->set_rules('escola_ensino_medio','Realização do ensino médio','required');
        $this->form_validation->set_rules('vez_cursinho_aluno','Quanto ao cursinho pré-vestibular','required|contains[socio_vezes_cursinho.idsocio_vezes_cursinho]');
        $this->form_validation->set_rules('atividade_remunerada_aluno','Exerce atividade remunerada','contains[socio_atividade_remunerada.idsocio_atividade_remunerada]');
        $this->form_validation->set_rules('vestibular[]','Vestibular','contains[faculdade_vestibular.idFaculdade]');
        $this->form_validation->set_rules('vestibular_aluno','Vestibular','contains[faculdade_vestibular.idFaculdade]');
        $this->form_validation->set_rules('concurso','Pretende prestar outros concursos?','is_natural|less_than[2]');
        $this->form_validation->set_rules('escolaridade_resp1_aluno','Qual o grau de escolaridade da sua mãe?','contains[socio_escolaridade_resp.idsocio_escolaridade_resp]');
        $this->form_validation->set_rules('escolaridade_resp2_aluno','Qual o grau de escolaridade da seu pai?','contains[socio_escolaridade_resp.idsocio_escolaridade_resp]');
        $this->form_validation->set_rules('info_atual[]','Como você se informar sobre atualidades?','contains[socio_info_atualidades.idsocio_atualidades]');
        $this->form_validation->set_rules('habito_cultural[]','Quais são seus hábitos culturais mais frequentes?','contains[socio_habito.idsocio_habito]');
         */       
        $this->form_validation->run();
        
        $data['page_title'] = 'Novo Aluno';
  
        //$data['escola_publica']=$this->input->post('escola');
        $data['err'] = "O formulário possui erros de validação!";
        //carrega cidades e estados
        //$this->load->model('socio_model');
        //$this->load->model('faculdade_vestibular_model');
        
        $this->load->model('cidade_model');
        
        $estados = $this->estado_model->get();
        foreach ($estados as $estado) {
            $uf[$estado['id']] = $estado['uf'];
        }
        $data['estados'] = $uf;
        $city[0] ='Selecionar Estado'; 
        $data['cidades'] = $city ;
        //$data['vestibular'] = $this->input->post('vestibular[]');
        //$data['info_atual'] = $this->input->post('info_atual[]');
        //$data['habito_cultural'] = $this->input->post('habito_cultural[]');
        /*
        $res = $this->socio_model->getAll();
        foreach ($res['etnia'] as $re) {
            $data['etnia'][$re['idsocio_etnia']]=$re['descricao_etnia'];
        }
        $data['etnia'][null] = "Prefiro não declarar";
        foreach ($res['ensino'] as $re) {
            $data['ensino'][$re['idsocio_ensino']]=$re['descricao_ensino'];
        }
        foreach ($res['escolaridade'] as $re) {
            $data['escolaridade'][$re['idsocio_escolaridade_resp']]=$re['descricao_escolaridade'];
        }
        $data['remunerada'][null]='Não';
        foreach ($res['remunerada'] as $re) {
            $data['remunerada'][$re['idsocio_atividade_remunerada']]='Sim, '.$re['descricao_atividade_remunerada'];
        }
        $data['habito_cult'] = $res['habito_cult'];
        $data['info_atualidades'] = $res['info_atualidades'];
        foreach ($res['vez'] as $re) {
            $data['vez'][$re['idsocio_vezes_cursinho']]=$re['descricao_vezes_cursinho'];
        }
        $data['conclusao'][date("Y")] = date("Y");
        $data['conclusao'][date("Y")-1] = date("Y")-1;
        $data['conclusao'][date("Y")-2] = date("Y")-2;
        $data['conclusao'][null] = 'Antes de '.(date("Y")-2);
        $data['renda']["0-1"] = "Menos que 1 salário mínimo";
        for ($i=1;$i<10;$i++){
            $data['renda']["$i-".($i+1)] = "De $i a ".($i+1)." salários mínimos";
        }
        $data['renda']["10-"]= "Mais de dez salários mínimos";
        $data['renda']["null"] = "Prefiro não declarar";
        
        $faculdades = $this->faculdade_vestibular_model->getFaculdades();
        
        foreach ($faculdades as $faculdade) {
            $data['vestibulares'][$faculdade['idFaculdade']] = $faculdade['nomeFaculdade_vestibular'];
        } 
        
        unset($data['vestibulares'][1]);
        unset($data['vestibulares'][2]);
        unset($data['vestibulares'][3]);
        
        $data['tipos_faculdade'] = array('Estadual'=>'Estadual','Federal'=>'Federal','Municipal'=>'Municipal','Privada'=>'Privada');
        */
        //volta para o template
        $this->template->show('novoaluno', $data);
        return;
    }


    public function matricula($id, $flag, $flag2){
			
		if ($this->session->userdata('erros')){
			$erros = $this->session->userdata('erros');
			
			foreach ($erros as $erro) {
				$data['err'] .= $erro;
			}
			
			$this->session->unset_userdata('erros');
		}
        $idusuario = base64_decode(urldecode($id));
        $idaluno = $idusuario;
        if(isset($flag)){
            switch ($flag) {
                case 'true':
                    $data['err'] = 'O aluno já está com uma matrícula ativa. Para Matricula-lo novamente, a matrícula que está ativa deve ser cancelada.';
                    break;
                case 'matriculado':                
                    $data['matriculado'] = 'Aluno matriculado com sucesso!';
                    if($flag2=='aluno'){ $data['caminho'] = 'index.php/aluno/busca_aluno';}
                    else{$data['caminho'] = 'index.php/matricula/matricularAlunos';}
                    break;
                case 'aluno':
                    $data['caminho'] = 'index.php/aluno/busca_aluno';
                    break;  
                case 'turma':
                    $data['err'] = 'Transfira o aluno para uma turma diferente da atual.';
                    break;              

                default:
                    # code...
                    break;
            }
        }else{
            $data['caminho'] = 'index.php/matricula/matricularAlunos';
        }       
        $data['page_title'] = 'Matricular Aluno';
        $this->load->model('unidade_model');
        $this->load->model('user_model');
        $this->load->model('faculdade_model');
        
        $id_data = $this->session->userdata('user');
                
        $usuario = $this->user_model->get($id_data);
        $data['user'] = $usuario;
        $unidade = $this->unidade_model->get($this->session->userdata('unidade'));
        $data['unidade'] = $unidade;       

        $faculdade = $this->faculdade_model->get($unidade['unidade_idfaculdade']);
        $data['faculdade'] = $faculdade;
        $nome_aluno = $this->user_model->get($idusuario);
        $data['info'] = $nome_aluno['nome_usuario'];
        $data['data_matricula']= date('d/m/Y');//data atual do servidor
        $data['data_cancelamento']= date('d/m/Y');//data atual do servidor
        // mostra as matrículas do aluno
        //seleciona os cursos da unidade
        $this->load->model('curso_model');
        $cursos = $this->curso_model->getAllCursos($unidade['idunidade']);

        foreach ($cursos as $curso) {            
            $nomes_cursos[$curso['idcurso']] = $curso['nome_curso'];
        }
        
        $data['nome_curso'] = $nomes_cursos;
        $data['cursos'] = $cursos;
        //seleciona as possives justificativas para caso houver cancelamento
        $this->load->model('justificativa_model');
        $justificativas = $this->justificativa_model->getJustMatricula();
        foreach ($justificativas as $jus ) {
            $justificativa[$jus['idjustificativa']] = $jus['descricao_justificativa'];           
        }

        $data['justificativa'] = $justificativa;

        //Join entre Turma e Curso da unidade para o dropdown de escolha da turma 
        $cursoturmas = $this->curso_model->cursoturma($unidade['idunidade']);
        foreach ($cursoturmas as $cursoturma) {            
            $turmacurso[$cursoturma['idturma']] = $cursoturma['curso_idcurso'];
            $date = date("Y",strtotime($cursoturma['data_inicio_turma']));            
            $turma[$cursoturma['idturma']] = $cursoturma['nome_turma'].' / '.$date;
        }

        $data['turmas'] = $turma;       
        $data['turmacurso'] = $turmacurso;
        $data['curso_sel'] = 0;
        
        //Select todas as matrículas
        $this->load->model('matricula_model');
        $this->load->model('turma_model');
        
        $data['matriculas'] = $this->matricula_model->getUltimaMatricula($idusuario);
        if (($data['matriculas'] != null)&&($data['matriculas'][0]['status']==1)){
            $data['nome_turma_sel'] = $data['matriculas'][0]['matricula_idturma'];
            $turma_atual = $this->turma_model->getTurma($data['nome_turma_sel']);
            $data['nome_curso_sel'] = $turma_atual['curso_idcurso'];
            $turmas_curso = $this->turma_model->get($turma_atual['curso_idcurso']); 
            foreach ($turmas_curso as $turma_curso) {
                $data['nome_turma'][$turma_curso['idturma']] = $turma_curso['nome_turma'];
            }
        } 
        $data['aluno'] = base64_encode($idaluno);
        $tipos  = array(0 => 'Cancelamento',
        				1 => 'Desligamento',
                        2 => 'Aprovação' ,
                        3 => 'Desistência',
                        4 => 'Abandono'
        );
        $data['tipos'] = $tipos;
        $this->template->show('matricula_aluno', $data); 
    }

    public function transferir($idmatricula){
       $this->load->model('matricula_model');
       $this->load->model('user_model');
       
       $this->load->helper('url');
       $this->load->helper('form');
       $this->load->library('form_validation');
       
       $this->form_validation->set_rules('id_matricula','matricula','required|valid_base64|base64_decode|contains[matricula.idmatricula,matricula.status,#1#,matricula.matricula_idturma !=,#'.$this->input->post('nome_turma').'#]');
       $this->form_validation->set_rules('nome_turma','Nome da Turma','required|contains[turma.idturma,turma.status,#1#,turma.curso_unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
       $this->form_validation->set_rules('data_matricula','Data de Transgerencia','required|valid_date');
       $matricula = $this->matricula_model->get(base64_decode($this->input->post('id_matricula')));
            
       if ($this->form_validation->run()){
            if ($this->user_model->userEstaNaUnidade($matricula['matricula_idaluno'],$this->session->userdata('unidade'))){
                $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_matricula'));
                $data_matricula =  $date->format('Y-m-d');
                
                $sql_data['data_trancamento'] = $data_matricula;
                $sql_data['status'] = 5;
                
                $this->matricula_model->update($this->input->post('id_matricula'), $sql_data);
                
                $matricula['data_matricula'] = $data_matricula;
                $matricula['matricula_idturma'] = $this->input->post('nome_turma');
                unset($matricula['idmatricula']);
                $this->matricula_model->create($matricula);
                $idaluno = base64_encode($matricula['matricula_idaluno']);
                $this->template->redirect('aluno/matricula/'.$idaluno.'/matriculado/aluno');
            }
            else $this->template->redirect('inicio');
       }
       else if ($matricula['matricula_idturma']==$this->input->post('nome_turma')){
            $this->template->redirect('aluno/matricula/'.base64_encode($matricula['matricula_idaluno']).'/turma');
       }              
       else $this->template->redirect('inicio');
    }

    public function matricular($id,$flag){
        $data['page_title'] = 'Matricular Aluno';
        $this->load->model('unidade_model');
        $this->load->model('user_model');
        $this->load->model('matricula_model');
        //Dados do usuário logado
        $id_data = $this->session->userdata('user');        
        $usuario = $this->user_model->get($id_data);
        $data['user'] = $usuario;
        $unidade = $this->unidade_model->get($this->session->userdata('unidade'));
        $data['unidade'] = $unidade;
        $idaluno = base64_decode($id);
        //Verifica se o aluno tem uma matrícula ativa
        if($this->matricula_model->matriculaAtiva($idaluno)){
            $idaluno = base64_encode($idaluno);            
            $this->template->redirect('aluno/matricula/'.$idaluno.'/true');
        }else{
            $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_matricula'));
            $data_matricula =  $date->format('Y-m-d');
            $sql_data = array(
                'data_matricula' =>$data_matricula,
                'data_trancamento' => NULL,
                'matricula_idaluno' => $idaluno,
                'matricula_idturma' => $this->input->post('nome_turma'),
                'status' => 1,
                'matricula_observacoes' => NUll
            );
            $this->matricula_model->create($sql_data);
            $idaluno = base64_encode($idaluno);
            if(isset($flag)){
                switch ($flag) {
                    case 'aluno':
                        $this->template->redirect('aluno/matricula/'.$idaluno.'/matriculado/aluno');
                        break;
                    
                    default:
                        # code...
                        break;
                }
            }else{
                $this->template->redirect('aluno/matricula/'.$idaluno.'/matriculado');
            }
            
        }
    }

    public function remove($id){
        //$idcurso = base64_decode(urldecode($id));
        $this->load->model('user_model');
        $sql_unidade['usuario_idusuario'] = $id;
        $sql_unidade['unidade_idunidade'] = $this->session->userdata('unidade');    
        $this->user_model->removeUnidadeUsuario($sql_unidade);
        if (count($this->user_model->unidades_usuario($id))==0)        
            $this->user_model->delete($id);
        $this->template->redirect('aluno/busca_aluno/true');
    }

    public function matricular_alunos($flag){
        $data = $this->template->loadCabecalho('Matrícula de Alunos ');
        if (isset($flag)) {
            switch ($flag) {
                case 'true':
                    $data['msg'] = 'Alunos matriculados com sucesso!';
                    break;
                case 'false':
                    $data['err'] = 'Erro ao matricular alunos! Certifique-se de não selecionar alunos já matriculados.';
                    break;
                default:
                    # code...
                    break;
            }
        }
        
       $this->load->model('curso_model');
       $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);
       foreach ($cursos as $curso) {            
           $dropcursos[$curso['idcurso']] = $curso['nome_curso'];
       }
       $data['data_matricula']= date('d/m/Y');//data atual do servidor
       $data['nome_curso'] = $dropcursos;
       $data['cursos'] = $cursos;
       $data['curso_sel'] = 0;

       $this->template->show('matricula_turma', $data);
    }

    public function matricularTurma(){
        $usuarios = $this->input->post('alunos');        
        $alunos = json_decode($usuarios);
        $this->load->model('matricula_model');
        foreach ($alunos as $aluno) { 
            if($this->matricula_model->matriculaAtiva($aluno->idusuario)){
               echo base_url().'index.php/aluno/matricular_alunos/false';
            }else{
                $date = DateTime::createFromFormat('d/m/Y', $this->input->post('datamatricula'));
                $data_matricula =  $date->format('Y-m-d');
                $sql_data = array(
                    'data_matricula' =>$data_matricula,
                    'data_trancamento' => NULL,
                    'matricula_idaluno' => $aluno->idusuario,
                    'matricula_idturma' => $this->input->post('turma'),
                    'status' => 1,
                    'matricula_observacoes' => NUll
                );
                $this->matricula_model->create($sql_data); 
            }
        }
        echo base_url().'index.php/aluno/matricular_alunos/true';
    }
 }