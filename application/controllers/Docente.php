<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Docente extends CI_Controller {
	
	function Docente() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }
    
    //criar o edita docente

     public function index(){
        $data = $this->template->loadCabecalho('Módulo Coordenador Docente');
        $this->session->set_userdata(array('view' => 5));
        $this->template->show('docente', $data);
     }    
     
     public function opcoes(){
        $data = $this->template->loadCabecalho('Novo Coordenador Docente');
        $this->template->show('docente_opcoes', $data);
     }

      public function novo(){
        $data = $this->template->loadCabecalho('Novo Coordenador Docente');

        $this->load->model('estado_model');
        $this->load->model('cidade_model');
        $this->load->model('departamento_model');

        $data['nome_usuario'] = '';
        $data['nome_social_usuario'] = '';
        $data['rg_usuario'] = '';
        $data['cpf_usuario'] = '';
        $data['data_nascimento_usuario'] = '';
        $data['logradouro_usuario'] = '';
        $data['bairro_usuario'] = '';
        $data['complemento_endereco_usuario'] = '';
        $data['cep_usuario'] = '';
        $data['cidade_usuario'] = '';
        $data['estado_usuario'] = '';
        $data['email_usuario'] = '';
        $data['telefone_usuario'] = '';
        $data['celular_usuario'] = '';
        $data['departamento_docente'] = '';
        $data['area_docente'] = '';

        $estados = $this->estado_model->get();
        
        if ($this->session->userdata('view')==6){//Apenas para Supervisores
            $this->load->model('faculdade_model');
            $faculdades = $this->faculdade_model->getFaculdades();
            foreach ($faculdades as $faculdade) {
                $drop_facul[$faculdade['idfaculdade']] = $faculdade['nome_faculdade'] .' - '.$faculdade['cidade_faculdade'];
            }
            $data['faculdades'] = $drop_facul;
        }
        
        foreach ($estados as $estado) {
            $uf[$estado['id']] = $estado['uf'];
        }
        $data['estados'] = $uf;
        $city[0] ='Selecionar Estado'; 
        //Dropdown de Departamentos da Faculdade 
        $departamentos = $this->departamento_model->get($data['faculdade']['idfaculdade']);
        foreach ($departamentos as $departamento) {
            $drop_dep[$departamento['iddepartamento']] = $departamento['nome_departamento'];
        }
        $data['departamento_docente'] = $drop_dep;
        $this->template->show('novodocente', $data);//chama a view novodocente, com o vetor de dados.
     }
    public function edita($iddocente){
        $data = $this->template->loadCabecalho('Edita Docente');

        $data['id'] = $iddocente;
        $this->load->model('estado_model');
        $this->load->model('departamento_model');
        $this->load->model('cursofaculdade_model');
        
        $id = $this->session->userdata('user');
       
        // Dados do Professor
        $this->load->model('docente_model');
        $docente = $this->docente_model->getDocente($iddocente);
        //Dados do usuário
        $userdocente = $this->user_model->get($iddocente);
        
        $date = DateTime::createFromFormat('Y-m-d', $userdocente['data_nascimento_usuario']);
        $nasc =  $date->format('d/m/Y'); 
        
        $cursofaculdade = $this->cursofaculdade_model->getCurso($coordenador['monitor_idcurso_faculdade']);

        //Valores dos Dados Gerais
        $data['nome_usuario'] = $userdocente['nome_usuario'];
        $data['nome_social_usuario'] = $userdocente['nome_social_usuario'];
        $data['email_usuario'] = $userdocente['email_usuario'];
        $data['rg_usuario'] = $userdocente['rg_usuario'];
        $data['cpf_usuario'] = $userdocente['cpf_usuario'];
        $data['data_nascimento_usuario'] = $nasc; 
        $data['telefone_usuario'] = $userdocente['telefone_usuario'];
        $data['celular_usuario'] = $userdocente['celular_usuario'];
        //Valores de endereço
        $data['logradouro_usuario'] = $userdocente['logradouro_usuario'];
        $data['numero_usuario'] = $userdocente['numero_usuario'];
        $data['bairro_usuario'] = $userdocente['bairro_usuario'];
        $data['complemento_endereco_usuario'] = $userdocente['complemento_endereco_usuario'];
        $data['estado_sel'] = $this->estado_model->getID($userdocente['estado_usuario']);
        $data['cidade_sel'] = $userdocente['cidade_usuario'];
        $data['cep_usuario'] = $userdocente['cep_usuario'];
        
        $data['departamento_docente'] = $docente['doc_iddepartamento'];
        $data['area_docente'] = $docente['area_docente'];
        
        if ($this->session->userdata('view')==6){//Apenas para Supervisores
            $this->load->model('faculdade_model');
            $faculdades = $this->faculdade_model->getFaculdades();
            foreach ($faculdades as $faculdade) {
                $drop_facul[$faculdade['idfaculdade']] = $faculdade['nome_faculdade'] .' - '.$faculdade['cidade_faculdade'];
            }
            $data['faculdades'] = $drop_facul;
            
                    
            $unidades = $this->user_model->unidades_usuario($data['id']);
            $data['nome_faculdade'] = $unidades[0]['unidade_idfaculdade'];
        }
        
        //Dropdown de estados
        $estados = $this->estado_model->get();
        foreach ($estados as $estado) {
            $uf[$estado['id']] = $estado['uf'];
        }
        $data['estados'] = $uf;
        //Dropdown de cidades        
        $this->load->model('cidade_model');
        $cidades = $this->cidade_model->get($data['estado_sel']);
        foreach ($cidades as $cidade) {
            $city[$cidade['nome']] = $cidade['nome'];
        }
        $data['cidades'] = $city;
        //Dropdown de Departamentos da Faculdade 
        $departamentos = $this->departamento_model->get($this->session->userdata('view')==6?$data['nome_faculdade']:$data['faculdade']['idfaculdade']);
        foreach ($departamentos as $departamento) {
            $drop_dep[$departamento['iddepartamento']] = $departamento['nome_departamento'];
        }
        $data['departamento_docente'] = $drop_dep;
        
        $this->template->show('novodocente', $data);
     }

    public function cria(){
          
         // carrega o cabeçalho com o título 'Cadastro de Monitor'
        $data = $this->template->loadCabecalho('Cadastro de Monitor');
        
        $this->load->model('docente_model');
        $this->load->model('unidade_model');

        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        if ($this->input->post('id')){
        
            $this->form_validation->set_rules('email_usuario','Email','required|not_contains[usuario.email_usuario,usuario.idusuario !=,#'.$this->input->post('id').']|valid_email');
            $this->form_validation->set_rules('cpf_usuario','CPF','required|not_contains[usuario.cpf_usuario,usuario.idusuario !=,#'.$this->input->post('id').']|exact_length[14]|valid_cpf|regex_match[/\d{3}\.\d{3}\.\d{3}\-\d{2}$/]');
            $this->form_validation->set_rules('id','id','contains[docente.usuario_idusuario]');
            
            if ($sql_data = $this->template->loadUsuario()){
                /********************************** ATUALIZA O USUÁRIO **********************************/
                $this->user_model->update($this->input->post('id'),$sql_data);
                
                $sql_docente = array(
                    'doc_iddepartamento' => $this->input->post('departamento_docente'),
                    'area_docente' => $this->input->post('area_docente')
                );
                /********************************** ATUALIZA DOCENTE **********************************/
       
                $this->docente_model->update($this->input->post('id'),$sql_docente); 
                $this->user_model->removeUnidadesUser($this->input->post('id'));
                
                $sql_unidade['usuario_idusuario'] = $this->input->post('id');
                $sql_unidade['tipo_usuario'] = 5;
                
                $unidades = $this->unidade_model->getUnidadesFaculdade($this->session->userdata('view')==6?$this->input->post('nome_faculdade'):$data['faculdade']['idfaculdade']);
                    
                foreach ($unidades as $unidade) {
                    $sql_unidade['unidade_idunidade'] = $unidade['idunidade'];
                    $this->user_model->createUnidadeUsuario($sql_unidade);
                }
                
                if ($this->session->userdata('view') == 6)
                    $this->template->redirect('docente/busca');
                else
                    $this->template->redirect('usuario/busca_funcionario/5');
                return;
            }
        }
        else{
            //VERIFICA SE O USUARIO ESTÁ SENDO REATIVADO. 
                
            $this->form_validation->set_rules('email_usuario','Email','required|valid_email|not_contains[usuario.email_usuario,usuario.cpf_usuario !=,cpf_usuario]');
            $this->form_validation->set_rules('cpf_usuario','CPF','required|contains[usuario.cpf_usuario,usuario.status,#0#]|exact_length[14]|valid_cpf|regex_match[/\d{3}\.\d{3}\.\d{3}\-\d{2}$/]');
            
            $verifUserInativo = $this->form_validation->run();
            
            //VERIFICA SE O USUÁRIO ESTÁ SENDO CADASTRADO PELA PRIMEIRA VEZ
            if (!$verifUserInativo){
                $this->form_validation->reset_validation();
                $this->form_validation->set_rules('email_usuario','Email','required|is_unique[usuario.email_usuario]|valid_email');
                $this->form_validation->set_rules('cpf_usuario','CPF','required|is_unique[usuario.cpf_usuario]|exact_length[14]|valid_cpf|regex_match[/\d{3}\.\d{3}\.\d{3}\-\d{2}$/]');
                        
                $verifUserNaoCadastrado = $this->form_validation->run();
            }
            
            if ($sql_data = $this->template->loadUsuario()){
                $sql_data['senha_usuario'] = $this->template->gerar_senha();
                $sql_docente = array(
                    'doc_iddepartamento' => $this->input->post('departamento_docente'),
                    'area_docente' => $this->input->post('area_docente')
                );
                if ($verifUserInativo){
                    
                    /********************************** ATUALIZA O USUÁRIO **********************************/
                    $iduser = $this->user_model->updateCPF($sql_data);
                    
                    $sql_unidade['usuario_idusuario'] = $iduser;
                    $sql_unidade['tipo_usuario'] = 5;
                    
                    /********************************** ATUALIZA OU CRIA DOCENTE **********************************/
                    
                    $docente = $this->docente_model->getDocente($iduser);
                    
                    if(empty($docente)) {
                        $sql_docente['usuario_idusuario']=$iduser;
                        $this->docente_model->create($sql_docente); 
                    }
                    else {
                        $this->docente_model->update($iduser,$sql_docente);
                    }  
                    
                    $unidades = $this->unidade_model->getUnidadesFaculdade($this->session->userdata('view')==6?$this->input->post('nome_faculdade'):$data['faculdade']['idfaculdade']);
                    
                    foreach ($unidades as $unidade) {
                        $sql_unidade['unidade_idunidade'] = $unidade['idunidade'];
                        $this->user_model->createUnidadeUsuario($sql_unidade);
                    }
                    
                    $this->load->library("MY_phpmailer");
                    
                    $this->my_phpmailer->enviarEmailCadastro($sql_data);
                    
                    if ($this->session->userdata('view') == 6)
                        $this->template->redirect('docente/busca');
                    else
                        $this->template->redirect('usuario/busca_funcionario/5');
                    return;
                }
                else if ($verifUserNaoCadastrado){
                    
                    /********************************** ATUALIZA O USUÁRIO **********************************/
                    $iduser = $this->user_model->create($sql_data);
                    
                    $sql_unidade['usuario_idusuario'] = $iduser;
                    $sql_unidade['tipo_usuario'] = 5;
                    
                    /********************************** ATUALIZA OU CRIA DOCENTE **********************************/
                    $sql_docente['usuario_idusuario']=$iduser;
                    $this->docente_model->create($sql_docente); 
                    
                    $unidades = $this->unidade_model->getUnidadesFaculdade($this->session->userdata('view')==6?$this->input->post('nome_faculdade'):$data['faculdade']['idfaculdade']);
                    
                    foreach ($unidades as $unidade) {
                        $sql_unidade['unidade_idunidade'] = $unidade['idunidade'];
                        $this->user_model->createUnidadeUsuario($sql_unidade);
                    }
                    
                    $this->load->library("MY_phpmailer");
                    
                    $this->my_phpmailer->enviarEmailCadastro($sql_data);
                    
                    if ($this->session->userdata('view') == 6)
                        $this->template->redirect('docente/busca');
                    else
                        $this->template->redirect('usuario/busca_funcionario/5');
                    return;
                }
            }
        }
        //USUÁRIO EXISTE E ENCONTRA-SE ATIVO OU HÁ ERROS DE VALIDAÇÃO
        // retorna os dados preenchidos para a página de cadastro
        $data['page_title'] = 'Novo Coordenado Docenter';
  
        $data['err'] = "O formulário possui erros de validação!";
        //carrega cidades e estados
                
        $this->load->model('cidade_model');
        $this->load->model('departamento_model');
        $this->load->model('faculdade_model');
        
        $estados = $this->estado_model->get();
        foreach ($estados as $estado) {
            $uf[$estado['id']] = $estado['uf'];
        }
        $data['estados'] = $uf;
        $data['cidades'] = $this->cidade_model->get($this->input->post('estado_usuario'));
        
        if ($this->session->userdata('view')==6){//Apenas para Supervisores
            $this->load->model('faculdade_model');
            $faculdades = $this->faculdade_model->getFaculdades();
            foreach ($faculdades as $faculdade) {
                $drop_facul[$faculdade['idfaculdade']] = $faculdade['nome_faculdade'] .' - '.$faculdade['cidade_faculdade'];
            }
            $data['faculdades'] = $drop_facul;
            
        }
        $departamentos = $this->departamento_model->get($this->session->userdata('view')==6?$this->input->post('nome_faculdade'):$data['faculdade']['idfaculdade']);
        foreach ($departamentos as $departamento) {
            $drop_dep[$departamento['iddepartamento']] = $departamento['nome_departamento'];
        }
        $data['departamento_docente'] = $drop_dep;
        $data['monitor'] = 'docente';
        
        //volta para o template
        $this->template->show('novodocente', $data);
        return;  
    }

     public function busca()
     {
        $data = $this->template->loadCabecalho('Lista de Coordenadores Docentes');
        $this->load->model('unidade_model');
        $this->load->model('user_model');
        $this->load->model('faculdade_model');
        $users = $this->user_model->getAllDocentes();
        foreach ($users as $key => $user) {
            $unidade = $this->unidade_model->get($user['unidade_idunidade']);
            $faculdade = $this->faculdade_model->get($unidade['unidade_idfaculdade']);
            $users[$key]['nome_faculdade'] = $faculdade['nome_faculdade'];
        }
        $data['docentes']=$users;
        $this->template->show('lista_docentes', $data);    
     }
     
 }