<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Curso extends CI_Controller {
    
    function Curso() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }

     public function index(){//mostra as opções de curso
        $data = $this->template->loadCabecalho('Opções de Curso');
        $this->template->show('curso_opcoes', $data);
     }  

     public function novo(){
        $data = $this->template->loadCabecalho('Nova Turma');
        
        //dados do curso
        $data['nome_curso'] = '';
        $ano = range(2000, 2099);
        foreach ($ano as $a) {
            $year[$a] = $a;
        }
        $semestres = range(1,24);
        foreach ($semestres as $s) {
            $sem[$s] = $s;
        }

        $data['ano_criacao'] = $year;
        $data['duracao'] = $sem;

        $this->template->show('novocurso', $data);//chama a view novocurso, com o vetor de dados.
     }
     public function cria(){
        $this->load->model('user_model');
        
        
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $data = $this->template->loadCabecalho('Cadastro de Turma');
        
        
        $data['view']  = 'coordenador';


         $sql_data = array(
            'unidade_idunidade' => $data['unidade']['idunidade'],
            'nome_curso' => $this->input->post('nome_curso'),
            'ano_criacao' => $this->input->post('ano_criacao'),
            'duracao' => $this->input->post('duracao')
        );
        
        $this->form_validation->set_rules('nome_curso','Nome do Curso','required');
        $this->form_validation->set_rules('ano_criacao','Ano de Criação','required|is_natural');
        $this->form_validation->set_rules('duracao','Duração','required|is_natural');
        

        if($this->form_validation->run()==FALSE){

            $data['page_title'] = 'Novo Curso';
            $data['err'] = "Todos os Campos devem ser preenchidos!";
            $ano = range(2000, 2099);
            foreach ($ano as $a) {
                $year[$a] = $a;
            }
           $semestres = range(1,24);
            foreach ($semestres as $s) {
                $sem[$s] = $s;
            }
    
            $data['ano_criacao'] = $year;
            $data['duracao'] = $sem;
                
    
            $this->template->show('novocurso', $data);//chama a view novocurso, com o vetor de dados.
        }
        else {
            $this->load->model('curso_model');
            if($this->input->post('idcurso')){//Update
    
                $this->curso_model->update($this->input->post('idcurso'),$sql_data);
                $this->lista(true);
            }else{//novo curso
                $this->curso_model->create($sql_data);
                $this->lista(true);
            }
        }
     }

     public function lista($flag){
        
        $data = $this->template->loadCabecalho('');
        $data['title'] = 'Cursos da unidade '.$data['unidade']['nome_cursinho_unidade'];   
        if(isset($flag)) $data['msg'] = 'Sucesso';  

        $this->load->model('curso_model');
        $data['cursos'] = $this->curso_model->getCursos($data['unidade']['idunidade']);

        $this->template->show('lista_curso', $data);
     }  

    public function edita($id){
        $idcurso = $id;
        $data = $this->template->loadCabecalho('Edita Curso');
        $this->load->model('curso_model');

        $curso = $this->curso_model->get($idcurso);

        $data['nome_curso'] = $curso['nome_curso'];
               $ano = range(2000, 2099);
        foreach ($ano as $a) {
            $year[$a] = $a;
        }
        $semestres = range(1,24);
        foreach ($semestres as $s) {
            $sem[$s] = $s;
        }

        $data['ano_criacao'] = $year;
        $data['duracao'] = $sem;
        
        $data['ano_sel'] =  $curso['ano_criacao'];
        $data['duracao_sel'] =  $curso['duracao'];
        
        
        $data['idcurso'] = $curso['idcurso'];

        $this->template->show('novocurso', $data);
    }

    public function remove($id){
        //$idcurso = base64_decode(urldecode($id));
        $this->load->model('curso_model');
        $this->curso_model->delete($id);
        $this->template->redirect('curso/lista/true');
    }
	
	public function obterDisciplinasCurso()
	{
		$this->load->model('disciplina_model');
		$this->load->model('area_model');
		$idcurso = $this->input->post('idCurso');
		
		$disciplinas = $this->disciplina_model->getDisciplinasCurso($idcurso);
		
		foreach ($disciplinas as $key=>$disciplina) {
			$area = $this->area_model->getArea($disciplina['area_disciplina']);
			$disciplinas[$key]['nome_area'] = $area['nome_area'];
			
		}
		
		$data['disciplinas'] = $disciplinas;
		
		echo json_encode($data);
		
	}

 }
