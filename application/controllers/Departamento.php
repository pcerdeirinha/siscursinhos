<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Departamento extends CI_Controller {
    
    function Departamento() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }
    
    public function lista($idfaculdade)
    {
        $data = $this->template->loadCabecalho('Lista de Departamentos');
        
        $this->load->model('departamento_model');
        $this->load->model('faculdade_model');
        
        $data['departamentos'] = $this->departamento_model->get($idfaculdade);
        $data['faculdade'] = $this->faculdade_model->get($idfaculdade);
        
        $this->template->show('lista_departamentos', $data);
    }
    
    public function cria()
    {
        $this->load->model('departamento_model');
        
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $sql_data = array('depto_idfaculdade' => $this->input->post('idfaculdade'),
                          'nome_departamento' => $this->input->post('nome_departamento'));
                          
        $this->form_validation->set_rules('nome_departamento','Nome','required|not_contains[departamento.nome_departamento,departamento.depto_idfaculdade,#'.$this->input->post('idfaculdade').'#]');
        $this->form_validation->set_rules('idfaculdade','','required|contains[faculdade.idfaculdade]');
        if ($this->form_validation->run()){
            $this->departamento_model->create($sql_data);
            $this->template->redirect('departamento/lista/'.$this->input->post('idfaculdade'));
        }
        $data = $this->template->loadCabecalho('Lista de Departamentos');
        
        $this->load->model('departamento_model');
        $this->load->model('faculdade_model');
        
        $data['departamentos'] = $this->departamento_model->get($this->input->post('idfaculdade'));
        $data['faculdade'] = $this->faculdade_model->get($this->input->post('idfaculdade'));
        
        $this->template->show('lista_departamentos', $data);

    }
    
    public function remove($iddepartamento)
    {
        $this->load->model('departamento_model');
        $dep =  $this->departamento_model->getDep($iddepartamento);
        $this->departamento_model->delete($iddepartamento);
        $this->template->redirect('departamento/lista/'.$dep['depto_idfaculdade']);   
    }
    
    public function getDepartamentos()
    {
        $id_faculdade = $this->input->post('faculdade');
        $this->load->model('departamento_model');
        $data['departamentos'] = $this->departamento_model->get($id_faculdade);
        echo json_encode($data);
    }
}