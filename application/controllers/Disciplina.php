<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Disciplina extends CI_Controller {
	
	function Disciplina() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
            $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }

     public function index(){ //mostra as opções de disciplina
     	$data = $this->template->loadCabecalho('Opções de Disciplina');
        $this->template->show('disciplina_opcoes', $data);
     }

     public function novo(){
        $data = $this->template->loadCabecalho('Nova Disciplina');
        
        $this->load->model('area_model');
                
        //seleciona os cursos da unidade do coordenador
        $this->load->model('curso_model');
        $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);
        
        foreach ($cursos as $curso) {            
            $dropcursos[$curso['idcurso']] = $curso['nome_curso'];
        }
        
        $areas = $this->area_model->getAreas();
        
        foreach ($areas as $area) {
            $grandeArea = $this->area_model->getGrandeArea($area['grande_area_idgrande_area']);
            $dadoArea['nome_area'] = $area['nome_area'];
            $dadoArea['nome_grande_area'] = $grandeArea['descricao_grande_area'];
            $dropareas[$area['idarea']] = $dadoArea;
        }
        
        $data['areas_drop'] = $dropareas;
        $data['cursos_drop'] = $dropcursos;
        $data['cursos'] = $cursos;

        //dados da disciplina
        $data['nome_disciplina'] = "";
        $data['curso_sel'] = 0;

        $this->template->show('novadisciplina', $data);//chama a view novadisciplina, com o vetor de dados.
     }

      public function cria(){
        $this->load->model('disciplina_model');
        
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $sql_data = array(            
            'nome_disciplina' => $this->input->post('nome_disciplina'),
            'area_disciplina' => $this->input->post('area_disciplina'),
            'ementa' => $this->input->post('ementa_disciplina'),
            'carga_horaria' => $this->input->post('carga_horaria'),
            'disciplina_idcurso' => $this->input->post('curso')
        );
        
        $data = $this->template->loadCabecalho('Cadastro de Disicplina');
        
        $this->form_validation->set_rules('nome_disciplina','Nome da Disciplina','required');
        $this->form_validation->set_rules('area_disciplina','Área da Disciplina','required|is_natural|contains[area.idarea]');
        $this->form_validation->set_rules('carga_horaria','Carga Horária','required|is_natural');
        $this->form_validation->set_rules('curso','Curso','required');
        
        if ($this->form_validation->run()==TRUE){
            
            if ($this->input->post('iddisciplina'))
                $this->disciplina_model->update($this->input->post('iddisciplina'),$sql_data);
            else
                $this->disciplina_model->create($sql_data); 

            $this->template->redirect('disciplina/lista/true');          
        }
        else{
            $data['page_title'] = 'Nova Disciplina';

            //seleciona os cursos da unidade do coordenador
            $this->load->model('curso_model');
            $this->load->model('area_model');
            $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);

            foreach ($cursos as $curso) {            
                $dropcursos[$curso['idcurso']] = $curso['nome_curso'];
            }
            $data['cursos_drop'] = $dropcursos;
            
           $areas = $this->area_model->getAreas();
        
            foreach ($areas as $area) {
                $grandeArea = $this->area_model->getGrandeArea($area['grande_area_idgrande_area']);
                $dadoArea['nome_area'] = $area['nome_area'];
                $dadoArea['nome_grande_area'] = $grandeArea['descricao_grande_area'];
                $dropareas[$area['idarea']] = $dadoArea;
            }
            $data['areas_drop'] = $dropareas;
            
            $this->template->show('novadisciplina', $data); //chama a view novadisciplina, com o vetor de dados.            
        }
     }

     public function lista($flag){
        $this->load->model('disciplina_model');
        $this->load->model('area_model');
        $this->load->model('curso_model');
         
        $data = $this->template->loadCabecalho('');
        $data['title'] = 'Disciplinas da unidade '.$data['unidade']['nome_cursinho_unidade'];
         
        if (isset($flag)) $data['msg'] = "Disciplina inserida com sucesso";
        
        //seleciona as disciplinas da unidade
        $data['disciplinas'] = $this->disciplina_model->getDisciplinasUnidade($data['unidade']['idunidade']);
        
        foreach ($data['disciplinas'] as $key=>$disciplina) {
            $area = $this->area_model->getArea($disciplina['area_disciplina']);
            $data['disciplinas'][$key]['area_disciplina'] = $area['nome_area'];
        }
       
        //seleciona os cursos da unidade
        $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);

        foreach ($cursos as $curso) {            
            $dropcursos[$curso['idcurso']] = $curso['nome_curso'];
        }
        $data['cursos_drop'] = $dropcursos;
        $data['cursos'] = $cursos;
        $this->template->show('lista_disciplina', $data);
     }

     public function edita($id){
        $data = $this->template->loadCabecalho('Edita Disciplina');
        
        $iddisciplina = $id;
        $this->load->model('area_model');
        $this->load->model('curso_model');
        $this->load->model('disciplina_model');

        $disciplina = $this->disciplina_model->getDisciplina($iddisciplina);

        //seleciona os cursos da unidade do coordenador
        $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);

        foreach ($cursos as $curso) {            
            $dropcursos[$curso['idcurso']] = $curso['nome_curso'];
        }
        $data['cursos_drop'] = $dropcursos;
        $data['cursos'] = $cursos;
            
       $areas = $this->area_model->getAreas();
    
        foreach ($areas as $area) {
            $grandeArea = $this->area_model->getGrandeArea($area['grande_area_idgrande_area']);
            $dadoArea['nome_area'] = $area['nome_area'];
            $dadoArea['nome_grande_area'] = $grandeArea['descricao_grande_area'];
            $dropareas[$area['idarea']] = $dadoArea;
        }
        $data['areas_drop'] = $dropareas;

        //dados da disciplina
        $data['iddisciplina'] = $disciplina['iddisciplina'];
        $data['unidade_idunidade'] = $unidade['idunidade'];  
        $data['nome_disciplina'] = $disciplina['nome_disciplina'];
        $data['area_disciplina'] = $disciplina['area_disciplina'];
        $data['curso_sel'] = $disciplina['curso_idcurso'];
        $data['area_sel'] = $disciplina['area_disciplina'];
        $data['carga_horaria'] = $disciplina['carga_horaria'];
        $data['ementa_disciplina'] = $disciplina['ementa'];

        $this->template->show('novadisciplina', $data);
    }

    public function obterAreas(){
        $this->load->model('area_model');
            
        $idgrande_area = $this->input->post('idFrente');
        
        $areas = $this->area_model->getAreasGrande($idgrande_area);
        
        foreach ($areas as $area) {
            $drop_area[$area['idarea']]=$area['nome_area'];
        }
        
        $data['areas'] = $drop_area;
        
        echo json_encode($data);
        
        
    }
    
    
    public function obterAreasDisp(){
        $this->load->model('area_model');
        $this->load->model('turma_model');
            
        $idgrande_area = $this->input->post('idFrente');
        $idTurma = $this->input->post('idTurma');
        
        $turma = $this->turma_model->getTurma($idTurma);
        
        $areas = $this->area_model->getAreasGrandeDisp($idgrande_area,$turma['curso_idcurso']);
        
        foreach ($areas as $area) {
            $drop_area[$area['idarea']]=$area['nome_area'];
        }
        
        $data['areas'] = $drop_area;
        
        echo json_encode($data);
    }
    
    public function obterDisciplinasDisp(){
        $this->load->model('disciplina_model');
        $this->load->model('turma_model');
            
        $idarea = $this->input->post('idArea');
        $idTurma = $this->input->post('idTurma');
        
        $turma = $this->turma_model->getTurma($idTurma);

        $disciplinas = $this->disciplina_model->getDisciplinasArea($idarea,$turma['curso_idcurso']);


        foreach ($disciplinas as $disciplina) {
            $drop_disciplinas[$disciplina['iddisciplina']]=$disciplina['nome_disciplina'];
        }

        
        $data['disciplinas'] = $drop_disciplinas;
        
        echo json_encode($data);
    }
    

    public function remove($id){
        //$idcurso = base64_decode(urldecode($id));
        $this->load->model('disciplina_model');
        $this->disciplina_model->delete($id);
        $this->template->redirect('disciplina/lista');
    }



}