<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Turma extends CI_Controller {
	
	function Turma() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }

     public function index(){//mostra as opções de turma
        $data = $this->template->loadCabecalho('Opções de Turma');
        $this->template->show('turma_opcoes', $data);
     }  

    public function getTurmaCurso(){
        $this->load->model('curso_model');
        $this->load->model('turma_model');
        
        $idcurso = $this->input->post('idcurso');
        if ($this->curso_model->UnidadeContemCurso($idcurso,$this->session->userdata('unidade'))!=0){
            $turmas = $this->turma_model->getTurmaCurso($idcurso);
            foreach ($turmas as $turma){
                $drop_turma[$turma['idturma']] = $turma['nome_turma'];
            }
            $data['turmas'] = $drop_turma;
            $data['err'] = 'ok';
            echo json_encode($data);
            return;
        }
        $data['err'] = 'false';
        echo json_encode($data);
        return;
     }

     public function novo(){
        $data = $this->template->loadCabecalho('Nova Turma'); 

        $this->load->model('curso_model');

        //seleciona os cursos da unidade do coordenador
        $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);

        foreach ($cursos as $curso) {            
            $dropcursos[$curso['idcurso']] = $curso['nome_curso'];
        }
        $data['cursos_drop'] = $dropcursos;
        $data['cursos'] = $cursos;

        //dados da turma
        $periodos = array('Matutino' => 'Matutino','Vespertino' => 'Vespertino','Noturno' => 'Noturno','Integral'=>'Integral');        
        
        $data['nome_turma'] = "";
        $data['periodo_turma'] = $periodos;
        $data['sala_turma'] = "";
        $data['curso_sel'] = 0;

        $this->template->show('novaturma', $data);//chama a view novaturma, com o vetor de dados.
     }

     public function cria(){
        $this->load->model('user_model');
        $this->load->model('turma_model');
        
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $data = $this->template->loadCabecalho('Cadastro de Turma');
        
        $sql_data = array(            
            'curso_idcurso' => $this->input->post('curso'),
            'curso_unidade_idunidade' => $data['unidade']['idunidade'],
            'nome_turma' => $this->input->post('nome_turma'),
            'periodo_turma' => $this->input->post('periodo_turma'),
            'sala_turma' => $this->input->post('sala_turma'),
            'num_vagas' => $this->input->post('num_vagas')
        );
        
        $this->form_validation->set_rules('nome_turma','Nome da Turma','required');
        $this->form_validation->set_rules('periodo_turma','Período','required');
        $this->form_validation->set_rules('data_inicio_turma','Data de Início da Turma',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_date'));
        $this->form_validation->set_rules('sala_turma','Sala','required');
        $this->form_validation->set_rules('num_vagas','Número de Vagas','required|is_natural');
        $this->form_validation->set_rules('curso','Curso','required|is_natural|contains[curso.idcurso,curso.unidade_idunidade,#'.$data['unidade']['idunidade'].'#]');

        if($this->form_validation->run()){
            $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_inicio_turma'));
            $ini =  $date->format('Y-m-d');
            $sql_data['data_inicio_turma']=$ini;
            
            
            if($this->input->post('idturma'))//Update
                $this->turma_model->update($this->input->post('idturma'),$sql_data);
            else
                $this->turma_model->create($sql_data);
            $this->template->redirect('turma/lista');
        }
        else{
            
            $this->load->model('curso_model');
            //seleciona os cursos da unidade do coordenador
            
            $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);

            foreach ($cursos as $curso) {            
                $dropcursos[$curso['idcurso']] = $curso['nome_curso'];
            }
            $data['cursos_drop'] = $dropcursos;
            $data['cursos'] = $cursos;

            //dados da turma

            $this->template->show('novaturma', $data);//chama a view novaturma, com o vetor de dados.
        }
     }

     public function lista(){
        $data = $this->template->loadCabecalho('');
        $data['page_title'] = 'Turmas da Unidade: '.$data['unidade']['nome_cursinho_unidade'];

        //seleciona as turmas da unidade
        $this->load->model('turma_model');
        $data['turmas'] = $this->turma_model->getTurmas($data['unidade']['idunidade']);

        //seleciona os cursos da unidade
        $this->load->model('curso_model');
        $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);

        foreach ($cursos as $curso) {            
            $dropcursos[$curso['idcurso']] = $curso['nome_curso'];
        }
        $data['cursos_drop'] = $dropcursos;
        $data['cursos'] = $cursos;

        $this->template->show('lista_turma', $data);
     }

     public function consolidarTurmas($flag){
        $data = $this->template->loadCabecalho('Consolidar Turmas');
            
        if (isset($flag)) {
            $data['msg']= 'Turma Consolidada com Sucesso!';
        }
        
        //seleciona as turmas da unidade
        $this->load->model('turma_model');
        $turmas = $this->turma_model->getTurmas($data['unidade']['idunidade']);
        $data['turmas'] = $turmas;

        foreach ($turmas as $turma) {
            $idTurma[$turma['idturma']] = base64_encode($turma['idturma']);
        }
        //seleciona os cursos da unidade
        $this->load->model('curso_model');
        $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);

        foreach ($cursos as $curso) {            
            $dropcursos[$curso['idcurso']] = $curso['nome_curso'];
            $duracao[$curso['idcurso']] = $curso['duracao'];
        }
        $data['duracao'] = $duracao;
        $data['cursos_drop'] = $dropcursos;
        $data['cursos'] = $cursos;
        $data['data_consolida'] = date('d/m/Y');
        $data['idTurma'] = $idTurma;
        $this->template->show('lista_turma_consolidar', $data);
     }

    public function consolidar($id){
            $idturma = base64_decode(urldecode($id));            
            $this->load->model('turma_model');
            $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_consolida'));
            $data_consolidar =  $date->format('Y-m-d');
            //$data_consolidar = date('Y-m-d');
            $sql_data = array(
                'data_fim_turma' => $data_consolidar,
                'status' => 2              
            );           
            $this->turma_model->update($idturma,$sql_data);
            $this->template->redirect('turma/consolidarTurmas/true');
    }    

    public function edita($id){
        $idturma = $id;
        $this->load->model('turma_model');
        $this->load->model('curso_model');
        $data = $this->template->loadCabecalho('Edita Turma');
        
        
        $turma = $this->turma_model->getTurma($idturma);
        $data['view']  = 'coordenador';
            
        //seleciona os cursos da unidade do coordenador
        $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);

        foreach ($cursos as $curso) {            
            $dropcursos[$curso['idcurso']] = $curso['nome_curso'];
        }
        $data['cursos_drop'] = $dropcursos;
        $data['cursos'] = $cursos;

        //dados da turma
        $data['idturma'] = $turma['idturma'];
        $data['nome_turma'] = $turma['nome_turma'];        
        $data['sala_turma'] = $turma['sala_turma'];
        $data['curso_sel'] = $turma['curso_idcurso'];        
        $data['num_vagas'] = $turma['num_vagas']; 
        
        $date = DateTime::createFromFormat('Y-m-d', $turma['data_inicio_turma']);
        $ini =  $date->format('d/m/Y');    
        $data['data_inicio_turma'] = $ini;

        $periodos = array('Matutino' => 'Matutino','Vespertino' => 'Vespertino','Noturno' => 'Noturno','Integral'=>'Integral');        
        //dropdown de periodo e de ano
        $data['periodo_turma'] = $periodos;
        //o que está selecionado em periodo e ano
        $data['ano_sel'] = $turma['ano_turma'];
        $data['periodo_sel'] = $turma['periodo_turma'];

        $this->template->show('novaturma', $data);
    }
         
    public function remove($id){
        $this->load->model('turma_model');
        $this->turma_model->delete($id);
        $this->template->redirect('turma/lista/true');
    }

    public function getAlunosTurma()
    {
        $this->load->model('aluno_model');
        
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('idTurma','idturma','required|contains[turma.idturma,turma.curso_unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
        
        if ($this->form_validation->run()){
            
            $idTurma = $this->input->post('idTurma');
            
            $alunos = $this->aluno_model->obterAlunoTurma($idTurma,date('Y-m-d'));
            $drop_alunos['nome_aluno'] = array_column($alunos, 'nome_usuario');
            $drop_alunos['cpf_aluno'] = array_column($alunos, 'cpf_usuario');
            $data['alunos']= $drop_alunos;
            $data['err'] = "ok";
    
            echo json_encode($data);
        
        }
        else{
            echo json_encode(validation_errors());
        }
    }
    
    public function exibirUniao(){
        $data = $this->template->loadCabecalho('Unir Turmas');
        
        //seleciona as turmas da unidade
        $this->load->model('turma_model');
        //seleciona os cursos da unidade
        $this->load->model('curso_model');
        $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);
        $cursos[0]['idcurso'];
        
        $data['periodo_turma'] = array('Matutino' => 'Matutino','Vespertino' => 'Vespertino','Noturno' => 'Noturno','Integral'=>'Integral');        
        
        foreach ($cursos as $curso) {            
            $dropcursos[$curso['idcurso']] = $curso['nome_curso'];
            $duracao[$curso['idcurso']] = $curso['duracao'];
        }
        
        $turmas = $this->turma_model->get($cursos[0]['idcurso']);
        $data['turmas'] = $turmas;

        foreach ($turmas as $turma) {
            $idTurma[$turma['idturma']] = base64_encode($turma['idturma']);
        }
        
        $data['duracao'] = $duracao;
        $data['cursos_drop'] = $dropcursos;
        $data['cursos'] = $cursos;
        $data['data_consolida'] = date('d/m/Y');
        $data['idTurma'] = $idTurma;
        
        
        $this->template->show('lista_turma_uniao', $data);
    }

    public function unir()
    {
        $this->load->model('turma_model');
        $this->load->model('aluno_model');
        $this->load->model('matricula_model');
        
        $turmas = $this->input->post('turmas[]');
        if (!empty($turmas)){
            $turma1 = $this->turma_model->getTurma($turmas[0]);
            
            $this->load->helper('url');
            $this->load->helper('form');
            $this->load->library('form_validation');
            
            $sql_data = array(            
                'curso_idcurso' => $turma1['curso_idcurso'],
                'curso_unidade_idunidade' => $this->session->userdata('unidade'),
                'nome_turma' => $this->input->post('nome_turma'),
                'periodo_turma' => $this->input->post('periodo_turma'),
                'sala_turma' => $this->input->post('sala_turma'),
                'num_vagas' => $this->input->post('num_vagas')
            );
            
            $this->form_validation->set_rules('nome_turma','Nome da Turma','required');
            $this->form_validation->set_rules('periodo_turma','Período','required');
            $this->form_validation->set_rules('data_inicio_turma','Data de Início da Turma',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_date'));
            $this->form_validation->set_rules('sala_turma','Sala','required');
            $this->form_validation->set_rules('num_vagas','Número de Vagas','required|is_natural');
            $this->form_validation->set_rules('turmas[]','Turmas','required|contains[turma.idturma,turma.curso_unidade_idunidade,#'.$this->session->userdata('unidade').'#,turma.curso_idcurso,#'.$turma1['curso_idcurso'].'#]');
            
            if ($this->form_validation->run()){
                $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_inicio_turma'));
                $ini =  $date->format('Y-m-d');
                $sql_data['data_inicio_turma']=$ini;
                
                $id_turma = $this->turma_model->create($sql_data);
                
                $sql_matricula['data_trancamento'] = $sql_data['data_inicio_turma'];
                $sql_matricula['status'] = 5;
                $sql_matricula['matricula_observacoes'] = 'União de Turmas';
                    
                $sql_nova_mat['data_matricula'] = $sql_data['data_inicio_turma'];
                $sql_nova_mat['matricula_idturma'] = $id_turma;
                $sql_nova_mat['status'] = 1;
                
                    
                $sql_turma['status'] = 0;
                $sql_turma['data_fim_turma'] = $sql_data['data_inicio_turma'];
                
                foreach ($turmas as  $turma) {
                    $matriculas = $this->aluno_model->getAlunosTurma($turma);
                    
                    $this->turma_model->update($turma,$sql_turma);
                    
                    foreach ($matriculas as $matricula) {
                        $this->matricula_model->update($matricula['idmatricula'], $sql_matricula);
                        
                        $sql_nova_mat['matricula_idaluno'] = $matricula['matricula_idaluno'];
                        $this->matricula_model->create($sql_nova_mat);
                        
                    }
                }
                $this->template->redirect('turma/lista');
            }
            
        }
    }

    public function relatorio_aluno_turma($idTurma)
    {
        $this->load->model('aluno_model');
        $this->load->model('user_model');
        $this->load->model('turma_model');
        
        $data= $this->template->loadCabecalho('Relatorio Aluno Turma');
        $this->load->library('pdf');
        
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('nome_turma','idturma','required|contains[turma.idturma,turma.curso_unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
        if ($this->form_validation->run()){
            $idTurma = $this->input->post('nome_turma');
            $turma = $this->turma_model->getTurma($idTurma);
            
            $alunos = $this->aluno_model->obterAlunoTurma($idTurma,date('Y-m-d'));
            $drop_alunos['nome'] = array_column($alunos, 'nome_usuario');
            $drop_alunos['nome_social'] = array_column($alunos, 'nome_social_usuario');
            $data['alunos']= $drop_alunos;
            
            $pdfFilePath = "/downloads/reports/$filename.pdf";  
            if (file_exists($pdfFilePath) == FALSE)
            {
                $date = DateTime::createFromFormat("Y-m-d",$turma['data_inicio_turma']);
        
                $filename=$turma['nome_turma'].'-'.$date->format("Y").'.pdf'; //save our workbook as this file name
                $data['turma'] = $turma['nome_turma'].'-'.$date->format("Y");

                $html = $this->load->view('relatorios/alunos_turma', $data, true); // render the view into HTML
                 
                $pdf = $this->pdf->load();
                $pdf->SetTitle('Relatório Alunos por Turma:'.$turma['nome_turma'].'-'.$date->format("Y"));
                $pdf->SetFooter('Página {PAGENO} de {nb}| |'.date("d/m/Y")); // Add a footer for good measure <img class="emoji" draggable="false" alt="😉" src="https://s.w.org/images/core/emoji/72x72/1f609.png">
                $pdf->WriteHTML($html); // write the HTML into the PDF
                $pdf->Output($filename,"I"); // save to file because we can
            }
             
            $this->template->redirect("");
        
        }
        else{
            echo json_encode(validation_errors());
        }
    }

 }
