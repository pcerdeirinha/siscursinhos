<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Simulado extends CI_Controller {
    
    function Simulado() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
        $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }
    
    
    public function index()
    {
        $this->load->model('curso_model');
        $this->load->model('turma_model');
        
        $data = $this->template->loadCabecalho('Simulado');
        
        $erro = $this->session->flashdata('erro');
        if ($erro!=''){
            $data['err'] = $erro['descricao'];
            $data['funcao'] = $erro['funcao'] .'('.$erro['parametro'].')';
        }
            
        if ($this->session->userdata('level')>2){
            
            $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);
            foreach ($cursos as $curso) {            
                $dropcursos[$curso['idcurso']] = $curso['nome_curso'];
            }
            $data['cursos_drop'] = $dropcursos;
            $data['cursos'] = $cursos;
            
            $turmas[0]='Selecionar curso';
            $data['turmas_drop']=$turmas;
        }
        else if ($this->session->userdata('level')==2){
            $this->load->model('oferta_model');
            
            $ofertas = $this->oferta_model->getOferMonUni($this->session->userdata('user'),$this->session->userdata('unidade'));
            
            foreach ($ofertas as $oferta) {
                $turma = $this->turma_model->getTurma($oferta['idturma']);
                $curso = $this->curso_model->get($turma['curso_idcurso']);
                $dropcursos[$turma['curso_idcurso']] = $curso['nome_curso'];
            }            
            $data['cursos_drop'] = $dropcursos;
            $data['cursos'] = $cursos;
            
            $turmas[0]='Selecionar turma';
            $data['turmas_drop']=$turmas;
            
        }
        $this->template->show('simulados_turma_sel', $data);
    }


    public function novo()
    {
        $this->load->model('area_model');
        $this->load->model('curso_model');
        $this->load->model('oferta_model');
        $this->load->model('disciplina_model');
        $this->load->model('turma_model');
        $this->load->model('aluno_model');

    	$data = $this->template->loadCabecalho('Novo Simulado');
        
        $idTurma = $this->input->post('turmas');
        
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');   
        
        $this->form_validation->set_rules('turmas','Turma','required|contains[turma.idturma,turma.curso_unidade_idunidade,#'.$data['unidade']['idunidade'].'#,turma.status,#1#]');
        
        if ($this->session->userdata('level')==2){
            $this->form_validation->set_rules('turmas','Turma','contains[oferta_disciplina.idturma,oferta_disciplina.monitor_idusuario,#'.$this->session->userdata('user').'#]');
        }
        
        if ($this->form_validation->run()==TRUE){
            $curso_sel = $this->input->post('cursos');
            $turma_sel = $this->input->post('turmas');
        
            if ($this->session->userdata('level')>2){
                
                $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);
                foreach ($cursos as $curso) {            
                    $dropcursos[$curso['idcurso']] = $curso['nome_curso'];
                }
                $data['cursos_drop'] = $dropcursos;
                $data['cursos'] = $cursos;
                
                $data['curso_sel'] = $curso_sel;
                
                $turmas = $this->turma_model->getTurmaCurso($curso_sel);
                foreach ($turmas as $turma) {
                    $dropturma[$turma['idturma']] = $turma['nome_turma'] .', '.$turma['periodo_turma'];
                }
            
                $data['turmas_drop']=$dropturma;
                $data['turma_sel'] = $turma_sel;
                
            }
            else if ($this->session->userdata('level')==2){
                $ofertas = $this->oferta_model->getOferMonUni($this->session->userdata('user'),$this->session->userdata('unidade'));
                
                foreach ($ofertas as $oferta) {
                    $turma = $this->turma_model->getTurma($oferta['idturma']);
                    $curso = $this->curso_model->get($turma['curso_idcurso']);
                    $dropcursos[$turma['curso_idcurso']] = $curso['nome_curso'];
                    if ($turma['curso_idcurso']==$curso_sel)
                        $dropturmas[$turma['idturma']] = $turma['nome_turma'] .', '.$turma['periodo_turma'];
                }            
                $data['cursos_drop'] = $dropcursos;
                
                $data['curso_sel'] = $curso_sel;
                
                $data['turmas_drop']=$dropturmas;
                $data['turma_sel'] = $turma_sel;
                
            }
            
            $turma = $this->turma_model->getTurma($idTurma);
            
            if ($this->session->userdata('level')>2){
            
                $disciplinas = $this->disciplina_model->getDisciplinasCurso($turma['curso_idcurso']);
                
                foreach ($disciplinas as $disciplina) {
                    $area = $this->area_model->getArea($disciplina['area_disciplina']);
                    $frente = $this->area_model->getGrandeArea($area['grande_area_idgrande_area']);
                    $drop_frente[$area['grande_area_idgrande_area']]= $frente['descricao_grande_area']; 
                }
            }
            else if ($this->session->userdata('level')==2){
                 $ofertas = $this->oferta_model->getOferMonUni($this->session->userdata('user'),$this->session->userdata('unidade'));
                 foreach ($ofertas as $oferta) {
                    $disciplina = $this->disciplina_model->getDisciplina($oferta['iddisciplina']);
                    $area = $this->area_model->getArea($disciplina['area_disciplina']);
                    $frente = $this->area_model->getGrandeArea($area['grande_area_idgrande_area']);
                    $drop_frente[$area['grande_area_idgrande_area']]= $frente['descricao_grande_area']; 
                }
            }
            
            $frentes = $this->area_model->getGrandesAreas();
            
            foreach ($frentes as $frente) {
               $drop_frenteT[$frente['idgrande_area']] = $frente['descricao_grande_area']; 
            }
            
            $data['nome_frente_disciplina'] = $drop_frente;
            $data['nome_frente'] = $drop_frenteT;
            $data['idTurma'] = $idTurma;
            
            $data['professor'] = ($this->session->userdata('level')==2)?true:false;
            
            $this->template->show('novosimulado', $data);
        }
        else{
            $this->template->redirect('simulado');
        }
    }

    private function criaError($idTurma)
    {
        $this->load->model('area_model');
        $this->load->model('disciplina_model');
        $this->load->model('turma_model');
        $this->load->model('oferta_model');
        $this->load->model('curso_model');
        $this->load->model('aluno_model');
        
        $data = $this->template->loadCabecalho('Novo Simulado');
        
        $curso_sel = $this->input->post('cursos');
        $turma_sel = $this->input->post('turmas');
    
        if ($this->session->userdata('level')>2){
            
            $cursos = $this->curso_model->getCursos($data['unidade']['idunidade']);
            foreach ($cursos as $curso) {            
                $dropcursos[$curso['idcurso']] = $curso['nome_curso'];
            }
            $data['cursos_drop'] = $dropcursos;
            $data['cursos'] = $cursos;
            
            $data['curso_sel'] = $curso_sel;
            
            $turmas = $this->turma_model->getTurmaCurso($curso_sel);
            foreach ($turmas as $turma) {
                $dropturma[$turma['idturma']] = $turma['nome_turma'];
            }
        
            $data['turmas_drop']=$dropturma;
            $data['turma_sel'] = $turma_sel;
            
        }
        else if ($this->session->userdata('level')==2){
            $ofertas = $this->oferta_model->getOferMonUni($this->session->userdata('user'),$this->session->userdata('unidade'));
            
            foreach ($ofertas as $oferta) {
                $turma = $this->turma_model->getTurma($oferta['idturma']);
                $curso = $this->curso_model->get($turma['curso_idcurso']);
                $dropcursos[$turma['curso_idcurso']] = $curso['nome_curso'];
                if ($turma['curso_idcurso']==$curso_sel)
                    $dropturmas[$turma['idturma']] = $turma['nome_turma'] .', '.$turma['periodo_turma'];
            }            
            $data['cursos_drop'] = $dropcursos;
            
            $data['curso_sel'] = $curso_sel;
            
            $data['turmas_drop']=$dropturmas;
            $data['turma_sel'] = $turma_sel;
            
        }
        
        $turma = $this->turma_model->getTurma($idTurma);

        if ($this->session->userdata('level')>2){
        
            $disciplinas = $this->disciplina_model->getDisciplinasCurso($turma['curso_idcurso']);
            
            foreach ($disciplinas as $disciplina) {
                $area = $this->area_model->getArea($disciplina['area_disciplina']);
                $frente = $this->area_model->getGrandeArea($area['grande_area_idgrande_area']);
                $drop_frente[$area['grande_area_idgrande_area']]= $frente['descricao_grande_area']; 
            }
        }
        else if ($this->session->userdata('level')==2){
            $ofertas = $this->oferta_model->getOferMonUni($this->session->userdata('user'),$this->session->userdata('unidade'));
             foreach ($ofertas as $oferta) {
                $disciplina = $this->disciplina_model->getDisciplina($oferta['iddisciplina']);
                $area = $this->area_model->getArea($disciplina['area_disciplina']);
                $frente = $this->area_model->getGrandeArea($area['grande_area_idgrande_area']);
                $drop_frente[$area['grande_area_idgrande_area']]= $frente['descricao_grande_area']; 
            }
        }
        
        $frentes = $this->area_model->getGrandesAreas();
        
       foreach ($frentes as $frente) {
           $drop_frenteT[$frente['idgrande_area']] = $frente['descricao_grande_area']; 
       }
        
        $data['nome_frente_disciplina'] = $drop_frente;
        $data['nome_frente'] = $drop_frenteT;
        $data['idTurma'] = $idTurma;
        
        $data['professor'] = ($this->session->userdata('level')==2)?true:false;
        $this->template->show('novosimulado', $data);
        return;
    }

    public function cria()
    {
        $this->load->model('simulado_model');
        $this->load->model('aluno_model');
        $this->load->model('turma_model');
        $this->load->model('user_model');

        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $sql_simulado = array(
            'descricao_simulado' => $this->input->post('descricao_simulado'),
            'turma_idturma' => $this->input->post('idTurma'),
        );
        
        $this->form_validation->set_rules('idTurma','idTurma','required|contains[turma.idturma,turma.status,#1#,turma.curso_unidade_idunidade,#'.$this->session->userdata('unidade').'#]');
    
        if ($this->session->userdata('level')==2){
            
            $this->form_validation->set_rules('idTurma','idTurma','contains[oferta_disciplina.idturma,oferta_disciplina.monitor_idusuario,#'.$this->session->userdata('user').'#]');
        }
        if ($this->form_validation->run()==TRUE){
            $this->form_validation->set_rules('data_simulado','Data do Simulado',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_date'));
            $this->form_validation->set_rules('peso_simulado','Peso',array('required','regex_match[/(([0-9])*\,([0-9])*)|([0-9]*)/]'));
            $this->form_validation->set_rules('descricao_simulado','Descrição','required');
            $this->form_validation->set_rules('nota_simulado','Nota Máxima',array('required','regex_match[/(([0-9])*\,([0-9])*)|([0-9]*)/]'));
            
            if ($this->form_validation->run()==TRUE){
                $sql_simulado['peso_simulado'] = str_replace(',', '.', $this->input->post('peso_simulado'));
                $sql_simulado['nota_maxima_simulado'] = str_replace(',', '.', $this->input->post('nota_simulado'));
                
                $turma = $this->turma_model->getTurma($sql_simulado['turma_idturma']);
                
                $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_simulado'));
                $sql_simulado['data_simulado']=$date->format('Y-m-d');
                
                if (($this->input->post('forma')=='frente')&&($this->session->userdata('level')>2)){
                    $this->form_validation->set_rules('nome_frente_frente','Frente','required|contains[grande_area.idgrande_area]');
                    
                    if ($this->form_validation->run()==TRUE){                
                        $sql_simulado['grande_area_idgrande_area'] = $this->input->post('nome_frente_frente');
                    }
                    else {
                        $this->criaError($sql_simulado['turma_idturma']);
                        return;
                    }
                }
                else if (($this->input->post('forma')=='area')&&($this->session->userdata('level')>2)){
                    $this->form_validation->set_rules('nome_frente_area','Frente','required|contains[grande_area.idgrande_area]');
                    $this->form_validation->set_rules('nome_area_area','Área','required|contains[area.idarea,area.grande_area_idgrande_area,#'.$this->input->post('nome_frente_area').'#]');
                    if ($this->form_validation->run()==TRUE){   
                        $sql_simulado['grande_area_idgrande_area'] = $this->input->post('nome_frente_area');
                        $sql_simulado['area_idarea'] = $this->input->post('nome_area_area');
                    }
                    else {
                        $this->criaError($sql_simulado['turma_idturma']);
                        return;
                    }
                }
                else if ($this->input->post('forma')=='disciplina'){
                    $this->form_validation->set_rules('nome_frente_disciplina','Frente','required|contains[grande_area.idgrande_area]');
                    $this->form_validation->set_rules('nome_area_disciplina','Área','required|contains[area.idarea,area.grande_area_idgrande_area,#'.$this->input->post('nome_frente_disciplina').'#]');
                    $this->form_validation->set_rules('nome_disciplina_disciplina','Disciplina','required|contains[disciplina.iddisciplina,disciplina.area_disciplina,#'.$this->input->post('nome_area_disciplina').'#,disciplina.status,#1#]');
                    
                    if ($this->session->userdata('level')==2){
                        $this->form_validation->set_rules('nome_disciplina_disciplina','Disciplina','required|contains[oferta_disciplina.iddisciplina,oferta_disciplina.idturma,#'.$sql_simulado['turma_idturma'].'#,oferta_disciplina.monitor_idusuario,#'.$this->session->userdata('user').'#]');
                    }
                    if ($this->form_validation->run()==TRUE){  
                        $sql_simulado['grande_area_idgrande_area'] = $this->input->post('nome_frente_disciplina');
                        $sql_simulado['area_idarea'] = $this->input->post('nome_area_disciplina');
                        $sql_simulado['disciplina_iddisciplina'] = $this->input->post('nome_disciplina_disciplina');  
                    }
                    else {
                        $this->criaError($sql_simulado['turma_idturma']);
                        return;
                    }          
                }
                else {
                    $this->criaError($sql_simulado['turma_idturma']);
                    return;
                }
                    
                //$alunos = $this->aluno_model->obterAlunoTurma($sql_simulado['turma_idturma'],$sql_simulado['data_simulado']);
                /*
                foreach ($alunos as $aluno) {
                    $this->form_validation->set_rules('nota'.$aluno['idusuario'],'nota'.$aluno['nome_usuario'],array('regex_match[/(([0-9])*\,([0-9])*)|([0-9]*)/]'));
                }
                */
                //Alunos Transferidos
                //$alunosTrans = $this->aluno_model->obterAlunoTurmaTrans($sql_simulado['turma_idturma'],$sql_simulado['data_simulado']);
                /*
                foreach ($alunosTrans as $aluno) {
                    $this->form_validation->set_rules('nota'.$aluno['idusuario'],'nota'.$aluno['nome_usuario'],array('regex_match[/(([0-9])*\,([0-9])*)|([0-9]*)/]'));
                }
                
                if ($this->form_validation->run()==TRUE){*/
                $idSimulado = $this->simulado_model->create($sql_simulado);  
                /*                          
                foreach ($alunos as $aluno) {
                    $sql_nota['aluno_idaluno'] = $aluno['idusuario'];
                    $sql_nota['simulado_idsimulado'] = $idSimulado; 
                    $sql_nota['valor_nota'] = null;
                    
                    $this->simulado_model->adicionarNota($sql_nota);
                }
                
                foreach ($alunosTrans as $aluno) {
                    $sql_nota['aluno_idaluno'] = $aluno['idusuario'];
                    $sql_nota['simulado_idsimulado'] = $idSimulado; 
                    $sql_nota['valor_nota'] = null;
                    $this->simulado_model->adicionarNota($sql_nota);
                }
                */
                $this->template->redirect('simulado/novo'); 
           }
           else{
               $this->criaError($sql_simulado['turma_idturma']);
               return;
           }
       }
       else{
           $this->template->redirect('simulado');
       }
           
    }

    public function buscaAlunos()
    {
        $this->load->model('aluno_model');
        $this->load->model('simulado_model');
        
        $idsimulado = $this->input->post('idSimulado');
        $simulado = $this->simulado_model->get($idsimulado);
        
        $data_a= $simulado['data_simulado'];
        $idturma = $simulado['turma_idturma'];
        
        $alunos = $this->aluno_model->obterAlunoTurma($idturma,$data_a);
        $alunos_r = $this->simulado_model->obterAlunoSimulado($idsimulado);

        foreach ($alunos as $aluno) {
            $drop_aluno[$aluno['idusuario']]['nome_aluno'] = $aluno['nome_social_usuario']==null?$aluno['nome_usuario']:$aluno['nome_social_usuario'];
            $drop_aluno[$aluno['idusuario']]['nota_aluno'] = "";
            foreach ($alunos_r as $key => $aluno_r) {
                if ($aluno_r['idusuario']==$aluno['idusuario']){
                    $drop_aluno[$aluno['idusuario']]['nota_aluno'] = $aluno_r['valor_nota']?number_format($aluno_r['valor_nota'], 2, ',', ''):"";        
                    unset($alunos_r[$key]);
                }
            }
        }
        
        $alunos = $this->aluno_model->obterAlunoTurmaTrans($idturma,$data_a);
        
        foreach ($alunos as $aluno) {
            $drop_aluno[$aluno['idusuario']]['nome_aluno'] = $aluno['nome_social_usuario']==null?$aluno['nome_usuario']:$aluno['nome_social_usuario'];
            $drop_aluno[$aluno['idusuario']]['nota_aluno'] = "";
            foreach ($alunos_r as $key => $aluno_r) {
                if ($aluno_r['idusuario']==$aluno['idusuario']){
                    $drop_aluno[$aluno['idusuario']]['nota_aluno'] = $aluno_r['valor_nota']?number_format($aluno_r['valor_nota'], 2, ',', ''):"";        
                    unset($alunos_r[$key]);
                }
            }
        }
        
        foreach ($alunos_r as $aluno) {
            $dados['nome_aluno'] = $aluno['nome_social_usuario']==null?$aluno['nome_usuario']:$aluno['nome_social_usuario'];
            $dados['nota_aluno'] = $aluno['valor_nota']?number_format($aluno['valor_nota'], 2, ',', ''):"";
            $drop_aluno[$aluno['idusuario']] = $dados;
        }
        
        $data['alunos'] = $drop_aluno;
        $data['err'] = "ok";
        echo json_encode($data);
    }


    public function busca()
    {
        $this->load->model('simulado_model');
        $this->load->model('area_model');
        $this->load->model('oferta_model');
        $this->load->model('disciplina_model');
        
    	$idTurma = $this->input->post('idTurma');
    	
    	$simulados = $this->simulado_model->getSimuladosTurma($idTurma);
        
        foreach ($simulados as $simulado) {
            if (($simulado['grande_area_idgrande_area']==null)&&($this->session->userdata('level')>2)){
                $sim['descricao'] = $simulado['descricao_simulado'];
                $date = DateTime::createFromFormat('Y-m-d', $simulado['data_simulado']);
                $sim['data'] = $date->format('d/m/Y'); 
                $sim['tipo'] = 'Geral';
                $sim['peso'] = number_format($simulado['peso_simulado'], 2, ',', '');
                $sim['nota_maxima'] = number_format($simulado['nota_maxima_simulado'], 2, ',', '');
                $dropsimulados[$simulado['idsimulado']] = $sim;
            }
            else if (($simulado['area_idarea']==null)&&($this->session->userdata('level')>2)) {
                $sim['descricao'] = $simulado['descricao_simulado'];
                $date = DateTime::createFromFormat('Y-m-d', $simulado['data_simulado']);
                $sim['data'] = $date->format('d/m/Y');
                $area = $this->area_model->getGrandeArea($simulado['grande_area_idgrande_area']);
                $sim['tipo'] = 'Grande Área: '.$area['descricao_grande_area'];
                $sim['peso'] = number_format($simulado['peso_simulado'], 2, ',', '');
                $sim['nota_maxima'] = number_format($simulado['nota_maxima_simulado'], 2, ',', '');
                $dropsimulados[$simulado['idsimulado']] = $sim; 
            }
            else if (($simulado['disciplina_iddisciplina']==null)&&($this->session->userdata('level')>2)) {
                $sim['descricao'] = $simulado['descricao_simulado'];
                $date = DateTime::createFromFormat('Y-m-d', $simulado['data_simulado']);
                $sim['data'] = $date->format('d/m/Y');
                $area = $this->area_model->getArea($simulado['area_idarea']);
                $sim['tipo'] = 'Área: '.$area['nome_area']; 
                $sim['peso'] = number_format($simulado['peso_simulado'], 2, ',', '');
                $sim['nota_maxima'] = number_format($simulado['nota_maxima_simulado'], 2, ',', '');
                $dropsimulados[$simulado['idsimulado']] = $sim;
            }
            else if ($simulado['disciplina_iddisciplina']!=null) {
                if ($this->session->userdata('level')==2){
                    $oferta = $this->oferta_model->getWhere(array('iddisciplina' => $simulado['disciplina_iddisciplina'],'idturma' => $simulado['turma_idturma']));
                    if ($oferta['monitor_idusuario']==$this->session->userdata('user')){
                        $sim['descricao'] = $simulado['descricao_simulado'];
                        $date = DateTime::createFromFormat('Y-m-d', $simulado['data_simulado']);
                        $sim['data'] = $date->format('d/m/Y');
                        $disciplina = $this->disciplina_model->getDisciplina($simulado['disciplina_iddisciplina']); 
                        $sim['tipo'] = 'Disciplina: '.$disciplina['nome_disciplina']; 
                        $sim['peso'] = number_format($simulado['peso_simulado'], 2, ',', '');
                        $sim['nota_maxima'] = number_format($simulado['nota_maxima_simulado'], 2, ',', '');
                        $dropsimulados[$simulado['idsimulado']] = $sim;
                    }
                }
                else if ($this->session->userdata('level')>2){
                    $sim['descricao'] = $simulado['descricao_simulado'];
                    $date = DateTime::createFromFormat('Y-m-d', $simulado['data_simulado']);
                    $sim['data'] = $date->format('d/m/Y');
                    $disciplina = $this->disciplina_model->getDisciplina($simulado['disciplina_iddisciplina']); 
                    $sim['tipo'] = 'Disciplina:'.$disciplina['nome_disciplina']; 
                    $sim['peso'] = number_format($simulado['peso_simulado'], 2, ',', '');
                    $sim['nota_maxima'] = number_format($simulado['nota_maxima_simulado'], 2, ',', '');
                    $dropsimulados[$simulado['idsimulado']] = $sim;
                }
            }
        }
        
        $data['simulados'] = $dropsimulados;
        $data['err'] = 'ok';
        
        echo json_encode($data);
        return;
    }
    
    public function buscaAlunosSimulado()
    {
        $this->load->model('simulado_model');
    
        $idsimulado = $this->input->post('idSimulado');
            
        $alunos = $this->simulado_model->obterAlunoSimulado($idsimulado);

        foreach ($alunos as $aluno) {
            $dados['nome_aluno'] = $aluno['nome_social_usuario']==null?$aluno['nome_usuario']:$aluno['nome_social_usuario'];
            $dados['nota_aluno'] = $aluno['valor_nota']?number_format($aluno['valor_nota'], 2, ',', ''):"";
            $drop_aluno[$aluno['idusuario']] = $dados;
        }
        
        $data['alunos'] = $drop_aluno;
        $data['err'] = "ok";
        echo json_encode($data);
    }
    
    public function edita()
    {
        $this->load->model('aluno_model');
        $this->load->model('simulado_model');

        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $idsimulado = $this->input->post('idSimulado');
        $simulado = $this->simulado_model->get($idsimulado);
        
        $data_a= $simulado['data_simulado'];
        $idturma = $simulado['turma_idturma'];
        
        $alunos_n = $this->aluno_model->obterAlunoTurma($idturma,$data_a);
        $alunos_r = $this->simulado_model->obterAlunoSimulado($idsimulado);
        $alunos_t = $this->aluno_model->obterAlunoTurmaTrans($idturma,$data_a);
        
        foreach ($alunos_n as $aluno) 
            $this->form_validation->set_rules('nota'.$aluno['idusuario'],'nota'.$aluno['nome_usuario'],array('regex_match[/(([0-9])*\,([0-9])*)|([0-9]*)/]'));
        
        foreach ($alunos_t as $aluno) 
             $this->form_validation->set_rules('nota'.$aluno['idusuario'],'nota'.$aluno['nome_usuario'],array('regex_match[/(([0-9])*\,([0-9])*)|([0-9]*)/]'));
        
        foreach ($alunos_r as $aluno) 
             $this->form_validation->set_rules('nota'.$aluno['idusuario'],'nota'.$aluno['nome_usuario'],array('regex_match[/(([0-9])*\,([0-9])*)|([0-9]*)/]'));

        $this->form_validation->set_rules('idSimulado','simulado','contains[simulado.idsimulado,turma.idturma,simulado.turma_idturma,turma.curso_unidade_idunidade,#'.$this->session->userdata('unidade').'#]');    
        if ($this->session->userdata('level')==2){
            $this->form_validation->set_rules('idSimulado','simulado','contains[simulado.idsimulado,oferta_disciplina.idturma,simulado.turma_idturma,oferta_disciplina.iddisciplina,simulado.disciplina_iddisciplina,oferta_disciplina.monitor_idusuario,#'.$this->session->userdata['user'].'#]');        
        }
        if ($this->form_validation->run()){
            
            foreach ($alunos_n as $aluno) {
                $sql_nota['aluno_idaluno'] = $aluno['idusuario'];
                $sql_nota['simulado_idsimulado'] = $idsimulado; 
                $sql_nota['valor_nota'] = $this->input->post("nota".$aluno['idusuario'])?str_replace(',', '.', $this->input->post("nota".$aluno['idusuario'])):null;
                if ($sql_nota['valor_nota']<=$simulado['nota_maxima_simulado'])
                    $this->simulado_model->replace($sql_nota);
                else{
                    $erro = array('parametro' => $idsimulado, 
                                   'descricao' => 'Nota registrada não pode ser maior que a nota máxima do simulado',
                                   'funcao' => 'obterNotas');
                    $this->session->set_flashdata('erro', $erro);
                } 
            }
            foreach ($alunos_r as $aluno) {
                $sql_nota['aluno_idaluno'] = $aluno['idusuario'];
                $sql_nota['simulado_idsimulado'] = $idsimulado; 
                $sql_nota['valor_nota'] = $this->input->post("nota".$aluno['idusuario'])?str_replace(',', '.', $this->input->post("nota".$aluno['idusuario'])):null;
                if ($sql_nota['valor_nota']<=$simulado['nota_maxima_simulado'])
                    $this->simulado_model->replace($sql_nota);
                else{
                    $erro = array('parametro' => $idsimulado, 
                                   'descricao' => 'Nota registrada não pode ser maior que a nota máxima do simulado',
                                   'funcao' => 'obterNotas');
                    $this->session->set_flashdata('erro', $erro);
                } 
            }
            foreach ($alunos_t as $aluno) {
                $sql_id['idaluno'] = $aluno['idusuario'];
                $sql_id['idsimulado'] = $idsimulado; 
                $sql_nota['valor_nota'] = $this->input->post("nota".$aluno['idusuario'])?str_replace(',', '.', $this->input->post("nota".$aluno['idusuario'])):null;
                if ($sql_nota['valor_nota']<=$simulado['nota_maxima_simulado'])
                    $this->simulado_model->update($sql_id,$sql_nota);
                else{
                    $erro = array('parametro' => $idsimulado, 
                                   'descricao' => 'Nota registrada não pode ser maior que a nota máxima do simulado',
                                   'funcao' => 'obterNotas');
                    $this->session->set_flashdata('erro', $erro);
                } 
                
            }
        }
        else{
            $erro = array('parametro' => $idsimulado, 
                                   'descricao' => 'Ocorreram falhas na última ação.',
                                   'funcao' => 'obterNotas');
            $this->session->set_flashdata('erro', $erro);
        }
            
        $this->template->redirect('simulado');
    }


    public function remove($id){
        $this->load->model('simulado_model');
        $this->load->model('user_model');
        $this->load->model('turma_model');
        $this->load->model('oferta_model');
        
        $simulado = $this->simulado_model->get($id);
        
        $turma = $this->turma_model->getTurma($simulado['turma_idturma']);
        
        if ($this->session->userdata('level')==2){
            $oferta = $this->oferta_model->getWhere(array('idturma' => $turma['idturma'],'iddisciplina' => $simulado['disciplina_iddisciplina']));
            if ($oferta['monitor_idusuario']==$this->session->userdata('user')){
                $this->simulado_model->delete($id);
            }
        }
        else if ($this->session->userdata('level')>2){
            if ($turma['curso_unidade_idunidade']==$this->session->userdata('unidade')){
                $this->simulado_model->delete($id);
            }
        }
        $this->template->redirect('simulado');
    }
    
    public function configura()
    {
        $this->load->model('simulado_model');
        $this->load->model('aluno_model');
        $this->load->model('turma_model');
        $this->load->model('user_model');

        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $idsimulado = $this->input->post('idSimulado');
        $simulado = $this->simulado_model->get($idsimulado);
        
        $this->form_validation->set_rules('idSimulado','simulado','contains[simulado.idsimulado,turma.idturma,simulado.turma_idturma,turma.curso_unidade_idunidade,#'.$this->session->userdata('unidade').'#]');    
        if ($this->session->userdata('level')==2){
            $this->form_validation->set_rules('idSimulado','simulado','contains[simulado.idsimulado,oferta_disciplina.idturma,simulado.turma_idturma,oferta_disciplina.iddisciplina,simulado.disciplina_iddisciplina,oferta_disciplina.monitor_idusuario,#'.$this->session->userdata['user'].'#]');        
        }
        $this->form_validation->set_rules('data_simulado','Data do Simulado',array('required','regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}$/]','exact_length[10]','valid_date'));
        $this->form_validation->set_rules('peso_simulado','Peso',array('required','regex_match[/(([0-9])*\,([0-9])*)|([0-9]*)/]'));
        $this->form_validation->set_rules('descricao_simulado','Descrição','required');
        $this->form_validation->set_rules('nota_simulado','Nota Máxima',array('required','regex_match[/(([0-9])*\,([0-9])*)|([0-9]*)/]'));
        if ($this->form_validation->run()==TRUE){
            $simulado['peso_simulado'] = str_replace(',', '.', $this->input->post('peso_simulado'));
            $simulado['nota_maxima_simulado'] = str_replace(',', '.', $this->input->post('nota_simulado'));
            
            $date = DateTime::createFromFormat('d/m/Y', $this->input->post('data_simulado'));
            $simulado['data_simulado']=$date->format('Y-m-d'); 
            
            $simulado['descricao_simulado'] = $this->input->post('descricao_simulado');  
            unset($simulado['idsimulado']);
            $this->simulado_model->updateSimulado($idsimulado,$simulado);  
       }
       else{
            $erro = array('parametro' => $idsimulado.',"'.$this->input->post('data_simulado').'","'.$this->input->post('descricao_simulado').'","'.$this->input->post('peso_simulado').'","'.$this->input->post('nota_simulado').'"',
                                           'descricao' => 'Há erros no formulário',
                                           'funcao' => 'configurarSimulado');
            $this->session->set_flashdata('erro', $erro);
       }
        $this->template->redirect('simulado');         
    }

    
	public function historico()
	{
		$this->load->model('simulado_model');
		$this->load->model('user_model');
		$this->load->model('disciplina_model');
		$this->load->model('area_model');
		
		$data = $this->template->loadCabecalho('Histórico do Aluno');
	
		if ($this->session->userdata('level')==1){
			$aluno = $this->user_model->get($this->session->userdata('user'));
			$simulados = $this->simulado_model->getSimuladoTurmaAluno($aluno['idusuario']);
			foreach ($simulados as $key => $simulado) {
				if (isset($simulado['disciplina_iddisciplina'])){
					$adicional = $this->disciplina_model->getDisciplina($simulado['disciplina_iddisciplina']);
					$simulados[$key]['nome_adicional'] = $adicional['nome_disciplina'];
				}
				else if (isset($simulado['area_idarea'])){
					$adicional = $this->area_model->getArea($simulado['area_idarea']);
					$simulados[$key]['nome_adicional'] = $adicional['nome_area'];
				}
				else if (isset($simulado['grande_area_idgrande_area'])){
					$adicional = $this->area_model->getGrandeArea($simulado['grande_area_idgrande_area']);
					$simulados[$key]['nome_adicional'] = $adicional['descricao_grande_area'];
				}
			}
			$data['aluno'] = $aluno;
			$data['simulados'] = $simulados;
			$this->template->show('historico',$data);
			
		}
		else
			$this->template->redirect('inicio');
	}

}

?>
