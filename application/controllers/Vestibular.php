<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vestibular extends CI_Controller {
    
    function Vestibular() {
        parent::__construct();
        if(!$this->session->userdata('logged'))
            $this->template->redirect('login');
        $this->template->controle_acesso($this->router->fetch_method(),$this->router->fetch_class());
     }
    
    
    public function criaFaculdade()
    {
        $this->load->model('faculdade_vestibular_model');
        
        $sql_faculdade = array('nomeFaculdade_vestibular' => $this->input->post('nome_faculdade'),
                               'tipoFaculdade' => $this->input->post('tipo_faculdade'),
                               'publica' => $this->input->post('publica'),
                                );

        $this->faculdade_vestibular_model->create($sql_faculdade);  
        
        $faculdades = $this->faculdade_vestibular_model->getFaculdades();
        
        foreach ($faculdades as $faculdade) {
            $drop_faculdade[$faculdade['idFaculdade']] = $faculdade['nomeFaculdade_vestibular'];
        } 
        
        $data['err'] = 'ok';
        $data['faculdade_drop'] = $drop_faculdade;
        
        echo json_encode($data);
            
        return;
    }
    
    
    public function criaCurso()
    {
        $this->load->model('curso_vestibular_model');
        
        $sql_curso = array('nomeCurso' => $this->input->post('nome_curso')
                                );
        $this->curso_vestibular_model->create($sql_curso);  
        
       $cursos = $this->curso_vestibular_model->getCursos();
       
        foreach ($cursos as $curso) {
            $drop_curso[$curso['idCurso']] = $curso['nomeCurso'];            
        }
        
        $data['err'] = 'ok';
        $data['curso_drop'] = $drop_curso;
        
        echo json_encode($data);
            
        return;
    }
    
}
?>