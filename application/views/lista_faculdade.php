<?php $this->template->menu($view) ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3><b>Lista de Faculdades</b></h3>
            
            <table id="faculdades" class="table table-hover">
                <thead>
                    <tr>
                        <th>Nome da Faculdade</th>
                        <th>Cidade da Faculdade</th> 
                        <th>Opções</th>
                    </tr>   
                </thead>
                <?php foreach ($faculdades as $faculdade) { ?>
                <?php $modal = str_replace(' ', '', $faculdade['nome_faculdade']) ?>
                <tr class="animated fadeInDown">
                    <td><?php echo $faculdade['nome_faculdade'];?></td>
                    <td><?php echo $faculdade['cidade_faculdade']; ?></td>           
                    <td>
                        <a href="<?php echo base_url('index.php/faculdade/edita'); echo '/'.$faculdade['idfaculdade'] ?>" data-toggle="tooltip" data-placement="top" title="Editar"><button type="button" class="btn btn-default"><i class="fa fa-pencil-square-o"></i></button></a>
                        &ensp;<a href="<?php echo base_url('index.php/departamento/lista'); echo '/'.$faculdade['idfaculdade'] ?>" data-toggle="tooltip" data-placement="top" title="Departamentos"><button type="button" class="btn btn-primary"><i class="fa fa-building" aria-hidden="true"></i></button></a>
                        &ensp;<a href="<?php echo base_url('index.php/curso_faculdade/busca'); echo '/'.$faculdade['idfaculdade'] ?>" data-toggle="tooltip" data-placement="top" title="Cursos"><button type="button" class="btn btn-primary"><i class="fa fa-graduation-cap" aria-hidden="true"></i></button></a>
                        
                        &ensp;<a href="#" data-toggle="tooltip" data-placement="top" title="Remover"><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#<?php echo $modal; ?>"><i class="fa fa-trash"></i></button></a>
                     </td>
                </tr>
                <div class="modal fade" id="<?php echo $modal;?>" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title text-danger" id="myModalLabel" align="CENTER">Atenção <i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i></h3>
                            </div>
                            <div class="modal-body">
                                <p align="CENTER">Você está prestes a remover a Faculdade: </p> 
                                <h4 align="CENTER"><?php echo $faculdade['nome_faculdade'];?></h4>
                                <p>Os seguintes itens relacionados também serão afetados:</p>
                                <ul>
                                    <li>Todas as unidades desta faculdade serão removidas.</li>
                                    <li>Tudo relacionado a estas unidades serão removidas</li>
                                </ul>
                                <p class="text-danger">As alterações acima citadas são irreversíveis.</p>
                                <h4 align="CENTER">Está certo disto?</h4>                               
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                                <button type="button" class="btn btn-danger"  onclick="location.href='<?php echo base_url(); ?>index.php/faculdade/remove/<?php echo $faculdade['idfaculdade'] ?>'">Sim, remover a Faculdade</button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </table>    
        </div>
        <div class="col-md-1 col-md-offset-9">
            <button class="btn btn-default" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>          
    </div>
</div>

<script type="text/javascript">


$(document).ready(function () {
    tabela('faculdades');
    $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/faculdade/opcoes";  
    });
}); 
</script>

<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<?php if(isset($msg)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $msg;?>");</script>
<?php }?>

