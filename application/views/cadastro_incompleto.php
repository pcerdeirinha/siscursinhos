<?php $this->template->menu($view) ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">            
                <?php echo form_open('usuario/conclui_cadastro'); ?>      
                <h3><b>Finalização do Cadastro</b></h3>
                <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#dadosGerais">Dados Gerais</a></li>
                </ul>                
        </div>
                
        <div class="tab-content">
            <div id="dadosGerais" class="tab-pane fade in active">                
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('email_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Email', 'email'); ?>
                        <?php echo form_input('email_usuario', set_value('email_usuario')?set_value('email_usuario'):$email_usuario, 'type="email", class="form-control" id="email" placeholder="Email"'); ?> 
                        <?php if (!(form_error('email_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('email_usuario'); ?></span>
                    </div>
                </div> 
            </div>
        </div>
        <div class="col-md-1 col-md-offset-8">
            <div class="form-save-buttons">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="col-md-1">
            <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>
    </div>
</div>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>

 