<?php $this->template->menu($view) ?>

<div class='container'>
    <div class="row">       
        <div class="col-md-8 col-md-offset-2">
        <center>        
        <h3><b>Busca de Aulas</b></h3>                 
        </center>
        </div>
        <div class="modal fade" id="ModalRemove" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title text-danger" id="myModalLabel" align="CENTER">Atenção <i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i></h3>
                </div>
                <div class="modal-body">
                    <p align="CENTER">Você está prestes a remover uma Aula: </p> 
                    <p>Os seguintes itens relacionados também serão afetados:</p>
                    <ul>
                        <li>Todas as faltas e presenças de alunos desta aula serão removidas.</li>
                    </ul>
                    <p class="text-danger">As alterações acima citadas são irreversíveis.</p>
                    <h4 align="CENTER">Está certo disto?</h4>                               
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    <button type="button" class="btn btn-danger"  id = "btn_del" >Sim, remover a Aula</button>
                </div>
            </div>
        </div>
    </div>      
        <div class="col-md-4 col-md-offset-2">
            <div class="form-group">                        
                <?php echo form_label('Monitor', 'nome_monitor'); ?>
                <?php echo form_dropdown('nome_monitor',$nome_monitor, set_value('nome_monitor')?set_value('nome_monitor'):$nome_monitor_sel, 'type="text" min="2", class="form-control" id="periodo" placeholder="Nome do Monitor"'); ?>                               
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">                  
                <?php echo form_label('Oferta', 'disciplina_oferta'); ?>
                <?php echo form_dropdown('disciplina_oferta',$disciplina_oferta, set_value('disciplina_oferta')?set_value('disciplina_oferta'):$disciplina_oferta_sel, 'type="text" min="2", class="form-control" id="periodo" placeholder="Nome do Monitor"'); ?>                               
            </div>
        </div>              
    </div>
    <div class="row">
        <br>
        <div class="col-md-4 col-md-offset-4">
            <div class="form-group"> 
                <button type="submit" class="btn btn-primary btn-block" id="buscar"><i class="fa fa-search"></i> Buscar</button>
                <button class="btn btn-default btn-block" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
            </div>          
        </div>                  
    </div>
    <div class="row">       
        <div class="col-md-8 col-md-offset-2">
            <div id="tabela_aulas">            
            </div>          
        </div>
    </div>
    <div class="modal fade" id="ModalFrequencia" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="myModalLabel" align="CENTER">Registro de Frequência</h3>
                </div>
                <div class="modal-body">
                    <?php echo form_open('aula/frequencia'); ?>
                    <?php echo form_hidden('idAula') ?>
                    
                    
                        <div class="row">       
                            <div class="col-md-8 col-md-offset-2">
                                <div id="tabela_alunos" class="pre-scrollable">            
                                </div>          
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    <button type="submit" class="btn btn-primary">Registrar</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div> 
    <div class="modal fade" id="ModalCria" tabindex="-1" role="dialog" aria-labellby="myModalLabel" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="myModalLabel" align="CENTER">Nova Aula</h3>
                </div>
                <div class="modal-body">
                    <?php echo form_open('aula/cria',array('id' => 'cria_form')); ?>
                    	<div class="form_group">
                    	    <?php echo form_label('Monitor', 'nome_monitor'); ?>
                            <?php echo form_dropdown('nome_monitor',$nome_monitor, set_value('nome_monitor')?set_value('nome_monitor'):$nome_monitor_sel, 'type="text" min="2", class="form-control" id="periodo" placeholder="Nome do Monitor"'); ?>
                    	</div>
                    	<div class="form-group">                  
                            <?php echo form_label('Disciplina - Turma', 'disciplina_oferta'); ?>
                            <?php echo form_dropdown('disciplina_oferta',$disciplina_oferta, set_value('disciplina_oferta')?set_value('disciplina_oferta'):$disciplina_oferta_sel, 'type="text" min="2", class="form-control" id="periodo" placeholder="Nome do Monitor"'); ?>                               
                        </div>
                        <div class="form-group <?php if (!(form_error('data_aula')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Data da aula', 'data_aula'); ?> 
                            <?php echo form_input('data_aula', set_value('data_aula')?set_value('data_aula'):$data_aula, 'type="date", class="form-control" id="data_aula" placeholder="Data da Aula" tipo="data"'); ?> 
                            <?php if (!(form_error('data_aula')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('data_aula'); ?></span>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Conteúdo da aula', 'conteudo_aula'); ?> 
                            <?php echo form_textarea('conteudo_aula', set_value('conteudo_aula')?set_value('conteudo_aula'):$conteudo_aula, 'type="date", class="form-control" id="conteudo_aula" placeholder="Conteúdo da aula"'); ?> 
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    <button type="submit" class="btn btn-primary">Registrar</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalCopia" tabindex="-1" role="dialog" aria-labellby="myModalLabel" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="myModalLabel" align="CENTER">Copiar Frequência</h3>
                </div>
                <div class="modal-body">
                    <?php echo form_open('aula/copia_frequencia',array('id' => 'copia_form')); ?>
                    <?php echo form_hidden('aula_copiada'); ?>
                        <div class="form-group">                  
                            <?php echo form_label('Disciplina - Turma', 'aula_copia'); ?>
                            <?php echo form_dropdown('aula_copia',$aula_copia, set_value('aula_copia')?set_value('aula_copia'):$aula_copia_sel, 'type="text" min="2", class="form-control" id="periodo" placeholder="Nome da Atribuição"'); ?>                               
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    <button type="submit" class="btn btn-primary">Registrar</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>   
    <div class="col-md-1 col-md-offset-2" id="div_novaAula" >
        <div class="form-save-buttons">
            <button class="btn btn-primary" onclick="carregar();"  data-toggle="modal" type="button" id="btnNovaAula" data-target="#ModalCria"><i class="fa fa-plus"></i> Nova Aula</button>
        </div>
    </div> 
</div>
<script type="text/javascript">

function carregar () {
    $('[name=idOferta]').val($('option:selected', "[name=disciplina_oferta]").val());
    $('[name=oferta_aula]').val($('option:selected', "[name=disciplina_oferta]").html());
  
}

function obterFrequencia (idAula) { 
    $("#tabela_alunos").html('');
    
   jQuery.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>" + "index.php/aula/buscaAlunos",
       dataType: 'json',
       data: {idAula:idAula},
       success: function(res) {
           console.log(res.alunos);
        rows = '';
        rows = $('<table class="table table-hover" id="alunos">');
        rows.append('<thead><tr><th>Nome</th>\
        <th>Presente - Todos como <div class="btn-group" data-toggle="buttons">\
                                    <label class="btn btn-secondary active">\
                                        <input type="radio" name="todos" id="Sim" autocomplete="off" value ="Sim" checked> Sim\
                                    </label>\
                                    <label class="btn btn-secondary">\
                                        <input type="radio" name="todos" id="Nao" autocomplete="off" value = "Nao"> Não\
                                    </label>\
                               </div>\
        </th></tr></thead><tbody>');
        if(res.err == "ok"){
                $('[name=idAula]').val(idAula);                   
                for(var i in res.alunos){
                    var newRow = $('<tr class="animated fadeInDown">');
                    var cols = "";
                    cols +='<td style = "vertical-align:middle">'+res.alunos[i].nome_aluno+'</td>';
                    if (res.alunos[i].falta_aluno==0){                           
                    cols +='<td>\
                               <div class="btn-group" data-toggle="buttons">\
                                    <label id="lbl_Sim" class="btn btn-secondary active">\
                                        <input type="radio" name="Presenca'+i+'" id="Sim" autocomplete="off" value ="Sim" checked> Sim\
                                    </label>\
                                    <label id="lbl_Nao" class="btn btn-secondary">\
                                        <input type="radio" name="Presenca'+i+'" id="Nao" autocomplete="off" value = "Nao"> Não\
                                    </label>\
                               </div>\
                           </td>';
                    }
                    else {
                   cols +='<td>\
                               <div class="btn-group" data-toggle="buttons">\
                                    <label id="lbl_Sim" class="btn btn-secondary">\
                                        <input type="radio" name="Presenca'+i+'" id="Sim" autocomplete="off" value ="Sim"> Sim\
                                    </label>\
                                    <label id="lbl_Nao" class="btn btn-secondary active">\
                                        <input type="radio" name="Presenca'+i+'" id="Nao" autocomplete="off" value = "Nao" checked> Não\
                                    </label>\
                               </div>\
                           </td>';
                   }     
                   
                    newRow.append(cols);                            
                    rows.append(newRow);                                                
                }
                cols+='</tbody></table>';
                $("#tabela_alunos").html(rows);
                /*$("#alunos").DataTable({            
                    "info" : false,
                    "searching": false,
                    "lengthChange": false,
                    "paging" : false,
                    "scr" : true,
                    "language":{
                        "emptyTable": "Nenhum dado encontrado.",
                        "search":     "Pesquisar"
                    }
                });*/
                $("#ModalFrequencia").modal({show: true});

           }else{
            $('[name=idAula]').val('');
            mensagem('error',res.err);
            $("#tabela_alunos").html('');
            $("#ModalFrequencia").modal({show: false});
           }
       }
   }); 
  
}

function removeAula($id) {
     document.getElementById('btn_del').onclick = function () {
         window.location = "<?php echo base_url(); ?>" + "index.php/aula/remove/" + $id;
     };
     $("#ModalRemove").modal({show: true});
}

function alteraAula(id_aula, id_oferta, data_aula, conteudo_aula){
    $('<input>').attr('type','hidden').attr('name','id_aula').appendTo('#cria_form');
    $('[name=id_aula]').val(id_aula);
    $('[name=disciplina_oferta]').val(id_oferta);
    $('[name=data_aula]').val(data_aula);
    $('[name=conteudo_aula]').val(conteudo_aula);
    $("#ModalCria").modal({show: true});
}


function copiarFrequencia(id_oferta,data_aula,id_aula){
    $('[name=aula_copiada]').val(id_aula);
    jQuery.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "index.php/aula/getAulasFrequentadasTurmaDia",
           dataType: 'json',
           data: {id_oferta:id_oferta,data_aula:data_aula},
           success: function(res) {
               console.log(res);
               var row = "";
               for (var i in res.aulas){
                    row+='<option value="' + res.aulas[i].idaula + '">'+res.aulas[i].nome_disciplina+' - '+ res.aulas[i].nome_usuario+'</option>';    
               }
               $("[name=aula_copia]").html(row);
               if (row!="")
                    $("#ModalCopia").modal({show: true});
               else {
                    mensagem('error',"Não existe nenhuma aula no dia com frequência registrada");  
               }
           }
       });
    
}

function carregarAulas () {           
    var idOferta = $("[name=disciplina_oferta]").val(); 
    jQuery.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "index.php/aula/busca",
           dataType: 'json',
           data: {idOferta:idOferta},
           success: function(res) {
               console.log(res);
               
            var rows = $('<table id="tb_aulas" class="table table-hover">');
            rows.append('<thead><tr><th>Data</th>\
            <th>Conteúdo</th>\
            <th>Opções</th></tr></thead>');
            if(res.err == "ok"){                   
                    for(var i in res.aulas_ofertadas){
                        var newRow = $('<tr class="animated fadeInDown">');
                        var cols = "";
                        cols +='<td>'+res.aulas_ofertadas[i].data_aula+'</td>';
                        cols +='<td>'+res.aulas_ofertadas[i].descricao_aula+'</td>'; 
                        if (res.aulas_ofertadas[i].frequencia_lancada==0)
                            cols += '<td><button type="button" class="btn btn-primary" data-toggle="modal" onclick="obterFrequencia('+i+')"  title="Registrar Frequência"><i class="fa fa-th-list"></i></button>';
                        else cols += '<td><button type="button" class="btn btn-warning" data-toggle="modal" onclick="obterFrequencia('+i+')" title="Editar Frequência"><i class="fa fa-th-list" aria-hidden="true"></i></button>';
                        cols += '&nbsp<button type="button" class="btn btn-warning" onclick="alteraAula('+i+','+res.aulas_ofertadas[i].id_oferta+',\''+res.aulas_ofertadas[i].data_aula+'\',\''+res.aulas_ofertadas[i].descricao_aula+'\')"  title="Alterar Aula"><i class="fa fa-pencil-square-o"></i></button>';
                        cols += '&nbsp<button type="button" class="btn btn-default" onclick="copiarFrequencia('+res.aulas_ofertadas[i].id_oferta+',\''+res.aulas_ofertadas[i].data_aula+'\','+i+')"  title="Copiar Frequência"><i class="fa fa-files-o" aria-hidden="true"></i></button>';
                        cols += '&nbsp<button type="button" class="btn btn-danger" onclick="removeAula('+i+');"  title="Remove Aula"><i class="fa fa-trash"></i></button></td>';
                        newRow.append(cols);                            
                        rows.append(newRow);                                                
                    }
                    rows.append("</table>");
                    $("#tabela_aulas").html(rows);
                    tabela('tb_aulas');
               }else{
                mensagem('error',res.err);
                $("#tabela_aulas").html('');
               }
            
           }
       });
}


$(document).on('change', 'input:radio[name="todos"]', function (event) {
	if ($('input:radio[name="todos"]:checked').val()=='Nao'){
		$('input#Sim').removeAttr("checked");
		$('input#Nao').attr("checked",true); 
		$('label#lbl_Nao').attr("class","btn btn-secondary active");
		$('label#lbl_Sim').attr("class","btn btn-secondary");
	}
	else{
		$('input#Nao').removeAttr("checked");
		$('input#Sim').attr("checked",true); 
		$('label#lbl_Sim').attr("class","btn btn-secondary active");
		$('label#lbl_Nao').attr("class","btn btn-secondary");
	}
	
});


$(document).ready(function () {
    $('#ModalCria').on('hidden.bs.modal', function (e) {
        $('[name=id_aula]').remove();
    });
    <?php 
    if (set_value('id_aula')!="") {
    ?>   
        $('<input>').attr('type','hidden').attr('name','id_aula').appendTo('#cria_form');
        $('[name=id_aula]').val(<?php echo set_value('id_aula');?>);
    <?php 
    }
    ?>    
       
    mascara();
    data();
    if ($('option:selected', "[name=disciplina_oferta]").val() == 0){
    var idMonitor= $('option:selected', "[name=nome_monitor]").val();
        jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/aula/buscaOfertas",
                  dataType: 'json',
                  data: {idMonitor: idMonitor},
                  success: function(res) {
                    console.log(res.ofertas_monitor);
                    var row ='';      
                    for(var i in res.ofertas_monitor){
                       row+='<option value="' + i + '">'+res.ofertas_monitor[i]+'</option>';
                    }
                    $("[name=disciplina_oferta]").html(row);
                  }
              });  
     }   
     else carregarAulas();
     <?php 
        if ($exibir==true)
            echo '$("#ModalCria").modal({show: true});';
    
    ?>
    $("[name=nome_monitor]").change(function(event) {
        event.preventDefault(); 
        var idMonitor= $('option:selected', this).val();
        jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/aula/buscaOfertas",
                  dataType: 'json',
                  data: {idMonitor: idMonitor},
                  success: function(res) {
                    console.log(res.ofertas_monitor);
                    var row ='';      
                    for(var i in res.ofertas_monitor){
                       row+='<option value="' + i + '">'+res.ofertas_monitor[i]+'</option>';
                                             
                    }
                    $("[name=disciplina_oferta]").html(row);
                  }
              });
    }); 
       

    
    
    
    
    mascara();
    $('[data-toggle="tooltip"]').tooltip();     
    $("#buscar").click(function(event){
        event.preventDefault();   
        carregarAulas();  
    });
	$("#voltar").click(function(event){
		<?php if ($view==2) {?>
		window.location.href = "<?php echo base_url(); ?>"+"index.php/professor"; 
		<?php }else if ($view==3) {?>
		window.location.href = "<?php echo base_url(); ?>"+"index.php/secretario";  
		<?php }else if ($view==4){?>
		window.location.href = "<?php echo base_url(); ?>"+"index.php/coordenador"; 
		<?php }else if ($view==5){?>
		window.location.href = "<?php echo base_url(); ?>"+"index.php/docente"; 
		<?php }?>
	});
});
</script>