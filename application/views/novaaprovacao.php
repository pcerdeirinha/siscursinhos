<?php $this->template->menu($view) ?>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
               <?php echo form_open('aprovacao/cria'); ?>
               <?php echo form_hidden('idaluno',$idaluno?$idaluno:set_value('idaluno')) ?>
                <?php if(isset($idaprovacao)||(set_value('idaprovacao')!='')){/*Então é Update*/?>
                            <h3><b>Edita Aprovação</b></h3>
                            <br>
                            <?php echo form_hidden('idaprovacao', $idaprovacao?$idaprovacao:set_value('idaprovacao')); 
                        }else{ ?>
                            <h3><b>Nova Aprovação</b></h3>                    
                            <br>
                <?php } ?> 
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#dadosAprovacao">Dados da Aprovação</a></li>
            </ul>
        </div>

        <div class="tab-content">
            <div id="dadosAprovacao" class="tab-pane fade in active">
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('nome_aluno')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Nome do Aluno', 'nome_aluno'); ?>
                        <?php echo form_input('nome_aluno', set_value('nome_aluno')?set_value('nome_aluno'):$nome_aluno, 'type="text", class="form-control" id="nome" placeholder="Nome da Aluno" readonly'); ?> 
                        <?php if (!(form_error('nome_aluno')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('nome_aluno'); ?></span>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-0">
                    <div class="form-group <?php if (!(form_error('curso_faculdade')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Curso Aprovado', 'curso_faculdade'); ?><br>
                        <div class="input-group">
                            <?php echo form_dropdown('curso_faculdade',$cursos_faculdade_drop,set_value('curso_faculdade')?set_value('curso_faculdade'):$curso_faculdade, 'type="text" min="2", class="form-control" id="curso" placeholder="Curso"'); ?>
                            <div class="input-group-btn">
                                    <button class="btn-secondary btn" type="button" id="btnNovoCurso" data-toggle="modal" data-target="#ModalCriaCurso">Novo Curso</button>
                           </div>                        
                        </div>
                        <?php if (!(form_error('curso_faculdade')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('curso_faculdade'); ?></span>
                    </div>
                </div>
                <div class="col-md-5 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('faculdade_aprovada')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Vestibular Aprovado', 'faculdade_aprovada'); ?><br>
                        <div class="input-group">
                            <?php echo form_dropdown('faculdade_aprovada',$faculdade_drop,set_value('faculdade_aprovada')?set_value('faculdade_aprovada'):$faculdade_aprovada, 'type="text" min="2", class="form-control" id="curso" placeholder="Curso"'); ?>
                            <div class="input-group-btn">
                                    <button class="btn-secondary btn" type="button" id="btnNovaFaculdade" data-toggle="modal" data-target="#ModalCriaFaculdade">Novo Vestibular</button>
                           </div>
                       </div>
                            <?php if (!(form_error('faculdade_aprovada')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('faculdade_aprovada'); ?></span>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-0">
                    <div class="form-group <?php if (!(form_error('data_aprovacao')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Data do Vestibular', 'data_aprovacao'); // Verificar os nomes ?> 
                        <?php echo form_input('data_aprovacao', set_value('data_aprovacao')?set_value('data_aprovacao'):$data_aprovacao, 'type="date", class="form-control" id="data_aprovacao" placeholder="Data de Aprovação" tipo="data"'); ?> 
                        <?php if (!(form_error('data_aprovacao')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('data_aprovacao'); ?></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1 col-md-offset-2">
            <div class="form-save-buttons">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
            </div>
        </div> 
        <?php echo form_close(); ?>
        <div class="col-md-1">
            <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>
        <div class="modal fade" id="ModalCriaCurso" tabindex="-1" role="dialog" aria-labellby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel" align="CENTER">Novo Curso</h3>
                    </div>
                    <div class="modal-body">
                            <div class="form-group">
                                <?php echo form_label('Nome do Curso', 'nome_curso'); ?> 
                                <?php echo form_input('nome_curso',set_value('nome_curso') , 'type="date", class="form-control" id="nome_curso" placeholder="Curso"'); ?> 
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <button type="button" class="btn btn-primary" id="btnCriaCurso">Registrar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="ModalCriaFaculdade" tabindex="-1" role="dialog" aria-labellby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel" align="CENTER">Novo Vestibular</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <?php echo form_label('Nome do Vestibular', 'nome_faculdade'); ?> 
                            <?php echo form_input('nome_faculdade',set_value('nome_faculdade') , 'type="date", class="form-control" id="nome_faculdade" placeholder="Faculdade"'); ?> 
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Tipo da Faculdade', 'tipo_faculdade'); ?> 
                            <?php echo form_dropdown('tipo_faculdade',$tipos_faculdade,set_value('tipo_faculdade'), 'type="date", class="form-control" id="tipo_faculdade" placeholder="Tipo"'); ?> 
                        </div>
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                <input type="radio" name="escola" id="optionsRadios1" value="0" checked>
                                Particular
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                <input type="radio" name="escola" id="optionsRadios2" value="1">
                                Pública
                                </label>
                            </div>                   
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <button type="button" class="btn btn-primary" id="btnCriaFaculdade">Registrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

function criaFaculdade(){
    jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/vestibular/criaFaculdade",
                  dataType: 'json',
                  data: {nome_faculdade: $("#nome_faculdade").val(),tipo_faculdade: $("#tipo_faculdade option:selected").val(),publica:$('input[name=escola]:checked').val()},
                  success: function(res) {
                     console.log(res.err);
                    if (res.err == 'ok')
                        var row ='';      
                        for(var i in res.faculdade_drop){
                                row+='<option  value = "'+i+'">'+res.faculdade_drop[i]+'</option>';
                            }
                         $("[name=faculdade_aprovada]").html(row);
                         $('#ModalCriaFaculdade').modal('toggle');
                         
                  }
         });
}


function criaCurso(){
    jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/vestibular/criaCurso",
                  dataType: 'json',
                  data: {nome_curso: $("#nome_curso").val()},
                  success: function(res) {
                     console.log(res.err);
                    if (res.err == 'ok')
                        var row ='';      
                        for(var i in res.curso_drop){
                                row+='<option  value = "'+i+'">'+res.curso_drop[i]+'</option>';
                            }
                         $("[name=curso_faculdade]").html(row);
                         $('#ModalCriaCurso').modal('toggle');
                         
                  }
         });
}


$(document).ready(function () {
    mascara();
    data();
    
    $("#btnCriaFaculdade").click(function(event){
        event.preventDefault(); 
        criaFaculdade();
    });
    $("#btnCriaCurso").click(function(event){
        event.preventDefault(); 
        criaCurso();
    });
    
    $("#voltar").click(function(event){
        window.location.href = "<?php echo base_url(); ?>";  
    });
}); 
</script>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
