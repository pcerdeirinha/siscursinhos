<?php $this->template->menu($view) ?>
<div class="container">
    <center>
    <div class="panel panel-default">
   	<div class="table-responsive">
	    <table class="table table-striped">
	        <tr>
	            <th>Nome do Aluno</th>
	            <th>CPF do Aluno</th>
	            <th>Status da Matrícula Mais Recente</th>
	            <th>Curso</th>
	            <th>Turma</th>
	            <th>Opções</th>
	        </tr>
	        <?php foreach ($useralunos as $aluno) { ?>
	
	        <td><?php echo $aluno['nome_usuario'];?></td>
	        <td><?php echo $aluno['cpf_usuario'];?></td>
	        <td>
	        <?php 
	        if(isset($status_matricula[$aluno['idusuario']])){
	            if($status_matricula[$aluno['idusuario']]){
	                echo 'Ativa';
	            }else{
	                echo $obs_matricula[$aluno['idusuario']];
	            }
	        }else{
	            echo 'Sem Matrícula';
	        }
	        ?></td>
	        <td><?php
	        if(isset($status_matricula[$aluno['idusuario']])){
	               echo $cursos_drop[$turmacurso[$turma_matriculada[$aluno['idusuario']]]];
	        }else echo '-----' ?>
	        </td>
	        <td><?php
	        if(isset($status_matricula[$aluno['idusuario']])){
	           echo $turmas[$turma_matriculada[$aluno['idusuario']]];
	        }else echo '-----' ?>
	        </td>
	        <td>
	            <?php echo anchor('aluno/edita/'.urlencode(base64_encode($aluno['idusuario'])), '<i class="fa fa-pencil-square-o"> Editar </i>'); ?>&ensp;&ensp;
	            <?php echo anchor('aluno/remove/'.urlencode(base64_encode($aluno['idusuario'])), '<i class="fa fa-times"> Remover </i>'); ?>&ensp;&ensp;
	            <?php echo anchor('aluno/matricula/'.urlencode(base64_encode($aluno['idusuario'])), '<i class="fa fa-pencil-square-o"> Matricular </i> '); ?>
	        </td>
	        </tr>
	        <?php } ?>
	    </table>
    </div>
    </div>
    </center>
</div>
<?php if(isset($err)){?>

<script> alert("<?php echo $err;?>")</script>
<?php } ?>
