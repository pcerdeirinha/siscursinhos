<?php $this->template->menu($view) ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3><b>Lista de Cursos</b></h3>
            
            <table id="cursos" class="table table-hover">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Opções</th>
                    </tr>   
                </thead>
                <?php foreach ($cursos_faculdade as $curso) { ?>
                <tr class="animated fadeInDown">
                    <td><?php echo $curso['nome_curso'];?></td>
                    <td>
                        <a href="<?php echo base_url('index.php/curso_faculdade/edita'); echo '/'.$curso['idcurso_faculdade'] ?>" data-toggle="tooltip" data-placement="top" title="Editar"><button type="button" class="btn btn-default"><i class="fa fa-pencil-square-o"></i></button></a>
                        &ensp;<a href="<?php echo base_url('index.php/curso_faculdade/remove'); echo '/'.$curso['idcurso_faculdade'] ?>" data-toggle="tooltip" data-placement="top" title="Remover"><button type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button></a>
                    </td>
                </tr>
                <?php } ?>
            </table>    
        </div>
        <div class="col-md-1 col-md-offset-2" >
            <div class="form-save-buttons">
                <button class="btn btn-primary" onclick="window.location='../novo/<?php echo $idfaculdade; ?>'"  data-toggle="modal" type="button" id="btnNovaAula" data-target="#ModalCria"><i class="fa fa-plus"></i> Novo Curso</button>
            </div>
        </div> 
        
        <div class="col-md-1 col-md-offset-9">
            <button class="btn btn-default" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>          
    </div>
</div>

<script type="text/javascript">


$(document).ready(function () {
    tabela('cursos');
    $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/faculdade/busca";  
    });
}); 
</script>

<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<?php if(isset($msg)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $msg;?>");</script>
<?php }?>

