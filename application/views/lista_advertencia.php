<?php $this->template->menu($view) ?>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h3><b>Suas Advertências</b></h3>
			
			<table id="adv_user" class="table table-bordered">
				<thead>
					<tr class="warning">
						<th>Pontos</th>
						<th>Descrição da advertência</th>	
						<th>Data</th>				
					</tr>	
				</thead>
				<?php 
				$pts_total = 0;
				if (count($advertencias)>0){
    				foreach ($advertencias as $advertencia) { ?>
    				<tr class="animated fadeInDown">
    					<td><center><?php echo $advertencia['pontos_advertencia'];?></center></td>
    					<td><?php echo $advertencia['descricao_advertencia']; ?></td>
    					<td><?php echo date("d/m/Y", strtotime($advertencia['data_advertencia'])); ?></td>
    				</tr>
    				<?php 
    					$pts_total += $advertencia['pontos_advertencia'];
    				}
                }
                else echo "<center><h3>Parabéns! Você não possui nenhuma advertência.</h3></center>" 
                 ?>
			</table>
			<div class="alert alert-danger" role="alert">Total de pontos: <?php echo $pts_total ?></div>
		</div>		
	</div>
</div>


<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<?php if(isset($msg)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $msg;?>");</script>
<?php }?>

