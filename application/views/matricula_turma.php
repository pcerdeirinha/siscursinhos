<?php $this->template->menu($view) ?>
<div class='container'>
	<div class="row">		
		<div class="col-md-8 col-md-offset-2">
		<center>		
		<h3><b>Inscrições de Alunos por Turma</b></h3>					
		</center>
		</div>		
		<div class="col-md-3 col-md-offset-2">
			<div class="input-group">
				<span class="input-group-addon" id="cpf-addon">CPF:</span>			    
			    <input type="text" class="form-control" id="cpf" placeholder="CPF" name="cpf" tipo="cpf">
			</div>
		</div>
		<div class="col-md-4">
			<div class="input-group">
				<span class="input-group-addon" id="nome-addon">Nome:</span>			   	
			   	<input type="text" class="form-control" id="nome" placeholder="Nome" name="nome">
			</div>
		</div>	
		<div class="col-md-1">
			<div class="input-group">			   	
			   	<input type="checkbox" id="concluinte" placeholder="Nome" name="concluinte" value = true> Concluintes
			</div>
		</div>				
	</div>
	<div class="row">
		<br>
		<div class="col-md-4 col-md-offset-4">
			<div class="form-group"> 
				<button type="submit" class="btn btn-primary btn-block" id="buscar"><i class="fa fa-search"></i> Buscar</button>				
			</div>			
		</div>					
	</div>
	<div class="row">		
		<div class="col-md-8 col-md-offset-2" id="tabela_alunos">					
		</div>
		<div class="col-md-8 col-md-offset-2">
			<center>		
			<h3><b>Alunos Selecionados</b></h3>					
			</center>			
		</div>
		<div class="col-md-8 col-md-offset-2">			
			<table id="alunossel" class="table table-hover table-bordered">
				<thead>
						<tr>
							<th>Nome</th>
							<th>CPF</th>
							<th>Opções</th>
						</tr>
				</thead>				
				<tbody>					
				</tbody>				
			</table>						
		</div>
		<div class="col-md-4 col-md-offset-4">
			<div class="form-group"> 
				<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#confirmar"><i class="fa fa-user-plus"></i> Inscrever Alunos</button>				
			</div>			
		</div>
		<div class="modal fade" id="confirmar" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="myModalLabel" align="CENTER">Confirmar Inscrições</h3>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12"><p>Selecione o curso, a turma e a data que deseja inscrever os alunos selecionados:</p></div> 
							<div class="col-md-4">
								<div class="form-group">                        
								    <?php echo form_label('Curso', 'nome_curso'); ?>
								    <?php echo form_dropdown('nome_curso',$nome_curso, set_value('nome_curso')?set_value('nome_curso'):$nome_curso_sel, 'type="text" min="2", class="form-control" id="curso" placeholder="Curso"'); ?>                               
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">                  
								    <?php echo form_label('Turma', 'nome_turma'); ?>
								    <?php echo form_dropdown('nome_turma',$nome_turma, set_value('nome_turma')?set_value('nome_turma'):$nome_turma_sel, 'type="text" min="2", class="form-control" id="turma" placeholder="Turma"'); ?>
								</div>							
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<?php echo form_label('Data da Inscrição', 'data_matricula'); ?> 
									<?php echo form_input('data_matricula',  $data_matricula, 'type="date", class="form-control" id="data_matricula" placeholder="Data matricula" tipo="data"'); ?> 
								</div>
							</div>
							<div class="col-md-12"><p class="text-warning" id="mensagemVagas"></p></div> 
						</div>																			
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
						<button type="button" class="btn btn-primary" id="matricular">Confirmar Inscrições</button>
					</div>
				</div>
			</div>
		</div>			
	</div>
		
</div>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<?php if(isset($msg)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $msg;?>");</script>
<?php }?>
		

<script type="text/javascript">


$(document).ready(function () {	
	
	
	mascara();
	data(true);
	var searching;
	var selected = [];	
	var t = tabela('alunossel',2);
	var p = tabela('alunos',2);
	$('[data-toggle="tooltip"]').tooltip();
	var idCurso= $('option:selected', "[name=nome_curso]").val();
	    jQuery.ajax({
	              type: "POST",
	              url: "<?php echo base_url(); ?>" + "index.php/turma/getTurmaCurso",
	              dataType: 'json',
	              data: {idcurso: idCurso},
	              success: function(res) {	                
	                var row ='';      
	                for(var i in res.turmas){
	                   row+='<option value="' + i + '">'+res.turmas[i]+'</option>';
	                                         
	                }
	                $("[name=nome_turma]").html(row);
	              }
	          });
	var turma= $('option:selected', "[name=nome_turma]").val();
	jQuery.ajax({
	          type: "POST",
	          url: "<?php echo base_url(); ?>" + "index.php/matricula/qtdMatriculas",
	          dataType: 'json',
	          data: {turma: turma},
	          success: function(res) {	              	
	          		if((parseInt(res.matriculas) + selected.length) > parseInt(res.vagas)){
	          			$("#mensagemVagas").html('<i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i> Atenção: O número de alunos selecionados excede o número de vagas desta turma!');
	          		}else{
	          			$("#mensagemVagas").html('');
	          		}	                
	            
	          }
	      });
	$("#buscar").click(function(event){
	    event.preventDefault();	
	    var concluinte = $("input[name=concluinte]:checked").val();              
	    var cpf = $("input[type=text][name=cpf]").val();
	    var nome = $("input[type=text][name=nome]").val(); 
	    jQuery.ajax({
	           type: "POST",
	           url: "<?php echo base_url(); ?>" + "index.php/matricula/buscaMatricula",
	           dataType: 'json',
	           data: {cpf: cpf, nome:nome, tipo: 0,concluinte:concluinte},
	           success: function(res) {
	           	searching = res;
	           	var rows = $('<table id="alunos" class="table table-hover table-bordered">');
	           	rows.append('<thead><tr><th>Nome</th><th>CPF</th><th>Opções</th></tr></thead>');
	           	if(res.err == "ok"){	               
	               		for(var i in res.usuarios){	
	               			if(selected.map(function(e) { return e.cpf_usuario; }).indexOf(res.usuarios[i].cpf_usuario)==-1){
		               			var newRow = $('<tr class="animated fadeInDown">');
		               			var cols = "";
		               			cols +='<td>'+res.usuarios[i].nome_usuario+'</td>';
		               			cols +='<td>'+res.usuarios[i].cpf_usuario+'</td>';	               				               			
		               			cols += '<td><button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Selecionar aluno" value="'+res.usuarios[i].cpf_usuario+'" botao="selecionar"><i class="fa fa-plus" aria-hidden="true""></i></button></td>';	               			
		               			newRow.append(cols);	               			
		               			rows.append(newRow);	
	               			}               			 	               			               		
	               		}
	               		$("#tabela_alunos").html(rows);
	               		p = tabela('alunos',2);               		

	               }else{
	               	mensagem('error',res.err);
	               	$("#tabela_alunos").html('');	               	
	               }
				
	           }
	       });  
	});
	$(document).on('click',"[botao='selecionar']", function () {
		if(selected.map(function(e) { return e.cpf_usuario; }).indexOf($(this).val())==-1){
			$("tr").removeClass('animeted fadeInDown');
			var pos = searching.usuarios.map(function(e) { return e.cpf_usuario; }).indexOf($(this).val());
			selected.push(searching.usuarios[pos]);
			t.row.add( [
				searching.usuarios[pos].nome_usuario,
				searching.usuarios[pos].cpf_usuario,
				'<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Retirar aluno" botao="retirar" value="'+$(this).val()+'"><i class="fa fa-minus" aria-hidden="true"></i></button>'		          
			      ] ).draw(false);		 
			searching.usuarios.splice(pos,1);	     
			p.row($(this).parents('tr')).remove().draw(false);
		}	   
	} );
	$(document).on('click',"[botao='retirar']", function () {
		var pos = selected.map(function(e) { return e.cpf_usuario; }).indexOf($(this).val());
		searching.usuarios.push(selected[pos]);
		p.row.add( [
			selected[pos].nome_usuario,
			selected[pos].cpf_usuario,
			'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Selecionar aluno" botao="selecionar" value="'+$(this).val()+'"><i class="fa fa-plus" aria-hidden="true""></i></button>'		          
		      ] ).draw(false);		 
		selected.splice(pos, 1);
		t.row($(this).parents('tr')).remove().draw(false);			
	} );
	$("[name=nome_curso]").change(function(event) {
	    event.preventDefault(); 
	    var idCurso= $('option:selected', "[name=nome_curso]").val();
	    jQuery.ajax({
	              type: "POST",
	              url: "<?php echo base_url(); ?>" + "index.php/turma/getTurmaCurso",
	              dataType: 'json',
	              data: {idcurso: idCurso},
	              success: function(res) {	                
	                var row ='';      
	                for(var i in res.turmas){
	                   row+='<option value="' + i + '">'+res.turmas[i]+'</option>';	                                         
	                }
	                $("[name=nome_turma]").html(row);
	              }
	          });
	});
	$("[name=nome_turma]").change(function(event) {
	    event.preventDefault(); 
	    var turma= $('option:selected', "[name=nome_turma]").val();
	    jQuery.ajax({
	              type: "POST",
	              url: "<?php echo base_url(); ?>" + "index.php/matricula/qtdMatriculas",
	              dataType: 'json',
	              data: {turma: turma},
	              success: function(res) {	              	
	              		if((parseInt(res.matriculas) + selected.length) > parseInt(res.vagas)){
	              			$("#mensagemVagas").html('<i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i> Atenção: O número de alunos selecionados excede o número de vagas desta turma!');
	              		}else{
	              			$("#mensagemVagas").html('');
	              		}	                
	                
	              }
	          });
	});
	$("#matricular").click(function(event){
		if(selected.length == 0){
			$('#confirmar').modal('hide');
			mensagem('error','Você precisa selecionar ao menos um aluno para matricular!');
		}else{
			if($('#data_matricula').val() == ''){
				$('#confirmar').modal('hide');
				mensagem('error','Você não pode deixar a data de matrícula vazia!');
			}else{
				var alunos = JSON.stringify(selected);							
				var turma = $('option:selected', "[name=nome_turma]").val();
				var datamatricula = $('#data_matricula').val();
				jQuery.ajax({
				          type: "POST",
				          url: "<?php echo base_url(); ?>" + "index.php/aluno/matricularTurma",
				          dataType: 'html',
				          data: {alunos: alunos, turma:turma, datamatricula:datamatricula},
				          success: function(res) {
				          	window.location.href = res;
				          }
				});
			}
		}
	    
	});	
	
	
});
</script>