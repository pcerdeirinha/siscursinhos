<?php $this->template->menu($view) ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3><b>Suas Advertências</b></h3>
            
            <table id="adv_user" class="table table-bordered">
                <thead>
                    <tr class="warning">
                        <th>Pontos</th>
                        <th>Descrição da advertência</th>   
                        <th>Data</th> 
                        <th>Opções</th>              
                    </tr>   
                </thead>
                <?php 
                $pts_total = 0;
                if (count($advertencias)>0){
                    foreach ($advertencias as $advertencia) { ?>
                    <tr class="animated fadeInDown">
                        <td><center><?php echo $advertencia['pontos_advertencia'];?></center></td>
                        <td><?php echo $advertencia['descricao_advertencia']; ?></td>
                        <td><?php echo date("d/m/Y", strtotime($advertencia['data_advertencia'])); ?></td>
                        <td><button type="button" class="btn btn-danger" onclick="location.href='<?php echo base_url(); ?>index.php/advertencia/remove/<?php echo $advertencia['idadvertencia'] ?>'" data-placement="top" title="Remover Advertência"><i class="fa fa-times-circle" aria-hidden="true"></i></button></td>
                    </tr>
                    <?php 
                        $pts_total += $advertencia['pontos_advertencia'];
                    }
                }
                else echo "<center><h3>Não há advertências para este usuário.</h3></center>" 
                 ?>
            </table>
            <div class="alert alert-danger" role="alert">Total de pontos: <?php echo $pts_total ?></div>
            <div class="col-md-1 col-md-offset-0" >
                <div class="form-save-buttons">
                    <button class="btn btn-primary" data-toggle="modal" type="button" id="btnNovaAdvertencia" data-target="#ModalCria"><i class="fa fa-plus"></i> Nova Advertência</button>
                </div>
            </div>
        </div>
        <?php echo form_open('advertencia/cria'); ?> 
        <?php echo form_hidden('idusuario',$idusuario?$idusuario:set_value('idusuario')) ?>
        <div class="modal fade" id="ModalCria" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel" align="CENTER">Adicionar Advertência</h3>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-0">
                                <div class="form-group <?php if (!(form_error('data_advertencia')=='')) echo 'has-error has-feedback'; ?>">
                                    <?php echo form_label('Data da Advertência', 'data_advertencia'); ?> 
                                    <?php echo form_input('data_advertencia', set_value('data_advertencia')?set_value('data_advertencia'):$data_advertencia, 'type="date", class="form-control" id="data_advertencia" placeholder="Data da Advertência" tipo="data"'); ?> 
                                    <?php if (!(form_error('data_advertencia')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                    <span class="text-danger"><?php echo form_error('data_advertencia'); ?></span>
                                </div>
                            </div>
                            <div class="col-md-6 col-md-offset-0">
                                <div class="form-group <?php if (!(form_error('pontos_advertencia')=='')) echo 'has-error has-feedback'; ?>">
                                    <?php echo form_label('Pontos', 'pontos_advertencia'); ?>
                                    <?php echo form_input('pontos_advertencia', set_value('pontos_advertencia')?set_value('pontos_advertencia'):$pontos_advertencia, 'type="text", class="form-control" id="numero" placeholder="N." tipo="numero"'); ?>
                                    <?php if (!(form_error('pontos_advertencia')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                    <span class="text-danger"><?php echo form_error('pontos_advertencia'); ?></span>
                                </div>
                            </div>
                             <div class="col-md-12 col-md-offset-0">
                                    <div class="form-group <?php if (!(form_error('descricao_advertencia')=='')) echo 'has-error has-feedback'; ?>">
                                        <?php echo form_label('Descrição da Advertência', 'descricao_advertencia'); ?> 
                                        <?php echo form_textarea('descricao_advertencia', set_value('descricao_advertencia')?set_value('descricao_advertencia'):$descricao_advertencia, 'class="form-control" id="descricao" placeholder="Descrição da Advertência"'); ?> 
                                        <?php if (!(form_error('descricao_advertencia')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                        <span class="text-danger"><?php echo form_error('descricao_advertencia'); ?></span>
                                    </div>
                             </div>
                        </div>                                                                          
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <button type="submit" class="btn btn-primary" id="adicionar">Adicionar</button>
                    </div>
                </div>
            </div>
        </div> 
        <?php echo form_close(); ?>      
    </div>
</div>


<script type="text/javascript">


$(document).ready(function () {
    mascara();
    data(true);
    
    
    $("#voltar").click(function(event){
        window.location.href = "<?php echo base_url(); ?>";  
    });
}); 
</script>

<?php if(isset($err)){?>
    <script type="text/javascript">
        mensagem('error',"<?php echo $err;?>");
        $('#ModalCria').modal('show');
    </script>
<?php }?>
<?php if(isset($msg)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $msg;?>");</script>
<?php }?>

