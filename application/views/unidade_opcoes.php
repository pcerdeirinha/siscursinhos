<?php $this->template->menu($view); ?>
<div>   

    <center>
        <h3>Cursinho</h3>
        <br><br>
        <table style="border: 0px; border-collapse: collapse;">
            <tr>
                
                <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="150px">
                    <a href="<?php echo base_url('index.php/unidade/novo')?>" class="btn btn-default">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                        Novo Cursinho
                    </a>
                </td>

                <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="150px">
                    <a href="<?php echo base_url('index.php/unidade/busca')?>" class="btn btn-primary">
                        <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                        Pesquisa de Cursinho
                    </a>
                </td>
            </tr>
    </table>
</center>

</div>
