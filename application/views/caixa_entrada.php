<?php $this->template->menu('inicio') ?>
<div class="container-fluid">
    <div class="row-content" >
        <div class="col-sm-3" >
            <aside role="complementary">
                <ul class="nav nav-pills nav-stacked navbar-default">
                  <li role="presentation" class="active"><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Caixa de Entrada</a></li>
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem/novo'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i> Nova Mensagem</a></li>
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem/enviado'); ?>"><i class="fa fa-share" aria-hidden="true"></i> Enviados</a></li>
                  <?php if ($view>1){ ?>
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem/grupo'); ?>"><i class="fa fa-users" aria-hidden="true"></i> Grupos</a></li>
                  <?php } ?>
                </ul>
            </aside>
        </div>
        <div class="col-sm-9">
            <?php if (count($mensagens)>0){ ?>
            <table class="table table-hover">
              <thead>
                  <th class="col-md-3">Remetente</th>
                  <th class="col-md-7">Assunto</th>
                  <th class="col-md-1">Opções</th>
              </thead>
              <tbody>
                <?php foreach ($mensagens as $mensagem): ?>
                    <tr class='clickable-row <?php  if($mensagem['count']!=0) echo "info"; ?>' style='cursor:pointer'>
                        <td class="redirect-column" data-href='<?php echo base_url('index.php/mensagem/ver/'.$mensagem['idmensagem']); ?>'>
                            <?php echo $mensagem['nome_usuario']; ?>
                        </td>
                        <td class="redirect-column" data-href='<?php echo base_url('index.php/mensagem/ver/'.$mensagem['idmensagem']); ?>'>
                            <?php echo $mensagem['assunto_mensagem'];
                                if($mensagem['count']!=0) echo '&nbsp;<span class="badge">'.$mensagem['count'].'</span>'; ?>
                        </td >
                        <td align="center">
                            <button type="button" title="Remover Mensagem com o Assunto: <?php echo $mensagem['assunto_mensagem'] ?>"  class="btn btn-danger" onclick="location.href='<?php echo base_url('index.php/mensagem/remove/'.$mensagem['idmensagem']); ?>'"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                <?php endforeach ?>
              </tbody>
            </table>  
            <?php } ?> 
          </div>
        </div>
    </div>
    
</div>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<script type="text/javascript">
$(document).ready(function(){
    $('.redirect-column').click(function(){
        window.location = $(this).data('href');
    });
});

</script>