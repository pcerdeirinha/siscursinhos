<?php $this->template->menu($view) ?>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
               <?php echo form_open('disciplina/cria'); ?>
                <?php if(isset($id)||(set_value('id')!='')){/*Então é Update*/?>
                            <h3><b>Edita Disciplina</b></h3>
                            <br>
                            <?php echo form_hidden('iddisciplina', $iddisciplina?$iddisciplina:set_value('iddisciplina')); 
                        }else{ ?>
                            <h3><b>Nova Disciplina</b></h3>                    
                            <br>
                <?php } ?> 
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#dadosDisciplina">Dados da Disciplina</a></li>
            </ul>
        </div>

        <div class="tab-content">
            <div id="dadosDisciplina" class="tab-pane fade in active">
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('nome_disciplina')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Nome da Disciplina', 'nome_disciplina'); ?>
                        <?php echo form_input('nome_disciplina', set_value('nome_disciplina')?set_value('nome_disciplina'):$nome_disciplina, 'type="text", class="form-control" id="nome" placeholder="Nome da Disciplina"'); ?> 
                        <?php if (!(form_error('nome_disciplina')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('nome_disciplina'); ?></span>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('area_disciplina')=='')) echo 'has-error has-feedback'; ?>">                        
                        <?php echo form_label('Área da Disciplina', 'area_disciplina'); ?>
                        <?php echo form_input('area_disciplina',set_value('area_disciplina')?set_value('area_disciplina'):$area_disciplina, 'type="text", class="form-control" id="area" placeholder="Área"'); ?>                               
                        <?php if (!(form_error('area_disciplina')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('area_disciplina'); ?></span>
                    </div>
                </div>

                <div class="col-md-4 col-md-offset-0">
                    <div class="form-group <?php if (!(form_error('curso')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Curso', 'curso'); ?><br>
                        <?php echo form_dropdown('curso',$cursos_drop,set_value('curso')?set_value('curso'):$curso, 'type="text" min="2", class="form-control" id="curso" placeholder="Curso"'); ?>
                        <?php if (!(form_error('curso')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('curso'); ?></span>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-1 col-md-offset-2">
            <div class="form-save-buttons">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
            </div>
        </div> 
        <?php echo form_close(); ?>
        <div class="col-md-1">
            <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>
    </div>
</div>

<script type="text/javascript">
console.log('')
$(document).ready(function () {
    $("#voltar").click(function(event){
        window.location.href = "<?php echo base_url(); ?>"+"index.php/disciplina";  
    });
}); 
</script>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
