<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0057)https://sistemas.fct.unesp.br/sentinela/login.open.action -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        

<title>:: UNESP : Portal de Sistemas de Gestão de Cursinhos  ::</title>
<link rel="shortcut icon" href="https://sistemas.fct.unesp.br/sentinela/skinunesp/imgs/favicon.ico">
<link rel="icon" href="https://sistemas.fct.unesp.br/sentinela/skinunesp/imgs/favicon.ico">
<meta name="description" content="null">
<meta name="keywords" content="Módulo, Autenticação, Sistemas, UNESP, Sentinela">
<meta name="robots" content="index">
<meta name="author" content="UNESP">
<meta name="language" content="pt-br">
<meta http-equiv="cache-control" content="no-cache, no-store, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<link rel="stylesheet" media="screen, projection" href="./__ UNESP _ Portal de Sistemas ___files/unesppadrao_core_nowidth.css" type="text/css">
<link rel="stylesheet" media="print" href="./__ UNESP _ Portal de Sistemas ___files/unesppadrao_core_print.css" type="text/css">
<link rel="stylesheet" media="screen, projection" href="./__ UNESP _ Portal de Sistemas ___files/unesppadrao_sistemas.css" type="text/css">
<link rel="stylesheet" media="print" href="./__ UNESP _ Portal de Sistemas ___files/unesppadrao_sistemas_print.css" type="text/css">
<link rel="stylesheet" media="screen, projection" href="./__ UNESP _ Portal de Sistemas ___files/jquery.tooltip.css" type="text/css">
<link rel="stylesheet" media="screen, projection" href="./__ UNESP _ Portal de Sistemas ___files/jquery.tooltip.css" type="text/css">
<link rel="stylesheet" media="screen, projection" href="./__ UNESP _ Portal de Sistemas ___files/jquery.Jcrop.css" type="text/css">
<link rel="stylesheet" media="screen, projection" href="./__ UNESP _ Portal de Sistemas ___files/jquery-ui-1.8.4.custom.css" type="text/css">
<link rel="stylesheet" media="screen, projection" href="./__ UNESP _ Portal de Sistemas ___files/tipsy.css" type="text/css">
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery.min.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery.xmldom.min.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery.Jcrop.min.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery.columnize.min.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery.webcam.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery.delegate.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery.form.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery.currency.min.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery.highlight.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery.tipsy.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery.tooltip.min.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery.field.min.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery-ui.min.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery.cookie.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery.ajaxmanager.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery.fileupload.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/jquery.tablednd.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/unesppadrao.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/unesppadrao_form.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/unesppadrao_menuflutuante.js"></script>
<script language="JavaScript" type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/unesppadrao_onload.js"></script>
        
    <meta name="header" content="Entre com seu e-mail e senha">
    
    
    <script type="text/javascript" src="./__ UNESP _ Portal de Sistemas ___files/mobiledetect.js"></script><style type="text/css"></style>
    <script>
    if(jQuery.browser.mobile == true){
    	window.location.href='/sentinela/mobile/login.do';
    }
    </script>
    
    <script type="text/javascript">
	    function documentOnLoad() {
	    	validateFORMLOGIN = new ValidationForm('formLogin');
            validateFORMLOGIN.init();
            
	    }
            function completeDomain(){
                domain = 'sistemas.fct.unesp.br';
                pos = domain.indexOf('.', 0);
                domain = domain.substring(pos+1, 999);
                usuario_value = document.getElementById('txt_usuario').value;
                if(usuario_value != '' && usuario_value.indexOf('@',0) == -1){
                                document.getElementById('txt_usuario').value = usuario_value+"@"+domain;
                }
            }
	</script>
        
    </head>
    <body onload="documentOnLoad()">
        
<div id="geral" style="left: 0px; width: 100%;">
<div id="header" style="top: 0px;">
<div class="tit">
<h1>UNIVERSIDADE ESTADUAL PAULISTA</h1>
<h1>"JÚLIO DE MESQUITA FILHO"</h1>
<h1 class="cp"> </h1>
</div>
<div class="unid">
<h1>Portal de Sistemas</h1>
</div>
<div class="unids">
<h1>Portal de Sistemas</h1>
</div>
<div class="menu"></div>
</div>

        
<div id="e" style="left: 0px; top: 0px;">
<div id="emapa"><!-- MONTADO EM TEMPO DE EXECUÇÃO PELO JAVASCRIPT --></div>
<div id="em">
<ul id="menuesq">
            

    




<li><a href="./__ UNESP _ Portal de Sistemas ___files/__ UNESP _ Portal de Sistemas __.html" title="Login">Login</a></li>
<li><a href="https://sistemas.fct.unesp.br/sentinela/user.openRecoveryPassword.action" title="Login">Recuperar Senha</a></li>
<li><a href="https://sistemas.fct.unesp.br/sentinela/faq.jsp" title="Dúvidas Freqüentes">F.A.Q.</a></li>


<li><a href="http://www.unesp.br/" title="Site Institucional" target="_blank">Institucional</a></li>


        
</ul>
<div id="dev">
</div>
</div>
</div>
<script language="JavaScript" type="text/javascript">
<!--
try {
CarregaMenu('menuesq');
} catch (e) { }
//-->
</script>
<hr>

        

        
<hr>
<div id="f" style="bottom: 0px;">
<div id="fb">
<div class="u">
Portal de Sistemas
</div>
<div class="m">
<ul>
            <li><a href="https://sistemas.fct.unesp.br/sentinela/" class="pri" title="Home">Inicio</a></li>
            <li><a href="http://www.unesp.br/" target="_blank" title="Institucional">Institucional</a></li>
            
                <li><a href="https://sistemas.fct.unesp.br/sentinela/" title="Login" class="ult">Login</a></li>
            
        
</ul>
</div>
</div>
</div>
<div id="c" align="center">
            <div id="ct" align="left">
            	<div id="toolbar" style="right: 175px;">
                    <span id="time"></span>
                    <a href="javascript:window.print()"><img src="./__ UNESP _ Portal de Sistemas ___files/icn_print.gif" alt="Imprimir">Imprimir</a>
                    <a href="javascript:redimensionar_toggle()" alt="Maximizar"><img id="toolbar_maximizar" alt="Maximizar" src="./__ UNESP _ Portal de Sistemas ___files/window_fullscreen.png"><img id="toolbar_restaurar" alt="Restaurar" src="./__ UNESP _ Portal de Sistemas ___files/window_nofullscreen.png" style="display: none;"></a><hr style="clear:both;display:none;"></div>
                <h2>Login</h2>
            </div>
            <div id="cc">
                <div class="infomsg">Entre com seu e-mail e senha</div>





               <!-- Exibe erros de banco de dados -->                
                

                <!-- Exibe mensagens caso existam -->
                
                
                <!-- Exibe erros caso existam -->
                

                <form name="formLogin" method="post" action="https://sistemas.fct.unesp.br/sentinela/login.action">
        <table class="form">
            <tbody><tr><th colspan="2">Login</th></tr>
	        <tr>
				<td width="35%" class="label">E-mail
				</td>
				<td width="65%">
                    <input name="txt_usuario" id="txt_usuario" type="text" class="setFocus v_required" title="Login" size="30" onblur="javascript:completeDomain();">
				</td>
	        </tr>
	        <tr>
				<td class="label">Senha</td>
				<td>
				    <input name="txt_senha" id="txt_senha" type="password" class="v_required" title="Senha" size="30">
				</td>
            </tr>
			<tr>
				<td colspan="2" class="barrabotao">
					<button name="btn_entrar" type="submit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false"><span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-e"></span><span class="ui-button-text">Entrar</span></button>
					<button name="btn_limpar" type="reset" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false"><span class="ui-button-icon-primary ui-icon ui-icon-refresh"></span><span class="ui-button-text">Limpar</span></button>
				</td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
        </tbody></table>
    </form>
            </div>
        </div></div>
        <div id="messagetop" style="top: -31px; display: none;">
            <div id="messagetop_close"><img border="0" src="./__ UNESP _ Portal de Sistemas ___files/invalid.png" alt="Fechar"></div>
            <div id="messagetop_content"></div>
        </div>
    
</body></html>