<?php $this->template->menu($view) ?>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
               <?php echo form_open('disciplina/cria'); ?>
                <?php if(isset($iddisciplina)||(set_value('iddisciplina')!='')){/*Então é Update*/?>
                            <h3><b>Edita Disciplina</b></h3>
                            <br>
                            <?php echo form_hidden('iddisciplina', $iddisciplina?$iddisciplina:set_value('iddisciplina')); 
                        }else{ ?>
                            <h3><b>Nova Disciplina</b></h3>                    
                            <br>
                <?php } ?> 
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#dadosDisciplina">Dados da Disciplina</a></li>
            </ul>
        </div>

        <div class="tab-content">
            <div id="dadosDisciplina" class="tab-pane fade in active">
                <div class="col-md-5 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('nome_disciplina')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Nome da Disciplina', 'nome_disciplina'); ?>
                        <?php echo form_input('nome_disciplina', set_value('nome_disciplina')?set_value('nome_disciplina'):$nome_disciplina, 'type="text", class="form-control" id="nome" placeholder="Nome da Disciplina"'); ?> 
                        <?php if (!(form_error('nome_disciplina')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('nome_disciplina'); ?></span>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-0">
                    <div class="form-group <?php if (!(form_error('carga_horaria')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Carga Horária', 'carga_horaria'); ?>
                        <div class="input-group">
                            <?php echo form_input('carga_horaria', set_value('carga_horaria')?set_value('carga_horaria'):$carga_horaria, 'type="text", class="form-control" id="carga" placeholder="Carga Horária" tipo="numero"'); ?> 
                            <span class="input-group-addon">(horas)</span>
                        </div>
                        <?php if (!(form_error('carga_horaria')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('carga_horaria'); ?></span>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('area_disciplina')=='')) echo 'has-error has-feedback'; ?>">                        
                        <?php echo form_label('Área da Disciplina', 'area_disciplina'); ?>
                           <select type="text" name='area_disciplina' class="form-control" id="area" placeholder="Área">
                               <?php 
                               $optgr = '';
                               foreach ($areas_drop as $key=>$area) {
                                   if ($optgr==''){
                                       $optgr=$area['nome_grande_area'];
                                       echo '<optgroup label="'.$area['nome_grande_area'].'">';
                                   }
                                   else if ($optgr!=$area['nome_grande_area']){
                                       echo '</optgroup><optgroup label="'.$area['nome_grande_area'].'">';
                                       $optgr=$area['nome_grande_area'];
                                   }
                                   if ($area_sel == $key) echo '<option value = '.$key.' selected>'.$area['nome_area'].'</option>';
                                   else echo '<option value = '.$key.'>'.$area['nome_area'].'</option>';
                               }
                                ?>
                               </optgroup>
                           </select>
                        <?php if (!(form_error('area_disciplina')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('area_disciplina'); ?></span>
                    </div>
                </div>

                <div class="col-md-4 col-md-offset-0">
                    <div class="form-group <?php if (!(form_error('curso')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Curso', 'curso'); ?><br>
                        <?php echo form_dropdown('curso',$cursos_drop,set_value('curso')?set_value('curso'):$curso, 'type="text" min="2", class="form-control" id="curso" placeholder="Curso"'); ?>
                        <?php if (!(form_error('curso')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('curso'); ?></span>
                    </div>
                </div>
                 <div class="col-md-8 col-md-offset-2">
                        <div class="form-group">
                            <?php echo form_label('Ementa da Disciplina', 'ementa_disciplina'); ?> 
                            <?php echo form_textarea('ementa_disciplina', set_value('ementa_disciplina')?set_value('ementa_disciplina'):$ementa_disciplina, 'class="form-control" id="ementa" placeholder="Conteúdo das Aulas"'); ?> 
                        </div>
                 </div>
            </div>
        </div>
        <div class="col-md-1 col-md-offset-8">
            <div class="form-save-buttons">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
            </div>
        </div> 
        <?php echo form_close(); ?>
        <div class="col-md-1">
            <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>
    </div>
</div>

<script type="text/javascript">
console.log('')
$(document).ready(function () {
    mascara();
    $("#voltar").click(function(event){
        window.location.href = "<?php echo base_url(); ?>"+"index.php/disciplina";  
    });
}); 
</script>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
