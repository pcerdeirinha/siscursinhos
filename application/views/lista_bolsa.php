<?php $this->template->menu($view) ?>
<div class="container">
    <div class='row'>
        <div class="col-md-8 col-md-offset-2">
            <?php if($bolsas == null){
                    echo '<center><h3>O Bolsista '.$monitor['nome_usuario'].' não possui histórico de bolsas!</h3></center>';                    
                    $mostrar = true; 
            }
            else{
                    echo '<center><h3>Histórico do Período de Atividade: '.$monitor['nome_usuario'].'</h3></center>';
                    $mostrar = true; 
            ?>            
                <table class="table table-hover table-bordered" id="matriculas">
                    <thead>
                        <tr>
                            <th>Data de Início</th>
                            <th>Data de Encerramento</th>
                            <th>Tipo</th>
                            <th>Opções</th>
                        </tr>                        
                    </thead>
                    <tbody>
                    <?php
                    
                    
                    foreach ($bolsas as $bolsa) { ?>
                    <?php
                    
                        if ((strtotime($bolsa['data_fim_bolsa'])>strtotime(date("Y-m-d")))&&(strtotime($bolsa['data_inicio_bolsa'])<=strtotime(date("Y-m-d")))){
                            echo '<tr class="success animated fadeInDown">';
                        }
                        else {                             
                            switch ($bolsa['tipo_bolsa']) {
                                case 0:
                                    echo '<tr class="info animated fadeInDown">';
                                    break;
                                case 1:
                                    echo '<tr class="active animated fadeInDown">';
                                    break;
                                default:
                                    echo '<tr class="danger animated fadeInDown">';
                                    break;
                            }
                        }
                    ?>                    
                        <td>
                            <?php 
                            $date = DateTime::createFromFormat('Y-m-d', $bolsa['data_inicio_bolsa'] );
                            $data_inicio =  $date->format('d/m/Y');
                            echo $data_inicio;?>
                        </td>
                        <td>
                            <?php 
                            $date = DateTime::createFromFormat('Y-m-d', $bolsa['data_fim_bolsa'] );
                            $data_fim =  $date->format('d/m/Y');
                            echo $data_fim;?>
                        </td>
                        <td>
                        <?php
                            switch ($bolsa['tipo_bolsa']) {
                                case 0:
                                    echo 'Voluntária';
                                    break;
                                case 1:
                                    echo 'Extensão';
                                    break;
                                default:
                                    echo 'Indisponível';
                                    break;
                            }                            
                        ?>
                        </td>
                        <td>
                           
          
                            <button type="button" class="btn btn-danger" onclick="removerBolsa(<?php echo $bolsa['id_bolsa']; ?>,'<?php echo $bolsa['data_inicio_bolsa']; ?>','<?php echo $bolsa['data_fim_bolsa']; ?>')" data-placement="top" title="Remover Bolsa"><i class="fa fa-times-circle" aria-hidden="true"></i></button>
                            <!--
                            <?php $mostrar = false;
                                $idmatricula = base64_encode($matricula['idmatricula']); 
                            //}else{?>
                                <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Informações da Matrícula" value="<?php echo base64_encode($matricula['idmatricula']); ?>" tipo="informacao"><i class="fa fa-info-circle" aria-hidden="true"></i></button>
                            <?php //} ?> -->
                        </td>
                    </tr>
                    <?php  } ?>
                    </tbody>
                </table>            
            <?php } ?>
        </div>
        <div class="col-md-4 col-md-offset-4">
            <button class="btn btn-default btn-block"  id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>        
    </div>   
    <div class='row'>
        <p></p>
        <div class="col-md-4 col-md-offset-4">
            <div class="form-group"> 
                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#adicionar"><i class="fa fa-money" aria-hidden="true"></i> Adicionar Período</button>             
            </div>          
        </div>
        <?php echo form_open('monitor/adicionarbolsa/'.$monitor['idusuario']); ?> 
        <div class="modal fade" id="adicionar" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel" align="CENTER">Adicionar Período</h3>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12"><p>Selecione a data de início e de fim das atividades e o tipo:</p></div>                            
                            <div class="col-md-4">
                                <div class="form-group <?php if (!(form_error('data_inicio_bolsa')=='')) echo 'has-error has-feedback'; ?>">
                                    <?php echo form_label('Data de Início', 'data_inicio_bolsa'); ?>
                                    <?php echo form_input('data_inicio_bolsa',set_value('data_inicio_bolsa'), 'type="date", class="form-control" id="curso" placeholder="Data de Início" tipo="data"'); ?>                               
                                    <?php if (!(form_error('data_inicio_bolsa')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                    <span class="text-danger"><?php echo form_error('data_inicio_bolsa'); ?></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group <?php if (!(form_error('data_fim_bolsa')=='')) echo 'has-error has-feedback'; ?>">
                                    <?php echo form_label('Data de Fim', 'data_fim_bolsa'); ?>
                                    <?php echo form_input('data_fim_bolsa',set_value('data_fim_bolsa'), 'type="date", class="form-control" id="curso" placeholder="Data de Fim" tipo="data"'); ?>                               
                                    <?php if (!(form_error('data_fim_bolsa')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                    <span class="text-danger"><?php echo form_error('data_fim_bolsa'); ?></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php echo form_label('Tipo', 'tipo_bolsa'); ?> 
                                    <?php echo form_dropdown('tipo_bolsa',array(0=>'Voluntário',1=>'Extensão'),1, 'class="form-control" id="tipo_bolsa" placeholder="Tipo" '); ?> 
                                </div>
                            </div>
                            <div class="col-md-12"><p class="text-warning" id="mensagemVagas"></p></div> 
                        </div>                                                                          
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <button type="submit" class="btn btn-primary" id="matricular">Adicionar</button>
                    </div>
                </div>
            </div>
        </div> 
        <?php echo form_close(); ?>
    </div>
   <div class="row">
    <?php echo form_open('monitor/removerbolsa/');
          echo form_hidden('idbolsa');
     ?> 
        <div class="modal fade" id="remover" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel" align="CENTER">Remover/Cancelar Bolsa</h3>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12"><p>Selecione o tipo de cancelamento:</p></div>                            
                            <div class="col-md-5">
                                <div class="form-group">                                                        
                                    <?php echo form_label('Tipo', 'tipos'); ?>
                                    <?php echo form_dropdown('tipos',array(0=>'Cancelar',1=>'Remover'),set_value('tipos')?set_value('tipos'):0, 'type="text" min="2", class="form-control" id="tipos" placeholder="Tipos"'); ?>                               
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group <?php if (!(form_error('data_cancelamento')=='')) echo 'has-error has-feedback'; ?>">
                                    <?php echo form_label('D. Cancelamento', 'data_cancelamento'); ?> 
                                    <?php echo form_input('data_cancelamento', $data_cancelamento, 'type="date", class="form-control" id="data_cancelamento" placeholder="Data cancelamento" tipo="data"'); ?> 
                                    <?php if (!(form_error('data_cancelamento')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                    <span class="text-danger"><?php echo form_error('data_cancelamento'); ?></span>

                                </div>
                            </div>
                            <div class="col-md-12"><p class="text-danger">O processo de cancelamento de bolsa é irreversível, portanto faça-o com cuidado.</p></div>   
                        </div>                                                                          
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <button type="submit" class="btn btn-danger" id="matricular">Remover Bolsa</button>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
    <div class="row">
        <div class="modal fade" id="informacao" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel" align="CENTER">Informações da Matrícula</h3>
                    </div>
                    <div class="modal-body">
                        <div class="row">                                                      
                            <div class="col-md-12">
                                <div class="panel panel-primary">
                                  <div class="panel-heading">Justificativa:</div>
                                  <div class="panel-body">
                                        <p name="jus" id="jus"></p> 
                                  </div>
                                </div>
                            </div>                            
                            <div class="col-md-12">
                                <div class="panel panel-primary">
                                  <div class="panel-heading">Observações:</div>
                                  <div class="panel-body">
                                        <p name="obs" id="observacoes"></p>
                                  </div>
                                </div>
                            </div>                            
                        </div>                                                                          
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>                        
                    </div>
                </div>
            </div>
        </div>
    </div> 
      
</div>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<?php if(isset($matriculado)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $matriculado;?>");</script>
<?php }?>
<script type="text/javascript">

function removerBolsa(id_bolsa,data_inicial,data_final) {
    $("[name=idbolsa]").val(id_bolsa);
    var data_h = new Date(<?php date('Y-m-d')?>);
    var data_f = new Date(data_final);
    var data_i = new Date(data_inicial);
    if ((data_f<data_h)|(data_i>data_h)){
        $("[name=tipos] option[value=0]").remove();
        $('#data_cancelamento').prop("disabled",true);
    }
    else if (0 == $('[name=tipos] option[value=0]').length){
         $('[name=tipos]').append('<option value="0" selected>Cancelar</option>')
         $('#data_cancelamento').prop("disabled",false);
    }
    $('#remover').modal('show');
}


$(document).ready(function () {
    mascara();
    data(true);
    <?php if ($erro=="remover"){?>
        $('#remover').modal('show');
    <?php } else if ($erro=="adicionar"){?>
        $('#adicionar').modal('show');
    <?php }?>
    
    if ($('option:selected', "[name=tipos]").val()==1)
        $('#data_cancelamento').prop("disabled",true);
    
    $("[name=tipos]").change(function(event) {
        event.preventDefault(); 
        if ($('option:selected', "[name=tipos]").val()==1)
            $('#data_cancelamento').prop("disabled",true);   
            else   
                $('#data_cancelamento').prop("disabled",false);
     });   
            
    $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/usuario/busca_funcionario/"+"<?php echo $monitor['tipo_usuario'] ?>";  
    });
}); 
</script>

<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
