<?php $this->template->menu($view) ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<center><?php echo '<h3>Aluno :'.$matricula['aluno'].'</h3>'?></center>
		</div>
		<div class="col-md-12">
		    <div class="panel panel-default">
			    <table class="table table-hover">
			    	<thead>
			    		<tr>
			    		    <th>Curso</th>
			    		    <th>Turma</th>
			    		    <th>Status</th>
			    		    <th>Data de Matricula</th>
			    		</tr>
			    	</thead>			       
				    <tr class="animated fadeInDown">
					    <td><?php echo $cursos_drop[$turmacurso[$matricula['matricula_idturma']]]; ?></td>
					    <td><?php echo $turmas[$matricula['matricula_idturma']];?></td>
					       <td><?php 
		            		if ($matricula['status']) echo 'Ativa';
		            		else echo 'Cancelada';  ?>
		            	</td>
					    <td><?php
					       	$date = DateTime::createFromFormat('Y-m-d H:i:s', $matricula['data_matricula'] );
					       	$data_matricula =  $date->format('d/m/Y');
					       	echo $data_matricula;
					       	//echo $matricula['data_matricula']; 
					       	?>
					    </td>    	
				    </tr>
			    </table>
		    </div>
		</div>
		<div class="col-md-2 col-md-offset-5">
		    <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
		</div>
		<div class="col-md-8 col-md-offset-2"><h3>Cancelar a Matrícula do Aluno</h3></div>
		<?php echo form_open('matricula/salva'); ?> 
		<div class="col-md-8 col-md-offset-2">
			<div class="form-group">
				<?php echo form_label('Escreva o motivo do cancelamento da matricula.', 'obs'); ?> 
				<span class="text-danger">*Obrigatório</span>				
				<?php echo form_input('obs', $obs, 'type="email", class="form-control" id="obs" placeholder="Observação"'); ?>  
				
			</div>
		</div>
		<?php echo form_hidden('idmatricula', $matricula['idmatricula']);//arrumar isso?>
		<div class="col-md-2 col-md-offset-5">
			<button class="btn btn-danger" type="submit" id="save"><i class="fa fa-user-times"></i> Cancelar Matrícula</button>
		</div>
		<?php echo form_close(); ?>
	</div>    
</div>        
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<script type="text/javascript">
$(document).ready(function () {
    mascara();
    data();
    $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/aluno/busca_aluno";  
    });
}); 
</script>