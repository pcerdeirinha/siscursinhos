<!DOCTYPE html>
    <?php $this->template->menu('login'); ?>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <div class="container">
                 <div class="row">
                 <div class="col-md-4"></div>
                 <div class="col-md-4" id="login">               
             <?php echo form_open('login/enviar_senha','id="form_esqueci"');?>                        
                 <h4 ALIGN="center" class="login_title"><i class="fa fa-user"></i> Esqueci minha Senha</h4>
                 <div class="form-group <?php if (!(form_error('email')=='')) echo 'has-error has-feedback'; ?>">
                    <label for="inputEmail" class="sr-only">Email</label>
                    <?php echo form_input('email', $email,'class="form-control" placeholder="Email" autofocus=""'); ?>
                    <?php if (!(form_error('email')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                    <span class="text-danger"><?php echo form_error('email'); ?></span>
                 </div>
                 <br>                       
                 <button class="btn btn-primary g-recaptcha" type="submit" id="entrar" data-sitekey="6Lf9NiYUAAAAAElrY6oNuH869dfRZ242w1I9XUon" data-callback="enviar"><i class="fa fa-sign-in"></i> Enviar para e-mail</button>
                 <?php //echo form_submit('login', '<i class="fa fa-sign-in"></i> Entrar', 'class="btn btn-primary"'); ?>
                 <button class="btn btn-default" type="button" id="limpar"><i class="fa fa-trash"></i> Limpar</button>
                 <p></p>
           </form>
                 </div>
                 </div>
       </div>
        <script type="text/javascript">
        function enviar(){
            $('#form_esqueci').submit();
        }
            $(document).ready(function () {
                $("#limpar").click(function(event){
                    $("input[type=text][name=email]").val("");
                    $("input[type=text][name=email]").focus();
                });
            });            
        </script>           
       
