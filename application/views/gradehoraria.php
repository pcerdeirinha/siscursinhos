<?php 
$this->template->menu($view);

?>
<br>
<div class="container">
 <!-- <div class="row">
 <div class="col-md-12"> -->
    <center>
    <?php echo form_open('gradehoraria/exibir'); ?>
    <table style="border: 0px; border-collapse: collapse;">
        <tr>
            
            <td style="border: 2px #E2EBEE; padding: 2px; text-align: center;" width="150px">
                <?php echo form_label('Escolha um curso: ', 'curso'); ?><br>
                <!-- <p><b>Escolha um curso: </b><br> -->
                    <?php echo form_dropdown('cursos', $cursos_drop, '','class="btn btn-default dropdown-toggle" name="cursos" id ="cursos", style="width: auto; height: 35px; float; font-size: 15px"'); ?>
                    <br><br>
                </p>
            </td>
        </tr>
        <tr>
            <td style="border: 2px #E2EBEE; padding: 2px; text-align: center;" width="150px">
                <!-- <p><b>Escolha uma turma: </b><br><br> -->
                    <?php echo form_label('Escolha uma turma', 'turma'); ?><br>
                    <?php echo form_dropdown('turmas', $turmas_drop, '','class="btn btn-default dropdown-toggle" name="turmas" id ="turmas", style="width: auto; height: 35px; float; font-size: 15px"'); ?>
                    <br><br>
                </p>
            </td>
        </tr>
    </table>
            <?php echo form_submit('save', 'Selecionar', 'class ="btn btn-primary" id="submit" style="width:100px;float:center;margin-left:0px"'); ?>
            
            </center>
            <br><br>
        <?php
        //var_dump($turmas);
        ?>

<?php echo form_close(); ?> 
</div>

<script type="text/javascript">
function getTurmasByCurso () {    
    var curso_sel = $('#cursos option:selected').val();
    jQuery.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>" + "index.php/gradehoraria/buscaTurma",
      dataType: 'json',
      data: {cursos: curso_sel},
      success: function(res) {
        var row ='';      
        for(var i in res.turmas){
          row+='<option value="'+res.turmas[i].idturma+'">'+
          res.turmas[i].nome_turma+","+res.turmas[i].periodo_turma+'</option>';
      }
      $("[name=turmas]").html(row);
  }
});      
}

$(document).ready(function () {
    getTurmasByCurso();
    //$('input[type=text]').addClass('uppercase');
    $("[name=cursos]").on("change", getTurmasByCurso);
    /*$("#voltar").click(function(event){
        window.location.href = "<?php echo base_url(); ?>"+"index.php/";  
    });*/
});
</script> 


