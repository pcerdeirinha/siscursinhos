<?php $this->template->menu($view); ?>
<div>	
    <center>
        <h3>Aluno</h3>
        <br><br>
        <table style="border: 0px; border-collapse: collapse;">
            <tr>
                <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="150px">
                    <a href="<?php echo base_url('index.php/aluno/novo')?>" class="btn btn-default">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                        Novo Aluno
                    </a>
                </td>
                <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="150px">
                    <a href="<?php echo base_url('index.php/aluno/busca_aluno')?>" class="btn btn-primary">
                        <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                        Pesquisa de Alunos
                    </a>
                </td>
            </tr>
        </table>
    </center>
</div>

<?php if(isset($msg)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $msg;?>");</script>
<?php }?>