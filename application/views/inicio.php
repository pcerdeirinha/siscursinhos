
<?php $this->template->menu('inicio');?>	
<body>                          
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Portal de Sistemas</h2>
                    <p>Seja bem vindo,<b> <?php echo $user['nome_social_usuario']!=null?$user['nome_social_usuario']:$user['nome_usuario']; ?></b>, ao Portal de Sistemas dos Cursinhos da Unesp - SGC</p>
                    <?php if ($numero_mensagens>0){ ?>
                    <p>Você possui <a href="<?php echo base_url('index.php/mensagem'); ?>"><span class="badge"><?php echo $numero_mensagens ?></span> mensagens</a> em sua caixa de email.</p>
                    <?php } ?>
                    <br>
                    <div>
                        <h4>Acesso aos Módulos</h4>                  
                        <br><br>
                        <center>
                        	<div class="table-responsive">
	                            <table style="border: 0px; border-collapse: collapse;">
	                                <tr>
	                                    <?php  
                                        if ($this->session->userdata('level')==6){
                                        ?>
                                        <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="250px">
                                            <a href="supervisor">
                                                <img src="../images/super-mod-ico.png" border="0" alt="SGC - Supervisor" title="SGC - Perfil: Supervisor" width="70"/><br />
                                                <b>SGC</b><br />
                                                Módulo Supervisor
                                            </a>
                                        </td>
                                        <?php
                                        } 
                       
	                                    if ($this->session->userdata('level')==5){
	                                    ?>
	                                    <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="250px">
	                                        <a href="docente">
	                                            <img src="../images/coorddoc-mod-ico.png" border="0" alt="SGC - Coordenador Docente" title="SGC - Perfil: Coordenador Docente" width="70"/><br />
	                                            <b>SGC</b><br />
	                                            Módulo Coordenador Docente
	                                        </a>
	                                    </td>
	                                    <?php
	                                    }
	
	                                    if ($this->session->userdata('level')==4){
	                                    ?>
	
	                                   <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="250px">
	                                        <a href="coordenador">
	                                            <img src="../images/coord-mod-ico-2.png" border="0" alt="SGC - Coordenador Discente" title="SGC - Perfil: Coordenador Discente" width="70"/><br />
	                                            <b>SGC</b><br />
	                                            Módulo Coordenador Discente
	                                        </a>
	                                    </td>
	                                    <?php
	                                    } 
	
	                                    if ($this->session->userdata('level')==3){
	                                    ?>
	
	                                    <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="250px">
	                                        <a href="secretario">
	                                            <img src="../images/secr-mod-ico-1.png" border="0" alt="SGC - Categoria: Secretário" title="SGC - Perfil: Secretário" width="70"/> <br />
	                                            <b>SGC</b><br />
	                                            Módulo Secretário
	                                        </a>
	                                    </td>
	
	                                    <?php
	                                    } 
	
	                                    if ($this->session->userdata('level')==2){
	                                    ?>
	
	                                    <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="250px">
	                                        <a href="professor">
	                                            <img src="../images/prof-mod-ico.png" border="0" alt="SGC - Categoria: Professor" title="SGC - Perfil: Professor" width="70"/> <br />
	                                            <b>SGC</b><br />
	                                            Módulo Professor
	                                        </a>
	                                    </td>
	                                    <?php
	                                    }
	
	                                    if ($this->session->userdata('level')==1){
	                                    ?>
	
	                                    <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="250px">
	                                        <a href="aluno">
	                                            <img src="../images/aluno-mod-ico.png" border="0" alt="SGC - Aluno" title="SGC - Perfil: Aluno" width="70"/><br />
	                                            <b>SGC</b><br />
	                                            Módulo Aluno
	                                        </a>
	                                    </td>
	                                    <?php
	                                    }
	                                    ?>
	
	                                </tr><tr>
	
	                                </tr>
	                            </table>
                            </div>
                        </center>
      
                    </div>


                </div>
            </div>
        </div>  
<?php if(isset($msg)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $msg;?>");</script>
<?php }?>