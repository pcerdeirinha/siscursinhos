<?php $this->template->menu($view) ?>
<div class='container'>
	<div class="row">		
		<div class="col-md-8 col-md-offset-2">
		<center>		
		<h3><b>Cancelamento de Inscrição</b></h3>					
		</center>
		</div>		
		<div class="col-md-3 col-md-offset-2">
			<div class="input-group">
				<span class="input-group-addon" id="cpf-addon">CPF:</span>			    
			    <input type="text" class="form-control" id="cpf" placeholder="CPF" name="cpf" tipo="cpf">
			</div>
		</div>
		<div class="col-md-5">
			<div class="input-group">
				<span class="input-group-addon" id="nome-addon">Nome:</span>			   	
			   	<input type="text" class="form-control" id="nome" placeholder="Nome" name="nome">
			</div>
		</div>				
	</div>
	<div class="row">
		<br>
		<div class="col-md-4 col-md-offset-4">
			<div class="form-group"> 
				<button type="submit" class="btn btn-primary btn-block" id="buscar"><i class="fa fa-search"></i> Buscar</button>				
			</div>			
		</div>					
	</div>
	<div class="row">		
		<div class="col-md-8 col-md-offset-2">
			<div id="tabela_alunos">			
			</div>			
		</div>			
	</div>
	<div class="row">
		<?php echo form_open('','id="formCancelar"'); ?> 
		    <div class="modal fade" id="cancelar" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
		        <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                    <h3 class="modal-title" id="myModalLabel" align="CENTER">Cancelar Inscrição</h3>
		                </div>
		                <div class="modal-body">
		                    <div class="row">
		                    	<div class="col-md-12"><p class="text-danger" id="nomeCancelamento"></p></div>
		                        <div class="col-md-12"><p>Selecione o tipo de cancelamento, a data e a justificativa:</p></div>
		                        <div class="col-md-4">
		                            <div class="form-group">                                                        
		                                <?php echo form_label('Tipo', 'tipos'); ?>
		                                <?php echo form_dropdown('tipos',$tipos, '', 'type="text" min="2", class="form-control" id="tipos" placeholder="Tipos"'); ?>                               
		                            </div>
		                        </div>
		                        <div class="col-md-5">
		                            <div class="form-group">                  
		                                <?php echo form_label('Justificativa', 'justificativa'); ?>
		                                <?php echo form_dropdown('justificativa',$justificativa, '', 'type="text" min="2", class="form-control" id="justificativa" placeholder="Justificativa"'); ?>
		                            </div>                              
		                        </div>
		                        <div class="col-md-3">
		                            <div class="form-group">
		                                <?php echo form_label('Data', 'data_cancelamento'); ?> 
		                                <?php echo form_input('data_cancelamento', $data_cancelamento, 'type="date", class="form-control" id="data_cancelamento" placeholder="Data cancelamento" tipo="data"'); ?> 
		                            </div>
		                        </div>
		                        <div class="col-md-12">
		                            <div class="form-group">
		                                <?php //echo form_label('Observações', 'matricula_observacoes'); ?> 
		                                <textarea name="matricula_observacoes" class="form-control" rows="3" id="matricula_observacoes" placeholder="Observações"></textarea>
		                                <?php //echo form_textarea('matricula_observacoes', $matricula_observacoes,'', 'class="form-control" rows="3" id="matricula_observacoes" placeholder="Observações"'); ?> 
		                            </div>
		                        </div>
		                        <div class="col-md-12"><p class="text-danger">O processo de cancelamento de inscrição é irreversível, portanto faça-o com cuidado.</p></div>   
		                    </div>                                                                          
		                </div>
		                <div class="modal-footer">
		                    <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
		                    <button type="submit" class="btn btn-danger" id="matricular">Cancelar Inscrição</button>
		                </div>
		            </div>
		        </div>
		    </div>
		    <?php echo form_close(); ?>	

	</div>	
</div>
<script type="text/javascript">
$(document).ready(function () {
	mascara();
	data(true);
	$('[data-toggle="tooltip"]').tooltip();
	var tipo = $('option:selected', "[name=tipos]").val();
	jQuery.ajax({
	          type: "POST",
	          url: "<?php echo base_url(); ?>" + "index.php/matricula/getJustificativa",
	          dataType: 'json',
	          data: {tipo: tipo},
	          success: function(res) {                  
	            var row ='';      
	            for(var i in res.justificativa){
	               row+='<option value="' + i + '">'+res.justificativa[i]+'</option>';                                          
	            }
	            $("[name=justificativa]").html(row);
	          }
	});
	$("[name=tipos]").change(function(event) {
	    event.preventDefault(); 
	    var tipo = $('option:selected', "[name=tipos]").val();
	    jQuery.ajax({
	              type: "POST",
	              url: "<?php echo base_url(); ?>" + "index.php/matricula/getJustificativa",
	              dataType: 'json',
	              data: {tipo: tipo},
	              success: function(res) {                  
	                var row ='';      
	                for(var i in res.justificativa){
	                   row+='<option value="' + i + '">'+res.justificativa[i]+'</option>';                                          
	                }
	                $("[name=justificativa]").html(row);
	              }
	    });
	});		
	$("#buscar").click(function(event){
	    event.preventDefault();	               
	    var cpf = $("input[type=text][name=cpf]").val();
	    var nome = $("input[type=text][name=nome]").val(); 
	    jQuery.ajax({
	           type: "POST",
	           url: "<?php echo base_url(); ?>" + "index.php/matricula/buscaMatricula",
	           dataType: 'json',
	           data: {cpf: cpf, nome:nome, tipo: 1},
	            success: function(res) {	                      
	           		var rows = $('<table id="alunos" class="table table-hover table-bordered">');
	           		rows.append('<thead><tr><th>Nome</th>\
					<th>CPF</th>\
					<th>Opções</th></tr></thead>');
	           		if(res.err == "ok"){	               
	            	   		for(var i in res.usuarios){	               			
	            	   			var newRow = $('<tr class="animated fadeInDown">');
	            	   			var cols = "";
	            	   			var value = res.matriculas[res.usuarios[i].idusuario];
	            	   			cols +='<td>'+res.usuarios[i].nome_usuario+'</td>';
	            	   			cols +='<td>'+res.usuarios[i].cpf_usuario+'</td>';	               			            			
	            	   			cols +='<td><button type="button" class="btn btn-danger" value="'+value+'" data-toggle="tooltip" data-placement="top" title="Desinscrever aluno" botao="desmatricular" nome="'+res.usuarios[i].nome_usuario+'"><i class="fa fa-user-times" aria-hidden="true"></i></button></td>';	               			
	            	   			newRow.append(cols);	               			
	            	   			rows.append(newRow);	               				               			               		
	            	   		}
	            	   		$("#tabela_alunos").html(rows);
	            	   		tabela('alunos',2); 
	            	}else{
	            	   	mensagem('error',res.err);
	            	   	$("#tabela_alunos").html('');
	            	}				
	            }
	       });  
	});
	$(document).on('click',"[botao='desmatricular']", function () {
		$("#formCancelar").attr("action",'<?php echo base_url();?>'+'index.php/matricula/cancelar/'+$(this).val()+'/true');
		$("#nomeCancelamento").html("Nome do aluno: "+$(this).attr("nome"));
		$('#cancelar').modal('show');			     
	} );	
});
</script>
<?php if(isset($msg)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $msg;?>");</script>
<?php }?>

<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>