<!DOCTYPE html>
	<?php $this->template->menu('login'); ?>
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<div class="container">
     <div class="row">
        <div class="col-md-4"></div>
	     <div class="col-md-4" id="login">
            <form>			   	 
			   	 <?php //echo form_open('login/validate');?>			   			
			   	 <h4 ALIGN="center" class="login_title"><i class="fa fa-user"></i> Login</h4>
			   	 <label for="inputEmail" class="sr-only">Email</label>
			   	 <?php echo form_input('email', $email,'class="form-control" placeholder="Email" autofocus=""'); ?>
			   	 <!---<input type="email" id="email" class="form-control" placeholder="Email" autofocus="">-->
			   	 <label for="inputPassword" class="sr-only">Senha</label>
			   	 <?php echo form_password('password', $password, 'class="form-control" placeholder="Senha"'); ?>
			   	 <!---<input type="password" id="password" class="form-control" placeholder="Senha">-->
			   	 <br>			   			
			   	 <button class="btn btn-primary g-recaptcha" type="submit" id="entrar" data-sitekey="6Lf9NiYUAAAAAElrY6oNuH869dfRZ242w1I9XUon" data-callback="enviar"><i class="fa fa-sign-in"></i> Entrar</button>
			   	 
			   	 <button class="btn btn-default" type="button" id="limpar"><i class="fa fa-trash"></i> Limpar</button>
			   	 <br><a href=<?php echo base_url('index.php/login/esquecisenha') ?>>Esqueci minha senha</a>
			   	 <p></p>
           </form>
	     </div>
        </div>
        <div class="modal fade" id="unidades_bolsista" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="turma_alunos" align="CENTER">Selecionar Unidade</h3>
                    </div>
                    <div class="modal-body">
                        <div id="tabela_unidades">
                        </div>                             
                    </div>
                </div>
            </div>
        </div>
	   </div>
        <script type="text/javascript">
        function enviar(){
            //event.preventDefault();                
            var email = $("input[type=text][name=email]").val();
            var password = $("input[type=password][name=password]").val();
              jQuery.ajax({
                   type: "POST",
                   url: "<?php echo base_url(); ?>" + "index.php/login/validate",
                   dataType: 'html',
                   data: {email: email, password:password, recaptcha:grecaptcha.getResponse() },
                   success: function(res) {
                       console.log(res);
                       if (res == "ok"){
                           window.location.href = "<?php echo base_url(); ?>"+"index.php/<?php echo $local; ?>";                              
                       }else if(res == "error"){
                          mensagem('error','Usuário/Senha não informados corretamente!');
                          $("input[type=text][name=email]").focus();
                          grecaptcha.reset();
                       }
                       else{
                                $('#tabela_unidades').html(res);
                                $('#unidades_bolsista').modal('show');
                                grecaptcha.reset();
                           }
                   }   
               });
        }
        
            $(document).ready(function () {
                $("#limpar").click(function(event){
                    $("input[type=text][name=email]").val("");
                    $("input[type=password][name=password]").val("");
                    $("input[type=text][name=email]").focus();
                });
            });            
        </script>			
	   
