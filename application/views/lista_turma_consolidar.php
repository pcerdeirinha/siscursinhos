<?php $this->template->menu($view) ?>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			
			<h3><b>Consolidar Turmas</b></h3>
			
			<table id='turmas' class="table table-hover">
				<thead>
					<tr>
						<th>Nome da Turma</th>
						<th>Curso</th>
						<th>Período</th>
						<th>Data Início</th>						
						<th>Opções</th>
					</tr>
				</thead>				
				<?php foreach ($turmas as $turma) {
				$modal = str_replace(' ', '', $turma['nome_turma']);?>
				<tr class="animated fadeInDown">
					<td><?php echo $turma['nome_turma'];?></td>
					<td><?php echo $cursos_drop[$turma['curso_idcurso']];?></td>
					<td><?php echo $turma['periodo_turma'];?></td>
					<td><?php $date = DateTime::createFromFormat('Y-m-d', $turma['data_inicio_turma']);
                              $data_inicio_turma =  $date->format('d/m/Y'); 
                              echo $data_inicio_turma;
                        ?>                              	
                    </td>					
					<td>
						<a href="#" data-toggle="tooltip" data-placement="top" title="Consolidar Turma">
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#<?php echo $modal; ?>"><i class="fa fa-calendar-check-o"></i></button>
						</a>						
					</td>
				</tr>				
				<?php } ?>
			</table>	
		</div>
		<div class="col-md-1 col-md-offset-9">
			<button class="btn btn-default" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
		</div>		
	</div>
	<div class="row">
		<?php foreach($turmas as $turma){
			$modal = str_replace(' ', '', $turma['nome_turma']);?>
			<?php echo form_open('turma/consolidar/'.$idTurma[$turma['idturma']]); ?> 
				<div class="modal fade" id="<?php echo $modal;?>" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h3 class="modal-title text-danger" id="myModalLabel" align="CENTER">Atenção <i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i></h3>
							</div>
							<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
								<p align="CENTER">Você está prestes a Consolidar a Turma: </p> 
								<h4 align="CENTER"><?php echo $turma['nome_turma'];?></h4>
								<p>Esta ação deve ser executada no final do período de duração desta Turma.</p>
								<p><strong>Período de duração da Turma: </strong><?php echo $duracao[$turma['curso_idcurso']]?> meses.</p>
								<p>Os seguintes itens relacionados também serão afetados:</p>
								<ul>
									<li>As matrículas de alunos nesta Turma serão consolidadas.</li>
									<li>As ofertas de disciplinas de professores para esta Turma serão consolidadas.</li>
									<li>Apos consolidada, não serão permitidas matrículas para esta Turma.</li>
									<li>Após consolidada, esta Turma não poderá aceitar ofertas de disciplinas, tampouco poderão ser editadas quaisquer informações referentes ao período letivo, como Simulados e Aulas.</li>
								</ul>
								<p>As informações desta Turma ainda poderão ser consultadas nos respectivos relatórios.</p>
								<p class="text-danger">As alterações acima citadas são irreversíveis.</p>
								</div>
								<div class="col-md-6 col-md-offset-3">
								<div class="form-group">
											<?php echo form_label('Selecione a data de Consolidação:', 'data_consolida'); ?> 
											<?php echo form_input('data_consolida',  $data_consolida, 'type="date", 		class="form-control" id="data_consolida" placeholder="Data consolida" tipo="data"')		; ?> 
										</div>									
								</div>
							</div>
								
								
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
								<button type="submit" class="btn btn-primary">Consolidar turma</button>
							</div>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
		<?php }?>	
		
	</div>
</div>
<script type="text/javascript">
$(document).ready(function () {
	tabela('turmas',4,3);
	mascara();
	data(true);
    $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/turma";  
    });
}); 
</script>

<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<?php if(isset($msg)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $msg;?>");</script>
<?php }?>



