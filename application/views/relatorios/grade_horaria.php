<html>
<head>
    <link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet">
    <script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
    <link rel="stylesheet" href="<?php echo base_url('css/css.css')?>">
</head>
<body>
    <header class = "relatorio">            
        <h4 class="tituloRelatorio">Sistema de Gestão de Cursinhos da Unesp</h4>        
    </header>                     
    <div class="transicaoRelatorio"><?php echo $unidade['nome_cursinho_unidade']; ?> - <?php echo $faculdade['nome_faculdade'];?>, <?php echo $unidade['cidade_unidade'];?> </div> 
    
    <div class="container">
       	<h2>
			<center>
			<?php
			echo "$nome_turma";
			?>
			</center>
		</h2>
		<br>
		<?php
			$maxpos=4; //Estabelece o minimo de 4 faixas de horário para montar a tabela
			foreach ($horarios as $horario){ 
				if ($horario['pos_horario'] > $maxpos){ //checa os registros de horário para verificar qual a posição máxima de horário existente
					$maxpos=$horario['pos_horario'];
				}
			}
			$filled = false;
		?>
	
		<div class="table-responsive">
			<table class="table table-bordered">
				<tr bgcolor="lightblue"><th><center>Segunda</center></th><th><center>Terça</center></th><th><center>Quarta</center></th>
					<th><center>Quinta</center></th><th><center>Sexta</center></th>
					<th><font color="gray"><center>Sábado</center></font></th>
				</tr>
			<?php 
			for ($tpos=1; $tpos < $maxpos+1; $tpos=$tpos+1) { //percorrendo linhas da tabela
				?><tr><?php
				for ($tdia=1; $tdia < 7; $tdia=$tdia+1){ //percorrendo colunas da tabela
					?><td><h4><strong><center><?php
					foreach ($horarios as $idhorario => $horario){
						if ($horario['pos_horario'] == $tpos && $horario['dia_horario'] == $tdia){ //Se existe um horário para a atual posição da tabela, inseri-lo
							?><p style="font-size:20px;"> <?php 
							echo $horario['nome_disciplina'];?></p>
							<small style="font-size:15px;"><b><?php 
							echo $horario['nome_professor'];?></b></small><?php
							$filled = true;	//o campo foi preenchido com um horário existente
						}
					}
					if (!$filled) {?>
					</strong><p class="lead">Vaga</p><strong>
					<?php	
					}
					$filled=false;
					?></center></h4></strong></td><?php
				}
				?></tr><?php
			}
				?>
			</table>
		</div>
		<br>
	</div>

</body>
</html>