<html>
<head>
    <link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet">
    <script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
    <link rel="stylesheet" href="<?php echo base_url('css/css.css')?>">
</head>
<body>
    <header class = "relatorio">            
        <h4 class="tituloRelatorio">Sistema de Gestão de Cursinhos da Unesp</h4>        
    </header>                     
    <div class="transicaoRelatorio"><?php echo $unidade['nome_cursinho_unidade']; ?> - <?php echo $faculdade['nome_faculdade'];?>, <?php echo $unidade['cidade_unidade'];?> </div> 
    
    <div class="container">
        <h3>Relatório de Frequência por Disciplina</h3>
        <h4>Turma: <?php echo $turma;  ?></h4> <br>
      
       <?php 
       for ($k=0;$k<count($disciplinas);$k++){
       
       $table=$disciplinas[$k];  ?> 
        
       <h4>Disciplina: <?php echo $table[0][1] ?></h4>
      <table class="tableRelatorio">
        <thead>
          <tr>
             <?php for($i=0;$i<count($table[1]);$i++)  {
                echo '<th>'.$table[1][$i].'</th>';
                $contDia[$i] = 0;
             }
            ?>
          </tr>
        </thead>
        <tbody>
          <?php 
          for($i=2;$i<count($table);$i++){
            $cont =0;
            $contPeso = 0;
            
          echo '<tr>';
            for($j=0;$j<count($table[$i]);$j++){
                if (($i==(count($table)-1))&&($j>0)){
                   echo '<td>'.number_format(($contDia[$j]/(($i-2)*substr($table[$i][$j], -2, 1))*100),"2").'%</td>'; 
                   //echo '<td>'.substr($table[$i][$j], -2, 1).'</td>';
                }
                else if ($j==(count($table[$i])-1)){
                    echo '<td>'.number_format((($cont/$contPeso)*100),"2").'%</td>';
                }
                else {
                    echo '<td>'.$table[$i][$j].'</td>';
                   
                    if ($j>0){
                        $contDia[$j] += $table[$i][$j];
                        $contPeso += substr($table[count($table)-1][$j], -2, 1);
                        $cont+=$table[$i][$j];
                    }
                }
            }
            
          echo '</tr>';
          }
          
          ?>
          
        </tbody>
      </table>
      
      <?php } ?>
</div>

</body>
</html>