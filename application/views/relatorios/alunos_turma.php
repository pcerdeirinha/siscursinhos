<html>
<head>
    <link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet">
    <script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
    <link rel="stylesheet" href="<?php echo base_url('css/css.css')?>">
</head>
<body>
    <header class = "relatorio">            
        <h4 class="tituloRelatorio">Sistema de Gestão de Cursinhos da Unesp</h4>        
    </header>                     
    <div class="transicaoRelatorio"><?php echo $unidade['nome_cursinho_unidade']; ?> - <?php echo $faculdade['nome_faculdade'];?>, <?php echo $unidade['cidade_unidade'];?> </div> 
    
    <div class="container">
        <h3>Relatório de Alunos por Turma</h3>
        <h4>Turma: <?php echo $turma;  ?></h4> <br>
      <table class="tableRelatorio">
        <thead>
          <tr>
            <th>Nome</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            foreach ($alunos['nome_social'] as $i => $aluno) {
                if ($aluno!=null)
                    echo '<tr><td>'.$aluno.'</td></tr>';
                else 
                    echo '<tr><td>'.$alunos['nome'][$i].'</td></tr>';
            }
          ?>
        </tbody>
      </table>
</div>

</body>
</html>