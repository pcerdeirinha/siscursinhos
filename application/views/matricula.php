<?php $this->template->menu($view) ?>
<div class="container">
    <center>
    <div class="panel panel-default">
    <table class="table table-striped">
        <tr>
            <th>Nome do Aluno</th>
            <th>CPF do Aluno</th>
            <th>Status da Matrícula Mais Recente</th>
            <th>Opções</th>
        </tr>
        <?php foreach ($useralunos as $aluno) { ?>

        <td><?php echo $aluno['nome_usuario'];?></td>
        <td><?php echo $aluno['cpf_usuario'];?></td>
        <td>
        <?php 
        if(isset($status_matricula[$aluno['idusuario']])){
            if($status_matricula[$aluno['idusuario']]){
                echo 'Ativa';
            }else{
                echo $obs_matricula[$aluno['idusuario']];
            }
        }else{
            echo 'Sem Matrícula';
        }
        ?></td>
        <td><?php echo anchor('curso/edita/'.$curso['idcurso'], '<i class="fa fa-pencil-square-o"> Editar </i>'); ?>&ensp;&ensp;
            <?php echo anchor('curso/remove/'.$curso['idcurso'], '<i class="fa fa-times"> Remover '); ?></td>
        </tr>
        <?php } ?>
    </table>
    </div>
    </center>
</div>
<?php if(isset($err)){?>

<script> alert("<?php echo $err;?>")</script>
<?php } ?>


