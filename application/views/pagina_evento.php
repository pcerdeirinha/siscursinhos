<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title><?php echo $evento['nome_evento']; ?></title>
  <link rel="icon" href="<?php echo base_url('images/favicon.ico')?>"/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
  <script src="<?php echo base_url('js/jquery.noty.packaged.min.js')?>"></script>
  <script src="<?php echo base_url('js/funcoes.js')?>"></script>  
  <link rel="stylesheet" href="<?php echo base_url('css/animate.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('css/font-awesome.min.css')?>">
  <style>
  body {
      font: 400 15px/1.8 Lato, sans-serif;
      color: #<?php echo $pagina_evento['cor_primaria_evento'];?>;
  }
  h3, h4 {
      margin: 10px 0 30px 0;
      letter-spacing: 10px;      
      font-size: 20px;
      color: #<?php echo $pagina_evento['cor_primaria_evento'];?>;
  }
  .descricao{
      background-color: #<?php echo $pagina_evento['cor_primaria_evento'];?>;
  }
  
  .container {
      padding: 80px 120px;
  }
  
  .container p, .container h3{
      color: #<?php echo $pagina_evento['cor_secundaria_evento'];?>;
  }
  .carousel-inner img {
      -webkit-filter: grayscale(90%);
      filter: grayscale(90%); /* make all photos black and white */ 
      width: 100%; /* Set width to 100% */
      margin: auto;
  }
  .carousel-caption h3, .carousel-caption p {
      color: #<?php echo $pagina_evento['cor_secundaria_evento'];?> !important;
  }
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; /* Hide the carousel text when the screen is less than 600 pixels wide */
    }
  }
  .bg-1 {
      background: #<?php echo $pagina_evento['cor_secundaria_evento'];?>;
      color: #<?php echo $pagina_evento['cor_primaria_evento'];?>;
  }
  .bg-1 h3 {color: #<?php echo $pagina_evento['cor_primaria_evento'];?>;}
  .bg-1 p {
      font-style: italic;
      color: #<?php echo $pagina_evento['cor_primaria_evento'];?>;  
  }
  .btn {
      padding: 10px 20px;
      background-color: #<?php echo $pagina_evento['cor_primaria_evento'];?>;
      color: #<?php echo $pagina_evento['cor_secundaria_evento'];?>;
      border-radius: 0;
      transition: .2s;
  }
  .btn:hover, .btn:focus {
      border: 1px solid #<?php echo $pagina_evento['cor_primaria_evento'];?>;
      background-color: #<?php echo $pagina_evento['cor_secundaria_evento'];?>;
      color: #<?php echo $pagina_evento['cor_primaria_evento'];?>;
  }
  .modal-header, h4, .close {
      background-color: #<?php echo $pagina_evento['cor_primaria_evento'];?>;
      color: #<?php echo $pagina_evento['cor_secundaria_evento'];?> !important;
      text-align: center;
      font-size: 30px;
  }
  .modal-header, .modal-body {
      padding: 40px 50px;
  }
  #googleMap {
      width: 100%;
      height: 400px;
      -webkit-filter: grayscale(100%);
      filter: grayscale(100%);
  }  
  .navbar {
      font-family: Montserrat, sans-serif;
      margin-bottom: 0;
      background-color: #<?php echo $pagina_evento['cor_primaria_evento'];?>;
      border: 0;
      font-size: 11px !important;
      letter-spacing: 4px;
      opacity: 0.9;
  }
  .navbar li a, .navbar .navbar-brand { 
      color: #<?php echo $pagina_evento['cor_secundaria_evento'];?> !important;
  }

  .navbar-nav li a:hover {
      color: #<?php echo $pagina_evento['cor_primaria_evento'];?> !important;
      background-color: #<?php echo $pagina_evento['cor_secundaria_evento'];?> !important;
  }
  .navbar-nav li.active a {
      color: #<?php echo $pagina_evento['cor_primaria_evento'];?> !important;
      background-color: #<?php echo $pagina_evento['cor_secundaria_evento'];?> !important;
  }
  .navbar-default .navbar-toggle {
      border-color: transparent;
  }
  footer {
      background-color: #<?php echo $pagina_evento['cor_primaria_evento'];?>;
      color: #<?php echo $pagina_evento['cor_secundaria_evento'];?>;
      padding: 32px;
  }
  footer a {
      color: #<?php echo $pagina_evento['cor_secundaria_evento'];?>;
  }
  footer a:hover {
      color: #777;
      text-decoration: none;
  }  
  .form-control {
      border-radius: 0;
  }
  textarea {
      resize: none;
  }
  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" id ="nome_user_nav" href="#myPage"><?php echo $user['nome_usuario']; ?></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#myPage">HOME</a></li>
        <li><a href="#descricao">DESCRIÇÃO</a></li>
        <li><a href="#contact">CONTATO</a></li>
        <li><a href="#googleMap">LOCALIZAÇÃO</a></li>
      </ul>
    </div>
  </div>
</nav>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <?php if (count($imagens)>1){ ?>
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <?php for ($i=1; $i<count($imagens);$i++){ ?>
      <li data-target="#myCarousel" data-slide-to="<?php echo $i ?>"></li>
      <?php } ?>
    </ol>
    <?php } ?>
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <?php foreach ($imagens as $key => $pagina): ?>    
      <div class="item <?php if ($key==0) echo "active"  ?>">
        <img src="<?php echo base_url(); ?>index.php/evento/imagem/<?php echo $pagina['idimagem_evento']; ?>">
        <div class="carousel-caption">
          <h3><?php echo $pagina['imagem_evento_titulo']; ?></h3>
          <p><?php echo $pagina['imagem_evento_descricao']; ?></p>
        </div>      
      </div>
      <?php endforeach ?>
    </div>
    <?php if (count($imagens)>1){ ?>    
    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
    <?php } ?>
</div>

<!-- Container (The Band Section) -->
<div class="descricao">
<div id="descricao" class="container text-center">
  <h3><?php echo $evento['nome_evento']; ?></h3>
  <p><?php echo $evento['descricao_evento']; ?></p>
  <br>
  <?php if ($pagina_evento['confirmar_presenca_evento']){//Confirmar Presenca ?>
      <button class="btn btn-default" type="button" data-toggle="modal" data-target="#myModal"><i class="fa fa-floppy-o"></i> Confirme sua presença!</button>
        <script>
          window.fbAsyncInit = function() {
            FB.init({
              appId      : '149251625623552',
              xfbml      : true,
              version    : 'v2.11'
            });
            FB.AppEvents.logPageView();
            FB.getLoginStatus(function(response) {
                    if (response.status === 'connected') {
                        FB.getLoginStatus(function(response) {
                                if (response.status==='connected'){
                                    FB.api('/me', 'GET', {fields: 'email,name,id'}, function(response) {
                                            $('[name=nome_estranho]').val(response.name);
                                            $('[name=email_estranho]').val(response.email);
                                        });
                                }
                        });
                    }
                });
          };
          (function(d, s, id){
                 var js, fjs = d.getElementsByTagName(s)[0];
                 if (d.getElementById(id)) {return;}
                 js = d.createElement(s); js.id = id;
                 js.src = "https://connect.facebook.net/pt_BR/sdk.js";
                 fjs.parentNode.insertBefore(js, fjs);
               }(document, 'script', 'facebook-jssdk'));
           function checkLoginState() {
              FB.getLoginStatus(function(response) {
                    if (response.status==='connected'){
                        FB.api('/me', 'GET', {fields: 'email,name,id'}, function(response) {
                                $('[name=nome_estranho]').val(response.name);
                                $('[name=email_estranho]').val(response.email);
                            });
                    }
              });
            }
    </script>
      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4><span class="glyphicon glyphicon-check"></span> Confirmar Presença</h4>
            </div>
            <div class="modal-body">
                <div id="internal_user">
                   <form role="form" id ="form_entrar" <?php if(isset($user['nome_usuario'])) echo 'style="display: none"';?>>
                      <div class="form-group">
                         <label for="email">Email</label>
                         <?php echo form_input('email', $email,'class="form-control" placeholder="Email"'); ?>
                     </div>
                     <div class="form-group">
                        <label for="password">Senha</label>
                        <?php echo form_password('password', $password, 'class="form-control" placeholder="Senha"'); ?>
                     </div>
                     <br>                       
                     <button class="btn btn-block g-recaptcha" type="button" id="entrar" data-sitekey="6Lf9NiYUAAAAAElrY6oNuH869dfRZ242w1I9XUon" data-callback="enviar"><span class="glyphicon glyphicon-log-in"></span>&nbsp;Entrar <i class="fa fa-circle-o-notch fa-spin" id ="loading_icon" style="font-size:24px;display: none"></i></button>
                   </form>    
                   <?php if ($confirmado==0){ ?>
                       <div id="form_confirma" <?php if(!isset($user['nome_usuario'])) echo 'style="display: none"';?>>
                           <h2 id="nome_confirmacao">Confirmar como: <?php echo $user['nome_usuario']; ?></h2>
                           <button class="btn btn-block" type="button" id="confirmar_user" onclick="confirmar_user();"><span class="glyphicon glyphicon-ok"></span>&nbsp;Confirmar Presença</button>
                           <button class="btn btn-block" type="button" id="sair_user" onclick="sair_user();"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sair</button>
                       </div>
                   <?php } else{ ?>
                       <div id="form_confirma" <?php if(!isset($user['nome_usuario'])) echo 'style="display: none"';?>>
                           <h2 id="nome_confirmacao">Confirmado como: <?php echo $user['nome_usuario']; ?></h2>
                           <button class="btn btn-block" type="button" id="confirmar_user" onclick="desfazer_confirmacao()"><span class="glyphicon glyphicon-remove"></span>&nbsp; Desfazer Confirmação</button>
                           <button class="btn btn-block" type="button" id="sair_user" onclick="sair_user()"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sair</button>
                       </div>
                   <?php } ?>   
               </div>
               <hr>
               <center>
                   <h2>Confirme com o Facebook:</h2>
                   <div class="fb-login-button" data-max-rows="1" data-size="large" onlogin="checkLoginState();" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="true" data-use-continue-as="true"></div>    
               </center> 
               <hr>
               <div class="form-group">
                   <label for="nome_estranho">Nome</label>
                   <?php echo form_input('nome_estranho', $nome_estranho,'class="form-control" placeholder="Nome"'); ?>
               </div>
               <div class="form-group">
                   <label for="email_estranho">Email</label>
                   <?php echo form_input('email_estranho', $email_estranho, 'class="form-control" placeholder="Email"'); ?>
               </div>
               <br>                       
               <button class="btn btn-block g-recaptcha" type="submit" id="enviar" data-sitekey="6Lf9NiYUAAAAAElrY6oNuH869dfRZ242w1I9XUon" data-callback="confirmar_estranho"><span class="glyphicon glyphicon-ok"></span>&nbsp;Enviar</button>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove"></span> Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
  <?php } //End Confirmar Presenca?>
</div>
</div>
<!-- Container (Contact Section) -->
<div id="contact" class="bg-1">
    <div class="container">
      <h3 class="text-center">Informações Gerais</h3>
      <p class="text-center"><em>Contato</em></p>
      <div class="row">
        <center>
            <div class="col-md-4 col-md-offset-4">
              <p><span class="glyphicon glyphicon-phone"></span>Telefone: <?php echo $pagina_evento['telefone_evento']; ?></p>
              <p><span class="glyphicon glyphicon-envelope"></span>Email: <?php echo $pagina_evento['email_evento']; ?></p>
            </div>
        </center>
      </div>
      <p class="text-center"><em>Data, Horário e Local</em></p>
      <div class="row">
        <center>
            <div class="col-md-4 col-md-offset-4">
              <?php setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                    mb_internal_encoding('UTF8'); 
                    mb_regex_encoding('UTF8');
                    
                    date_default_timezone_set('America/Sao_Paulo'); ?>
              <?php if (isset($evento['data_fim_evento'])){?>
              <p><span class="glyphicon glyphicon-calendar"></span>De <?php echo strftime('%A, %d de %B de %Y',strtotime($evento['data_inicio_evento']));?> a <?php echo strftime('%A, %d de %B de %Y',strtotime($evento['data_fim_evento'])); ?></p>
              <?php }else{ ?>
              <p><span class="glyphicon glyphicon-calendar"></span><?php echo strftime('%A, %d de %B de %Y',strtotime($evento['data_inicio_evento']));?></p>
              <?php } ?>
              <?php if (isset($evento['horario_inicio_evento'])){?>
              <p><span class="glyphicon glyphicon-time"></span>Das <?php echo date('H:i',strtotime($evento['horario_inicio_evento'])); ?> às <?php echo date('H:i',strtotime($evento['horario_fim_evento'])); ?></p>
              <?php } ?>
              <p><span class="glyphicon glyphicon-map-marker"></span><?php echo $pagina_evento['endereco_evento']; ?></p>
            </div>
        </center>
      </div>
      
  </div>
 </div>
<!-- Add Google Maps -->
<div id="googleMap"></div>
<script>
function myMap() {
    var geocoder, map;
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        'address': '<?php echo $pagina_evento['endereco_evento'];  ?>'
    }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var myOptions = {
                zoom: 15,
                center: results[0].geometry.location,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("googleMap"), myOptions);

            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
        }
    });  
}  
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBAXk93Wz-rwF5tfFZqd44h7qYnHi9X8mA&callback=myMap"></script>


<!-- Footer -->
<footer class="text-center">
  <a class="up-arrow" href="#myPage" data-toggle="tooltip" title="INÍCIO">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </a><br><br>
  <p>Gerado Pelo Sistema de Gestão de Cursinhos - <a href="<?php echo base_url(); ?>" data-toggle="tooltip" title="Ir para o portal">SGC</a></p> 
</footer>

<script>
    <?php if ($pagina_evento['confirmar_presenca_evento']){//Confirmar Presenca ?>
    function enviar(){
        event.preventDefault();   
        $('#loading_icon').css('display','inline');             
        var email = $("input[type=text][name=email]").val();
        var password = $("input[type=password][name=password]").val();
        jQuery.ajax({
               type: "POST",
               url: "<?php echo base_url(); ?>" + "index.php/login/validate",
               dataType: 'html',
               data: {email: email, password:password, recaptcha:grecaptcha.getResponse() },
               success: function(res) {
                   console.log(res);
                   if(res == "error"){
                      mensagem('error','Usuário/Senha não informados corretamente!');
                      $("input[type=text][name=email]").focus();
                      grecaptcha.reset();
                   }
                   else{
                       jQuery.ajax({
                               type: "POST",
                               url: "<?php echo base_url(); ?>" + "index.php/usuario/get_data",
                               dataType: 'json',
                               success: function(result) {
                                   $('#nome_confirmacao').html('Confirmar como: '+result);
                                   $('#form_entrar').css('display','none');
                                   $('#nome_user_nav').html(result);
                                   $('#form_confirma').css('display','inline');
                                   grecaptcha.reset();
                               }   
                           });
                   }
               }   
           });
    }


    function confirmar_user(){
        jQuery.ajax({
                   type: "POST",
                   url: "<?php echo base_url(); ?>index.php/evento/confirmar_usuario",
                   dataType: 'html',
                   data: {id_evento: <?php echo $evento['idevento']; ?>},
                   success: function(res) {
                        grecaptcha.reset();
                        if (res=='ok'){
                            $('#confirmar_user').html('<span class="glyphicon glyphicon-remove"></span>&nbsp; Desfazer Confirmação');
                            $('#confirmar_user').attr('onclick','desfazer_confirmacao()');
                            $('#nome_confirmacao').html('Confirmado como: <?php echo $user['nome_usuario']; ?>');
                            mensagem('success','Usuário confirmado!');
                        }
                        else {
                            mensagem('error','Ocorreu alguma falha. Recarregue a página e tente novamente.');
                        }
                   }   
               });
    }
    
    function desfazer_confirmacao () {
        jQuery.ajax({
                   type: "POST",
                   url: "<?php echo base_url(); ?>index.php/evento/desconfirmar_usuario",
                   dataType: 'html',
                   data: {id_evento: <?php echo $evento['idevento']; ?>},
                   success: function(res) {
                        if (res=='ok'){
                            $('#confirmar_user').html('<span class="glyphicon glyphicon-ok"></span>&nbsp;Confirmar Presença');
                            $('#confirmar_user').attr('onclick','confirmar_user()');
                            $('#nome_confirmacao').html('Confirmar como: <?php echo $user['nome_usuario']; ?>');                        
                            mensagem('success','Confirmação removida.');
                        } else {
                            mensagem('error','Ocorreu alguma falha. Recarregue a página e tente novamente.');
                        }
                   }   
               });
        }
         


    function confirmar_estranho () {
        var email = $("input[type=text][name=email_estranho]").val();
        var nome = $("input[type=text][name=nome_estranho]").val();
        jQuery.ajax({
                   type: "POST",
                   url: "<?php echo base_url(); ?>index.php/evento/confirmar_usuario",
                   dataType: 'html',
                   data: {id_evento: <?php echo $evento['idevento']; ?>,email_usuario:email,nome_usuario:nome,recaptcha:grecaptcha.getResponse()},
                   success: function(res) {
                        console.log(res);
                   }   
               });
    }
    
    function sair_user () {
        jQuery.ajax({
                   type: "POST",
                   url: "<?php echo base_url(); ?>index.php/login/logout",
                   dataType: 'html',
                   success: function(res) {
                       $('#nome_user_nav').html('');
                       $('#form_entrar').css('display','inline');
                       $('#form_confirma').css('display','none');
                       $('#loading_icon').css('display','none'); 
                       grecaptcha.reset();
                   }   
               });
    }
      var CaptchaCallback = function() {
            $('.g-recaptcha').each(function(index, el) {
              grecaptcha.render(el, {'sitekey' : '6Lf9NiYUAAAAAElrY6oNuH869dfRZ242w1I9XUon'});
            });
      };
    <?php }//End Confirmar Presenca ?>
  
    
$(document).ready(function(){
    
  // Initialize Tooltip
  $('[data-toggle="tooltip"]').tooltip(); 
  
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {

      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
})
</script>

</body>
</html>
