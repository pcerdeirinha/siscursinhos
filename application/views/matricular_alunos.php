<?php $this->template->menu($view) ?>
<div class='container'>
	<div class="row">		
		<div class="col-md-8 col-md-offset-2">
		<center>		
		<h3><b>Inscrever Alunos</b></h3>					
		</center>
		</div>		
		<div class="col-md-3 col-md-offset-2">
			<div class="input-group">
				<span class="input-group-addon" id="cpf-addon">CPF:</span>			    
			    <input type="text" class="form-control" id="cpf" placeholder="CPF" name="cpf" tipo="cpf">
			</div>
		</div>
		<div class="col-md-5">
			<div class="input-group">
				<span class="input-group-addon" id="nome-addon">Nome:</span>			   	
			   	<input type="text" class="form-control" id="nome" placeholder="Nome" name="nome">
			</div>
		</div>				
	</div>
	<div class="row">
		<br>
		<div class="col-md-4 col-md-offset-4">
			<div class="form-group"> 
				<button type="submit" class="btn btn-primary btn-block" id="buscar"><i class="fa fa-search"></i> Buscar</button>				
			</div>			
		</div>					
	</div>
	<div class="row">		
		<div class="col-md-8 col-md-offset-2">
			<div id="tabela_alunos">			
			</div>			
		</div>		
	</div>	
</div>
<script type="text/javascript">
$(document).ready(function () {
	mascara();
	$('[data-toggle="tooltip"]').tooltip();		
	$("#buscar").click(function(event){
	    event.preventDefault();	               
	    var cpf = $("input[type=text][name=cpf]").val();
	    var nome = $("input[type=text][name=nome]").val(); 
	    jQuery.ajax({
	           type: "POST",
	           url: "<?php echo base_url(); ?>" + "index.php/matricula/buscaMatricula",
	           dataType: 'json',
	           data: {cpf: cpf, nome:nome, tipo: 0},
	           success: function(res) {	           
	           	var rows = $('<table id="alunos" class="table table-hover table-bordered">');
	           	rows.append('<thead><tr><th>Nome</th>\
				<th>CPF</th>\
				<th>Opções</th></tr></thead>');
	           	if(res.err == "ok"){	               
	               		for(var i in res.usuarios){
	               			var matricular = "<?php echo base_url(); ?>" + "index.php/aluno/matricula/"+res.alunos[res.usuarios[i].idusuario];
	               			var newRow = $('<tr class="animated fadeInDown">');
	               			var cols = "";
	               			cols +='<td>'+res.usuarios[i].nome_usuario+'</td>';
	               			cols +='<td>'+res.usuarios[i].cpf_usuario+'</td>';	               			            			
	               			cols += '<td><a href="'+matricular+'" data-toggle="tooltip" data-placement="top" title="Inscrever aluno"><button type="button" class="btn btn-primary"><i class="fa fa-user-plus" aria-hidden="true"></i></button></a></td>';	               			
	               			newRow.append(cols);	               			
	               			rows.append(newRow);	               				               			               		
	               		}
	               		$("#tabela_alunos").html(rows);
	               		tabela('alunos',2);               		

	               }else{
	               	mensagem('error',res.err);
	               	$("#tabela_alunos").html('');
	               	$("#modals").html('');
	               }
				
	           }
	       });  
	});	
});
</script>