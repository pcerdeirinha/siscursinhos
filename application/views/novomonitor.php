<?php $this->template->menu($view) ?>
<br>
<div class="container">
    <div class="row">        
        <div class="col-md-8 col-md-offset-2">   
                    
                            
                <?php echo form_open($monitor.'/cria','',array('cidade_selected'=>set_value('cidade_usuario')?set_value('cidade_usuario'):$cidade_sel,
                                                               'curso_selected'=>set_value('curso_monitor')?set_value('curso_monitor'):curso_monitor_sel
                                                              )
                                    ); 
                ?>        
                <?php if(isset($id)||(set_value('id')!='')){/*Então é Update*/?>
                            <h3><b>Edita <?php echo $funcao_monitor?></b></h3>
                            <br>
                            <?php echo form_hidden('id', $id?$id:set_value('id')); 
                        }else{ ?>
                            <h3><b>Novo <?php echo $funcao_monitor?></b></h3>                    <br>
                <?php } ?>  
                <br>
                <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#dadosGerais">Dados Gerais</a></li>
                        <li><a data-toggle="tab" href="#endereco">Endereço</a></li>
                        <li><a data-toggle="tab" href="#academico">Dados acadêmicos</a></li>
                        <li><a data-toggle="tab" href="#projeto">Dados relativos ao projeto</a></li> 
                        <li><a data-toggle="tab" href="#bancario">Dados bancários</a></li>
                </ul>                
        </div>

        <div class="tab-content">
            <div id="dadosGerais" class="tab-pane fade in active">                
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('nome_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <label for="nome">Nome</label>                                    
                        <?php echo form_input('nome_usuario', set_value('nome_usuario')?set_value('nome_usuario'):$nome_usuario, 'type="text", class="form-control" id="nome" placeholder="Nome"'); ?>
                        <?php if (!(form_error('nome_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('nome_usuario'); ?></span>
                    
                    </div>
                </div>
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        <label for="nome">Nome Social</label>                                    
                        <?php echo form_input('nome_social_usuario', set_value('nome_social_usuario')?set_value('nome_social_usuario'):$nome_social_usuario, 'type="text", class="form-control" id="nome_social" placeholder="Nome Social"'); ?>
                    </div>
                </div>
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('email_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Email', 'email'); ?>
                        <?php echo form_input('email_usuario', set_value('email_usuario')?set_value('email_usuario'):$email_usuario, 'type="email", class="form-control" id="email" placeholder="Email"'); ?> 
                        <?php if (!(form_error('email_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('email_usuario'); ?></span>
                    </div>
                </div> 
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('rg_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('RG', 'rg'); ?>        
                         <?php echo form_input('rg_usuario', set_value('rg_usuario')?set_value('rg_usuario'):$rg_usuario, 'type="text", class="form-control" id="rg" placeholder="RG"'); ?>
                        <?php if (!(form_error('rg_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('rg_usuario'); ?></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div id="divCPF" class="form-group <?php if (!(form_error('cpf_usuario')=='')) echo 'has-error'; ?>" >
                        <?php echo form_label('CPF', 'cpf'); ?>
                        <div class="input-group">
                            <div class="has-feedback">
                                <?php echo form_input('cpf_usuario', set_value('cpf_usuario')?set_value('cpf_usuario'):$cpf_usuario, 'type="text", class="form-control" id="cpf" placeholder="CPF" tipo="cpf"'); ?>
                                <span class="glyphicon glyphicon-remove form-control-feedback" id="spanErrorCPF"  aria-hidden="true" <?php if (form_error('cpf_usuario')=='') echo 'style= display:none;"' ?>></span>
                            </div>    
                                <div class="input-group-btn">
                                    <button class="btn-secondary btn" type="button" id="btnPreencherCPF"  disabled >Preencher</button>
                               </div>
                        </div>
                        <span class="text-danger" id="spanCPFServer"><?php echo form_error('cpf_usuario'); ?></span>
                        <span class="text-danger" id="spanCPF" style="display:none">O CPF não é válido</span>

                    </div>
                </div>
                <div class="col-md-5 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('data_nascimento_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Data de Nascimento', 'data_nasc'); ?> 
                        <?php echo form_input('data_nascimento_usuario', set_value('data_nascimento_usuario')?set_value('data_nascimento_usuario'):$data_nascimento_usuario, 'type="date", class="form-control" id="data_nasc" placeholder="Data de nascimento" tipo="data"'); ?> 
                        <?php if (!(form_error('data_nascimento_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('data_nascimento_usuario'); ?></span>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('telefone_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Telefone', 'telefone'); ?>
                        <?php echo form_input('telefone_usuario', set_value('telefone_usuario')?set_value('telefone_usuario'):$telefone_usuario, 'type="tel" min="8" max="10", class="form-control" id="telefone" placeholder="DDD + Telefone" tipo="telefone"'); ?>
                        <?php if (!(form_error('telefone_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('telefone_usuario'); ?></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group <?php if (!(form_error('celular_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Celular', 'celular'); ?>
                        <?php echo form_input('celular_usuario', set_value('celular_usuario')?set_value('celular_usuario'):$celular_usuario, 'type="tel" min="8" max="10", class="form-control" id="celular" placeholder="DDD + Celular" tipo="celular"'); ?>
                        <?php if (!(form_error('celular_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('celular_usuario'); ?></span>
                    </div>
                </div>                                                  
            </div>
            <div id="endereco" class="tab-pane fade">
                <br>
                <div class="col-md-6 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('logradouro_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Logradouro', 'logradouro'); ?>
                        <?php echo form_input('logradouro_usuario', set_value('logradouro_usuario')?set_value('logradouro_usuario'):$logradouro_usuario, 'type="text", class="form-control" id="logradouro" placeholder="Endereço"'); ?>
                        <?php if (!(form_error('logradouro_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('logradouro_usuario'); ?></span>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group <?php if (!(form_error('numero_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Numero', 'numero'); ?>
                        <?php echo form_input('numero_usuario', set_value('numero_usuario')?set_value('numero_usuario'):$numero_usuario, 'type="text", class="form-control" id="numero" placeholder="N." tipo="numero"'); ?>
                        <?php if (!(form_error('numero_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('numero_usuario'); ?></span>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('bairro_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Bairro', 'bairro'); ?>
                        <?php echo form_input('bairro_usuario', set_value('bairro_usuario')?set_value('bairro_usuario'):$bairro_usuario, 'type="text", class="form-control" id="bairro" placeholder="Bairro"'); ?>
                        <?php if (!(form_error('bairro_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('bairro_usuario'); ?></span>
                   
                    </div>
                </div >
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo form_label('Complemento', 'complemento'); ?>
                        <?php echo form_input('complemento_endereco_usuario', set_value('complemento_endereco_usuario')?set_value('complemento_endereco_usuario'):$complementor_endereco_usuario, 'type="text", class="form-control" id="complemento" placeholder="Complemento de endereço"'); ?>
                        <?php if (!(form_error('bairro_usuario')=='')) echo '<br>'; //ver isso aqui ?>

                    </div>
                </div> 
                <div class="col-md-1 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('estado_usuario')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('UF', 'uf'); ?><br>
                        <?php echo form_dropdown('estado_usuario', $estados, set_value('estado_usuario')?set_value('estado_usuario'):$estado_sel, 'type="text" min="2", class="form-control" id="uf" placeholder="Estado"'); ?>
                        <?php if (!(form_error('estado_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                    </div>  
                </div>
                <div class="col-md-4">
                    <div class="form-group <?php if (!(form_error('cidade_usuario')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Cidade', 'cidade'); ?>
                        <?php echo form_dropdown('cidade_usuario', $cidades, '' ,'type="text", class="form-control" id="cidade" placeholder="Cidade"'); ?>
                        <span class="text-danger"><?php echo form_error('cidade_usuario'); ?></span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group <?php if (!(form_error('cep_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('CEP', 'cep'); ?>             
                        <?php echo form_input('cep_usuario', set_value('cep_usuario')?set_value('cep_usuario'):$cep_usuario,'type="text", class="form-control" id="cep" placeholder="CEP" tipo="cep"'); ?>
                        <?php if (!(form_error('cep_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('cep_usuario'); ?></span>
                    </div>
                </div>              
            </div>
            <div  id="academico" class="tab-pane fade">
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Departamento', 'dep'); ?>
                        <?php echo form_dropdown('departamento_monitor', $departamento_monitor, set_value('departamento_monitor')?set_value('departamento_monitor'):$departamento_monitor_sel, 'type="text", class="form-control" id="dep" placeholder="Departamento"'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo form_label('Curso', 'curso'); ?>
                        <?php echo form_dropdown('curso_monitor', $curso_monitor, $curso_monitor_sel, 'type="text", class="form-control" id="curso_monitor" placeholder="Curso"'); ?>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('periodo_curso_monitor')=='')) echo 'has-error has-feedback'; ?>">                        
                        <?php echo form_label('Período', 'periodo_curso_monitor'); ?>
                        <?php echo form_dropdown('periodo_curso_monitor',set_value('periodo_curso_monitor')?set_value('periodo_curso_monitor'):$periodo_curso_monitor, $periodo_sel, 'type="text" min="2", class="form-control" id="periodo" placeholder="Período"'); ?>                               
                        <?php if (!(form_error('periodo_curso_monitor')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('periodo_curso_monitor'); ?></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group <?php if (!(form_error('ano_curso_monitor')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Ano de Ingresso no Curso', 'anocurso'); ?>
                        <?php echo form_input('ano_curso_monitor', set_value('ano_curso_monitor')?set_value('ano_curso_monitor'):$ano_curso_monitor, 'type="text", class="form-control" id="anocurso" placeholder="Ano" tipo="numero" maxlength=4'); ?>
                        <?php if (!(form_error('ano_curso_monitor')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('ano_curso_monitor'); ?></span>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2 ">
                    <div class="form-group">
                        <?php echo form_label('RA', 'ra'); ?>
                        <?php echo form_input('ra_monitor', set_value('ra_monitor')?set_value('ra_monitor'):$ra_monitor, 'type="text", class="form-control" id="ra" placeholder="RA"'); ?>
                    </div>
                </div>
            </div>
            <div id="projeto" class="tab-pane fade">
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Função', 'funcao'); ?>
                        <?php echo form_input('funcao_monitor', $funcao_monitor, 'type="text", class="form-control" id="funcao" placeholder="Função (disciplina)" readonly'); ?> 
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('data_admissao_monitor')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Data de Admissão', 'admissao'); ?>
                        <?php echo form_input('data_admissao_monitor', set_value('data_admissao_monitor')?set_value('data_admissao_monitor'):$data_admissao_monitor, 'type="date", class="form-control" id="admissao" placeholder="Data de admissão" tipo="data"'); ?>
                        <?php if (!(form_error('data_admissao_monitor')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('data_admissao_monitor'); ?></span>
                    </div>
                </div>               
            </div>
            <div id="bancario" class="tab-pane fade">
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">                        
                        <?php echo form_label('Instituição Financeira', 'bancno'); ?>
                        <?php echo form_input('banco_monitor', set_value('banco_monitor')?set_value('banco_monitor'):$banco_monitor, 'type="text", class="form-control" id="bancno" placeholder="Nome do banco"'); ?>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Agencia', 'agencia'); ?>
                        <?php echo form_input('agencia_monitor', set_value('agencia_monitor')?set_value('agencia_monitor'):$agencia_monitor, 'type="text", class="form-control" id="agencia" placeholder="Número da agência"'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo form_label('Conta', 'conta'); ?>
                        <?php echo form_input('conta_monitor', set_value('conta_monitor')?set_value('conta_monitor'):$conta_monitor, 'type="text", class="form-control" id="conta" placeholder="Número da conta"'); ?>
                    </div>
                </div>                
            </div>            
        </div>
        <div class="col-md-1 col-md-offset-8">
            <div class="form-save-buttons">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="col-md-1">
            <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div> 
    </div>
</div>
<script type="text/javascript">
function obterCidade(uf,cidade){
    jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/usuario/buscaCidade",
                  dataType: 'json',
                  data: {uf: uf},
                  success: function(res) {
                    var row ='';      
                    for(var i in res.cidades){
                        if (res.cidades[i]!=cidade)
                            row+='<option  value = "'+res.cidades[i]+'">'+res.cidades[i]+'</option>';
                         else row+='<option  value = "'+res.cidades[i]+'" selected="select"">'+res.cidades[i]+'</option>';
                    }
                    $("[name=cidade_usuario]").html(row);
                  }
         });
}

var dados;
var cpf;

$(document).ready(function () {
    var uf = $('option:selected', '#uf').val();
    var cidade = document.getElementsByName("cidade_selected")[0].value;
    obterCidade(uf,cidade);
    
    var curso = $("[name=curso_selected]").val();
    var dep = $('option:selected', "[name=departamento_monitor]").val();
        jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/professor/buscaCursos",
                  dataType: 'json',
                  data: {dep: dep},
                  success: function(res) {
                    console.log(res.curso_monitor);
                    var row ='';      
                    for(var i in res.curso_monitor){
                        if (i==curso)
                            row+='<option value="' + i + '" selected="select">'+res.curso_monitor[i]+'</option>';
                        else  row+='<option value="' + i + '">'+res.curso_monitor[i]+'</option>';
                                             
                    }
                    $("[name=curso_monitor]").html(row);
                  }
              });      
       

    
    mascara();
    data();
    $('input[type=text]').addClass('uppercase');
    $("[name=estado_usuario]").change(function(event) {
       event.preventDefault(); 
       var uf = $('option:selected', '#uf').val();
       obterCidade(uf,'');
    });
    $("[name=departamento_monitor]").change(function(event) {
       event.preventDefault(); 
       var dep = $('option:selected', this).val();
        jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/professor/buscaCursos",
                  dataType: 'json',
                  data: {dep: dep},
                  success: function(res) {
                    console.log(res.curso_monitor);
                    var row ='';      
                    for(var i in res.curso_monitor){
                        row+='<option value="' + i + '">'+res.curso_monitor[i]+'</option>';                        
                    }
                    $("[name=curso_monitor]").html(row);
                  }
              });      
       
    });
    $("[name=curso_monitor]").change(function(event) {
       event.preventDefault(); 
       var dep = $('option:selected', this).val();
    });
    $("#voltar").click(function(event){
        var monitor = "<?php echo $monitor ?>"
        console.log(monitor);
        window.location.href = "<?php echo base_url(); ?>"+"index.php/" + monitor + "/opcoes";  
    });
        $("[name=cpf_usuario]").on('keyup',function(event){
        var cpf = $("[name=cpf_usuario]").val();
        if (TestaCPF(cpf)){
            jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/usuario/obterUsuarioValido",
                  dataType: 'json',
                  data: {cpf: cpf},
                  success: function(res) {
                    console.log(res);
                    if (res==null){
                        document.getElementById("spanErrorCPF").className= "glyphicon glyphicon-ok form-control-feedback";
                        document.getElementById("divCPF").className = "form-group has-success";
                        document.getElementById("spanCPF").style.display="";
                        document.getElementById("spanCPF").innerHTML="O CPF é válido";
                        document.getElementById("spanCPF").className="text-success"; 
                     }
                    else if (res[0].info=="false"){
                          document.getElementById("divCPF").className = "form-group has-error";
                          document.getElementById("spanErrorCPF").style.display = ""; 
                          document.getElementById("spanCPFServer").style.display = "none"; 
                          document.getElementById("spanCPF").style.display = "";
                          document.getElementById("spanCPF").innerHTML="O CPF precisa ser único";
                          document.getElementById("spanCPF").className="text-danger";
                          document.getElementById("spanErrorCPF").className= "glyphicon glyphicon-remove form-control-feedback";

                    }else {
                        dados=res[0];
                        document.getElementById("spanCPF").style.display = "";
                        document.getElementById("spanErrorCPF").className= "glyphicon glyphicon-alert form-control-feedback"
                        document.getElementById("divCPF").className = "form-group has-warning";
                        document.getElementById("btnPreencherCPF").disabled = false;
                        document.getElementById("spanCPF").innerHTML="O CPF já possui registro. Deseja obter os dados?";
                        document.getElementById("spanCPF").className="text-warning";
                    }
                    
                 }
              });
        } 
        else {
           document.getElementById("divCPF").className = "form-group has-error";
           document.getElementById("spanErrorCPF").style.display = ""; 
           document.getElementById("spanCPFServer").style.display = "none"; 
           document.getElementById("spanCPF").style.display = "";
           document.getElementById("spanCPF").innerHTML="O CPF não é válido";
           document.getElementById("spanCPF").className="text-danger";
           document.getElementById("spanErrorCPF").className= "glyphicon glyphicon-remove form-control-feedback";

        }
    });
        $("#btnPreencherCPF").click(function(event){
             document.getElementById("nome").value=dados.nome_usuario;
             document.getElementById("rg").value=dados.rg_usuario;
             var data_nasc = dados.data_nascimento_usuario.split('-');
             document.getElementById("data_nasc").value=data_nasc[2]+"/"+data_nasc[1]+"/"+data_nasc[0];
             document.getElementById("logradouro").value=dados.logradouro_usuario;
             document.getElementById("numero").value=dados.numero_usuario;
             document.getElementById("bairro").value=dados.bairro_usuario;
             document.getElementById("complemento").value=dados.complemento_endereco_usuario;
             document.getElementById("cep").value=dados.cep_usuario;
             document.getElementById("rg").value=dados.rg_usuario;
             document.getElementById("email").value=dados.email_usuario;
             document.getElementById("telefone").value=dados.telefone_usuario;
             document.getElementById("celular").value=dados.celular_usuario;
             $('#uf option:contains("'+dados.estado_usuario+'")').prop('selected',true);
             obterCidade($('#uf option:contains("'+dados.estado_usuario+'")').val(),dados.cidade_usuario);
             document.getElementById("spanErrorCPF").className= "glyphicon glyphicon-ok form-control-feedback";
             document.getElementById("divCPF").className = "form-group has-success";
             document.getElementById("spanCPF").style.display="";
             document.getElementById("spanCPF").innerHTML="O CPF é válido";
             document.getElementById("spanCPF").className="text-success"; 
             cpf=dados.cpf_usuario;
    }); 
});

function TestaCPF(cpf) {  
    cpf = cpf.replace(/[^\d]+/g,'');    
    if(cpf == '') return false; 
    // Elimina CPFs invalidos conhecidos    
    if (cpf.length != 11 || 
        cpf == "00000000000" || 
        cpf == "11111111111" || 
        cpf == "22222222222" || 
        cpf == "33333333333" || 
        cpf == "44444444444" || 
        cpf == "55555555555" || 
        cpf == "66666666666" || 
        cpf == "77777777777" || 
        cpf == "88888888888" || 
        cpf == "99999999999")
            return false;       
    // Valida 1o digito 
    add = 0;    
    for (i=0; i < 9; i ++)       
        add += parseInt(cpf.charAt(i)) * (10 - i);  
        rev = 11 - (add % 11);  
        if (rev == 10 || rev == 11)     
            rev = 0;    
        if (rev != parseInt(cpf.charAt(9)))     
            return false;       
    // Valida 2o digito 
    add = 0;    
    for (i = 0; i < 10; i ++)        
        add += parseInt(cpf.charAt(i)) * (11 - i);  
    rev = 11 - (add % 11);  
    if (rev == 10 || rev == 11) 
        rev = 0;    
    if (rev != parseInt(cpf.charAt(10)))
        return false;       
    return true;   
}
</script>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
