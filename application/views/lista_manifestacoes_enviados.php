<?php $this->template->menu($view) ?>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			
			<h3><b>Lista de Manifestações</b></h3>
			<div class="table-responsive">
			<table id='manifestacoes' class="table table-hover">
				<thead>
					<tr>
						<th>Tipo</th>
						<th>Destino</th>
						<th>Assunto</th>
						<th>Data de Envio</th>						
						<th>Opções</th>
					</tr>
				</thead>		
				<?php foreach ($manifestacoes as $manifestacao) {?>
				<?php switch ($manifestacao['idtipo']) {
                            case 1:
                                echo '<tr class="danger animated fadeInDown">';
                                break;
                            case 5:
                                echo '<tr class="success animated fadeInDown">';
                                break;
                            case 4:
                                echo '<tr class="active animated fadeInDown">';
                                break;
                            case 2:
                                echo '<tr class="warning animated fadeInDown">';
                                break;
                            case 3:
                                echo '<tr class="info animated fadeInDown">';
                                break;
                            case 6:
                                echo '<tr class="info animated fadeInDown">';
                                break;
                            default:
                                echo '<tr class="animated fadeInDown">';
                                break;
                    }  ?>	
								<td><?php echo $manifestacao['descricao_tipo'];?></td>
					<td><?php echo $manifestacao['descricao_representante'] ?></td>
					<td><?php echo $manifestacao['ouvidoria_assunto_idassunto']!=null?$manifestacao['descricao_assunto']:"Não informado";?></td>
					<td><?php $date = DateTime::createFromFormat('Y-m-d H:i:s', $manifestacao['data_criacao_manifestacao']);
                              $data_manifestacao =  $date->format('d/m/Y'); 
                              echo $data_manifestacao;
                        ?>                              	
                    </td>					
					<td>
						<a href="edita/<?php echo $manifestacao['idmanifestacao'] ?>" data-toggle="tooltip" data-placement="top" title="Editar"><button type="button" class="btn btn-default"><i class="fa fa-pencil-square-o"></i></button></a>
						&ensp;<a href="#" data-toggle="tooltip" data-placement="top" title="Remover"><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#remover_manifestacao"><i class="fa fa-trash"></i></button></a>						
					</td>
				</tr>
				<?php } ?>
			</table>	
			</div>
			<div class="modal fade" id="remover_manifestacao" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h3 class="modal-title text-danger" id="myModalLabel" align="CENTER">Atenção <i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i></h3>
						</div>
						<div class="modal-body">
							<p align="CENTER">Você está prestes a remover a Turma: </p> 
							<h4 align="CENTER"><?php echo $turma['nome_turma'];?></h4>
							<p>Os seguintes itens relacionados também serão afetados:</p>
							<ul>
								<li>As matrículas de alunos nesta turma serão canceladas.</li>
								<li>As ofertas de disciplinas de professores para esta turma serão canceladas.</li>
							</ul>
							<p class="text-danger">As alterações acima citadas são irreversíveis.</p>
							<h4 align="CENTER">Está certo disto?</h4>								
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
							<button type="button" class="btn btn-danger" onclick="location.href='<?php echo base_url(); ?>index.php/turma/remove/<?php echo $turma['idturma'] ?>'">Sim, remover a turma</button>
						</div>
					</div>
				</div>
			</div>
					
		</div>
		<div class="col-md-1 col-md-offset-9">
			<button class="btn btn-default" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
		</div>		
	</div>
</div>
<script type="text/javascript">
$(document).ready(function () {
	tabela('manifestacoes',4,3);
    $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/inicio";  
    });
}); 
</script>

<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>


