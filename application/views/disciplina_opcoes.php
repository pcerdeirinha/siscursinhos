<?php $this->template->menu($view); ?>
<div>	

    <center>
        <h3>Disciplinas</h3>
        <br><br>
        <table style="border: 0px; border-collapse: collapse;">
            <tr>
                <?php  

                ?>
                <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="150px">
                    <a href="disciplina/novo" class="btn btn-default">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                        Nova Disciplina
                    </a>
                </td>

                <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="150px">
                    <a href="disciplina/lista" class="btn btn-primary">
                        <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                        Pesquisa de Disciplinas
                    </a>
                </td>


            </tr><tr>

        </tr>
    </table>
</center>

</div>