<?php $this->template->menu($view) ?>
<div class='container'>
	<div class="row">		
		<div class="col-md-7 col-md-offset-2">
		<center>
		<div class="panel panel-default">
		<h3><b>Busca de Alunos</b></h3>
		<br>
		
			<form class="form-inline" >				
					<div class="form-group">
		    			<label for="input">CPF: </label>
		    			<input type="text" class="form-control" id="cpf" placeholder="CPF" name="cpf">
					</div>
					&ensp;&ensp;
					<div class="form-group">
				    	<label for="input">Nome: </label>
				    	<input type="text" class="form-control" id="nome" placeholder="Nome" name="nome">
					</div>
					<button type="submit" class="btn btn-primary" id="buscar"><i class="fa fa-search"></i> Buscar</button>
					<br>
					<br>									
			</form>			
		</div>
		</center>
		</div>	
	</div>
	<div class="row">		
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default" id="tabela_alunos">			
			</div>			
		</div>
		<center>
			<button type="submit" class="btn btn-primary" id="matricular"><i class="fa fa-pencil-square-o"></i> Matricular Alunos Selecionados </button>				
			<?php echo form_hidden('turma', $turma); ?>
		</center>
	</div>	
</div>
<script type="text/javascript">
$(document).ready(function () {
	Buscar('','');
	function Buscar (cpf, nome) {		    
		    jQuery.ajax({
		           type: "POST",
		           url: "<?php echo base_url(); ?>" + "index.php/turma/matricula_aluno",
		           dataType: 'json',
		           data: {cpf: cpf, nome:nome},
		           success: function(res) {
		           	var rows = $('<table class="table table-striped">');
		           	rows.append('<tr><th>Nome</th>\
					<th>CPF</th>\
					<th>Status da Matrícula Mais Recente</th>\
					<th>Curso</th>\
					<th>Turma</th>\
					<th><i class="fa fa-check-square-o"></i></th></tr>');
		           	if(res.err == "ok"){	               
		               		for(var i in res.useralunos){		               			         			
		               			var newRow = $('<tr class="animated fadeInDown">');
		               			var cb = "CB" + res.useralunos[i].idusuario;
		               			var cols = "";
		               			cols +='<td>'+res.useralunos[i].nome_usuario+'</td>';
		               			cols +='<td>'+res.useralunos[i].cpf_usuario+'</td>';
		               			cols +='<td>'+res.matricula[res.useralunos[i].idusuario]+'</td>';
		               			cols +='<td>'+res.curso[res.useralunos[i].idusuario]+'</td>';
		               			cols +='<td>'+res.turma[res.useralunos[i].idusuario]+'</td>';
		               			cols += '<td><input name="chkboxName" type="checkbox" id="' + cb +'" value="' + res.useralunos[i].idusuario +'" /></td>';
		               			newRow.append(cols);	               			
		               			rows.append(newRow);	               			               		
		               		}
		               		$("#tabela_alunos").html(rows);
		               		}else{
		               	mensagem('error',res.err);
		               	$("#tabela_alunos").html('');
		               }
					
		           }
		       });
		
	}
	$("#buscar").click(function(event){
	    event.preventDefault();
	    var cpf = $("input[type=text][name=cpf]").val();
	    var nome = $("input[type=text][name=nome]").val(); 
	    Buscar(cpf, nome);   
	});
	$("#matricular").click(function(event){
	    event.preventDefault();	               
	    var resposta = $("input[name=chkboxName]:checked").map( function () {return this.value;}).toArray();
	    var turma = $("input[type=hidden][name=turma]").val(); 
			jQuery.ajax({
	           type: "POST",
	           url: "<?php echo base_url(); ?>" + "index.php/turma/confirma_matriculas",
	           dataType: 'html',
	           data: {resposta:resposta, turma:turma},
	           success: function(res) {
	           		mensagem('success','Matrículas Realizadas com Sucesso!!');
	           		Buscar('','');            		
	           }	           
	       });
	});
});
</script>

	
