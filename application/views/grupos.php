<?php $this->template->menu('inicio') ?>
<div class="container-fluid">
    <div class="row-content" >
        <div class="col-sm-3" >
            <aside role="complementary">
                <ul class="nav nav-pills nav-stacked navbar-default">
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem'); ?>"><i class="fa fa-envelope" aria-hidden="true"></i> Caixa de Entrada</a></li>
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem/novo'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i> Nova Mensagem</a></li>
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem/enviado'); ?>"><i class="fa fa-share" aria-hidden="true"></i> Enviados</a></li>
                  <li role="presentation" class="active"><a href="#"><i class="fa fa-users" aria-hidden="true"></i> Grupos</a></li>
                </ul>
            </aside>
        </div>
        <div class="col-sm-9">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingPadrao">
                        <a style="color:black; text-decoration: none;" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePadrao" aria-expanded="true" aria-controls="collapseOne" class="menu" id="menuPadrao">
                          <center>    
                              <h4 style="display: inline;" class="panel-title">
                                Grupos Padrões
                              </h4>
                              <span class="pull-right spanMenu" id="spanmenuPadrao">
                                  <i class="fa fa-chevron-left" aria-hidden="true"></i>
                              </span>
                          </center>
                        </a>
                    </div>
                    <div id="collapsePadrao" class="panel-collapse collapse in">
                       <div class="list-group">
                              <?php 
                              foreach ($grupos_padroes as $key=>$grupo) {
                                  if (isset($grupo)){
                                     echo '<a href="#" data-toggle="collapsePadrao"  class="list-group-item submenu" id="item'.$key.'">'.$gruposNome[$key].'<span class="pull-right spanSubMenu" id="spanitem'.$key.'"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></a>';
                                     echo '<div class="collapse list-group-submenu" data-parent="#item'.$key.'" id="subitem'.$key.'">';
                                     foreach ($grupo as $key_g => $grupo_item) {
                                        echo '<a href="'.base_url('index.php/mensagem/mensagem_grupo/'.$key.'/'.$key_g).'" class="list-group-item sub-sub-item list-group-item-info" data-parent="#submenu1">'.$grupo_item.'</a>';    
                                     }
                                     echo '</div>';    
                                  }else{
                                      echo '<a href="'.base_url('index.php/mensagem/mensagem_grupo/'.$key).'" class="list-group-item">'.$gruposNome[$key].'</a>';
                                  }                           
                              }
                              ?>
                       </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingPersonalizado">
                      <h4 class="panel-title">
                        <a style="color:black; text-decoration: none;" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePersonalizado" aria-expanded="false" aria-controls="collapseOne" class="menu" id="menuPersonalizado">
                          <center>    
                              <h4 style="display: inline;" class="panel-title">
                                Grupos Personalizados
                              </h4>
                              <span class="pull-right spanMenu" id="spanmenuPersonalizado">
                                  <i class="fa fa-chevron-down" aria-hidden="true"></i>
                              </span>
                          </center>
                        </a>
                      </h4>
                    </div>
                    <div id="collapsePersonalizado" class="panel-collapse collapse">
                      <div class="list-group">
                        <?php 
                            foreach ($grupos_personalizados as $key => $grupo) {
                                echo '<a href="'.base_url('index.php/mensagem/mensagem_grupo/'.$key).'" class="list-group-item">'.$gruposNome[$key].'</a>';
                            } 
                        ?>
                        <a href="#" class="list-group-item list-group-item-success" data-toggle="modal" data-target="#myModal"  >Criar Novo Grupo<div class="pull-right"><i class="fa fa-plus-circle" aria-hidden="true"></i></div></a>
                      </div>
                    </div>
                </div>
           </div> 
        </div>

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Novo Grupo Personalizado</h4>
                    </div>
                    <?php echo form_open('mensagem/cria_grupo'); ?>
                    <div class="modal-body">
                        <div class="dropdown" id="drop">
                            <div class="input-group">
                                <span class="input-group-addon" id="nome-addon">Nome:</span>    
                                <?php echo form_input('nome_grupo',set_value('nome_grupo'),'class="form-control" id="nome" placeholder="Nome do Grupo" autocomplete="false"'); ?>
                            </div>
                            <br>
                            <span id="btn-search" data-toggle="dropdown"></span>
                            <div class="input-group <?php if (!(form_error('usuarios')=='')) echo 'has-error has-feedback'; ?>">
                                <span class="input-group-addon" id="user-addon" >Usuários:</span>
                                <?php echo form_input('usuarios', set_value('usuarios')?set_value('usuarios'):$usuarios, 'type="text", class="form-control" id="users" placeholder="Usuários" autocomplete="false"'); ?> 
                            </div>
                            <ul class="dropdown-menu" id="drop_usuarios">
                            </ul>
                        </div>
                        <div class="row">
                            <div id="usuarios" class="col-md-12"></div>
                        </div>  
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Criar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if(validation_errors()){?>
    <script type="text/javascript">mensagem('error',"<?php echo form_error('nome_grupo');?>");</script>
<?php }?>
<script type="text/javascript">
var usuariosAdd = [];

function buscaUsuario(nome,aberto) {
    event.preventDefault(); 
    jQuery.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>" + "index.php/usuario/buscaNomeEmail",
              dataType: 'json',
              data: {nome: nome, usuarios:usuariosAdd},
              success: function(res) {
                    if (res.err = 'ok'){
                        var row = '';
                        var j=0;
                        for (var i in res.users)
                            row += '<li><a class="'+j++ +' drop_link"  href="#" onclick="adicionar(\''+res.users[i].nome_usuario+'\','+res.users[i].idusuario+')">'+res.users[i].nome_usuario+'<br><small>'+res.users[i].email_usuario+' - '+res.users[i].nome_cursinho_unidade+'</small></a></li>';
                        $('#drop_usuarios').html(row);
                        if (j!=0){
                                if (!$('#drop').hasClass('open')){
                                    $('#drop').addClass('open');
                                    $('#btn-search').attr('aria-expanded','true');
                                }
                        }else{
                            console.log('a');
                            $('#drop').attr('class','dropdown');
                            $('#btn-search').attr('aria-expanded','false');
                        }
                    }else{
                        $('#drop').attr('class','dropdown');
                        $('#btn-search').attr('aria-expanded','false');
                    }
              }
          });
}

function adicionar(nome,id){
    if (usuariosAdd.indexOf(id)==-1){
        var itens = $('#usuarios').html();
        itens += '<span class="label label-default" style="display: inline-block; "id='+id+'>'+nome+'&nbsp;<a href="#" style="color: inherit;" onclick="fechar('+id+')"><i class="fa fa-times" aria-hidden="true"></i></a></span>&nbsp'
        $('#usuarios').html(itens);
        
            $('<input>').attr({
                type: 'hidden',
                id: id,
                name: 'usuario[]',
                value: id
            }).appendTo('form');
        usuariosAdd.push(id);
        console.log(usuariosAdd)
        $('[name=usuarios]').val('');
    }
}

function fechar(id){
    $('#'+id).remove();
    $('#'+id).remove();
    usuariosAdd.splice(usuariosAdd.indexOf(id),1);   
}


$(document).on('focus',"a.drop_link", function() {
        $(this).parent().addClass('active');
    }).on('blur',"a.drop_link", function() {
        $(this).parent().removeClass('active');
    });

$().ready(function (){
    $(".submenu").on("click",function(){
        $(".list-group-submenu").collapse('hide');
        $("#sub"+$(this).attr('id')).collapse('toggle');
        if ($('#span'+$(this).attr('id')).html()=='<i class="fa fa-chevron-down" aria-hidden="true"></i>'){
            $(".spanSubMenu").html('<i class="fa fa-chevron-down" aria-hidden="true"></i>');
            $('#span'+$(this).attr('id')).html('<i class="fa fa-chevron-left" aria-hidden="true"></i>');    
        }
        else {
            $('#span'+$(this).attr('id')).html('<i class="fa fa-chevron-down" aria-hidden="true"></i>');   
        } 
    });
    $(".menu").on("click",function(){
        if ($('#span'+$(this).attr('id')).html()=='<i class="fa fa-chevron-down" aria-hidden="true"></i>'){
            $(".spanMenu").html('<i class="fa fa-chevron-down" aria-hidden="true"></i>');
            $('#span'+$(this).attr('id')).html('<i class="fa fa-chevron-left" aria-hidden="true"></i>');    
        }
        else {
            $('#span'+$(this).attr('id')).html('<i class="fa fa-chevron-down" aria-hidden="true"></i>');   
        } 
    });
    var aberto = false;
    $(".dropdown").on("show.bs.dropdown", function(event){
        aberto = true;
    });
    $(".dropdown").on("hide.bs.dropdown", function(event){
        aberto = false;
    });
    $("[name=usuarios]").on('keyup',function(event){
        var nome = $('[name=usuarios]').val();
        if (event.keyCode==40)
            $(".0").trigger('focus');
        else{
            if(nome==''){
                $('#drop').remove('open');
                $('#btn-search').attr('aria-expanded','false');
            }
            if(nome!=''){
                buscaUsuario(nome,aberto);
            }
        }
    });
    
});
</script>