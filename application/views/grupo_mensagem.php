<?php $this->template->menu('inicio') ?>
<div class="container-fluid">
    <div class="row-content" >
        <div class="col-sm-3" >
            <aside role="complementary">
                <ul class="nav nav-pills nav-stacked navbar-default">
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem'); ?>"><i class="fa fa-envelope" aria-hidden="true"></i> Caixa de Entrada</a></li>
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem/novo'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i> Nova Mensagem</a></li>
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem/enviado'); ?>"><i class="fa fa-share" aria-hidden="true"></i> Enviados</a></li>
                  <li role="presentation" class="active"><a href="<?php echo base_url('index.php/mensagem/grupo'); ?>"><i class="fa fa-users" aria-hidden="true"></i> Grupos</a></li>
                </ul>
            </aside>
        </div>
        <div class="col-sm-9">
            <div class="btn-group btn-group-justified">
                <a class="btn btn-default" type="button" data-toggle="modal" href="#modalMembros" id="btnVerMembros">Ver Membros</a>
                <?php if ($personalizado){ ?>
                <a class="btn btn-default" type="button" data-toggle="modal" href="#modalGrupo" id="btnAdicionarMembros">Adicionar Membros</a>
                <?php } ?>
                <a type="button" class="btn btn-danger" href="<?php echo base_url('index.php/mensagem/remove_grupo/'.$idgrupo); ?>">Remover Grupo</a>
            </div>
            <?php if (count($mensagens)>0){ ?>
            <table class="table">
              <thead>
                  <th class="col-md-3">Remetente</th>
                  <th class="col-md-7">Assunto</th>
                  <th class="col-md-1">Opções</th>
              </thead>
              <tbody>
                <?php foreach ($mensagens as $mensagem): ?>
                    
                <tr class='clickable-row' style='cursor:pointer' >
                    <td class ="redirect-column" data-href='<?php echo base_url('index.php/mensagem/ver/'.$mensagem['idmensagem']); ?>'>
                        <?php echo $mensagem['nome_usuario']; ?>
                    </td>
                    <td class = "redirect-column" data-href='<?php echo base_url('index.php/mensagem/ver/'.$mensagem['idmensagem']); ?>'>
                        <?php echo $mensagem['assunto_mensagem'];
                            if($mensagem['count']!=0) echo '&nbsp;<span class="badge">'.$mensagem['count'].'</span>'; ?>
                    </td>
                    <td align="center">
                        <button type="button" title="Remover Mensagem com o Assunto: <?php echo $mensagem['assunto_mensagem'] ?>"  class="btn btn-danger" onclick="location.href='<?php echo base_url('index.php/mensagem/remove/'.$mensagem['idmensagem']); ?>'"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                
                <?php endforeach ?>
              </tbody>
            </table>  
            <?php } ?> 
            <div id="modalMembros" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Membros do Grupo</h4>
                        </div>
                        <div class="modal-body">
                            <div class="pre-scrollable">
                                <table class="table">
                                    <thead>
                                        <th>
                                            Nome
                                        </th>
                                        <?php if ($personalizado){ ?>
                                        <th>
                                            Opções
                                        </th>
                                        <?php } ?>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($usuarios as $key => $usuario): ?>
                                            <tr>
                                                <td><?php echo $usuario['nome_usuario'] ?></td>
                                                <?php if($personalizado){ ?>
                                                    <td><button type="button" title="Remover <?php echo $usuario['nome_usuario'] ?> do Grupo"  class="btn btn-danger" onclick="location.href='<?php echo base_url(); ?>index.php/mensagem/remove_usuario/<?php echo $idgrupo; ?>/<?php echo $usuario['idusuario']; ?>'"><i class="fa fa-trash"></i></button></td>
                                                <?php } ?>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>    
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div id="modalGrupo" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Adicionar Membros</h4>
                    </div>
                    <?php echo form_open('mensagem/cria_grupo'); ?>
                    <?php echo form_hidden('idgrupo',$idgrupo); ?>
                    <div class="modal-body">
                        <div class="dropdown" id="drop">
                            <span id="btn-search" data-toggle="dropdown"></span>
                            <div class="input-group <?php if (!(form_error('usuarios')=='')) echo 'has-error has-feedback'; ?>">
                                <span class="input-group-addon" id="user-addon" >Usuários:</span>
                                <?php echo form_input('usuarios', set_value('usuarios')?set_value('usuarios'):$users, 'type="text", class="form-control" id="users" placeholder="Usuários" autocomplete="false"'); ?> 
                            </div>
                            <ul class="dropdown-menu" id="drop_usuarios">
                            </ul>
                        </div>
                        <div class="row">
                            <div id="usuarios" class="col-md-12"></div>
                        </div>  
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Adicionar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
        </div>
    </div>
    
</div>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<script type="text/javascript">
var usuariosAdd = [];

function buscaUsuario(nome,aberto) {
    event.preventDefault(); 
    jQuery.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>" + "index.php/usuario/buscaNomeEmail",
              dataType: 'json',
              data: {nome: nome, usuarios:usuariosAdd},
              success: function(res) {
                    if (res.err = 'ok'){
                        var row = '';
                        var j=0;
                        for (var i in res.users)
                            row += '<li><a class="'+j++ +' drop_link"  href="#" onclick="adicionar(\''+res.users[i].nome_usuario+'\','+res.users[i].idusuario+')">'+res.users[i].nome_usuario+'<br><small>'+res.users[i].email_usuario+' - '+res.users[i].nome_cursinho_unidade+'</small></a></li>';
                        $('#drop_usuarios').html(row);
                        if (j!=0){
                                if (!$('#drop').hasClass('open')){
                                    $('#drop').addClass('open');
                                    $('#btn-search').attr('aria-expanded','true');
                                }
                        }else{
                            console.log('a');
                            $('#drop').attr('class','dropdown');
                            $('#btn-search').attr('aria-expanded','false');
                        }
                    }else{
                        $('#drop').attr('class','dropdown');
                        $('#btn-search').attr('aria-expanded','false');
                    }
              }
          });
}

function adicionar(nome,id){
    if (usuariosAdd.indexOf(id)==-1){
        var itens = $('#usuarios').html();
        itens += '<span class="label label-default" style="display: inline-block; "id='+id+'>'+nome+'&nbsp;<a href="#" style="color: inherit;" onclick="fechar('+id+')"><i class="fa fa-times" aria-hidden="true"></i></a></span>&nbsp'
        $('#usuarios').html(itens);
        
            $('<input>').attr({
                type: 'hidden',
                id: id,
                name: 'usuario[]',
                value: id
            }).appendTo('form');
        usuariosAdd.push(id);
        console.log(usuariosAdd)
        $('[name=usuarios]').val('');
    }
}

function fechar(id){
    $('#'+id).remove();
    $('#'+id).remove();
    usuariosAdd.splice(usuariosAdd.indexOf(id),1);   
}


$(document).on('focus',"a.drop_link", function() {
        $(this).parent().addClass('active');
    }).on('blur',"a.drop_link", function() {
        $(this).parent().removeClass('active');
    });

$().ready(function (){
    var aberto = false;
    $(".dropdown").on("show.bs.dropdown", function(event){
        aberto = true;
    });
    $(".dropdown").on("hide.bs.dropdown", function(event){
        aberto = false;
    });
    $("[name=usuarios]").on('keyup',function(event){
        var nome = $('[name=usuarios]').val();
        if (event.keyCode==40)
            $(".0").trigger('focus');
        else{
            if(nome==''){
                $('#drop').remove('open');
                $('#btn-search').attr('aria-expanded','false');
            }
            if(nome!=''){
                buscaUsuario(nome,aberto);
            }
        }
    });
    $('.redirect-column').click(function(){
        window.location = $(this).data('href');
    });
});





</script>