<?php $this->template->menu($view) ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3><b>Lista de Coordenadores Docentes</b></h3>
            
            <table id="faculdades" class="table table-hover">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Cidade</th> 
                        <th>Faculdade</th>
                        <th>Opções</th>
                    </tr>   
                </thead>
                <?php foreach ($docentes as $docente) { ?>
                <tr class="animated fadeInDown">
                    <td><?php echo $docente['nome_usuario'];?></td>
                    <td><?php echo $docente['cidade_usuario']; ?></td> 
                     <td><?php echo $docente['nome_faculdade']; ?></td>              
                    <td>
                        <a href="<?php echo base_url('index.php/docente/edita'); echo '/'.$docente['idusuario'] ?>" data-toggle="tooltip" data-placement="top" title="Editar"><button type="button" class="btn btn-default"><i class="fa fa-pencil-square-o"></i></button></a>
                     </td>
                </tr>
                <?php } ?>
            </table>    
        </div>
        <div class="col-md-1 col-md-offset-9">
            <button class="btn btn-default" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>          
    </div>
</div>

<script type="text/javascript">


$(document).ready(function () {
    tabela('faculdades');
    $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/docente/opcoes";  
    });
}); 
</script>

<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<?php if(isset($msg)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $msg;?>");</script>
<?php }?>

