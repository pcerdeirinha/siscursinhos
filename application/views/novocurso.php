<?php $this->template->menu($view) ?>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <?php echo form_open('curso/cria'); ?>
            <?php if((isset($idcurso))||(set_value('idcurso')!='')){/*Então é Update*/?>
                    <h3><b>Edita Curso</b></h3>
                    <br>
                    <?php echo form_hidden('idcurso', $idcurso); 
                }else{ ?>
                    <h3><b>Novo Curso</b></h3>
                    <br>
        <?php } ?> 
            <br>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#dadosCurso">Dados do Curso</a></li>
            </ul>
        </div>
        
         
        <div class="tab-content">
            <div id="dadosurso" class="tab-pane fade in active">
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('nome_curso')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Nome do Curso', 'nome_curso'); ?>
                        <?php echo form_input('nome_curso',set_value('nome_curso')?set_value('nome_curso'):$nome_curso, 'type="text", class="form-control" id="nome" placeholder="Nome do Curso"'); ?> 
                        <?php if (!(form_error('nome_curso')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('nome_curso'); ?></span>
                    </div>
                </div>
                <div class="col-md-2 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('ano_criacao')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Ano criação', 'ano'); ?><br>
                            <?php echo form_dropdown('ano_criacao', $ano_criacao, set_value('ano_criacao')?set_value('ano_criacao'):$ano_sel, 'type="text" min="2", class="form-control" id="ano"'); ?>
                            <?php if (!(form_error('ano_criacao')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('ano_criacao'); ?></span>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group <?php if (!(form_error('duracao')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Duração (em meses)', 'duracao'); ?><br>
                            <?php echo form_dropdown('duracao', $duracao,set_value('duracao')?set_value('duracao'):$duracao_sel, 'type="text" min="2", class="form-control" id="duracao"'); ?>
                            <?php if (!(form_error('duracao')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('duracao'); ?></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1 col-md-offset-8">
            <div class="form-save-buttons">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="col-md-1">
            <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function () {
    $("#voltar").click(function(event){
        window.location.href = "<?php echo base_url('index.php/curso'); ?>"; 
    });
}); 
</script>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>