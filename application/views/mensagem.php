<?php $this->template->menu('inicio') ?>
<div class="container-fluid">
    <div class="row-content" >
        <div class="col-sm-3" >
            <aside role="complementary">
                <ul class="nav nav-pills nav-stacked navbar-default">
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem'); ?>"><i class="fa fa-envelope" aria-hidden="true"></i> Caixa de Entrada</a></li>
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem/novo'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i> Nova Mensagem</a></li>
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem/enviado'); ?>"><i class="fa fa-share" aria-hidden="true"></i> Enviados</a></li>
                  <?php if ($view>1){ ?>
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem/grupo'); ?>"><i class="fa fa-users" aria-hidden="true"></i> Grupos</a></li>
                  <?php } ?>
                </ul>
            </aside>
        </div>
        <div class="col-sm-9">
            <div class="col-md-4">
                <button class="btn btn-default btn-block" type="button" data-toggle="modal" data-target="#modalMembros" id="btnVerMembros">Ver Membros da Conversa</button>
            </div>
            <br><hr>
            <?php foreach ($recados as $key => $recado){
                $nome = explode(' ', $recado['nome_social_usuario']==null?$recado['nome_usuario']:$recado['nome_social_usuario']);    
                //print_r($recado);
            ?>
            <div class="media">
              <div class="media-left">
                <span class="fa-stack fa-3x">
                    <i class="fa fa-circle fa-stack-2x" style="color:
                        <?php
                            switch ($recado['tipo_usuario']) {
                                case 5:
                                    echo "#490A3D";
                                    break;
                                case 4:
                                    echo "#BD1550";
                                    break;
                                case 3:
                                    echo "#E97F02";
                                    break;
                                case 2:
                                    echo "#F8CA00";
                                    break;
                                case 1:
                                    echo "#8A9B0F";
                                    break;
                                default:
                                    echo "#000000";
                                    break;
                            }
                        
                        ?>;">
                    </i>
                    <strong class="fa-stack-1x fa-stack-text fa-inverse">
                        <?php
                        if (count($nome)>1)
                            echo strtoupper($nome[0]{0}).strtoupper($nome[count($nome)-1]{0});
                        else
                            echo strtoupper($nome[0]{0});
                            ?>
                    </strong>
                </span>
              </div>
              <div class="media-body">
                <h4 class="media-heading"><?php echo $recado['nome_social_usuario']==null?$recado['nome_usuario']:$recado['nome_social_usuario']; ?>, diz:</h4>
                <p id="recado<?php echo $recado['idrecado'];?>"><?php echo $recado['corpo_recado']; ?></p>
                <?php if ($recado['resposta_mensagem']!=0){ ?>
                    <small>
                        <a href="#" onclick="abrirResposta(<?php echo $recado['idrecado'] ?>,<?php echo $recado['resposta_mensagem'] ?>);">Responder</a>
                    </small>
                <?php } ?>
                <?php if ($respostas[$key]){ ?>
                <div class="media">
                  <hr>
                  <!--INICIO RESPOSTA-->
                  <?php foreach ($respostas[$key] as $key_r => $resposta){
                     $nome_re = explode(' ', $resposta['nome_social_usuario']==null?$resposta['nome_usuario']:$resposta['nome_social_usuario']);    
                   ?>
                     <div class="media-left">
                        <span class="fa-stack fa-2x">
                            <i class="fa fa-circle fa-stack-2x" style="color:
                            <?php
                                switch ($resposta['tipo_usuario']) {
                                    case 5:
                                        echo "#490A3D";
                                        break;
                                    case 4:
                                        echo "#BD1550";
                                        break;
                                    case 3:
                                        echo "#E97F02";
                                        break;
                                    case 2:
                                        echo "#F8CA00";
                                        break;
                                    case 1:
                                        echo "#8A9B0F";
                                        break;
                                    default:
                                        echo "#000000";
                                        break;
                                }
                            
                            ?>;">
                            </i>
                            <strong class="fa-stack-1x fa-stack-text fa-inverse">
                                <?php
                                if (count($nome_re)>1)
                                    echo strtoupper($nome_re[0]{0}).strtoupper($nome_re[count($nome_re)-1]{0});
                                else
                                    echo strtoupper($nome_re[0]{0});
                                    ?>
                            </strong>
                        </span>
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading"><?php echo $resposta['nome_social_usuario']==null?$resposta['nome_usuario']:$resposta['nome_social_usuario'] ?>, responde <b><?php if ($resposta['publica_resposta']==1) echo 'publicamente:'; else echo 'confidencialmente:' ?></b></h5>
                        <p><?php echo $resposta['recado_resposta']; ?></p>
                    </div>
                    <hr>
                    <?php } ?>
                  <!--FIM RESPOSTA -->
                </div>    
              <?php } ?>
              </div>
            </div>
            <hr>
            <?php } ?>
            <?php if ($recados[0]['resposta_mensagem']){
                echo form_open('mensagem/resposta/'.$recados[0]['mensagem_idmensagem']); ?>
            <div class="form-group <?php if (!(form_error('assunto')=='')) echo 'has-error has-feedback'; ?>">
                <?php echo form_label('Resposta:','resposta') ?>
                <?php echo form_textarea('resposta', set_value('resposta')?set_value('resposta'):$resp, 'type="text" class="form-control" id="resposta" placeholder="Resposta"'); ?> 
            </div>
            <div class="col-md-1 col-md-offset-10">
                <div class="form-save-buttons">
                    <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Enviar</button>&ensp;
                </div>
            </div>
                <?php echo form_close(); 
            } ?>
            <div class="col-md-1">
                <div class="form-save-buttons">
                    <button class="btn btn-defaut" type="button" id="voltar" onclick="location.href= '<?php echo base_url('index.php/mensagem'); ?>';"><i class="fa fa-reply"></i>Voltar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalResposta" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Resposta</h4>
                    </div>
                    <?php echo form_open('mensagem/resposta_recado'); ?>
                    <?php echo form_hidden('idRecado'); ?>
                    <div class="modal-body">
                        <p id="recado"></p>
                        <div class="form-group <?php if (!(form_error('assunto')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Resposta:','resposta') ?>
                            <?php echo form_textarea('resposta', set_value('resposta')?set_value('resposta'):$resp, 'type="text" class="form-control" id="resposta" placeholder="Resposta"'); ?> 
                        </div>       
                        <div class="radio" id="privada">
                            <label>
                                <input type="radio" name="tipo_resposta" id="res_privada" value="0" checked>
                                Resposta Privada
                            </label>
                        </div>
                        <div class="radio" id="publica">
                            <label>
                                <input type="radio" name="tipo_resposta" id="res_publica" value="1">
                                Resposta Pública
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Enviar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
        <div id="modalMembros" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Membros da Conversa</h4>
                        </div>
                        <div class="modal-body">
                            <div class="pre-scrollable">
                                <table class="table">
                                    <thead>
                                        <th>
                                            Nome
                                        </th>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($usuarios as $key => $usuario): ?>
                                            <tr>
                                                <td><?php echo $usuario['nome_usuario'] ?></td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>    
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
</div>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<script type="text/javascript">
 function abrirResposta(id,tipo){
     $("#modalResposta").modal("show");
     $("[name=idRecado]").val(id);
     if (tipo==1){
         $("#res_publica").attr("checked","checked");
         $("#privada").hide();
     }else if (tipo==2){
         $("#res_privada").attr("checked","checked");
         $("#publica").hide();
     }
     $("#recado").html($("#recado"+id).html());
 }
</script>