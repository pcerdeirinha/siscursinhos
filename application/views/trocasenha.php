<?php $this->template->menu('inicio');?>   

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">    
                <h3>Troca de Senha</h3>
            </div>
            <?php echo form_open('usuario/updatesenha'); ?> 
            <div class="col-md-8 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('senha_atual')=='')) echo 'has-error has-feedback'; ?>">
                        <label for="nome">Digite sua senha atual: </label>                                    
                        <?php echo form_password('senha_atual', set_value('senha_atual')?set_value('senha_atual'):$senha_atual, 'class="form-control" id="senha_atual" placeholder="Senha atual"'); ?>
                        <?php if (!(form_error('senha_atual')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('senha_atual'); ?></span>
                    </div>
            </div>            
            <div class="col-md-8 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('senha_nova')=='')) echo 'has-error has-feedback'; ?>">
                        <label for="nome">Digite a nova senha: </label>                                    
                        <?php echo form_password('senha_nova', set_value('senha_nova')?set_value('senha_nova'):$senha_nova, 'class="form-control" id="senha_nova" placeholder="Nova senha"'); ?>
                        <?php if (!(form_error('senha_nova')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('senha_nova'); ?></span>
                    </div>
            </div>            
            <div class="col-md-8 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('senha_redigite')=='')) echo 'has-error has-feedback'; ?>">
                        <label for="nome">Repita a nova senha: </label>                                    
                        <?php echo form_password('senha_redigite', set_value('senha_redigite')?set_value('senha_redigite'):$senha_redigite, 'class="form-control" id="senha_redigite" placeholder="Redigite a nova senha"'); ?>
                        <?php if (!(form_error('senha_redigite')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('senha_redigite'); ?></span>
                    </div>
            </div>
            <div class="col-md-1 col-md-offset-8">
                <div class="form-save-buttons">
                    <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
                </div>
            </div>
            <?php echo form_close(); ?>
            <div class="col-md-2">
                <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
            </div>            
        </div>
    </div>
<?php if(isset($err)){?>
<script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>

<script type="text/javascript">
    $(document).ready(function () {    
        $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/inicio";  
        });
    }); 
</script>

    

  
