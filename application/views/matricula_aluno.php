<?php $this->template->menu($view) ?>
<div class="container">
    <div class='row'>
        <div class="col-md-8 col-md-offset-2">
            <?php if($matriculas == null){
                    echo '<center><h3>O(A) Aluno(a) '.$info.' não possui histórico de inscrições!</h3></center>';                    
                    $mostrar = true; 
            }
            else{
                    echo '<center><h3>Histórico de inscrições do aluno: '.$info.'</h3></center>';
                    $mostrar = true; 
            ?>            
            <div class="table-responsive">
                <table class="table table-hover table-bordered" id="matriculas">
                    <thead>
                        <tr>
                            <th>Curso</th>
                            <th>Turma</th>
                            <th>Status</th>
                            <th>Incrição</th>
                            <th>Trancamento</th>                            
                            <th>Opções</th>
                        </tr>                        
                    </thead>
                    <tbody>
                    <?php foreach ($matriculas as $matricula) { ?>
                    <?php
                        switch ($matricula['status']) {
                            case 0:
                                echo '<tr class="danger animated fadeInDown">';
                                break;
                            case 1:
                                echo '<tr class="success animated fadeInDown">';
                                break;
                            case 2:
                                echo '<tr class="active animated fadeInDown">';
                                break;
                            case 3:
                                echo '<tr class="warning animated fadeInDown">';
                                break;
                            case 4:
                                echo '<tr class="warning animated fadeInDown">';
                                break;
                            case 5:
                                echo '<tr class="info animated fadeInDown">';
                                break;
                            case 6:
                                echo '<tr class="info animated fadeInDown">';
                                break;
                            default:
                                echo '<tr class="danger animated fadeInDown">';
                                break;
                        }                            
                    ?>                    
                        <td><?php echo $nome_curso[$turmacurso[$matricula['matricula_idturma']]]; ?></td>
                        <td><?php echo $turmas[$matricula['matricula_idturma']];?></td>
                        <td>
                        <?php
                            switch ($matricula['status']) {
                                case 0:
                                    echo 'Cancelada';
                                    break;
                                case 1:
                                    echo 'Ativa';
                                    break;
                                case 2:
                                    echo 'Aprovação';
                                    break;
                                case 3:
                                    echo 'Desistência';
                                    break;
                                case 4:
                                    echo 'Abandono';
                                    break;
                                case 5:
                                    echo 'Transferência';
                                    break;
                                case 6:
                                    echo 'Consolidada';
                                    break;
                                
                                default:
                                    echo 'Cancelada';
                                    break;
                            }                            
                        ?>
                        </td>
                        <td>
                            <?php 
                            $date = DateTime::createFromFormat('Y-m-d H:i:s', $matricula['data_matricula'] );
                            $datas_matricula =  $date->format('d/m/Y');
                            echo $datas_matricula;?>
                        </td>
                        <td>
                            <?php if(isset($matricula['data_trancamento'])){
                                $date = DateTime::createFromFormat('Y-m-d', $matricula['data_trancamento'] );
                                $data_trancamento =  $date->format('d/m/Y'); 
                                echo $data_trancamento;
                            }else echo '--/--/----';  
                            ?>
                        </td>                       
                        <td>
                            <?php if($matricula['status']==1){
                                $idmatricula = base64_encode($matricula['idmatricula']);?>
                                <button type="button" class="btn btn-danger"  data-toggle="modal" data-target="#cancelar" data-placement="top" title="Cancelamento de Matrícula"><i class="fa fa-user-times"></i></button>
                                <button type="button" class="btn btn-primary"  onclick = "transferirAluno('<?php echo $idmatricula; ?>');" title="Transferência"><i class="fa fa-exchange" aria-hidden="true"></i></button>
                            <?php $mostrar = false;
                            }else{?>
                                <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Informações da Matrícula" value="<?php echo base64_encode($matricula['idmatricula']); ?>" tipo="informacao"><i class="fa fa-info-circle" aria-hidden="true"></i></button>
                            <?php } ?> 
                        </td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>      
            </div>      
            <?php } ?>
        </div>
        <div class="col-md-4 col-md-offset-4">
            <button class="btn btn-default btn-block"  id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>        
    </div>
<?php if ($mostrar){?>    
    <div class='row'>
        <p></p>
        <div class="col-md-4 col-md-offset-4">
            <div class="form-group"> 
                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#matricular"><i class="fa fa-user-plus"></i> Inscrever</button>             
            </div>          
        </div>
        <?php echo form_open('aluno/matricular/'.$aluno); ?> 
        <div class="modal fade" id="matricular" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel" align="CENTER">Inscrição</h3>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12"><p>Selecione o curso, a turma e a data que deseja inscrever o(a) aluno(a) <?php echo $info; ?>:</p></div>                            
                            <div class="col-md-4">
                                <div class="form-group">                        
                                    <?php echo form_label('Curso', 'nome_curso'); ?>
                                    <?php echo form_dropdown('nome_curso',$nome_curso, set_value('nome_curso')?set_value('nome_curso'):$nome_curso_sel, 'type="text" min="2", class="form-control" id="curso" placeholder="Curso"'); ?>                               
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">                  
                                    <?php echo form_label('Turma', 'nome_turma'); ?>
                                    <?php echo form_dropdown('nome_turma',$nome_turma, set_value('nome_turma')?set_value('nome_turma'):$nome_turma_sel, 'type="text" min="2", class="form-control" id="turma" placeholder="Turma"'); ?>
                                </div>                          
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php echo form_label('Data da Inscrição', 'data_matricula'); ?> 
                                    <?php echo form_input('data_matricula', $data_matricula, 'type="date", class="form-control" id="data_matricula" placeholder="Data matricula" tipo="data"'); ?> 
                                </div>
                            </div>
                            <div class="col-md-12"><p class="text-warning" id="mensagemVagas"></p></div> 
                        </div>                                                                          
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <button type="submit" class="btn btn-primary" id="matricular">Inscrever</button>
                    </div>
                </div>
            </div>
        </div> 
        <?php echo form_close(); ?>
    </div>
<?php }else{ ?>
    
    <div class='row'>
        <?php echo form_open('aluno/transferir/'.$aluno); ?> 
        <?php echo form_hidden('id_matricula'); ?>
        <div class="modal fade" id="transferirModal" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel" align="CENTER">Transferência</h3>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12"><p>Selecione o curso, a turma e a data que deseja transferir o(a) aluno(a) <?php echo $info; ?>:</p></div>                            
                            <div class="col-md-4">
                                <div class="form-group">                        
                                    <?php echo form_label('Curso', 'nome_curso'); ?>
                                    <?php echo form_dropdown('nome_curso',$nome_curso, set_value('nome_curso')?set_value('nome_curso'):$nome_curso_sel, 'type="text" min="2", class="form-control" id="curso" placeholder="Curso"'); ?>                               
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">                  
                                    <?php echo form_label('Turma', 'nome_turma'); ?>
                                    <?php echo form_dropdown('nome_turma',$nome_turma, set_value('nome_turma')?set_value('nome_turma'):$nome_turma_sel, 'type="text" min="2", class="form-control" id="turma" placeholder="Turma"'); ?>
                                </div>                          
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php echo form_label('Data da Transferência', 'data_matricula'); ?> 
                                    <?php echo form_input('data_matricula', $data_matricula, 'type="date", class="form-control" id="data_matricula" placeholder="Data matricula" tipo="data"'); ?> 
                                </div>
                            </div>
                            <div class="col-md-12"><p class="text-warning" id="mensagemVagas"></p></div> 
                        </div>                                                                          
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <button type="submit" class="btn btn-primary" id="matricular">Transferir</button>
                    </div>
                </div>
            </div>
        </div> 
        <?php echo form_close(); ?>
    </div>
    
    
   <div class="row">
    <?php echo form_open('matricula/cancelar/'.$idmatricula); ?> 
        <div class="modal fade" id="cancelar" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel" align="CENTER">Cancelar Matrícula</h3>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12"><p>Selecione o tipo de cancelamento, a data e a justificativa:</p></div>                            
                            <div class="col-md-4">
                                <div class="form-group">                                                        
                                    <?php echo form_label('Tipo', 'tipos'); ?>
                                    <?php echo form_dropdown('tipos',$tipos, '', 'type="text" min="2", class="form-control" id="tipos" placeholder="Tipos"'); ?>                               
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">                  
                                    <?php echo form_label('Justificativa', 'justificativa'); ?>
                                    <?php echo form_dropdown('justificativa',$justificativa, '', 'type="text" min="2", class="form-control" id="justificativa" placeholder="Justificativa"'); ?>
                                </div>                              
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <?php echo form_label('D. Cancelamento', 'data_cancelamento'); ?> 
                                    <?php echo form_input('data_cancelamento', $data_cancelamento, 'type="date", class="form-control" id="data_cancelamento" placeholder="Data cancelamento" tipo="data"'); ?> 
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <?php //echo form_label('Observações', 'matricula_observacoes'); ?> 
                                    <textarea name="matricula_observacoes" class="form-control" rows="3" id="matricula_observacoes" placeholder="Observações"  maxlength="300"></textarea>
                                    <?php //echo form_textarea('matricula_observacoes', $matricula_observacoes,'', 'class="form-control" rows="3" id="matricula_observacoes" placeholder="Observações"'); ?> 
                                </div>
                            </div>
                            <div class="col-md-12"><p class="text-danger">O processo de cancelamento de matrícula é irreversível, portanto faça-o com cuidado.</p></div>   
                        </div>                                                                          
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <button type="submit" class="btn btn-danger" id="matricular">Cancelar Matrícula</button>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
<?php } ?>
    <div class="row">
        <div class="modal fade" id="informacao" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel" align="CENTER">Informações da Matrícula</h3>
                    </div>
                    <div class="modal-body">
                        <div class="row">                                                      
                            <div class="col-md-12">
                                <div class="panel panel-primary">
                                  <div class="panel-heading">Justificativa:</div>
                                  <div class="panel-body">
                                        <p name="jus" id="jus"></p> 
                                  </div>
                                </div>
                            </div>                            
                            <div class="col-md-12">
                                <div class="panel panel-primary">
                                  <div class="panel-heading">Observações:</div>
                                  <div class="panel-body">
                                        <p name="obs" id="observacoes"></p>
                                  </div>
                                </div>
                            </div>                            
                        </div>                                                                          
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>                        
                    </div>
                </div>
            </div>
        </div>
    </div> 
      
</div>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<?php if(isset($matriculado)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $matriculado;?>");</script>
<?php }?>
<script type="text/javascript">
function transferirAluno(idmatricula) {
    $('[name=id_matricula]').val(idmatricula);
    $("#transferirModal").modal({show: true});
}


$(document).ready(function () {
    mascara();
    data(true);
    tabela('matriculas',5,3);
    <?php if ($mostrar){?>
    var idCurso= $('option:selected', "[name=nome_curso]").val();
        jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/turma/getTurmaCurso",
                  dataType: 'json',
                  data: {idcurso: idCurso},
                  success: function(res) {                    
                    var row ='';      
                    for(var i in res.turmas){
                       row+='<option value="' + i + '">'+res.turmas[i]+'</option>';
                                             
                    }
                    $("[name=nome_turma]").html(row);
                  }
              });
    <?php }?>
    var turma= $('option:selected', "[name=nome_turma]").val();
    jQuery.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>" + "index.php/matricula/qtdMatriculas",
              dataType: 'json',
              data: {turma: turma},
              success: function(res) {                  
                    if((parseInt(res.matriculas) + 1) > parseInt(res.vagas)){
                        $("#mensagemVagas").html('<i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i> Atenção: O número de alunos selecionados excede o número de vagas desta turma!');
                    }else{
                        $("#mensagemVagas").html('');
                    }                   
                
              }
          });
    var tipo = $('option:selected', "[name=tipos]").val();
    jQuery.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>" + "index.php/matricula/getJustificativa",
              dataType: 'json',
              data: {tipo: tipo},
              success: function(res) {                  
                var row ='';      
                for(var i in res.justificativa){
                   row+='<option value="' + i + '">'+res.justificativa[i]+'</option>';                                          
                }
                $("[name=justificativa]").html(row);
              }
    });
    $("[name=nome_curso]").change(function(event) {
        event.preventDefault(); 
        var idCurso= $('option:selected', "[name=nome_curso]").val();
        jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/turma/getTurmaCurso",
                  dataType: 'json',
                  data: {idcurso: idCurso},
                  success: function(res) {                  
                    var row ='';      
                    for(var i in res.turmas){
                       row+='<option value="' + i + '">'+res.turmas[i]+'</option>';                                          
                    }
                    $("[name=nome_turma]").html(row);
                  }
        });
    });
    $("[name=nome_turma]").change(function(event) {
        event.preventDefault(); 
        var turma= $('option:selected', "[name=nome_turma]").val();
        jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/matricula/qtdMatriculas",
                  dataType: 'json',
                  data: {turma: turma},
                  success: function(res) {                  
                        if((parseInt(res.matriculas) + 1) > parseInt(res.vagas)){
                            $("#mensagemVagas").html('<i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i> Atenção: O número de alunos selecionados excede o número de vagas desta turma!');
                        }else{
                            $("#mensagemVagas").html('');
                        }                   
                    
                  }
              });
    });
    $("[name=tipos]").change(function(event) {
        event.preventDefault(); 
        var tipo = $('option:selected', "[name=tipos]").val();
        jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/matricula/getJustificativa",
                  dataType: 'json',
                  data: {tipo: tipo},
                  success: function(res) {                  
                    var row ='';      
                    for(var i in res.justificativa){
                       row+='<option value="' + i + '">'+res.justificativa[i]+'</option>';                                          
                    }
                    $("[name=justificativa]").html(row);
                  }
        });
    });
    $(document).on('click',"[tipo=informacao]", function () {
        var matricula = $(this).val();
        jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/matricula/infoMatricula",
                  dataType: 'json',
                  data: {matricula: matricula},
                  success: function(res) {
                    console.log(res);
                    $('[name="jus"]').html(res.justificativa);
                    $('[name="obs"]').html(res.observacao); 
                  }
        });
        $("#informacao").modal('show');            
    } );
    
    $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"<?php echo $caminho; ?>";  
    });
}); 
</script>

