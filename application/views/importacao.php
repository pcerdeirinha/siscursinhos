<?php $this->template->menu($view) ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">            
                <?php echo form_open('importacao/importar_alunos',' enctype="multipart/form-data"'); ?>      
                <h3><b>Importação de Alunos</b></h3>
                
                <p>Baixe o modelo <a href="<?php echo base_url('index.php/importacao/modelo') ?>"> aqui</a></p>
        </div>
                
        <div class="col-md-6 col-md-offset-2">
            <div class="form-group">
                <?php echo form_label('Planilha', 'planilha'); ?>
                <?php echo form_upload('planilha','','accept = ".xlsx"'); ?> 
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <?php echo form_label('Formato da Data', 'data'); ?>
                <?php echo form_dropdown('data',array('dd/mm/YYYY' => 'd/m/Y', 'dd-mm-YYYY' => 'd-m-Y', 'YYYY/mm/dd' => 'Y/m/d', 'YYYY-mm-dd' => 'Y-m-d', 'mm/dd/YYYY' => 'm/d/Y', 'mm-dd-YYYY' => 'm-d-Y'),''); ?> 
            </div>
        </div> 
        <div class="col-md-1 col-md-offset-8">
            <div class="form-save-buttons">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="col-md-1">
            <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>
    </div>
</div>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>

 