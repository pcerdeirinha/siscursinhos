<?php 
$this->template->menu($view);
?>
<link href="<?php echo base_url('css/fullcalendar.min.css')?>" rel="stylesheet" />
<link href="<?php echo base_url('css/fullcalendar.print.min.css')?>" rel="stylesheet" media="print" />
<script src="<?php echo base_url('js/moment.min.js')?>"></script>
<script src="<?php echo base_url('js/fullcalendar.min.js')?>"></script>
<script src="<?php echo base_url('js/locale/pt-br.js')?>"></script>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div id="calendar"></div>
        </div>
        <div class="modal fade" id="evento_modal" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="nome_evento_modal" align="CENTER"></h3>
                    </div>
                    <div class="modal-body">
                        <p align="CENTER" id="descricao_evento_modal"></p> 
                        <a href='#' id="pagina_modal" class="btn btn-primary btn-block">Ir para página do Evento</a>
                        <?php if ($view>2){?>
                            <hr>
                            <h4 align="CENTER">Convidar Turmas</h4> 
                            <div class="row">       
                                <div class="col-md-8 col-md-offset-2">
                                    <?php echo form_hidden('id_evento_convidar'); ?>
                                    <div class="form-group">
                                    <?php foreach ($turmas as $key => $turma): ?>
                                        <div class="checkbox">
                                            <label><input type="checkbox" id="turmas" name="turmas[]" value="<?php echo $key ?>"><?php echo $turma ?></label>
                                        </div>
                                    <?php endforeach ?> 
                                    </div>         
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary btn-block" onclick="convidar_turmas()" data-dismiss="modal">Convidar Turmas</button>
                        <?php }?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    var convidar_turmas = function(){
        var id_evento = $('[name=id_evento_convidar]').val();
        var turmas= $('#turmas:checked').serializeArray();
        console.log(turmas);
        jQuery.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "index.php/evento/convidar_turmas",
           dataType: 'json',
           data: {turmas,evento:id_evento},
           success: function(res) {
                if (res=="ok")
                    mensagem('success','Turmas Convidadas com sucesso');
              }
           }); 
    }

    $(document).ready(function() {

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            defaultDate: new Date().toJSON().slice(0,10).replace(/-/g,'/'),
            events: [
                <?php
                $i = 0;  
                foreach ($eventos as $evento) {
                    if ($i++>0) echo ',';
                    echo "{";
                    echo "id: ".$evento['idevento'].",";
                    echo "title: '".$evento['nome_evento']."',";
                    if ($evento['horario_inicio_evento']!=null)
                        echo "start: '".$evento['data_inicio_evento']."T".$evento['horario_inicio_evento']."',";
                    else
                        echo "start: '".$evento['data_inicio_evento']."',";
                    if ($evento['horario_fim_evento']==null)
                        echo "end: '".date('Y-m-d', strtotime($evento['data_fim_evento']." +1 day"))."'"; 
                    else
                        echo "end: '".date('Y-m-d', strtotime($evento['data_fim_evento']))."T".$evento['horario_fim_evento']."'";
                    echo ",url: '#none'";
                    echo "}";
                }
                ?>
            ],
            eventClick: function(event) {
                jQuery.ajax({
                           type: "POST",
                           url: "<?php echo base_url(); ?>" + "index.php/evento/obter_informacoes",
                           dataType: 'json',
                           data: {idevento:event.id},
                           success: function(res) {
                                $("#nome_evento_modal").html(res.nome_evento);
                                $("#descricao_evento_modal").html(res.descricao_evento);
                                $("#pagina_modal").attr('href',"<?php echo base_url(); ?>index.php/evento/pagina/"+event.id);
                                <?php if ($view>2){?>
                                    $("[name=id_evento_convidar]").val(event.id);
                                <?php }?>
                                $("#evento_modal").modal();
                           }
                });
            }
        });
        
    });

</script>   
