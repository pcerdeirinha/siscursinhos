<?php $this->template->menu($view) ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3><b>Lista de Cursinhos</b></h3>
            
            <table id="unidades" class="table table-hover">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Cidade</th> 
                        <th>Faculdade</th> 
                        <th>Opções</th>
                    </tr>   
                </thead>
                <?php foreach ($unidades as $unidade) { ?>
                <?php $modal = str_replace(' ', '', $unidade['nome_unidade']) ?>
                <tr class="animated fadeInDown">
                    <td><?php echo $unidade['nome_cursinho_unidade'];?></td>
                    <td><?php echo $unidade['cidade_unidade']; ?></td> 
                    <td><?php echo $unidade['nome_faculdade']; ?></td>           
                    <td>
                        <a href="<?php echo base_url('index.php/unidade/edita'); echo '/'.$unidade['idunidade'] ?>" data-toggle="tooltip" data-placement="top" title="Editar"><button type="button" class="btn btn-default"><i class="fa fa-pencil-square-o"></i></button></a>
                        &ensp;<a href="<?php echo base_url('index.php/unidade/remove'); echo '/'.$unidade['idunidade'] ?>" data-toggle="tooltip" data-placement="top" title="Remover"><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#<?php echo $modal; ?>"><i class="fa fa-trash"></i></button></a>
                     </td>
                </tr>
                <div class="modal fade" id="<?php echo $modal;?>" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title text-danger" id="myModalLabel" align="CENTER">Atenção <i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i></h3>
                            </div>
                            <div class="modal-body">
                                <p align="CENTER">Você está prestes a remover a Unidade: </p> 
                                <h4 align="CENTER"><?php echo $unidade['nome_unidade'];?></h4>
                                <p>Os seguintes itens relacionados também serão afetados:</p>
                                <ul>
                                    <li>Tudo relacionado a esta unidade será removida.</li>
                                </ul>
                                <p class="text-danger">As alterações acima citadas são irreversíveis.</p>
                                <h4 align="CENTER">Está certo disto?</h4>                               
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                                <button type="button" class="btn btn-danger"  onclick="location.href='<?php echo base_url(); ?>index.php/unidade/remove/<?php echo $faculdade['idfaculdade'] ?>'">Sim, remover a Unidade</button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </table>    
        </div>
        <div class="col-md-1 col-md-offset-9">
            <button class="btn btn-default" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>          
    </div>
</div>

<script type="text/javascript">


$(document).ready(function () {
    tabela('unidades');
    $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/unidade/opcoes";  
    });
}); 
</script>

<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<?php if(isset($msg)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $msg;?>");</script>
<?php }?>

