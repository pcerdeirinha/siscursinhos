<?php $this->template->menu($view); ?>
<div>   

    <center>
        <h3>Coordenador Docente</h3>
        <br><br>
        <table style="border: 0px; border-collapse: collapse;">
            <tr>
                
                <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="150px">
                    <a href="<?php echo base_url('index.php/docente/novo')?>" class="btn btn-default">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                        <!-- <img src="../images/coorddoc-mod-ico.png" border="0" alt="SGC - Curso: Novo" title="SGC - Curso: Novo" width="70"/><br /> -->
                        <!--<b>SGC</b><br /> -->
                        Novo Coordenador Docente
                    </a>
                </td>

                <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="150px">
                    <?php if($view==6){ ?>
                    <a href="<?php echo base_url('index.php/docente/busca')?>" class="btn btn-primary">
                    <?php } else { ?>
                    <a href="<?php echo base_url('index.php/usuario/busca_funcionario/5')?>" class="btn btn-primary">
                    <?php } ?>
                        <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                        Pesquisa de Coordenadores Docentes
                    </a>
                </td>

        </tr>
    </table>
</center>

</div>