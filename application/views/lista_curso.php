<?php $this->template->menu($view) ?>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h3><b>Lista de Cursos</b></h3>
			
			<table id="cursos" class="table table-hover">
				<thead>
					<tr>
						<th>Nome do Curso</th>
						<th>Duração (em meses)</th>	
						<th>Ano de criação</th>				
						<th>Opções</th>
					</tr>	
				</thead>
				<?php foreach ($cursos as $curso) { ?>
				<?php $modal = str_replace(' ', '', $curso['nome_curso']) ?>
				<tr class="animated fadeInDown">
					<td><?php echo $curso['nome_curso'];?></td>
					<td><?php echo $curso['duracao']; ?></td>
					<td><?php echo $curso['ano_criacao']; ?></td>				
					<td>
						<a href="<?php echo base_url('index.php/curso/edita'); echo '/'.$curso['idcurso'] ?>" data-toggle="tooltip" data-placement="top" title="Editar"><button type="button" class="btn btn-default"><i class="fa fa-pencil-square-o"></i></button></a>
						&ensp;<a href="#" data-toggle="tooltip" data-placement="top" title="Remover"><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#<?php echo $modal; ?>"><i class="fa fa-trash"></i></button></a>
						&ensp;<a href="#" data-toggle="tooltip" data-placement="top" title="Grade Curricular"><button type="button" class="btn btn-primary" onclick="obterGrade(<?php echo $curso['idcurso'] ?>,'<?php echo $curso['nome_curso']?>')"><i class="fa fa-info-circle"></i></button></a>
						<?php //echo anchor('curso/edita/'.urlencode(base64_encode($curso['idcurso'])), '<i class="fa fa-pencil-square-o"> Editar </i>'); ?>&ensp;&ensp;
						<?php //echo anchor('curso/remove/'.urlencode(base64_encode($curso['idcurso'])), '<i class="fa fa-times"> Remover '); ?>
					</td>
				</tr>
				<div class="modal fade" id="<?php echo $modal;?>" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h3 class="modal-title text-danger" id="myModalLabel" align="CENTER">Atenção <i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i></h3>
							</div>
							<div class="modal-body">
								<p align="CENTER">Você está prestes a remover o Curso: </p> 
								<h4 align="CENTER"><?php echo $curso['nome_curso'];?></h4>
								<p>Os seguintes itens relacionados também serão afetados:</p>
								<ul>
									<li>Todas as turmas deste curso serão removidas.</li>
									<li>As matrículas de alunos relacionados as turmas removidas serão canceladas.</li>
									<li>As ofertas de disciplínas de professores relacionadas as turmas removidas serão canceladas.</li>
								</ul>
								<p class="text-danger">As alterações acima citadas são irreversíveis.</p>
								<h4 align="CENTER">Está certo disto?</h4>								
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
								<button type="button" class="btn btn-danger"  onclick="location.href='<?php echo base_url(); ?>index.php/curso/remove/<?php echo $curso['idcurso'] ?>'">Sim, remover o curso</button>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
			</table>
		    <div class="modal fade" id="ModalGradeCurricular" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
		        <div class="modal-dialog" role="document">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                    <h3 class="modal-title" id="nome_grade" align="CENTER">Grade Curricular</h3>
		                </div>
		                <div class="modal-body">
		                        <div class="row">       
		                            <div class="col-md-8 col-md-offset-2">
		                                <div id="tabela_grade">
		                                </div>          
		                            </div>
		                        </div>
		                </div>
		                <div class="modal-footer">
		                    <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
		                </div>
		            </div>
		        </div>
		    </div>							
				
		</div>
		<div class="col-md-1 col-md-offset-9">
			<button class="btn btn-default" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
		</div>			
	</div>
</div>

<script type="text/javascript">

function obterGrade(idCurso,nomeCurso) {
    $("#nome_grade").html('Grade Curricular: '+nomeCurso);
    jQuery.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>" + "index.php/curso/obterDisciplinasCurso",
       dataType: 'json',
       data: {idCurso:idCurso},
       success: function(res) {
           console.log(res.disciplinas);
           	var rows = '';
            rows = $('<table class="table table-bordered" id="disciplinas">');
            rows.append('<thead><tr><th class="col-md-4">Nome da Disciplina</th>\
            <th class="col-md-4">Área</th><th class="col-md-4">Carga horária</th></tr></thead>');
           for (var i in res.disciplinas){
            		 	var newRow = $('<tr class="animated fadeInDown">');
                        var cols = "";
                        cols +='<td class="col-md-4">'+res.disciplinas[i].nome_disciplina+'</td>';                          
                        cols +='<td class="col-md-4">'+res.disciplinas[i].nome_area+'</td>';
                        cols +='<td class="col-md-4">'+res.disciplinas[i].carga_horaria+' horas</td>';
                        newRow.append(cols);                            
                        rows.append(newRow);
                    }
                    rows.append('</table>');
                                                
                    $("#tabela_grade").html(rows);
                     $('#ModalGradeCurricular').modal('show')
           
          }
       });
}

$(document).ready(function () {
	tabela('cursos');
    $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/curso";  
    });
}); 
</script>

<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<?php if(isset($msg)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $msg;?>");</script>
<?php }?>

