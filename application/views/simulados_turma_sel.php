<?php 
$this->template->menu($view);

?>
<div class='container'>
    <?php echo form_open('simulado/novo'); ?>
    <div class="row">       
        <div class="col-md-8 col-md-offset-2">
        <center>        
        <h3><b>Busca de Simulados</b></h3>                 
        </center>
        </div>      
        <div class="col-md-4 col-md-offset-2">
            <div class="form-group">                        
                <?php echo form_label('Curso:', 'cursos'); ?><br>
                <?php echo form_dropdown('cursos', $cursos_drop,'','class="form-control" id ="cursos"'); ?>
                 
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">                  
                <?php echo form_label('Turma', 'turma'); ?><br>
                <?php echo form_dropdown('turmas', $turmas_drop, '','class="form-control" id ="turmas"'); ?>
            </div>
        </div>              
    </div>
    <div class="modal fade" id="ModalRemove" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title text-danger" id="myModalLabel" align="CENTER">Atenção <i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i></h3>
                </div>
                <div class="modal-body">
                    <p align="CENTER">Você está prestes a remover um Simulado: </p> 
                    <p>Os seguintes itens relacionados também serão afetados:</p>
                    <ul>
                        <li>Todas as notas de alunos deste simulado serão removidas.</li>
                    </ul>
                    <p class="text-danger">As alterações acima citadas são irreversíveis.</p>
                    <h4 align="CENTER">Está certo disto?</h4>                               
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    <button type="button" class="btn btn-danger"  id = "btn_del" >Sim, remover o Simulado</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <br>
        <div class="col-md-4 col-md-offset-4">
            <div class="form-group"> 
                <button type="button" class="btn btn-primary btn-block" id="buscar"><i class="fa fa-search"></i> Buscar</button>
                <button type="button" class="btn btn-default btn-block" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
            </div>          
        </div>                  
    </div>
    <div class="row">       
        <div class="col-md-8 col-md-offset-2">
            <div id="tabela_simulados">            
            </div>          
        </div>
    </div>
    <div class="col-md-1 col-md-offset-2" >
        <div class="form-save-buttons">
            <button class="btn btn-primary" type="submit"><i class="fa fa-plus"></i> Novo Simulado</button>
        </div>
    </div> 
    <?php echo form_close(); ?>
    <div class="modal fade" id="ModalAlunos" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <?php echo form_open('simulado/edita') ?>
                <?php echo form_hidden('idSimulado') ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="myModalLabel" align="CENTER">Registro de Notas</h3>
                </div>
                <div class="modal-body">
                        <div class="row">       
                            <div class="col-md-8 col-md-offset-2">
                                <div id="tabela_alunos">
                                </div>          
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    <button type="submit" class="btn btn-primary"  >Registrar</button>
                    
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalConfigura" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <?php echo form_open('simulado/configura') ?>
                <?php echo form_hidden('idSimulado') ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="myModalLabel" align="CENTER">Reconfigurar Simulado</h3>
                </div>
                <div class="modal-body">
                        <div class="row">       
                            <div class="col-md-12">
                                <div class="col-md-9">
                                    <div class="form-group <?php if (!(form_error('descricao_simulado')=='')) echo 'has-error has-feedback'; ?>">
                                        <label for="descricao_simulado">Descrição</label>                                    
                                        <?php echo form_input('descricao_simulado', set_value('descricao_simulado')?set_value('descricao_simulado'):$descricao_simulado, 'type="text", class="form-control" id="descricao" placeholder="Descricao"'); ?>
                                        <?php if (!(form_error('descricao_simulado')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                        <span class="text-danger"><?php echo form_error('descricao_simulado'); ?></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group <?php if (!(form_error('peso_simulado')=='')) echo 'has-error has-feedback'; ?>">
                                        <label for="peso_simulado">Peso</label>                                    
                                        <?php echo form_input('peso_simulado', set_value('peso_simulado')?set_value('peso_simulado'):$peso_simulado, 'type="text", class="form-control" id="peso" placeholder="Peso" tipo = "real"'); ?>
                                        <?php if (!(form_error('peso_simulado')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                        <span class="text-danger"><?php echo form_error('peso_simulado'); ?></span>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group <?php if (!(form_error('data_simulado')=='')) echo 'has-error has-feedback'; ?>">
                                        <?php echo form_label('Data do Simulado', 'data_simulado'); ?> 
                                        <?php echo form_input('data_simulado', set_value('data_simulado')?set_value('data_simulado'):$data__simulado, 'type="date", class="form-control" id="data_aula" placeholder="Data da Aula" tipo="data"'); ?> 
                                        <?php if (!(form_error('data_simulado')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                        <span class="text-danger"><?php echo form_error('data_simulado'); ?></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group <?php if (!(form_error('nota_simulado')=='')) echo 'has-error has-feedback'; ?>">
                                        <label for="nota_simulado">Nota Máxima</label>                                    
                                        <?php echo form_input('nota_simulado', set_value('nota_simulado')?set_value('nota_simulado'):$nota_simulado, 'type="text", class="form-control" id="nota" placeholder="Nota" tipo = "real"'); ?>
                                        <?php if (!(form_error('nota_simulado')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                        <span class="text-danger"><?php echo form_error('nota_simulado'); ?></span>
                                    </div>
                                </div>          
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    <button type="submit" class="btn btn-primary"  >Registrar</button>
                    
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<script type="text/javascript">
function getTurmasByCurso () {    
    var curso_sel = $('#cursos option:selected').val();
    jQuery.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "index.php/gradehoraria/buscaTurma",
          dataType: 'json',
          data: {cursos: curso_sel},
          success: function(res) {
             console.log(res.turmas);
            var row ='';      
            for(var i in res.turmas){
              row+='<option value="'+res.turmas[i].idturma+'">'+
              res.turmas[i].nome_turma+", "+res.turmas[i].periodo_turma+'</option>';
          }
          $("[name=turmas]").html(row);
      }
    });      
}

function removeSimulado($id) {
     document.getElementById('btn_del').onclick = function () {
         window.location = "<?php echo base_url(); ?>" + "index.php/simulado/remove/" + $id;
     };
     $("#ModalRemove").modal({show: true});
}

function obterNotas (idSimulado) {
    $('[name=idSimulado]').val(idSimulado);
    jQuery.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "index.php/simulado/buscaAlunos",
           dataType: 'json',
           data: {idSimulado:idSimulado},
           success: function(res) {
            rows = '';
            rows = $('<table class="table table-hover" id="alunos">');
            rows.append('<thead><tr><th class="col-md-8">Nome</th>\
            <th class="col-md-4">Nota</th></tr></thead>');
            if(res.err == "ok"){                
                    for(var i in res.alunos){
                        var newRow = $('<tr class="animated fadeInDown">');
                        var cols = "";
                        cols +='<td class="col-md-8">'+res.alunos[i].nome_aluno+'</td>';                          
                        cols +='<td class="col-md-4"><input type="text" name="nota'+i+'" value="'+res.alunos[i].nota_aluno+'" class="form-control" tipo="real" maxlength="7"></td>';
                        newRow.append(cols);                            
                        rows.append(newRow);
                    }
                    rows.append('</table>');
                                                
                    $("#tabela_alunos").html(rows);
                    mascara();
                    $("#ModalAlunos").modal({show: true});
           }
           else {
               $("#tabela_alunos").html('');
               $("#ModalAlunos").modal({show: true});
           }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            $("#tabela_alunos").html('');
            $("#ModalAlunos").modal({show: false});
        }
   }); 
}

function configurarSimulado (idSimulado,data,descricao,peso,nota_maxima) {
    $("#ModalConfigura").modal({show: true});
    $('[name=idSimulado]').val(idSimulado);
    $('[name=data_simulado]').val(data);
    $('[name=descricao_simulado]').val(descricao);
    $('[name=peso_simulado]').val(peso);
    $('[name=nota_simulado]').val(nota_maxima);
}

function getSimulados() {
    var idTurma = $("[name=turmas]").val(); 
    jQuery.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "index.php/simulado/busca",
           dataType: 'json',
           data: {idTurma:idTurma},
           success: function(res) {
               console.log(res.simulados);
            var rows = $('<table id="tb_simulados" class="table table-hover">');
            rows.append('<thead><tr><th>Data</th>\
            <th>Descrição</th>\
            <th>Tipo</th>\
            <th>Opções</th></tr></thead>');
            if(res.err == "ok"){                   
                    for(var i in res.simulados){
                        var newRow = $('<tr class="animated fadeInDown">');
                        var cols = "";
                        cols +='<td>'+res.simulados[i].data+'</td>';
                        cols +='<td>'+res.simulados[i].descricao+'</td>'; 
                        cols +='<td>'+res.simulados[i].tipo+'</td>'; 
                        cols +='<td><button type="button" class="btn btn-primary" data-toggle="modal" onclick="obterNotas('+i+');"  title="Editar Resultados"><i class="fa fa-tasks" aria-hidden="true"></i></button>&nbsp<button type="button" class="btn btn-danger" onclick="removeSimulado('+i+');"  title="Remover Simulado"><i class="fa fa-trash"></i></button>&nbsp';
                        cols += '<button type="button" class="btn" data-toggle="modal" onclick="configurarSimulado('+i+',\''+res.simulados[i].data+'\',\''+res.simulados[i].descricao+'\',\''+res.simulados[i].peso+'\',\''+res.simulados[i].nota_maxima+'\');"  title="Reconfigurar Simulado"><i class="fa fa-cogs" aria-hidden="true"></i></button></td>';
                        newRow.append(cols);                            
                        rows.append(newRow);                                                
                    }
                    rows.append("</table>");
                    $("#tabela_simulados").html(rows);
                    tabela('tb_simulados');
               }else{
                mensagem('error',res.err);
                $("#tabela_simulados").html('');
               }
           }
       });
}


$(document).ready(function () {
    getTurmasByCurso();
    $("[name=cursos]").on("change", getTurmasByCurso);
    <?php if (isset($funcao)) echo $funcao.';';?>
    mascara();
	$("#voltar").click(function(event){
		<?php if ($view==2) {?>
		window.location.href = "<?php echo base_url(); ?>"+"index.php/professor"; 
		<?php }else if ($view==3) {?>
		window.location.href = "<?php echo base_url(); ?>"+"index.php/secretario";  
		<?php }else if ($view==4){?>
		window.location.href = "<?php echo base_url(); ?>"+"index.php/coordenador"; 
		<?php }else if ($view==5){?>
		window.location.href = "<?php echo base_url(); ?>"+"index.php/docente"; 
		<?php }?>
	});



    $("#buscar").click(function(event){
        event.preventDefault();   
        getSimulados();
    });
});
</script> 
