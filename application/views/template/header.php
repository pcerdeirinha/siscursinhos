<!DOCTYPE html>
<html lang="en">
<head>
	  <head>
		   <meta charset="utf-8">
		   <meta http-equiv="X-UA-Compatible" content="IE=edge">
		   <meta name="viewport" content="width=device-width, initial-scale=1">		   
		   <link rel="icon" href="<?php echo base_url('images/favicon.ico')?>"/>
		   <link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet">
		   <link rel="stylesheet" href="<?php echo base_url('css/font-awesome.min.css')?>">
		   <link rel="stylesheet" href="<?php echo base_url('css/css.css')?>">
		   <link rel="stylesheet" href="<?php echo base_url('css/animate.css')?>">
		   <link rel="stylesheet" href="<?php echo base_url('css/buttons.css')?>">
		   <link rel="stylesheet" href="<?php echo base_url('css/bootstrap-datepicker3.min.css')?>">
		   <link rel="stylesheet" href="<?php echo base_url('css/dataTables.bootstrap.min.css')?>">		   	   	   		   	
		   <link rel="stylesheet" href="<?php echo base_url('css/bootstrap-datetimepicker.min.css')?>">                           
		   <script src="<?php echo base_url('js/jquery-1.11.3.min.js')?>"></script>
		   <script src="<?php echo base_url('js/moment.min.js')?>"></script>
		   <script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
		   <script src="<?php echo base_url('js/jquery.dataTables.min.js')?>"></script>
		   <script src="<?php echo base_url('js/dataTables.bootstrap.min.js')?>"></script>	   
		   <script src="<?php echo base_url('js/jquery.noty.packaged.min.js')?>"></script>
		   <script src="<?php echo base_url('js/jquery.mask.min.js')?>"></script>
		   <script src="<?php echo base_url('js/bootstrap-datetimepicker.min.js')?>"></script>
		   <script src="<?php echo base_url('js/bootstrap-datepicker.min.js')?>"></script>
		   <script src="<?php echo base_url('js/bootstrap-datepicker.pt-BR.min.js')?>"></script>
		   <script src="<?php echo base_url('js/funcoes.js')?>"></script>
	</head>
    <title>:: UNESP : Sistema de Gestão de Cursinhos ::<?php if (isset($page_title)) { echo ' | '.$page_title; } ?></title>
 		<header class="pagina">				
 				<h4 class="titulo" ALIGN="left">Sistema de Gestão de Cursinhos da Unesp</h4>							
 		</header> 		   
</head>
<body>
