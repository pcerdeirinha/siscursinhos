<?php if ($view=="login"):?>
<div class="transicao"></div> 
<nav class="navbar navbar-default ">
            <div class="container-fluid">
               <div class="navbar-header">
                   <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                   </button>
               </div>
               <div class="collapse navbar-collapse" id="menu" id="menu">
                  <ul class="nav navbar-nav">
                     <li><a href="#">F.A.Q</a></li>
                     <li><a href="http://www.unesp.br">Institucional</a></li>
                  <ul>
               </div>
            </div>
         </nav>
<?php endif;?>
<?php if($view !="login"): ?>
<div class="transicao" align="center"><?php echo $unidade['nome_cursinho_unidade']; ?> - <?php echo $faculdade['nome_faculdade'];?>, <?php echo $unidade['cidade_unidade'];?> </div> 
<?php endif;?>
<?php if($view=="inicio"): ?>
      <nav class="navbar navbar-default ">
                  <div class="container-fluid">
                     <div class="navbar-header">
                         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
                           <span class="sr-only">Toggle navigation</span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                         </button>
                     </div>
                     <div class="collapse navbar-collapse" id="menu">
                        <ul class="nav navbar-nav">
                           <li><a href="<?php echo base_url('index.php/inicio')?>"><i class="fa fa-home"></i> Início</a></li>
                           <li><a href="<?php echo base_url('index.php/usuario/trocasenha')?>"><i class="fa fa-key"></i> Alterar senha</a></li>
                           <li><a href="<?php echo base_url('index.php/mensagem')?>"><i class="fa fa-envelope"></i> Mensagens</a></li>
                           <!--<li><a href="#"><i class="fa fa-file-text"></i> Documentos</a></li>-->       
                           <li><a href="#"><i class="fa fa-comments" aria-hidden="true"></i> Ouvidoria</a></li>       
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                           <?php if (isset($unidades)){ ?>
                           <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
                                aria-expanded="false"><i class="fa fa-exchange" aria-hidden="true"></i> Trocar Cursinho</a>
                                <ul class="dropdown-menu">
                                    <?php foreach ($unidades as $unidade) { ?>
                                        <li><a href="<?php echo base_url('index.php/login/unidade/'.$unidade['unidade_idunidade']);?>"><?php echo $unidade['nome_cursinho_unidade'].' - '.$unidade['cidade_unidade'] ?></a></li>
                                    <?php } ?>
                                </ul>
                           </li>
                           <?php }?>
                           <li><a href="<?php echo base_url('index.php/login/logout')?>"><i class="fa fa-sign-out"></i> Sair</a></li>
                        </ul>
                     </div>
                  </div>
               </nav>
   <?php endif; ?>

   <?php if($view==1): ?>    
   <nav class="navbar navbar-default ">
      <div class="container-fluid">
         <div class="navbar-header">
           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
         </button>
      </div>
      <div class="collapse navbar-collapse" id="menu">
         <ul class="nav navbar-nav">
            	  <li><a href="<?php echo base_url('index.php/aluno')?>"><i class="fa fa-home"></i> Início</a></li>
    			  <li><a href="#"><i class="fa fa-pencil-square-o"></i> Alterar meus dados</a></li>
			      <!--<li><a href="#"><i class="fa fa-envelope"></i> Mensagens</a></li>-->
			      <li><a href="<?php echo base_url('index.php/gradehoraria/exibir')?>"><i class="fa fa-clock-o"></i> Grade horária</a></li>
			      <!--<li><a href="#"><i class="fa fa-file-text"></i> Solicitar documentos</a></li>-->
			      <!--<li><a href="#"><i class="fa fa-exclamation-triangle"></i> Advertências</a></li>-->
			      <li><a href="<?php echo base_url('index.php/simulado/historico')?>"><i class="fa fa-line-chart" aria-hidden="true"></i> Histórico Escolar</a></li>
      
   </ul>
   <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url('index.php/inicio')?>"><i class="fa fa-sign-out"></i> Sair do Módulo</a></li>
   </ul>
</div>
</div>
</nav>
<?php endif; ?>

<?php if($view==2): ?>

   <nav class="navbar navbar-default ">
      <div class="container-fluid">
         <div class="navbar-header">
           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
         </button>
      </div>
      <div class="collapse navbar-collapse" id="menu">
         <ul class="nav navbar-nav">
            <li><a href="<?php echo base_url('index.php/professor')?>"><i class="fa fa-home"></i> Início</a></li>
         <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-users"></i> Usuários</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/aluno/opcoes')?>">Alunos</a></li>
               <li><a href="<?php echo base_url('index.php/professor/opcoes')?>">Professores-Monitores</a></li>
               <li><a href="<?php echo base_url('index.php/secretario/opcoes')?>">Secretários</a></li>
               <li><a href="<?php echo base_url('index.php/coordenador/opcoes')?>">Coordenadores Discentes</a></li>
               <li><a href="<?php echo base_url('index.php/docente/opcoes')?>"> Coordenadores Docentes</a></li>
            </ul>
         </li> -->
         <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-book"></i> Cadastros de curso</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/curso')?>">Cursos</a></li>
               <li><a href="<?php echo base_url('index.php/turma')?>">Turmas</a></li>
               <li><a href="<?php echo base_url('index.php/disciplina')?>">Disciplinas</a></li>
               <li role="separator" class="divider"></li>
               <li><a href="<?php echo base_url('index.php/oferta_disciplina')?>">Ofertas de disciplina</a></li>
            </ul>
         </li> -->

         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-list-alt"></i> Aulas</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/gradehoraria')?>"><i class="fa fa-clock-o"></i> Grade horária</a></li>                           
               <li><a href="<?php echo base_url('index.php/aula/novo')?>"><i class="fa fa-list"></i> Lista de frequência</a></li>
               <li><a href="<?php echo base_url('index.php/simulado')?>"><i class="fa fa-bar-chart"></i> Simulados</a></li>
            </ul>
         </li>

         <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-file-text-o"></i> Documentos</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/relatorio')?>"><i class="fa fa-file-text"></i> Relatórios</a></li>
               <li><a href="#"><i class="fa fa-file-text-o"></i> Solicitações de documentos</a></li>
               <li><a href="<?php echo base_url('index.php/documento')?>"><i class="fa fa-share-square-o" aria-hidden="true"></i> Links compartilhados</a></li>
            </ul>
         </li> -->

         <!--<li><a href="<?php echo base_url('index.php/advertencia')?>"><i class="fa fa-exclamation-triangle"></i> Advertências</a></li>

         <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-cogs"></i> Outros</a></a>
            <ul class="dropdown-menu">
               <li><a href="#"><i class="fa fa-pencil-square-o"></i> Alterar meus dados</a></li>
               <li><a href="<?php echo base_url('index.php/aluno/busca_aprovacao')?>"><i class="fa fa-trophy"></i> Aprovações</a></li>
            </ul>
         </li> -->
      </ul>

      <ul class="nav navbar-nav navbar-right">
         <li><a href="<?php echo base_url('index.php/inicio')?>"><i class="fa fa-sign-out"></i> Sair do Módulo</a></li>
      </ul>
   </ul>
</div>
</div>
</nav>
<?php endif; ?>

<?php if($view==3): ?>  

   <nav class="navbar navbar-default ">
      <div class="container-fluid">
         <div class="navbar-header">
           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
         </button>
      </div>
      <div class="collapse navbar-collapse" id="menu">
         <ul class="nav navbar-nav">

           <li><a href="<?php echo base_url('index.php/secretario')?>"><i class="fa fa-home"></i> Início</a></li>
           <!-- <li><a href="#"><i class="fa fa-users"></i></i> Dados de usuários</a></li> -->
           
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-users"></i> Usuários</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/aluno/opcoes')?>">Alunos</a></li>
               <li><a href="<?php echo base_url('index.php/professor/opcoes')?>">Professores-Monitores</a></li>
               <li><a href="<?php echo base_url('index.php/secretario/opcoes')?>">Secretários</a></li>
               <!-- <li><a href="<?php echo base_url('index.php/coordenador/opcoes')?>">Coordenadores Discentes</a></li> -->
               <!-- <li><a href="<?php echo base_url('index.php/docente/opcoes')?>"> Coordenadores Docentes</a></li> -->
            </ul>
         </li>
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-book"></i> Cadastros de curso</a></a>
            <ul class="dropdown-menu">
               <!-- <li><a href="<?php echo base_url('index.php/curso')?>">Cursos</a></li> -->
               <li><a href="<?php echo base_url('index.php/turma')?>">Turmas</a></li>
               <!-- <li><a href="<?php echo base_url('index.php/disciplina')?>">Disciplinas</a></li> -->
               <li role="separator" class="divider"></li>
               <li><a href="<?php echo base_url('index.php/oferta_disciplina')?>">Ofertas de disciplina</a></li>
            </ul>
         </li>

         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-pencil-square-o"></i> Inscrições</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/matricula/matricularAlunos')?>"> Inscrever alunos</a></li>
               <li><a href="<?php echo base_url('index.php/matricula/desmatricularAlunos')?>"> Cancelar Inscrição de alunos</a></li>
               <li><a href=" <?php echo base_url('index.php/aluno/matricular_alunos')?>"> Inscrever por turma</a></li> 
            </ul>
         </li>
         
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-list-alt"></i> Aulas</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/gradehoraria')?>"><i class="fa fa-clock-o"></i> Grade horária</a></li>                           
               <li><a href="<?php echo base_url('index.php/aula/novo')?>"><i class="fa fa-list"></i> Lista de frequência</a></li>
               <li><a href="<?php echo base_url('index.php/simulado')?>"><i class="fa fa-bar-chart"></i> Simulados</a></li>
            </ul>
         </li>

         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-file-text-o"></i> Documentos</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/relatorio')?>"><i class="fa fa-file-text"></i> Relatórios</a></li>
               <!-- <li><a href="#"><i class="fa fa-file-text-o"></i> Solicitações de documentos</a></li> -->
               <!-- <li><a href="<?php echo base_url('index.php/documento')?>"><i class="fa fa-share-square-o" aria-hidden="true"></i> Links compartilhados</a></li> -->
            </ul>
         </li>

         <!--<li><a href="<?php echo base_url('index.php/advertencia')?>"><i class="fa fa-exclamation-triangle"></i> Advertências</a></li>-->

         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-cogs"></i> Outros</a></a>
            <ul class="dropdown-menu">
               <!-- <li><a href="#"><i class="fa fa-pencil-square-o"></i> Alterar meus dados</a></li> -->
               <!-- <li><a href="#"><i class="fa fa-key"></i> Permissões</a></li> -->
               <li><a href="<?php echo base_url('index.php/aluno/busca_aprovacao')?>"><i class="fa fa-trophy"></i> Aprovações</a></li>
            </ul>
         </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
         <li><a href="<?php echo base_url('index.php/inicio')?>"><i class="fa fa-sign-out"></i> Sair do Módulo</a></li>
      </ul>
   </ul>
</div>
</div>
</nav>
<?php endif; ?>


<?php if($view == 4): ?>  

   <nav class="navbar navbar-default">
      <div class="container-fluid">
         <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
         </button>
      </div>
      <div class="collapse navbar-collapse" id="menu">
         <ul class="nav navbar-nav">

           <li><a href="<?php echo base_url('index.php/coordenador')?>"><i class="fa fa-home"></i> Início</a></li>
           <!-- <li><a href="#"><i class="fa fa-users"></i></i> Dados de usuários</a></li> -->
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-users"></i> Usuários</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/aluno/opcoes')?>">Alunos</a></li>
               <li><a href="<?php echo base_url('index.php/professor/opcoes')?>">Professores-Monitores</a></li>
               <li><a href="<?php echo base_url('index.php/secretario/opcoes')?>">Secretários</a></li>
               <li><a href="<?php echo base_url('index.php/coordenador/opcoes')?>">Coordenadores Discentes</a></li>
               <!-- <li><a href="<?php echo base_url('index.php/docente/opcoes')?>"> Coordenadores Docentes</a></li> -->
            </ul>
         </li>
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-book"></i> Cadastros de curso</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/curso')?>">Cursos</a></li>
               <li><a href="<?php echo base_url('index.php/turma')?>">Turmas</a></li>
               <li><a href="<?php echo base_url('index.php/disciplina')?>">Disciplinas</a></li>
               <li role="separator" class="divider"></li>
               <li><a href="<?php echo base_url('index.php/oferta_disciplina')?>">Ofertas de disciplina</a></li>
            </ul>
         </li>

         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-pencil-square-o"></i> Inscrições</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/matricula/matricularAlunos')?>"> Inscrever alunos</a></li>
               <li><a href="<?php echo base_url('index.php/matricula/desmatricularAlunos')?>"> Cancelar Inscrição de alunos</a></li>
               <li><a href="<?php echo base_url('index.php/aluno/matricular_alunos')?>"> Inscriver por turma</a></li> 
            </ul>
         </li>

         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-list-alt"></i> Aulas</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/gradehoraria')?>"><i class="fa fa-clock-o"></i> Grade horária</a></li>                           
               <li><a href="<?php echo base_url('index.php/aula/novo')?>"><i class="fa fa-list"></i> Lista de frequência</a></li>
              <li><a href="<?php echo base_url('index.php/simulado')?>"><i class="fa fa-bar-chart"></i> Simulados</a></li>
            </ul>
         </li>

         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-file-text-o"></i> Documentos</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/relatorio')?>"><i class="fa fa-file-text"></i> Relatórios</a></li>
               <!-- <li><a href="#"><i class="fa fa-file-text-o"></i> Solicitações de documentos</a></li> -->
               <!-- <li><a href="<?php echo base_url('index.php/documento')?>"><i class="fa fa-share-square-o" aria-hidden="true"></i> Links compartilhados</a></li> -->
            </ul>
         </li>

         <!--<li><a href="<?php echo base_url('index.php/advertencia')?>"><i class="fa fa-exclamation-triangle"></i> Advertências</a></li>-->

         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-cogs"></i> Outros</a></a>
            <ul class="dropdown-menu">
               <!-- <li><a href="#"><i class="fa fa-pencil-square-o"></i> Alterar meus dados</a></li> -->
               <!-- <li><a href="#"><i class="fa fa-key"></i> Permissões</a></li> -->
               <li><a href="<?php echo base_url('index.php/aluno/busca_aprovacao')?>"><i class="fa fa-trophy"></i> Aprovações</a></li>
               <li><a href="<?php echo base_url('index.php/monitor/atividade')?>"><i class="fa fa-user" aria-hidden="true"></i> Atividade dos Monitores</a></li>
            </ul>
         </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
         <li><a href="<?php echo base_url('index.php/inicio')?>"><i class="fa fa-sign-out"></i> Sair do Módulo</a></li>
      </ul>
   </ul>
</div>
</div>
</nav>
<?php endif; ?>


<?php if($view == 5): ?>  

   <nav class="navbar navbar-default">
      <div class="container-fluid">
         <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
         </button>
      </div>
      <div class="collapse navbar-collapse" id="menu">
         <ul class="nav navbar-nav">

           <li><a href="<?php echo base_url('index.php/docente')?>"><i class="fa fa-home"></i> Início</a></li>
           <!-- <li><a href="#"><i class="fa fa-users"></i></i> Dados de usuários</a></li> -->
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-users"></i> Usuários</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/aluno/opcoes')?>">Alunos</a></li>
               <li><a href="<?php echo base_url('index.php/professor/opcoes')?>">Professores-Monitores</a></li>
               <li><a href="<?php echo base_url('index.php/secretario/opcoes')?>">Secretários</a></li>
               <li><a href="<?php echo base_url('index.php/coordenador/opcoes')?>">Coordenadores Discentes</a></li>
               <li><a href="<?php echo base_url('index.php/docente/opcoes')?>"> Coordenadores Docentes</a></li>
            </ul>
         </li>
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-book"></i> Cadastros de curso</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/curso')?>">Cursos</a></li>
               <li><a href="<?php echo base_url('index.php/turma')?>">Turmas</a></li>
               <li><a href="<?php echo base_url('index.php/disciplina')?>">Disciplinas</a></li>
               <li role="separator" class="divider"></li>
               <li><a href="<?php echo base_url('index.php/oferta_disciplina')?>">Ofertas de disciplina</a></li>
            </ul>
         </li>

         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-pencil-square-o"></i> Inscrições</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/matricula/matricularAlunos')?>"> Inscrever alunos</a></li>
               <li><a href="<?php echo base_url('index.php/matricula/desmatricularAlunos')?>"> Cancelar Inscrição de alunos</a></li>
               <li><a href="<?php echo base_url('index.php/aluno/matricular_alunos')?>"> Inscrever por turma</a></li> 
            </ul>
         </li>

         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-list-alt"></i> Aulas</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/gradehoraria')?>"><i class="fa fa-clock-o"></i> Grade horária</a></li>                           
               <li><a href="<?php echo base_url('index.php/aula/novo')?>"><i class="fa fa-list"></i> Lista de frequência</a></li>
              <li><a href="<?php echo base_url('index.php/simulado')?>"><i class="fa fa-bar-chart"></i> Simulados</a></li>
            </ul>
         </li>

         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-file-text-o"></i> Documentos</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/relatorio')?>"><i class="fa fa-file-text"></i> Relatórios</a></li>
               <!-- <li><a href="#"><i class="fa fa-file-text-o"></i> Solicitações de documentos</a></li> -->
               <!-- <li><a href="<?php echo base_url('index.php/documento')?>"><i class="fa fa-share-square-o" aria-hidden="true"></i> Links compartilhados</a></li> -->
            </ul>
         </li>

         <!--<li><a href="<?php echo base_url('index.php/advertencia')?>"><i class="fa fa-exclamation-triangle"></i> Advertências</a></li>-->

         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-cogs"></i> Outros</a></a>
            <ul class="dropdown-menu">
               <!-- <li><a href="#"><i class="fa fa-pencil-square-o"></i> Alterar meus dados</a></li> -->
               <!-- <li><a href="#"><i class="fa fa-key"></i> Permissões</a></li> -->
               <li><a href="<?php echo base_url('index.php/aluno/busca_aprovacao')?>"><i class="fa fa-trophy"></i> Aprovações</a></li>
               <li><a href="<?php echo base_url('index.php/monitor/atividade')?>"><i class="fa fa-user" aria-hidden="true"></i> Atividade dos Monitores</a></li>
            </ul>
         </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
         <li><a href="<?php echo base_url('index.php/inicio')?>"><i class="fa fa-sign-out"></i> Sair do Módulo</a></li>
      </ul>
   </ul>
</div>
</div>
</nav>
<?php endif; ?>

<?php if($view == 6): ?>  

   <nav class="navbar navbar-default">
      <div class="container-fluid">
         <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
         </button>
      </div>
      <div class="collapse navbar-collapse" id="menu">
         <ul class="nav navbar-nav">

           <li><a href="<?php echo base_url('index.php/supervisor')?>"><i class="fa fa-home"></i> Início</a></li>
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-users"></i> Administração</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/faculdade/opcoes')?>">Faculdades</a></li>
               <li><a href="<?php echo base_url('index.php/unidade/opcoes')?>">Cursinhos</a></li>
               <li><a href="<?php echo base_url('index.php/docente/opcoes')?>"> Coordenadores Docentes</a></li>
            </ul>
         </li>


         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"><i class="fa fa-file-text-o"></i> Documentos</a></a>
            <ul class="dropdown-menu">
               <li><a href="<?php echo base_url('index.php/relatorio')?>"><i class="fa fa-file-text"></i> Relatórios</a></li>
               <!-- <li><a href="#"><i class="fa fa-file-text-o"></i> Solicitações de documentos</a></li> -->
               <!-- <li><a href="<?php echo base_url('index.php/documento')?>"><i class="fa fa-share-square-o" aria-hidden="true"></i> Links compartilhados</a></li> -->
            </ul>
         </li>
      </ul>
       
      <ul class="nav navbar-nav navbar-right">
         <li><a href="<?php echo base_url('index.php/inicio')?>"><i class="fa fa-sign-out"></i> Sair do Módulo</a></li>
      </ul>
   </ul>
</div>
</div>
</nav>
<?php endif; ?>

   <?php //} ?>

 