<?php if ($view=="login"):?>
<div class="transicao"></div> 
<nav class="navbar navbar-default">
            <div class="container">
               <div class="collapse navbar-collapse">
                  <ul class="nav navbar-nav">
                     <li><a href="#">F.A.Q</a></li>
                     <li><a href="http://www.unesp.br">Institucional</a></li>
                  <ul>
               </div>
            </div>
         </nav>
<?php endif;?>
<?php if($view=="inicio"): ?>
   <div class="transicao" align="center"><?php echo $unidade['nome_cursinho_unidade']; ?> - <?php echo $unidade['nome_faculdade'];?>, <?php echo $unidade['cidade_unidade'];?> </div> 
   <?php var_dump($unidade) ?>

      <nav class="navbar navbar-default">
                  <div class="container">
                     <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                           <?php if($page_title!="Trocar Senha"):?>
                           <li><a href="usuario/trocasenha"><i class="fa fa-key"></i> Troca de senha</a></li>
                        <?php else: ?>
                           <li><a href="<?php echo base_url('index.php/inicio')?>"><i class="fa fa-home"></i> Início</a></li>
                        <?php endif; ?>
                           <li><a href="#"><i class="fa fa-envelope"></i> Mensagens</a></li>
                           <li><a href="#"><i class="fa fa-file-text"></i> Documentos</a></li>                          
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                           <li><a href="<?php echo base_url('index.php/login/logout')?>"><i class="fa fa-sign-out"></i> Sair</a></li>
                        </ul>
                     </div>
                  </div>
               </nav>
   <?php endif; ?>

<?php if($view=="aluno"): ?>
   <div class="transicao"> </div> 
   <?php //if($user['tipo_usuario']==2){ /*Aluno*/?>
      <nav class="navbar navbar-default">
                  <div class="container">
                     <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                           <?php if($page_title!="Alterar dados"):?>
                           <li><a href="#"><i class="fa fa-pencil-square-o"></i> Alterar meus dados</a></li>
                        <?php else: ?>
                           <li><a href="<?php echo base_url('index.php/inicio')?>"><i class="fa fa-home"></i> Início</a></li>
                        <?php endif; ?>
                           <li><a href="#"><i class="fa fa-envelope"></i> Mensagens</a></li>
                           <li><a href="<?php echo base_url('index.php/gradehoraria')?>"><i class="fa fa-clock-o"></i> Grade horária</a></li>
                           <li><a href="#"><i class="fa fa-file-text"></i> Solicitar documentos</a></li>
                           <li><a href="#"><i class="fa fa-exclamation-triangle"></i> Advertências</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                           <li><a href="<?php echo base_url('index.php/inicio')?>"><i class="fa fa-sign-out"></i> Voltar</a></li>
                        </ul>
                     </div>
                  </div>
               </nav>
   <?php endif; ?>



   <?php if($view=="professor"): ?>
   <div class="transicao"> </div> 

      <nav class="navbar navbar-default">
                  <div class="container">
                     <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                           <?php if($page_title!="Alterar dados"):?>
                           <li><a href="#"><i class="fa fa-pencil-square-o"></i> Alterar meus dados</a></li>
                        <?php else: ?>
                           <li><a href="<?php echo base_url('index.php/inicio')?>"><i class="fa fa-home"></i> Início</a></li>
                        <?php endif; ?>
                           <li><a href="#"><i class="fa fa-envelope"></i> Mensagens</a></li>
                           <li><a href="<?php echo base_url('index.php/gradehoraria')?>"><i class="fa fa-clock-o"></i> Grade horária</a></li>
                           <!-- <li><a href="#"><i class="fa fa-users"></i></i> Dados de usuários</a></li> -->
                           <li><a href="#"><i class="fa fa-file-text"></i> Solicitar documentos</a></li>
                           <li><a href="#"><i class="fa fa-exclamation-triangle"></i> Advertências</a></li>
                           
                           <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
                              aria-expanded="false"><i class="fa fa-users"></i></i> Dados de usuários</a></a>
                              <ul class="dropdown-menu">
                                 <li><a href="#">Alunos</a></li>
                              </ul>
                           </li>
                           <li><a href="#"><i class="fa fa-list"></i></i> Lista de presença</a></li>
                           <li><a href="#"><i class="fa fa-file"></i> Compartilhar arquivos</a></li>
                        
                           <li><a href="#"><i class="fa fa-bar-chart"></i> Avaliações</a></li>
                           
                        <ul class="nav navbar-nav navbar-right">
                           <li><a href="<?php echo base_url('index.php/inicio')?>"><i class="fa fa-sign-out"></i> Voltar</a></li>
                        </ul>
                        </ul>
                     </div>
                  </div>
               </nav>
   <?php endif; ?>

   <?php if($view=="secretario"): ?>
   <div class="transicao"> </div> 

      <nav class="navbar navbar-default">
                  <div class="container">
                     <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                           <?php if($page_title!="Alterar dados"):?>
                           <li><a href="#"><i class="fa fa-pencil-square-o"></i> Alterar meus dados</a></li>
                        <?php else: ?>
                           <li><a href="<?php echo base_url('index.php/inicio')?>"><i class="fa fa-home"></i> Início</a></li>
                        <?php endif; ?>
                           <li><a href="#"><i class="fa fa-envelope"></i> Mensagens</a></li>
                           <li><a href="<?php echo base_url('index.php/gradehoraria')?>"><i class="fa fa-clock-o"></i> Grade horária</a></li>
                           <!-- <li><a href="#"><i class="fa fa-users"></i></i> Dados de usuários</a></li> -->
                           <li><a href="#"><i class="fa fa-file-text"></i> Solicitações de documentos</a></li>
                           <li><a href="#"><i class="fa fa-exclamation-triangle"></i> Advertências</a></li>
                           
                           <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
                              aria-expanded="false"><i class="fa fa-users"></i></i> Dados de usuários</a></a>
                              <ul class="dropdown-menu">
                                 <li><a href="#">Alunos</a></li>
                                 <li><a href="#">Monitores</a></li>
                              </ul>
                           </li>
                           <li><a href="#"><i class="fa fa-list"></i></i> Lista de presença</a></li>
                           <li><a href="#"><i class="fa fa-file"></i> Compartilhar arquivos</a></li>
                           <li><a href="#"><i class="fa fa-bar-chart"></i> Avaliações</a></li>
                           <li><a href="#"><i class="fa fa-key"></i> Permissões</a></li>
                           <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
                              aria-expanded="false"><i class="fa fa-book"></i></i> Cadastros de curso</a></a>
                              <ul class="dropdown-menu">
                                 <li><a href="#">Disciplinas</a></li>
                                 <li><a href="#">Turmas</a></li>
                                 <li><a href="#">Cursos</a></li>
                              </ul>
                           </li>
                           <li><a href="#"><i class="fa fa-trophy"></i> Aprovações</a></li>
                           
                        <ul class="nav navbar-nav navbar-right">
                           <li><a href="<?php echo base_url('index.php/inicio')?>"><i class="fa fa-sign-out"></i> Voltar</a></li>
                        </ul>
                        </ul>
                     </div>
                  </div>
               </nav>
   <?php endif; ?>

   <?php if($view == "coordenador" || $view == "docente"): ?>
   <div class="transicao"> </div> 

      <nav class="navbar navbar-default">
                  <div class="container">
                     <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                           <?php if($page_title!="Alterar dados"):?>
                           <li><a href="#"><i class="fa fa-pencil-square-o"></i> Alterar meus dados</a></li>
                        <?php else: ?>
                           <li><a href="<?php echo base_url('index.php/inicio')?>"><i class="fa fa-home"></i> Início</a></li>
                        <?php endif; ?>
                           <li><a href="#"><i class="fa fa-envelope"></i> Mensagens</a></li>
                           <li><a href="<?php echo base_url('index.php/gradehoraria')?>"><i class="fa fa-clock-o"></i> Grade horária</a></li>
                           <!-- <li><a href="#"><i class="fa fa-users"></i></i> Dados de usuários</a></li> -->
                           <li><a href="#"><i class="fa fa-file-text"></i> Solicitações de documentos</a></li>
                           <li><a href="#"><i class="fa fa-exclamation-triangle"></i> Advertências</a></li>
                           
                           <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
                              aria-expanded="false"><i class="fa fa-users"></i></i> Dados de usuários</a></a>
                              <ul class="dropdown-menu">
                                 <li><a href="<?php echo base_url('index.php/aluno/opcoes')?>">Alunos</a></li>
                                 <li><a href="#">Monitores</a></li>
                                 <li><a href="#">Coordenadores Docentes</a></li>
                                 <li><a href="<?php echo base_url('index.php/aluno/curso')?>">Novo Aluno</a></li>
                                 <li><a href="<?php echo base_url('index.php/professor/novo')?>">Novo Professor-Monitor</a></li>
                                 <li><a href="<?php echo base_url('index.php/secretario/novo')?>">Novo Secretário</a></li>
                                 <li><a href="<?php echo base_url('index.php/coordenador/novo')?>">Novo Coordenador</a></li>
                                 <li><a href="<?php echo base_url('index.php/docente/novo')?>">Novo Docente</a></li>
                              </ul>
                           </li>
                           <li><a href="#"><i class="fa fa-list"></i></i> Lista de presença</a></li>
                           <li><a href="#"><i class="fa fa-file"></i> Compartilhar arquivos</a></li>
                           <li><a href="#"><i class="fa fa-bar-chart"></i> Avaliações</a></li>
                           <li><a href="#"><i class="fa fa-key"></i> Permissões</a></li>
                           <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" 
                              aria-expanded="false"><i class="fa fa-book"></i></i> Cadastros de curso</a></a>
                              <ul class="dropdown-menu">
                                 <li><a href="#">Disciplinas</a></li>
                                 <li><a href="<?php echo base_url('index.php/turma')?>">Turmas</a></li>
                                 <li><a href="<?php echo base_url('index.php/curso')?>">Cursos</a></li>
                              </ul>
                           </li>
                           <li><a href="#"><i class="fa fa-trophy"></i> Aprovações</a></li>
                           
                        <ul class="nav navbar-nav navbar-right">
                           <li><a href="<?php echo base_url('index.php/inicio')?>"><i class="fa fa-sign-out"></i> Voltar</a></li>
                        </ul>
                        </ul>
                     </div>
                  </div>
               </nav>
   <?php endif; ?>

   <?php //} ?>

 