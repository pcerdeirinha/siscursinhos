<?php $this->template->menu($view) ?>
<div class='container'>
    <div class="row">       
        <div class="col-md-8 col-md-offset-2">
        <center>        
        <h3><b>Busca de Ofertas de Disciplina</b></h3>                 
        </center>
        </div>      
        <div class="col-md-4 col-md-offset-2">
            <div class="form-group">                        
                <?php echo form_label('Curso', 'nome_curso'); ?>
                <?php echo form_dropdown('nome_curso',$nome_curso, set_value('nome_curso')?set_value('nome_curso'):$nome_curso_sel, 'type="text" min="2", class="form-control" id="curso" placeholder="Curso"'); ?>                               
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">                  
                <?php echo form_label('Turma', 'nome_turma'); ?>
                <?php echo form_dropdown('nome_turma',$nome_turma, set_value('nome_turma')?set_value('nome_turma'):$nome_turma_sel, 'type="text" min="2", class="form-control" id="turma" placeholder="Turma"'); ?>
            </div>
        </div>              
    </div>
    <div class="row">
        <br>
        <div class="col-md-4 col-md-offset-4">
            <div class="form-group"> 
                <button type="submit" class="btn btn-primary btn-block" id="buscar"><i class="fa fa-search"></i> Buscar</button>
                <button class="btn btn-default btn-block" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
            </div>          
        </div>                  
    </div>
    <div class="row">       
        <div class="col-md-8 col-md-offset-2">
            <div id="tabela_disciplinas">            
            </div>          
        </div>
    </div>
    <div class="modal fade" id="ModalOferta" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="myModalLabel" align="CENTER">Nova Oferta</h3>
                </div>
                <div class="modal-body">
                    <?php echo form_open('oferta_disciplina/cria'); ?>
                    <?php echo form_hidden('iddisciplina',set_value('iddisciplina')); ?>
                    <?php echo form_hidden('idturma',set_value('idturma')); ?>
                        <div class="form-group">
                            <?php echo form_label('Disciplina', 'nome_disciplina'); ?> 
                            <?php echo form_input('nome_disciplina',set_value('nome_disciplina'), 'type="date", class="form-control" id="oferta_aula" placeholder="Oferta" readonly'); ?> 
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Turma', 'nome_turma_'); ?> 
                            <?php echo form_input('nome_turma_',set_value('nome_turma_'), 'type="date", class="form-control" id="oferta_aula" placeholder="Oferta" readonly'); ?> 
                        </div>
                        <div id = "dataDiv"class="form-group <?php if (!(form_error('data_inicio_oferta')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Data de Início', 'data_inicio_oferta'); ?> 
                            <?php echo form_input('data_inicio_oferta', set_value('data_inicio_oferta')?set_value('data_inicio_oferta'):$data_inicio_oferta, 'type="date", class="form-control" id="data_aula" placeholder="Data da Aula" tipo="data"'); ?> 
                            <?php if (!(form_error('data_inicio_oferta')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" id="dataSpanX" aria-hidden="true"></span>'; ?>
                            <span class="text-danger" id="dataSpan" ><?php echo form_error('data_inicio_oferta'); ?></span>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Monitor', 'nome_monitor'); ?> 
                            <?php echo form_dropdown('nome_monitor',$nome_monitor, set_value('nome_monitor')?set_value('nome_monitor'):$nome_monitor_sel, 'type="text" min="2", class="form-control" id="turma" placeholder="Turma"'); ?>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    <button type="submit" class="btn btn-primary">Registrar</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title text-danger" id="myModalLabel" align="CENTER">Atenção <i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i></h3>
                </div>
                <div class="modal-body">
                    <p align="CENTER">Você está prestes a remover uma Oferta de Disciplina! </p> 
                    <p>Os seguintes itens relacionados também serão afetados:</p>
                    <ul>
                        <li>Todos os alunos da turma não terão mais aulas desta disciplina.</li>
                        <li>Será impossível realizar ou editar o lançamento de frequência desta disciplina para a turma desta oferta.</li>
                    </ul>
                    <p class="text-warning">Após isto é possível ceder a disciplina para outro professor.</p>
                    <h4 align="CENTER">Está certo disto?</h4>
                    
                    <?php echo form_open('oferta_disciplina/remove'); ?>
                    <div id = "dataDiv1" class="form-group <?php if (!(form_error('data_fim_oferta')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Data de Fim da Oferta', 'data_fim_oferta'); ?> 
                        <?php echo form_input('data_fim_oferta', set_value('data_fim_oferta')?set_value('data_fim_oferta'):$data_fim_oferta, 'type="date", class="form-control" id="data_aula" placeholder="Data da Aula" tipo="data"'); ?> 
                        <?php if (!(form_error('data_fim_oferta')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" id = "dataSpanX1" aria-hidden="true"></span>'; ?>
                        <span id = "dataSpan1" class="text-danger"><?php echo form_error('data_fim_oferta'); ?></span>
                    </div>                                                       
                </div>
                <div class="modal-footer">
      
                    <?php echo form_hidden('iddisciplinaD',set_value('iddisciplinaD')); ?>
                    <?php echo form_hidden('idturmaD',set_value('idturmaD')); ?>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    <button type="submit" class="btn btn-danger">Sim, remover a oferta da disciplina</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>   
</div>
<script type="text/javascript">

function carregarDados(idturma,nometurma,iddisciplina,nomedisciplina) {
    $('[name=iddisciplina]').val(iddisciplina);
    $('[name=idturma]').val(idturma);
    $('[name=nome_disciplina]').val(nomedisciplina);
    $('[name=nome_turma_]').val(nometurma);
}

function carregarDadosDel(idturma,iddisciplina) {
    $('[name=iddisciplinaD]').val(iddisciplina);
    $('[name=idturmaD]').val(idturma);
}


function carregarAulas () {
    var idTurma = $('option:selected', "[name=nome_turma]").val();   
        jQuery.ajax({
               type: "POST",
               url: "<?php echo base_url(); ?>" + "index.php/oferta_disciplina/getOfertasTurma",
               dataType: 'json',
               data: {idTurma:idTurma},
               success: function(res) {
                   console.log(res.disciplinas);
                var rows = $('<table class="table table-hover" id="disciplinas">');
                rows.append('<thead><tr><th>Disciplina</th>\
                <th>Professor</th>\
                <th>Opções</th></tr></thead>');
                if(res.err == "ok"){                   
                        for(var i in res.disciplinas){
                            var newRow = $('<tr class="animated fadeInDown">');
                            var cols = "";
                            cols +='<td>'+res.disciplinas[i].nome_disciplina+'</td>';
                            cols +='<td>'+res.disciplinas[i].nome_monitor+'</td>';      
                            if (res.disciplinas[i].id_oferta=='')                     
                                cols += '<td><button type="button" class="btn btn-primary" onclick="carregarDados('+idTurma+',\''+$('option:selected', "[name=nome_turma]").html()+'\','+res.disciplinas[i].id_disciplina+',\''+res.disciplinas[i].nome_disciplina+'\');" data-toggle="modal"  data-target="#ModalOferta"><i class="fa fa-plus-square" aria-hidden="true"></i></button></td>';
                            else{ 
                                cols += '<td><button type="button" class="btn btn-warning" onclick="carregarDados('+idTurma+',\''+$('option:selected', "[name=nome_turma]").html()+'\','+res.disciplinas[i].id_disciplina+',\''+res.disciplinas[i].nome_disciplina+'\');" data-toggle="modal"  data-target="#ModalOferta"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>';
                                cols += '&nbsp<button type="button" class="btn btn-danger" onclick="carregarDadosDel('+idTurma+','+res.disciplinas[i].id_disciplina+');" data-toggle="modal"  data-target="#ModalDelete"><i class="fa fa-trash"></i></button></td>';
                                 }
                            newRow.append(cols);                            
                            rows.append(newRow);                                                
                        }
                        $("#tabela_disciplinas").html(rows);
                        tabela('disciplinas'); 

                   }else{
                    mensagem('error',res.err);
                    $("#tabela_disciplinas").html('');
                   }
               }
           });
}


$(document).ready(function () {
    
   <?php 
        if (set_value('iddisciplina')!=''){
            echo 'carregarAulas();';
            echo '$("#ModalOferta").modal({show: true});';
        }
        else if (set_value('iddisciplinaD')!=''){
            echo 'carregarAulas();';
            echo '$("#ModalDelete").modal({show: true});';
        }
    
    ?>
    
    mascara();
    data();
    if ($('option:selected', "[name=nome_turma]").val() == 0){
        var idCurso= $('option:selected', "[name=nome_curso]").val();
            jQuery.ajax({
                      type: "POST",
                      url: "<?php echo base_url(); ?>" + "index.php/turma/getTurmaCurso",
                      dataType: 'json',
                      data: {idcurso: idCurso},
                      success: function(res) {
                        console.log(res.turmas);
                        var row =''; 
                        if (res.err = 'ok'){
                            for(var i in res.turmas){
                               row+='<option value="' + i + '">'+res.turmas[i]+'</option>';
                                                     
                            }
                        }
                        $("[name=nome_turma]").html(row);
                      }
                  });
    }
    $("[name=nome_curso]").change(function(event) {
        event.preventDefault(); 
        var idCurso= $('option:selected', "[name=nome_curso]").val();
        jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/turma/getTurmaCurso",
                  dataType: 'json',
                  data: {idcurso: idCurso},
                  success: function(res) {
                    console.log(res.turmas);
                    var row ='';      
                    for(var i in res.turmas){
                       row+='<option value="' + i + '">'+res.turmas[i]+'</option>';
                                             
                    }
                    $("[name=nome_turma]").html(row);
                  }
              });
    }); 
    $('#ModalOferta').on('hidden.bs.modal', function () {
        document.getElementById("dataDiv").className = "form-group";
        document.getElementById("dataSpanX").style.display="none";
        document.getElementById("dataSpan").style.display="none";
        $('[name=data_inicio_oferta]').val('');
    });
    $('#ModalDelete').on('hidden.bs.modal', function () {
        document.getElementById("dataDiv1").className = "form-group";
        document.getElementById("dataSpanX1").style.display="none";
        document.getElementById("dataSpan1").style.display="none";
        $('[name=data_fim_oferta]').val('');
    });

    $('[data-toggle="tooltip"]').tooltip();     
    $("#buscar").click(function(event){
        event.preventDefault();                
        carregarAulas();  
    });
    $("#voltar").click(function(event){
    	<?php if ($view==5){ ?>
        window.location.href = "<?php echo base_url(); ?>"+"index.php/docente";  
        <?php }else if ($view==4){ ?>
        window.location.href = "<?php echo base_url(); ?>"+"index.php/coordenador";
        <?php }else if ($view==3){ ?>
    	window.location.href = "<?php echo base_url(); ?>"+"index.php/secretario";
    	<?php } ?>
    });
});
</script>