<?php $this->template->menu($view) ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">            
                <?php echo form_open('aluno/cria','',array('cidade_selected'=>set_value('cidade_usuario')?set_value('cidade_usuario'):$cidade_sel)); ?>      
                <?php if(isset($id)||(set_value('id')!='')){/*Então é Update*/?>
                            <h3><b>Edita Aluno</b></h3>
                            <br>
                            <?php echo form_hidden('id', $id?$id:set_value('id')); 
                        }else{ ?>
                            <h3><b>Novo Aluno</b></h3>                    <br>
                <?php } ?> 
                <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#dadosGerais">Dados Gerais</a></li>
                        <li><a data-toggle="tab" href="#endereco">Endereço</a></li>
                        <!--<li><a data-toggle="tab" href="#socioEconomicos">Dados Socioeconômicos</a></li>-->                            
                </ul>                
        </div>
                
        <div class="tab-content">
            <div id="dadosGerais" class="tab-pane fade in active">                
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('nome_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <label for="nome">Nome</label>                                    
                        <?php echo form_input('nome_usuario', set_value('nome_usuario')?set_value('nome_usuario'):$nome_usuario, 'type="text", class="form-control" id="nome" placeholder="Nome"'); ?>
                    	<?php if (!(form_error('nome_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('nome_usuario'); ?></span>
                    
                    </div>
                </div>
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        <label for="nome">Nome Social</label>                                    
                        <?php echo form_input('nome_social_usuario', set_value('nome_social_usuario')?set_value('nome_social_usuario'):$nome_social_usuario, 'type="text", class="form-control" id="nome_social" placeholder="Nome Social"'); ?>
                    </div>
                </div>
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('email_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Email', 'email'); ?>
                        <?php echo form_input('email_usuario', set_value('email_usuario')?set_value('email_usuario'):$email_usuario, 'type="email", class="form-control" id="email" placeholder="Email"'); ?> 
                    	<?php if (!(form_error('email_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('email_usuario'); ?></span>
                    </div>
                </div> 
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('rg_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('RG', 'rg'); ?>        
                         <?php echo form_input('rg_usuario', set_value('rg_usuario')?set_value('rg_usuario'):$rg_usuario, 'type="text", class="form-control" id="rg" placeholder="RG"'); ?>
                    	<?php if (!(form_error('rg_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('rg_usuario'); ?></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div id="divCPF" class="form-group <?php if (!(form_error('cpf_usuario')=='')) echo 'has-error'; ?>" >
                        <?php echo form_label('CPF', 'cpf'); ?>
                        <div class="input-group">
                            <div class="has-feedback">
                                <?php echo form_input('cpf_usuario', set_value('cpf_usuario')?set_value('cpf_usuario'):$cpf_usuario, 'type="text", class="form-control" id="cpf" placeholder="CPF" tipo="cpf"'); ?>
                                <span class="glyphicon glyphicon-remove form-control-feedback" id="spanErrorCPF"  aria-hidden="true" <?php if (form_error('cpf_usuario')=='') echo 'style= display:none;"' ?>></span>
                            </div>    
                                <div class="input-group-btn">
                                    <button class="btn-secondary btn" type="button" id="btnPreencherCPF"  disabled >Preencher</button>
                               </div>
                        </div>
                    	<span class="text-danger" id="spanCPFServer"><?php echo form_error('cpf_usuario'); ?></span>
                    	<span class="text-danger" id="spanCPF" style="display:none">O CPF não é válido</span>

                    </div>
                </div>
                <div class="col-md-5 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('data_nascimento_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Data de Nascimento', 'data_nasc'); ?> 
                        <?php echo form_input('data_nascimento_usuario', set_value('data_nascimento_usuario')?set_value('data_nascimento_usuario'):$data_nascimento_usuario, 'type="date", class="form-control" id="data_nasc" placeholder="Data de nascimento" tipo="data"'); ?> 
                    	<?php if (!(form_error('data_nascimento_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('data_nascimento_usuario'); ?></span>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('telefone_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Telefone', 'telefone'); ?>
                        <?php echo form_input('telefone_usuario', set_value('telefone_usuario')?set_value('telefone_usuario'):$telefone_usuario, 'type="tel" min="8" max="10", class="form-control" id="telefone" placeholder="DDD + Telefone" tipo="telefone"'); ?>
                    	<?php if (!(form_error('telefone_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                    	<span class="text-danger"><?php echo form_error('telefone_usuario'); ?></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group <?php if (!(form_error('celular_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Celular', 'celular'); ?>
                        <?php echo form_input('celular_usuario', set_value('celular_usuario')?set_value('celular_usuario'):$celular_usuario, 'type="tel" min="8" max="10", class="form-control" id="celular" placeholder="DDD + Celular" tipo="celular"'); ?>
                        <?php if (!(form_error('celular_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('celular_usuario'); ?></span>
                    </div>
                </div>                                                  
            </div>
            <div id="endereco" class="tab-pane fade">
                <br>
                <div class="col-md-6 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('logradouro_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Logradouro', 'logradouro'); ?>
                        <?php echo form_input('logradouro_usuario', set_value('logradouro_usuario')?set_value('logradouro_usuario'):$logradouro_usuario, 'type="text", class="form-control" id="logradouro" placeholder="Endereço"'); ?>
                        <?php if (!(form_error('logradouro_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('logradouro_usuario'); ?></span>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group <?php if (!(form_error('numero_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Numero', 'numero'); ?>
                        <?php echo form_input('numero_usuario', set_value('numero_usuario')?set_value('numero_usuario'):$numero_usuario, 'type="text", class="form-control" id="numero" placeholder="N." tipo="numero"'); ?>
                        <?php if (!(form_error('numero_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('numero_usuario'); ?></span>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('bairro_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Bairro', 'bairro'); ?>
                        <?php echo form_input('bairro_usuario', set_value('bairro_usuario')?set_value('bairro_usuario'):$bairro_usuario, 'type="text", class="form-control" id="bairro" placeholder="Bairro"'); ?>
                        <?php if (!(form_error('bairro_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('bairro_usuario'); ?></span>
                   
                    </div>
                </div >
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo form_label('Complemento', 'complemento'); ?>
                        <?php echo form_input('complemento_endereco_usuario', set_value('complemento_endereco_usuario')?set_value('complemento_endereco_usuario'):$complemento_endereco_usuario, 'type="text", class="form-control" id="complemento" placeholder="Complemento de endereço"'); ?>
                        <?php if (!(form_error('bairro_usuario')=='')) echo '<br>'; //ver isso aqui ?>

                    </div>
                </div> 
                <div class="col-md-1 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('estado_usuario')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('UF', 'uf'); ?><br>
                        <?php echo form_dropdown('estado_usuario', $estados, set_value('estado_usuario')?set_value('estado_usuario'):$estado_sel, 'type="text" min="2", class="form-control" id="uf" placeholder="Estado"'); ?>
                        <?php if (!(form_error('estado_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                    </div>  
                </div>
                <div class="col-md-4">
                    <div class="form-group <?php if (!(form_error('cidade_usuario')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Cidade', 'cidade'); ?>
                        <?php echo form_dropdown('cidade_usuario', $cidades, '' ,'type="text", class="form-control" id="cidade" placeholder="Cidade"'); ?>
                        <span class="text-danger"><?php echo form_error('cidade_usuario'); ?></span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group <?php if (!(form_error('cep_usuario')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('CEP', 'cep'); ?>             
                        <?php echo form_input('cep_usuario', set_value('cep_usuario')?set_value('cep_usuario'):$cep_usuario,'type="text", class="form-control" id="cep" placeholder="CEP" tipo="cep"'); ?>
                        <?php if (!(form_error('cep_usuario')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('cep_usuario'); ?></span>
                    </div>
                </div>              
            </div>
            <!--
            <div id="socioEconomicos" class="tab-pane fade">
                <br>
                <!--
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Nome da mãe', 'mae'); ?>    
                        <?php echo form_input('nome_mae_aluno', set_value('nome_mae_aluno')?set_value('nome_mae_aluno'):$nome_mae_aluno, 'type="text", class="form-control" id="mae" placeholder="Nome da mãe ou responsável"'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo form_label('Nome do pai', 'pai'); ?>         
                        <?php echo form_input('nome_pai_aluno', set_value('nome_pai_aluno')?set_value('nome_pai_aluno'):$nome_pai_aluno, 'type="text", class="form-control" id="pai" placeholder="Nome do pai ou responsável"'); ?>
                    </div>
                </div>
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Local de nascimento', 'localnasc'); ?>
                        <?php echo form_input('local_nascimento_aluno', set_value('local_nascimento_aluno')?set_value('local_nascimento_aluno'):$local_nascimento_aluno, 'type="text", class="form-control" id="localnasc" placeholder="Cidade de nascimento"'); ?>
                    </div>
                </div>
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Para que curso pretende prestar vestibular?', 'vestcurso'); ?>
                        <?php echo form_input('curso_vestibular_aluno', set_value('curso_vestibular_aluno')?set_value('curso_vestibular_aluno'):$curso_vestibular_aluno, 'type="text", class="form-control" id="vestcurso" placeholder="Curso pretendido"'); ?>                        
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Universidade(s) pública(s) que pretende prestar', 'unipub'); ?>
                        <?php echo form_input('universidade_pretendida_publica', set_value('universidade_pretendida_publica')?set_value('universidade_pretendida_publica'):$universidade_pretendida_publica, 'type="text", class="form-control" id="unipub" placeholder="Universidade(s) pública(s) pretendida(s)"'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo form_label('Universidade(s) particular(es) que pretende prestar', 'unipart'); ?>
                        <?php echo form_input('universidade_pretendida_particular', set_value('universidade_pretendida_particular')?set_value('universidade_pretendida_particular'):$universidade_pretendida_particular, 'type="text", class="form-control" id="univpart" placeholder="Universidade(s) particular(es) pretendida(s)"'); ?>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Escola onde cursa ou cursou o Ensino Médio', 'EEM'); ?>
                        <?php echo form_input('escola_ensino_medio', set_value('escola_ensino_medio')?set_value('escola_ensino_medio'):$escola_ensino_medio, 'type="text", class="form-control" id="EEM" placeholder="Escola onde cursa/cursou Ensino Médio"'); ?>
                    </div>
                </div>
                <div class="col-md-4">                    
                    <div class="radio">
                        <label>
                        <input type="radio" name="escola" id="optionsRadios1" value="0" <?php if ($escola_publica==0) echo "checked";?>>
                        Escola Particular
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                        <input type="radio" name="escola" id="optionsRadios2" value="1" <?php if ($escola_publica==1) echo "checked";?>>
                        Escola Pública
                        </label>
                    </div>      
                    <?php echo form_error('escola'); ?>              
                </div>                
                <div class="col-md-5 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('ano_conclusao_em')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Ano de Conclusão do Ensino Médio', 'anoconclusao'); ?>
                        <?php echo form_input('ano_conclusao_em', set_value('ano_conclusao_em')?set_value('ano_conclusao_em'):$ano_conclusao_em, 'type="text", class="form-control" id="anoconclusao" placeholder="Ano de conclusão do Ensino Médio"'); ?>
                        <?php if (!(form_error('ano_conclusao_em')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('ano_conclusao_em'); ?></span>
                    </div>
                </div>
                --> 
                <!--
                <div class="col-md-12">                   
                    <div class="col-md-2 col-md-offset-2">
                        <div class="form-group <?php if (!(form_error('sexo_aluno')=='')) echo 'has-error'; ?>">
                            <?php echo form_label('Sexo', 'sexo_aluno'); ?>
                            <?php echo form_dropdown('sexo_aluno',array(0=>'Feminino',1=>'Masculino',null=>'Outros'),set_value('sexo_aluno')==null?"0":set_value('sexo_aluno'),'class="form-control" id="sexo" placeholder="Sexo"'); ?>
                            <span class="text-danger"><?php echo form_error('sexo_aluno'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group <?php if (!(form_error('outro_sexo_aluno')=='')) echo 'has-error'; ?>">
                            <?php echo form_label('Outro Sexo', 'outro_sexo_aluno'); ?>
                            <?php echo form_input('outro_sexo_aluno',set_value('outro_sexo_aluno') ,'type="text", class="form-control" id="outro_sexo" placeholder="Outro Sexo"'); ?>
                            <?php if (!(form_error('outro_sexo_aluno')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('outro_sexo_aluno'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group <?php if (!(form_error('etnia_aluno')=='')) echo 'has-error'; ?>">
                            <?php echo form_label('Em relação cor/etnia, você se considera', 'etnia_aluno'); ?>
                            <?php echo form_dropdown('etnia_aluno',$etnia,set_value('etnia_aluno')==null?2:set_value('etnia_aluno'),'class="form-control" id="etnia" placeholder="Etnia"'); ?>
                            <span class="text-danger"><?php echo form_error('etnia_aluno'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-2">
                        <div class="form-group <?php if (!(form_error('realizacao_ensino_fund_aluno')=='')) echo 'has-error'; ?>">
                            <?php echo form_label('Realização do ensino fundamental', 'realizacao_ensino_fund_aluno'); ?>
                            <?php echo form_dropdown('realizacao_ensino_fund_aluno',$ensino,set_value('realizacao_ensino_fund_aluno')==null?1:set_value('realizacao_ensino_fund_aluno'),'class="form-control" id="ensino_fundamental" placeholder="Ensino Fundamental"'); ?>
                            <span class="text-danger"><?php echo form_error('realizacao_ensino_fund_aluno'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group <?php if (!(form_error('realizacao_ensino_medio_aluno')=='')) echo 'has-error'; ?>">
                            <?php echo form_label('Realização do ensino médio', 'realizacao_ensino_medio_aluno'); ?>
                            <?php echo form_dropdown('realizacao_ensino_medio_aluno',$ensino,set_value('realizacao_ensino_medio_aluno')==null?1:set_value('realizacao_ensino_medio_aluno'),'class="form-control" id="ensino_medio" placeholder="Ensino Medio"'); ?>
                            <span class="text-danger"><?php echo form_error('realizacao_ensino_medio_aluno'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-2">
                        <div class="form-group <?php if (!(form_error('ano_conclusao_em')=='')) echo 'has-error'; ?>">
                            <?php echo form_label('Ano de conclusão do ensino médio', 'ano_conclusao_em'); ?>
                            <?php echo form_dropdown('ano_conclusao_em',$conclusao,set_value('ano_conclusao_em'),'class="form-control" id="ensino_fundamental" placeholder="Ensino Fundamental"'); ?>
                            <span class="text-danger"><?php echo form_error('ano_conclusao_em'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group <?php if (!(form_error('escola_ensino_medio')=='')) echo 'has-error'; ?>">
                            <?php echo form_label('Nome da escola que cursa/concluiu o ensino médio', 'escola_ensino_medio'); ?>
                            <?php echo form_input('escola_ensino_medio',set_value('escola_ensino_medio') ,'type="text", class="form-control" id="escola_medio" placeholder="Nome da Escola"'); ?>
                            <?php if (!(form_error('escola_ensino_medio')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('escola_ensino_medio'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-2">
                        <div class="form-group <?php if (!(form_error('vez_cursinho_aluno')=='')) echo 'has-error'; ?>">
                            <?php echo form_label('Quanto ao cursinho pré-vestibular', 'vez_cursinho_aluno'); ?>
                            <?php echo form_dropdown('vez_cursinho_aluno',$vez,set_value('vez_cursinho_aluno')==null?0:set_value('vez_cursinho_aluno'),'class="form-control" id="vez" placeholder="Vezes"'); ?>
                            <span class="text-danger"><?php echo form_error('vez_cursinho_aluno'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group <?php if (!(form_error('nome_cursinho_anterior_aluno')=='')) echo 'has-error'; ?>">
                            <?php echo form_label('Nome do cursinho, caso tenha cursado', 'nome_cursinho_anterior_aluno'); ?>
                            <?php echo form_input('nome_cursinho_anterior_aluno',set_value('nome_cursinho_anterior_aluno') ,'type="text", class="form-control" id="escola_cursinjo" placeholder="Nome do Cursinho"'); ?>
                            <?php if (!(form_error('escola_ensino_medio')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('nome_cursinho_anterior_aluno'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-2">
                        <div class="form-group <?php if (!(form_error('atividade_remunerada_aluno')=='')) echo 'has-error'; ?>">
                            <?php echo form_label('Exerce atividade remunerada', 'atividade_remunerada_aluno'); ?>
                            <?php echo form_dropdown('atividade_remunerada_aluno',$remunerada,set_value('atividade_remunerada_aluno')==null?0:set_value('atividade_remunerada_aluno'),'class="form-control" id="atividade_remunerada_aluno"'); ?>
                            <span class="text-danger"><?php echo form_error('atividade_remunerada_aluno'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group <?php if (!(form_error('renda_familiar_aluno')=='')) echo 'has-error'; ?>">
                            <?php echo form_label('Qual é a sua renda familiar mensal?', 'renda_familiar_aluno'); ?>
                            <?php echo form_dropdown('renda_familiar_aluno',$renda,set_value('renda_familiar_aluno')==null?"01":set_value('renda_familiar_aluno'),'class="form-control" id="renda_familiar_aluno"'); ?>
                            <span class="text-danger"><?php echo form_error('renda_familiar_aluno'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-2">
                        <div class="form-group">
                            <?php echo form_label('Neste ano, que vestibulares você pretende prestar?'); ?>
                            <div class="checkbox">
                                <label><input type="checkbox" <?php if (in_array(1, $vestibular)) echo "checked"; ?> value="1" name="vestibular[]">UNESP</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" <?php if (in_array(2, $vestibular)) echo "checked"; ?>  value="2" name="vestibular[]">FUVEST</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" <?php if (in_array(3, $vestibular)) echo "checked"; ?>  value="3" name="vestibular[]">UNICAMP</label>
                            </div>
                            Outro
                            <div class="input-group">
                                <?php echo form_dropdown('vestibular_aluno',$vestibulares,set_value('vestibular_aluno')==null?0:set_value('vestibular_aluno'),'class="form-control" id="vestibular_aluno"'); ?>
                                <div class="input-group-btn">
                                        <button class="btn-secondary btn" type="button" id="btnNovaFaculdade" data-toggle="modal" data-target="#ModalCriaFaculdade">Novo Vestibular</button>
                               </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?php echo form_label('Pretende prestar outros concursos?'); ?>
                            <div class="radio">
                                <label><input type="radio" name="concurso" value="01" <?php if (set_value('concurso_aluno')==1) echo "checked"; ?>>Sim</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="concurso" value="00" <?php if (set_value('concurso_aluno')==0) echo "checked"; ?>>Não</label>
                            </div>
                            <br>
                            <?php echo form_label('Caso a resposta seja afirmativa qual (is):', 'nome_concurso_aluno'); ?>
                            <?php echo form_input('nome_concurso_aluno',set_value('nome_concurso_aluno') ,'type="text", class="form-control" id="concurso_aluno" placeholder="Concursos"'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-2">
                        <div class="form-group <?php if (!(form_error('escolaridade_resp1_aluno')=='')) echo 'has-error'; ?>">
                            <?php echo form_label('Qual o grau de escolaridade da sua mãe?', 'escolaridade_resp1_aluno'); ?>
                            <?php echo form_dropdown('escolaridade_resp1_aluno',$escolaridade,set_value('escolaridade_resp1_aluno')==null?4:set_value('escolaridade_resp1_aluno'),'class="form-control" id="escolaridade_resp1_aluno"'); ?>
                            <span class="text-danger"><?php echo form_error('escolaridade_resp1_aluno'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group <?php if (!(form_error('escolaridade_resp2_aluno')=='')) echo 'has-error'; ?>">
                            <?php echo form_label('Qual o grau de escolaridade do seu pai?', 'escolaridade_resp2_aluno'); ?>
                            <?php echo form_dropdown('escolaridade_resp2_aluno',$escolaridade,set_value('escolaridade_resp2_aluno')==null?4:set_value('escolaridade_resp2_aluno'),'class="form-control" id="escolaridade_resp1_aluno"'); ?>
                            <span class="text-danger"><?php echo form_error('escolaridade_resp2_aluno'); ?></span>
                        </div>
                    </div>
                </div>     
                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-2">
                        <div class="form-group">
                            <?php echo form_label('Como você se informar sobre atualidades?'); ?>
                            <?php foreach ($info_atualidades as $key => $info): ?>
                                    <div class="checkbox">
                                        <label><input type="checkbox" <?php if (in_array($info['idsocio_atualidades'], $info_atual)) echo "checked"; ?> value="<?php echo $info['idsocio_atualidades']; ?>" name="info_atual[]"><?php echo $info['descricao_info_atualidades']; ?></label>
                                    </div>
                            <?php endforeach ?>
                            <?php echo form_label('Outros:', 'info_atualidades_aluno'); ?>
                            <?php echo form_input('info_atualidades_aluno',set_value('info_atualidades_aluno') ,'type="text", class="form-control" id="info_atualidades_aluno" placeholder="Outras Maneiras"'); ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?php echo form_label('Quais são seus hábitos culturais mais frequentes?'); ?>
                            <?php foreach ($habito_cult as $key => $habito): ?>
                                    <div class="checkbox">
                                        <label><input type="checkbox" <?php if (in_array($habito['idsocio_habito'], $habito_cultural)) echo "checked"; ?> value="<?php echo $habito['idsocio_habito']; ?>" name="habito_cultural[]"><?php echo $habito['descricao_habito']; ?></label>
                                    </div>
                            <?php endforeach ?>
                            <?php echo form_label('Outros:', 'habitos_aluno'); ?>
                            <?php echo form_input('habitos_aluno',set_value('habitos_aluno') ,'type="text", class="form-control" id="habitos_aluno" placeholder="Outros Hábitos"'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('organizacao_cultura_aluno')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Você faz parte de algum tipo de organização associativa de cultura?', 'organizacao_cultura_aluno'); ?>
                        <?php echo form_input('organizacao_cultura_aluno',set_value('organizacao_cultura_aluno') ,'type="text", class="form-control" id="organizacao_cultura_aluno" placeholder="Organizações Associativas de Cultura"'); ?>
                        <?php if (!(form_error('organizacao_cultura_aluno')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('organizacao_cultura_aluno'); ?></span>
                    </div>
                </div>
                
            </div>
            -->
        </div>
        <div class="modal fade" id="ModalCriaFaculdade" tabindex="-1" role="dialog" aria-labellby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel" align="CENTER">Novo Vestibular</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <?php echo form_label('Nome do Vestibular', 'nome_faculdade'); ?> 
                            <?php echo form_input('nome_faculdade',set_value('nome_faculdade') , 'type="date", class="form-control" id="nome_faculdade" placeholder="Faculdade"'); ?> 
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Tipo da Faculdade', 'tipo_faculdade'); ?> 
                            <?php echo form_dropdown('tipo_faculdade',$tipos_faculdade,set_value('tipo_faculdade'), 'type="date", class="form-control" id="tipo_faculdade" placeholder="Tipo"'); ?> 
                        </div>
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                <input type="radio" name="escola" id="optionsRadios1" value="0" checked>
                                Particular
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                <input type="radio" name="escola" id="optionsRadios2" value="1">
                                Pública
                                </label>
                            </div>                   
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <button type="button" class="btn btn-primary" id="btnCriaFaculdade">Registrar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1 col-md-offset-8">
            <div class="form-save-buttons">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="col-md-1">
            <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>
    </div>
</div>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<script type="text/javascript">

function criaFaculdade(){
    jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/vestibular/criaFaculdade",
                  dataType: 'json',
                  data: {nome_faculdade: $("#nome_faculdade").val(),tipo_faculdade: $("#tipo_faculdade option:selected").val(),publica:$('input[name=escola]:checked').val()},
                  success: function(res) {
                     console.log(res.err);
                    if (res.err == 'ok')
                        var row ='<option  value">Selection</option>';      
                        for(var i in res.faculdade_drop){
                                if (i>3){
                                    row+='<option  value = "'+i+'">'+res.faculdade_drop[i]+'</option>';
                                }
                            }
                         $("[name=vestibular_aluno]").html(row);
                         $('#ModalCriaFaculdade').modal('toggle');
                         
                  }
         });
}

function obterCidade(uf,cidade){
    jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/usuario/buscaCidade",
                  dataType: 'json',
                  data: {uf: uf},
                  success: function(res) {
                    var row ='';      
                    for(var i in res.cidades){
                        if (res.cidades[i]!=cidade)
                            row+='<option  value = "'+res.cidades[i]+'">'+res.cidades[i]+'</option>';
                         else row+='<option  value = "'+res.cidades[i]+'" selected="select"">'+res.cidades[i]+'</option>';
                    }
                    $("[name=cidade_usuario]").html(row);
                  }
         });
}

var dados;
var cpf;
$(document).ready(function () {
    var uf = $('option:selected', this).val();
        var cidade = document.getElementsByName("cidade_selected")[0].value;
        obterCidade(uf,cidade);
    mascara();
    data();
    $('input[type=text]').addClass('uppercase');
    $("[name=estado_usuario]").on('change',function(event) {
       event.preventDefault(); 
       var uf = $('option:selected', this).val();
       obterCidade(uf,'');
    });
    $("[name=cpf_usuario]").on('keyup',function(event){
        var cpf = $("[name=cpf_usuario]").val();
        if (TestaCPF(cpf)){
            jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/usuario/obterUsuario",
                  dataType: 'json',
                  data: {cpf: cpf},
                  success: function(res) {
                    if (res==null){
                        document.getElementById("spanErrorCPF").className= "glyphicon glyphicon-ok form-control-feedback";
                        document.getElementById("divCPF").className = "form-group has-success";
                        document.getElementById("spanCPF").style.display="";
                        document.getElementById("spanCPF").innerHTML="O CPF é válido";
                        document.getElementById("spanCPF").className="text-success"; 
                     }
                    else if ((res[0].status==0)&&(cpf!=res[0].cpf_ususario)){
                        dados=res[0];
                        document.getElementById("spanCPF").style.display = "";
                        document.getElementById("spanErrorCPF").className= "glyphicon glyphicon-alert form-control-feedback"
                        document.getElementById("divCPF").className = "form-group has-warning";
                        document.getElementById("btnPreencherCPF").disabled = false;
                        document.getElementById("spanCPF").innerHTML="O CPF já possui registro. Deseja obter os dados?";
                        document.getElementById("spanCPF").className="text-warning";
                    }
                    else if (res[0].status==1){
                          document.getElementById("divCPF").className = "form-group has-error";
                          document.getElementById("spanErrorCPF").style.display = ""; 
                          document.getElementById("spanCPFServer").style.display = "none"; 
                          document.getElementById("spanCPF").style.display = "";
                          document.getElementById("spanCPF").innerHTML="O CPF precisa ser único";
                          document.getElementById("spanCPF").className="text-danger";
                          document.getElementById("spanErrorCPF").className= "glyphicon glyphicon-remove form-control-feedback";

                    }
                 }
              });
        } 
        else {
           document.getElementById("divCPF").className = "form-group has-error";
           document.getElementById("spanErrorCPF").style.display = ""; 
           document.getElementById("spanCPFServer").style.display = "none"; 
           document.getElementById("spanCPF").style.display = "";
           document.getElementById("spanCPF").innerHTML="O CPF não é válido";
           document.getElementById("spanCPF").className="text-danger";
           document.getElementById("spanErrorCPF").className= "glyphicon glyphicon-remove form-control-feedback";

        }
    });
    $("#voltar").click(function(event){
        window.location.href = "<?php echo base_url(); ?>"+"index.php/aluno/opcoes";  
    });
    $("#btnPreencherCPF").click(function(event){
        if (dados.status==0){
             document.getElementById("nome").value=dados.nome_usuario;
             document.getElementById("rg").value=dados.rg_usuario;
             var data_nasc = dados.data_nascimento_usuario.split('-');
             document.getElementById("data_nasc").value=data_nasc[2]+"/"+data_nasc[1]+"/"+data_nasc[0];
             document.getElementById("logradouro").value=dados.logradouro_usuario;
             document.getElementById("numero").value=dados.numero_usuario;
             document.getElementById("bairro").value=dados.bairro_usuario;
             document.getElementById("complemento").value=dados.complemento_endereco_usuario;
             document.getElementById("cep").value=dados.cep_usuario;
             document.getElementById("rg").value=dados.rg_usuario;
             document.getElementById("email").value=dados.email_usuario;
             document.getElementById("telefone").value=dados.telefone_usuario;
             document.getElementById("celular").value=dados.celular_usuario;
             $('#uf option:contains("'+dados.estado_usuario+'")').prop('selected',true);
             obterCidade($('#uf option:contains("'+dados.estado_usuario+'")').val(),dados.cidade_usuario);
             document.getElementById("spanErrorCPF").className= "glyphicon glyphicon-ok form-control-feedback";
             document.getElementById("divCPF").className = "form-group has-success";
             document.getElementById("spanCPF").style.display="";
             document.getElementById("spanCPF").innerHTML="O CPF é válido";
             document.getElementById("spanCPF").className="text-success"; 
             cpf=dados.cpf_usuario;
         }
    });
    $("#btnCriaFaculdade").click(function(event){
        event.preventDefault(); 
        criaFaculdade();
    });    
});

function TestaCPF(cpf) {  
    cpf = cpf.replace(/[^\d]+/g,'');    
    if(cpf == '') return false; 
    // Elimina CPFs invalidos conhecidos    
    if (cpf.length != 11 || 
        cpf == "00000000000" || 
        cpf == "11111111111" || 
        cpf == "22222222222" || 
        cpf == "33333333333" || 
        cpf == "44444444444" || 
        cpf == "55555555555" || 
        cpf == "66666666666" || 
        cpf == "77777777777" || 
        cpf == "88888888888" || 
        cpf == "99999999999")
            return false;       
    // Valida 1o digito 
    add = 0;    
    for (i=0; i < 9; i ++)       
        add += parseInt(cpf.charAt(i)) * (10 - i);  
        rev = 11 - (add % 11);  
        if (rev == 10 || rev == 11)     
            rev = 0;    
        if (rev != parseInt(cpf.charAt(9)))     
            return false;       
    // Valida 2o digito 
    add = 0;    
    for (i = 0; i < 10; i ++)        
        add += parseInt(cpf.charAt(i)) * (11 - i);  
    rev = 11 - (add % 11);  
    if (rev == 10 || rev == 11) 
        rev = 0;    
    if (rev != parseInt(cpf.charAt(10)))
        return false;       
    return true;   
}
</script>

 