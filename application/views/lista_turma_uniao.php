<?php $this->template->menu($view) ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <center><h3><b>Unir Turmas</b></h3></center>
            <br>
            <div class="col-md-6">
                <div class="input-group">
                <span class="input-group-addon" id="nome-addon">Curso:</span>                
                <?php echo form_dropdown('idcurso',$cursos_drop,'','class = "form-control"'); ?>
                </div>
            </div>
            <?php echo form_open('turma/unir'); ?>
            <div class="pre-scrollable col-md-12">
                <table id='turmas' class="table table-hover" >
                    <thead>
                        <tr>
                            <th>Nome da Turma</th>
                            <th>Período</th>
                            <th><center>Selecione</center></th>
                        </tr>
                    </thead>                
                    <tbody id="itens">
                        <?php foreach ($turmas as $turma) {
                        $modal = str_replace(' ', '', $turma['nome_turma']);?>
                        <tr class="animated fadeInDown">
                            <td><?php echo $turma['nome_turma'];?></td>
                            <td><?php echo $turma['periodo_turma'];?></td>
                            </td>                   
                            <td>
                                <center><?php echo form_checkbox('turmas[]',$turma['idturma'],false);?></center>
                            </td>
                        </tr>               
                        <?php } ?>
                    </tbody>
                </table>    
            </div>
        </div>
        <div class="col-md-2 col-md-offset-7">
            <button class="btn btn-primary" id="unir" data-toggle="modal" type="button" data-target="#ModalTurmas"><i class="fa fa-cubes"></i> Unir Turmas Selecionadas</button>
        </div>      
        <div class="col-md-1">
            <button class="btn btn-default" type="button" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>      
    </div>
    <div class="row">
        <div class="modal fade" id="ModalTurmas" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel" align="CENTER">Unir Turmas</h3>
                    </div>
                    <div class="modal-body">
                        <div class="row">       
                            <div class="col-md-6">
                                <div class="form-group <?php if (!(form_error('nome_turma')=='')) echo 'has-error has-feedback'; ?>">
                                    <?php echo form_label('Nome da Turma', 'nom_turma'); ?>
                                    <?php echo form_input('nome_turma', set_value('nome_turma')?set_value('nome_turma'):$nome_turma, 'type="text", class="form-control" id="nome" placeholder="Nome da Turma"'); ?> 
                                    <?php if (!(form_error('nome_turma')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                    <span class="text-danger"><?php echo form_error('nome_turma'); ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group <?php if (!(form_error('periodo_turma')=='')) echo 'has-error has-feedback'; ?>">                        
                                    <?php echo form_label('Período', 'an_curso'); ?>
                                    <?php echo form_dropdown('periodo_turma',set_value('periodo_turma')?set_value('periodo_turma'):$periodo_turma, $periodo_sel, 'type="text" min="2", class="form-control" id="periodo" placeholder="Período"'); ?>                               
                                    <?php if (!(form_error('periodo_turma')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                    <span class="text-danger"><?php echo form_error('periodo_turma'); ?></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group <?php if (!(form_error('sala_turma')=='')) echo 'has-error has-feedback'; ?>">
                                    <?php echo form_label('Sala', 'sala'); ?>
                                    <?php echo form_input('sala_turma', set_value('sala_turma')?set_value('sala_turma'):$sala_turma, 'type="text", class="form-control" id="sala" placeholder="Sala"'); ?>        
                                    <?php if (!(form_error('sala_turma')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                    <span class="text-danger"><?php echo form_error('sala_turma'); ?></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div  class="form-group <?php if (!(form_error('num_vagas')=='')) echo 'has-error has-feedback'; ?>">
                                    <?php echo form_label('Numero de Vagas', 'vagas'); ?>
                                    <?php echo form_input('num_vagas', set_value('num_vagas')?set_value('num_vagas'):$num_vagas, 'class="form-control" id="Num. Vagas" placeholder="Vagas" tipo="numero"'); ?>
                                    <?php if (!(form_error('num_vagas')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                    <span class="text-danger"><?php echo form_error('num_vagas'); ?></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group <?php if (!(form_error('data_inicio_turma')=='')) echo 'has-error has-feedback'; ?>">
                                    <?php echo form_label('Data Inicial da Turma', 'turmabl'); ?>
                                    <?php echo form_input('data_inicio_turma',set_value('data_inicio_turma')?set_value('data_inicio_turma'):$data_inicio_turma, 'type="text" min="2", class="form-control" id="ano" placeholder="Data de início" tipo = "data"'); ?>                        
                                    <?php if (!(form_error('data_inicio_turma')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                    <span class="text-danger"><?php echo form_error('data_inicio_turma'); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <button type="submit" class="btn btn-primary">Registrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript">
function getTurmasByCurso () {    
    var curso_sel = $('[name=idcurso] option:selected').val();
    jQuery.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>" + "index.php/gradehoraria/buscaTurma",
      dataType: 'json',
      data: {cursos: curso_sel},
      success: function(res) {
        var row ='';      
        for(var i in res.turmas){
           row+='<tr class="animated fadeInDown">';
           row+='   <td>'+res.turmas[i].nome_turma+'</td>';
           row+='   <td>'+res.turmas[i].periodo_turma+'</td>';
           row+='   <td><center><input type="checkbox" name="turmas[]" value="'+res.turmas[i].idturma+'"></center></td>';
           row+='</tr>';
      }
      $("#itens").html(row);
  }
});      
}

    



$(document).ready(function () {
    $("[name=idcurso]").on("change", getTurmasByCurso);
    mascara();
    data(true);
    $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/turma";  
    });
}); 
</script>

<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<?php if(isset($msg)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $msg;?>");</script>
<?php }?>



