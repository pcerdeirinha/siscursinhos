<?php $this->template->menu($view); ?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<div class="container">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        
    <?php 
    
    $i = 0;
    if (isset($simulados[$i]['disciplina_iddisciplina'])){
    ?>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingDisciplina">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDisciplina" aria-expanded="true" aria-controls="collapseOne">
              Avaliações por Disciplina
            </a>
          </h4>
        </div>
        <div id="collapseDisciplina" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">
              <div class = "container">
                  <div class = "row">
                      <div id="btnGrp_disciplinas" class="btn-group-vertical col-xs-4">
                      <?php
                      $j=0;
                      $idDiscAnterior="";
                      for ($j;$j<count($simulados);$j++){
                            if (!isset($simulados[$j]['disciplina_iddisciplina']))
                                break;
                            else{
                                if ($idDiscAnterior!=$simulados[$j]['disciplina_iddisciplina']){
                                    echo '<button type="button" class="btn btn-secondary" id="btnSimuladoDisciplina'.$simulados[$j]['disciplina_iddisciplina'].'">'.$simulados[$j]['nome_adicional'].'</button>';
                                    $idDiscAnterior = $simulados[$j]['disciplina_iddisciplina'];
                                }
                            }
                        }
                       ?>    
                      </div>
                      <div id="grafico_disciplina" class="col-xs-4"></div>
                      <div id="grafico_disciplina_media" class="col-xs-4"></div>
                  </div>
                <script>
                var notas = [];
                var media = [];
                <?php 
                    $i = 0;
                    $disciplina = $simulados[$i]['disciplina_iddisciplina'];
                    $nome_disciplina = $simulados[$i]['nome_adicional'];
                    $soma = 0;
                    $div = 0;
                    $l =0;
                    $m = 0;
                    echo "notas[".$simulados[$i]['disciplina_iddisciplina']."] = [];";
                    for ($i;$i<count($simulados);$i++){
                        if (!isset($simulados[$i]['disciplina_iddisciplina']))
                            break;
                        else{
                                if($disciplina!=$simulados[$i]['disciplina_iddisciplina']){
                                    echo "media[".$m."] = [];";
                                    echo "media[".$m."][0] = '".$nome_disciplina."';";
                                    echo "media[".$m."][1] = ".$soma/$div.";";
                                    $nome_disciplina = $simulados[$i]['nome_adicional'];
                                    $disciplina = $simulados[$i]['disciplina_iddisciplina'];
                                    $m++;
                                    $soma = 0;
                                    $div = 0;
                                    $l =0;
                                    echo "notas[".$simulados[$i]['disciplina_iddisciplina']."] = [];";
                                    //echo "notas[".$simulados[$i]['disciplina_iddisciplina']."][".$l."] = [];";
                                }        
                                echo "notas[".$simulados[$i]['disciplina_iddisciplina']."][".$l."] = [];";
                                echo "notas[".$simulados[$i]['disciplina_iddisciplina']."][".$l."][0]='".$simulados[$i]['descricao_simulado']."';";
                                echo "notas[".$simulados[$i]['disciplina_iddisciplina']."][".$l."][1]=".$simulados[$i]['valor_nota']/$simulados[$i]['nota_maxima_simulado'].";";
                                $soma += $simulados[$i]['peso_simulado']*($simulados[$i]['valor_nota']/$simulados[$i]['nota_maxima_simulado']);
                                $div += $simulados[$i]['peso_simulado'];
                                echo "notas[".$simulados[$i]['disciplina_iddisciplina']."][".$l++."][2]=".$soma/$div.";";
                            }
                            echo "media[".$m."] = [];";
                            echo "media[".$m."][0] = '".$nome_disciplina."';";
                            echo "media[".$m."][1] = ".$soma/$div.";";
                                    
                    }
                    
                    ?>
                
                             
                  google.charts.load('current', {'packages':['corechart']});
            
                  google.charts.setOnLoadCallback(drawNotasDisciplinaChart);
                  
                  function drawNotasDisciplinaChart() {
                        var dataMedia = new google.visualization.DataTable();
                        dataMedia.addColumn('string','Descrição do Simulado');
                        dataMedia.addColumn('number','Resultado do Simulado');
                        dataMedia.addColumn('number','Média da Disciplina');
                        dataMedia.addRows(<?php echo "notas[".$simulados[0]['disciplina_iddisciplina']."]";?>)
                        var options = {'title':'Disciplina:<?php echo $simulados[0]['nome_adicional']?>',        
                                       'width':400,
                                       'height':300,
                                       seriesType: 'bars',
                                       series: {1: {type: 'line'}},
                                       legend: { position: "none" },
                                       vAxis: { format:'#%'},
                                       animation : {duration: 500}
                                       
                                       };
                        var chart = new google.visualization.ComboChart(document.getElementById('grafico_disciplina'));
                        chart.draw(dataMedia, options);
                        
                               
                        var btns = document.getElementById('btnGrp_disciplinas');
                        btns.onclick = function (e) {
                              dataMedia = new google.visualization.DataTable();
                              dataMedia.addColumn('string','Descrição do Simulado');
                              dataMedia.addColumn('number','Resultado do Simulado');
                              dataMedia.addColumn('number','Média da Disciplina');
                              <?php
                              $k=0;
                              $idDiscAnterior="";
                              for ($k;$k<count($simulados);$k++){
                                    if (!isset($simulados[$k]['disciplina_iddisciplina']))
                                        break;
                                    else{
                                        if ($idDiscAnterior!=$simulados[$k]['disciplina_iddisciplina']){
                                            echo "if (e.target.tagName === 'BUTTON' && e.target.id === 'btnSimuladoDisciplina".$simulados[$k]['disciplina_iddisciplina']."'){";
                                            echo "  dataMedia.addRows(notas[".$simulados[$k]['disciplina_iddisciplina']."]);";
                                            echo " options.title = 'Disciplina:".$simulados[$k]['nome_adicional']."'; chart.draw(dataMedia, options); }"; 
                                            $idDiscAnterior = $simulados[$k]['disciplina_iddisciplina'];
                                        }
                                    }
                                }
                               ?>
                        };
                  }
                         
                  
                  google.charts.setOnLoadCallback(drawMediaDisciplinaChart);
                            
                   
                  function drawMediaDisciplinaChart() {
                            var dataMedia = new google.visualization.DataTable();
                            dataMedia.addColumn('string','Nome da Disciplina');
                            dataMedia.addColumn('number','Média na Disciplina');
                            dataMedia.addRows(media);
                  
            
                           var options = {'title':'Média das Disciplinas',
                                   'width':400,
                                   'height':300,
                                   legend: { position: "none" }};
            
                    var chart = new google.visualization.ColumnChart(document.getElementById('grafico_disciplina_media'));
                    chart.draw(dataMedia, options);
                  }      
          </script>
                  
  
          </div>
        </div>
      </div>
    </div>
    <?php
    }
    if (isset($simulados[$i]['area_idarea'])){
    ?>  
        
    
      
      
      
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingArea">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseArea" aria-expanded="false" aria-controls="collapseTwo">
              Avaliações por Área
            </a>
          </h4>
        </div>
        <div id="collapseArea" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
          <div class="panel-body">
              <div class = "container">
                  <div class = "row">
                      <div id="btnGrp_areas" class="btn-group-vertical col-xs-4">
                      <?php
                      
                      $idAreaAnterior="";
                      for ($j;$j<count($simulados);$j++){
                            if (!isset($simulados[$j]['area_idarea']))
                                break;
                            else{
                                if ($idAreaAnterior!=$simulados[$j]['area_idarea']){
                                    echo '<button type="button" class="btn btn-secondary" id="btnSimuladoArea'.$simulados[$j]['area_idarea'].'">'.$simulados[$j]['nome_adicional'].'</button>';
                                    $idAreaAnterior = $simulados[$j]['area_idarea'];
                                }
                            }
                        }
                       ?>    
                      </div>
                      <div id="grafico_area" class="col-xs-4"></div>
                      <div id="grafico_area_media" class="col-xs-4"></div>
                  </div>
               <script>
                var notasA = [];
                var mediaA = [];
                <?php 
                    
                    $area = $simulados[$i]['area_idarea'];
                    $nome_area = $simulados[$i]['nome_adicional'];
                    $soma = 0;
                    $div = 0;
                    $l =0;
                    $m = 0;
                    echo "notasA[".$simulados[$i]['area_idarea']."] = [];";
                    for ($i;$i<count($simulados);$i++){
                        if (!isset($simulados[$i]['area_idarea']))
                            break;
                        else{
                                if($area!=$simulados[$i]['area_idarea']){
                                    echo "mediaA[".$m."] = [];";
                                    echo "mediaA[".$m."][0] = '".$nome_area."';";
                                    echo "mediaA[".$m."][1] = ".$soma/$div.";";
                                    $nome_disciplina = $simulados[$i]['nome_adicional'];
                                    $disciplina = $simulados[$i]['area_idarea'];
                                    $m++;
                                    $soma = 0;
                                    $div = 0;
                                    $l =0;
                                    echo "notasA[".$simulados[$i]['area_idarea']."] = [];";
                                }        
                                echo "notasA[".$simulados[$i]['area_idarea']."][".$l."] = [];";
                                echo "notasA[".$simulados[$i]['area_idarea']."][".$l."][0]='".$simulados[$i]['descricao_simulado']."';";
                                echo "notasA[".$simulados[$i]['area_idarea']."][".$l."][1]=".$simulados[$i]['valor_nota']/$simulados[$i]['nota_maxima_simulado'].";";
                                $soma += $simulados[$i]['peso_simulado']*($simulados[$i]['valor_nota']/$simulados[$i]['nota_maxima_simulado']);
                                $div += $simulados[$i]['peso_simulado'];
                                echo "notasA[".$simulados[$i]['area_idarea']."][".$l++."][2]=".$soma/$div.";";
                            }
                            echo "mediaA[".$m."] = [];";
                            echo "mediaA[".$m."][0] = '".$nome_area."';";
                            echo "mediaA[".$m."][1] = ".$soma/$div.";";
                    }
                    ?>
                
                             
                  google.charts.load('current', {'packages':['corechart']});
            
                  google.charts.setOnLoadCallback(drawNotasAreaChart);
                  
                  function drawNotasAreaChart() {
                        var dataArea = new google.visualization.DataTable();
                        dataArea.addColumn('string','Descrição do Simulado');
                        dataArea.addColumn('number','Resultado do Simulado');
                        dataArea.addColumn('number','Média da Área');
                        dataArea.addRows(<?php echo "notasA[".$simulados[$k]['area_idarea']."]";?>)
                        var options = {'title':'Área:<?php echo $simulados[$k]['nome_adicional']?>',        
                                       'width':400,
                                       'height':300,
                                       vAxis: { format:'#%'},
                                       ticks: [0, .3, .6, .9, 1],
                                       seriesType: 'bars',
                                       series: {1: {type: 'line'}},
                                       legend: { position: "none" },
                                       animation : {duration: 500}
                                       
                                       };
                        var chart = new google.visualization.ComboChart(document.getElementById('grafico_area'));
                        chart.draw(dataArea, options);         
                               
                        var btns = document.getElementById('btnGrp_areas');
                        btns.onclick = function (e) {
                              dataArea = new google.visualization.DataTable();
                              dataArea.addColumn('string','Descrição do Simulado');
                              dataArea.addColumn('number','Resultado do Simulado');
                              dataArea.addColumn('number','Média da Área');
                              <?php
                              $idDiscAnterior="";
                              for ($k;$k<count($simulados);$k++){
                                    if (!isset($simulados[$k]['area_idarea']))
                                        break;
                                    else{
                                        if ($idDiscAnterior!=$simulados[$k]['area_idarea']){
                                            echo "if (e.target.tagName === 'BUTTON' && e.target.id === 'btnSimuladoArea".$simulados[$k]['area_idarea']."'){";
                                            echo "  dataArea.addRows(notasA[".$simulados[$k]['area_idarea']."]);";
                                            echo " options.title = 'Área:".$simulados[$k]['nome_adicional']."'; chart.draw(dataArea, options); }"; 
                                            $idDiscAnterior = $simulados[$k]['area_idarea'];
                                        }
                                    }
                                }
                               ?>
                                
                        };
                  }
                  google.charts.setOnLoadCallback(drawMediaAreaChart);
                  function drawMediaAreaChart() {
                            var dataMediaA = new google.visualization.DataTable();
                            dataMediaA.addColumn('string','Nome da Área');
                            dataMediaA.addColumn('number','Média na Área');
                            dataMediaA.addRows(mediaA);
                            var options = {'title':'Média das Áreas',
                                   'width':400,
                                   'height':300,
                                   legend: { position: "none" }};
            
                    var chart = new google.visualization.ColumnChart(document.getElementById('grafico_area_media'));
                    chart.draw(dataMediaA, options);
                  }      
                </script>
                  
  
            </div>
          </div>
        </div>
      </div>
      
    <?php
    }
    if (isset($simulados[$i]['grande_area_idgrande_area'])){
    ?>
      
      
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingGrandeArea">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseGrandeArea" aria-expanded="false" aria-controls="collapseThree">
              Avaliações por Grande Área
            </a>
          </h4>
        </div>
        <div id="collapseGrandeArea" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
          <div class="panel-body">
            <div class = "container">
                  <div class = "row">
                      <div id="btnGrp_grande" class="btn-group-vertical col-xs-4">
                      <?php
                      
                      $idGrandeAnterior="";
                      for ($j;$j<count($simulados);$j++){
                            if (!isset($simulados[$j]['grande_area_idgrande_area']))
                                break;
                            else{
                                if ($idGrandeAnterior!=$simulados[$j]['grande_area_idgrande_area']){
                                    echo '<button type="button" class="btn btn-secondary" id="btnSimuladoGrande'.$simulados[$j]['grande_area_idgrande_area'].'">'.$simulados[$j]['nome_adicional'].'</button>';
                                    $idGrandeAnterior = $simulados[$j]['grande_area_idgrande_area'];
                                }
                            }
                        }
                       ?>    
                      </div>
                      <div id="grafico_grande" class="col-xs-4"></div>
                      <div id="grafico_grande_media" class="col-xs-4"></div>
                  </div>
               <script>
                var notasG = [];
                var mediaG = [];
                <?php 
                    
                    $grande = $simulados[$i]['grande_area_idgrande_area'];
                    $nome_grande = $simulados[$i]['nome_adicional'];
                    $soma = 0;
                    $div = 0;
                    $l =0;
                    $m = 0;
                    echo "notasG[".$simulados[$i]['grande_area_idgrande_area']."] = [];";
                    for ($i;$i<count($simulados);$i++){
                        if (!isset($simulados[$i]['grande_area_idgrande_area']))
                            break;
                        else{
                                if($grande!=$simulados[$i]['grande_area_idgrande_area']){
                                    echo "mediaG[".$m."] = [];";
                                    echo "mediaG[".$m."][0] = '".$nome_grande."';";
                                    echo "mediaG[".$m."][1] = ".$soma/$div.";";
                                    $nome_disciplina = $simulados[$i]['nome_adicional'];
                                    $area = $simulados[$i]['grande_area_idgrande_area'];
                                    $m++;
                                    $soma = 0;
                                    $div = 0;
                                    $l =0;
                                    echo "notasG[".$simulados[$i]['grande_area_idgrande_area']."] = [];";
                                }        
                                echo "notasG[".$simulados[$i]['grande_area_idgrande_area']."][".$l."] = [];";
                                echo "notasG[".$simulados[$i]['grande_area_idgrande_area']."][".$l."][0]='".$simulados[$i]['descricao_simulado']."';";
                                echo "notasG[".$simulados[$i]['grande_area_idgrande_area']."][".$l."][1]=".$simulados[$i]['valor_nota']/$simulados[$i]['nota_maxima_simulado'].";";
                                $soma += $simulados[$i]['peso_simulado']*($simulados[$i]['valor_nota']/$simulados[$i]['nota_maxima_simulado']);
                                $div += $simulados[$i]['peso_simulado'];
                                echo "notasG[".$simulados[$i]['grande_area_idgrande_area']."][".$l++."][2]=".$soma/$div.";";
                            }
                            echo "mediaG[".$m."] = [];";
                            echo "mediaG[".$m."][0] = '".$nome_grande."';";
                            echo "mediaG[".$m."][1] = ".$soma/$div.";";
                    }
                    ?>
                
                             
                  google.charts.load('current', {'packages':['corechart']});
            
                  google.charts.setOnLoadCallback(drawNotasGrandeChart);
                  
                  function drawNotasGrandeChart() {
                        var dataAreaG = new google.visualization.DataTable();
                        dataAreaG.addColumn('string','Descrição do Simulado');
                        dataAreaG.addColumn('number','Resultado do Simulado');
                        dataAreaG.addColumn('number','Média da Grande Área');
                        dataAreaG.addRows(<?php echo "notasG[".$simulados[$k]['grande_area_idgrande_area']."]";?>)
                        var options = {'title':'Grande Área:<?php echo $simulados[$k]['nome_adicional']?>',        
                                       'width':400,
                                       'height':300,
                                       vAxis: { format:'#%'},
                                       seriesType: 'bars',
                                       series: {1: {type: 'line'}},
                                       legend: { position: "none" },
                                       animation : {duration: 500}
                                       
                                       };
                        var chart = new google.visualization.ComboChart(document.getElementById('grafico_grande'));
                        chart.draw(dataAreaG, options);         
                               
                        var btns = document.getElementById('btnGrp_grande');
                        btns.onclick = function (e) {
                              dataAreaG = new google.visualization.DataTable();
                              dataAreaG.addColumn('string','Descrição do Simulado');
                              dataAreaG.addColumn('number','Resultado do Simulado');
                              dataAreaG.addColumn('number','Média da Grande Área');
                              <?php
                              $idGrandeAnterior="";
                              for ($k;$k<count($simulados);$k++){
                                    if (!isset($simulados[$k]['grande_area_idgrande_area']))
                                        break;
                                    else{
                                        if ($idDiscAnterior!=$simulados[$k]['grande_area_idgrande_area']){
                                            echo "if (e.target.tagName === 'BUTTON' && e.target.id === 'btnSimuladoGrande".$simulados[$k]['grande_area_idgrande_area']."'){";
                                            echo "  dataMediaG.addRows(notasG[".$simulados[$k]['grande_area_idgrande_area']."]);";
                                            echo " options.title = 'Grande Área:".$simulados[$k]['nome_adicional']."'; }"; 
                                            $idDiscAnterior = $simulados[$k]['grande_area_idgrande_area'];
                                        }
                                    }
                                }
                               ?>
                                chart.draw(dataAreaG, options);
                        };
                  }
                  google.charts.setOnLoadCallback(drawMediaGrandeChart);
                  function drawMediaGrandeChart() {
                            var dataMediaG = new google.visualization.DataTable();
                            dataMediaG.addColumn('string','Nome da Grande Área');
                            dataMediaG.addColumn('number','Média na Grande Área');
                            dataMediaG.addRows(mediaG);
                            var options = {'title':'Média das Grandes Áreas',
                                   'width':400,
                                   'height':300,
                                    vAxis: { format:'#%'},
                                    seriesType: 'bars',
                                    series: {1: {type: 'line'}},
                                    legend: { position: "none" },
                                    animation : {duration: 500}}
            
                    var chart = new google.visualization.ColumnChart(document.getElementById('grafico_grande_media'));
                    chart.draw(dataMediaG, options);
                  }      
                </script>
            </div>
          </div>
        </div>
      </div>
      
      
      <?php 
      }
      if (isset($simulados[$i])){
      ?>
      
          <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingGeral">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseGeral" aria-expanded="false" aria-controls="collapseThree">
              Avaliações Gerais
            </a>
          </h4>
        </div>
        <div id="collapseGeral" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
          <div class="panel-body">
              <div class = "container">
                  <div class = "row">
                      <div id="grafico_geral" class="col-xs-6"></div>
                      <div id="grafico_geral_media" class="col-xs-6"></div>
                  </div>
               <script>
                var notasF = [];
                var mediaF = [];
                <?php 
                    
                    $soma = 0;
                    $div = 0;
                    $l =0;
                    $m = 0;
                    echo "notasF[".$l."] = [];";
                    for ($i;$i<count($simulados);$i++){
                        echo "notasF[".$l."] = [];";
                        echo "notasF[".$l."][0]='".$simulados[$i]['descricao_simulado']."';";
                        echo "notasF[".$l."][1]=".$simulados[$i]['valor_nota']/$simulados[$i]['nota_maxima_simulado'].";";
                        $soma += $simulados[$i]['peso_simulado']*($simulados[$i]['valor_nota']/$simulados[$i]['nota_maxima_simulado']);
                        $div += $simulados[$i]['peso_simulado'];
                        echo "notasF[".$l++."][2]=".$soma/$div.";";
                    }
                    echo "mediaF[".$m."] = [];";
                    echo "mediaF[".$m."][0] = 'Simulados Gerais';";
                    echo "mediaF[".$m."][1] = ".$soma/$div.";";
                    ?>
                             
                  google.charts.load('current', {'packages':['corechart']});
            
                  google.charts.setOnLoadCallback(drawNotasGeralChart);
                  
                  function drawNotasGeralChart() {
                        var dataAreaF = new google.visualization.DataTable();
                        dataAreaF.addColumn('string','Descrição do Simulado');
                        dataAreaF.addColumn('number','Resultado do Simulado');
                        dataAreaF.addColumn('number','Média Geral');
                        dataAreaF.addRows(notasF)
                        var options = {'title':'Simulados Gerais',        
                                       'width':400,
                                       'height':300,
                                       vAxis: { format:'#%'},
                                       seriesType: 'bars',
                                       series: {1: {type: 'line'}},
                                       legend: { position: "none" },
                                       animation : {duration: 500}
                                       
                                       };
                        var chart = new google.visualization.ComboChart(document.getElementById('grafico_geral'));
                        chart.draw(dataAreaF, options);         
                  }
                  google.charts.setOnLoadCallback(drawMediaGeralChart);
                  function drawMediaGeralChart() {
                            var dataMediaF = new google.visualization.DataTable();
                            dataMediaF.addColumn('string','Nome:');
                            dataMediaF.addColumn('number','Média:');
                            dataMediaF.addRows(mediaF);
                            var options = {'title':'Média de Avaliações Gerais',
                                   'width':400,
                                   'height':300,
                                   legend: { position: "none" }};
            
                    var chart = new google.visualization.ColumnChart(document.getElementById('grafico_geral_media'));
                    chart.draw(dataMediaF, options);
                  }      
                </script>
            </div>
          </div>
        </div>
      </div>
      <br><br><br>
    <p align="center">
        <button class="btn btn-default" id="voltar"><i class="fa fa-arrow-circle-left"></i> Voltar</button>
    </p>
      <?php 
        }
      ?>
      
    </div>
</div>
<script>
        $(document).ready(function () {
        $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/aluno"; 
        });
    }); 
    
    
</script>