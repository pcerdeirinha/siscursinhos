<?php $this->template->menu($view) ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">            
                <?php echo form_open('evento/cria','enctype="multipart/form-data"'); ?>      
                <?php if(isset($id)||(set_value('id')!='')){/*Então é Update*/?>
                            <h3><b>Edita Evento</b></h3>
                            <br>
                            <?php echo form_hidden('id', $id?$id:set_value('id')); 
                        }else{ ?>
                            <h3><b>Novo Evento</b></h3>                    <br>
                <?php } ?> 
                <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#dadosGerais">Dados Gerais</a></li>    
                        <li><a data-toggle="tab" href="#endereco">Endereço</a></li>     
                        <li><a data-toggle="tab" href="#configuracao">Configuração</a></li>                     
                </ul>                
        </div>
                
        <div class="tab-content">
            <div id="dadosGerais" class="tab-pane fade in active">                
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group <?php if (!(form_error('nome_evento')=='')) echo 'has-error has-feedback'; ?>">
                            <label for="nome">Nome</label>                                    
                            <?php echo form_input('nome_evento', set_value('nome_evento')?set_value('nome_evento'):$nome_evento, 'type="text", class="form-control" id="nome" placeholder="Nome"'); ?>
                            <?php if (!(form_error('nome_evento')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('nome_evento'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-md-offset-2">
                        <div class="form-group <?php if (!(form_error('data_inicio_evento')=='')) echo 'has-error has-feedback'; ?>">
                            <label for="data_inicio">Data de Início</label>                                    
                            <?php echo form_input('data_inicio_evento', set_value('data_inicio_evento')?set_value('data_inicio_evento'):$data_inicio_evento, 'type="text", class="form-control" id="data_inicio" placeholder="Data de Início" tipo="data"'); ?>
                            <?php if (!(form_error('data_inicio_evento')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('data_inicio_evento'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group <?php if (!(form_error('data_fim_evento')=='')) echo 'has-error has-feedback'; ?>">
                            <label for="data_fim">Data de Fim</label>                                    
                            <?php echo form_input('data_fim_evento', set_value('data_fim_evento')?set_value('data_fim_evento'):$data_fim_evento, 'type="text", class="form-control" id="data_fim" placeholder="Data de Fim" tipo="data"'); ?>
                            <?php if (!(form_error('data_fim_evento')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('data_fim_evento'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group <?php if (!(form_error('hora_inicio_evento')=='')) echo 'has-error has-feedback'; ?>">
                            <label for="hora_inicio">Horário de Início</label>                                    
                            <?php echo form_input('hora_inicio_evento', set_value('hora_inicio_evento')?set_value('hora_inicio_evento'):$hora_inicio_evento, 'type="text", class="form-control" id="hora_inicio" placeholder="Horário de Início" tipo="hora"'); ?>
                            <?php if (!(form_error('hora_inicio_evento')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('hora_inicio_evento'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group <?php if (!(form_error('hora_fim_evento')=='')) echo 'has-error has-feedback'; ?>">
                            <label for="hora_fim">Horário de Fim</label>                                    
                            <?php echo form_input('hora_fim_evento', set_value('hora_fim_evento')?set_value('hora_fim_evento'):$hora_fim_evento, 'type="text", class="form-control" id="hora_fim" placeholder="Horário de Fim" tipo="hora"'); ?>
                            <?php if (!(form_error('hora_fim_evento')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('hora_fim_evento'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-2">
                        <div class="form-group <?php if (!(form_error('telefone_evento')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Telefone de Contato', 'telefone'); ?>
                            <?php echo form_input('telefone_evento', set_value('telefone_evento')?set_value('telefone_evento'):$telefone_evento, 'type="tel" min="8" max="10", class="form-control" id="telefone" placeholder="DDD + Telefone" tipo="celular"'); ?>
                            <?php if (!(form_error('telefone_evento')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('telefone_evento'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group <?php if (!(form_error('email_evento')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Email de Contato', 'email'); ?>
                            <?php echo form_input('email_evento', set_value('email_evento')?set_value('email_evento'):$email_evento, 'type="email", class="form-control" id="email" placeholder="Email"'); ?> 
                            <?php if (!(form_error('email_evento')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('email_evento'); ?></span>
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                            <div class="form-group <?php if (!(form_error('descricao_evento')=='')) echo 'has-error has-feedback'; ?>">
                                <?php echo form_label('Descrição do Evento', 'descricao_evento'); ?> 
                                <?php echo form_textarea('descricao_evento', set_value('descricao_evento')?set_value('descricao_evento'):$descricao_evento, 'class="form-control" id="descricao_evento" placeholder="Descrição do Evento"'); ?> 
                            <span class="text-danger"><?php echo form_error('descricao_evento'); ?></span>
                            </div>
                     </div>
                </div>
            </div>
            <div id="endereco" class="tab-pane fade">                
                <br>
                <div class="row">
                    <div class="col-md-6 col-md-offset-2">
                        <div class="form-group <?php if (!(form_error('logradouro_evento')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Logradouro', 'logradouro'); ?>
                            <?php echo form_input('logradouro_evento', set_value('logradouro_evento')?set_value('logradouro_evento'):$logradouro_evento, 'type="text", class="form-control" id="logradouro" placeholder="Endereço"'); ?>
                            <?php if (!(form_error('logradouro_evento')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('logradouro_evento'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group <?php if (!(form_error('numero_evento')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Numero', 'numero'); ?>
                            <?php echo form_input('numero_evento', set_value('numero_evento')?set_value('numero_evento'):$numero_evento, 'type="text", class="form-control" id="numero" placeholder="N." tipo="numero"'); ?>
                            <?php if (!(form_error('numero_evento')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('numero_evento'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-2">
                        <div class="form-group <?php if (!(form_error('bairro_evento')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Bairro', 'bairro'); ?>
                            <?php echo form_input('bairro_evento', set_value('bairro_evento')?set_value('bairro_evento'):$bairro_evento, 'type="text", class="form-control" id="bairro" placeholder="Bairro"'); ?>
                            <?php if (!(form_error('bairro_evento')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('bairro_evento'); ?></span>
                       
                        </div>
                    </div >
                    <div class="col-md-4">
                        <div class="form-group">
                            <?php echo form_label('Complemento', 'complemento'); ?>
                            <?php echo form_input('complemento_endereco_evento', set_value('complemento_endereco_evento')?set_value('complemento_endereco_evento'):$complemento_endereco_evento, 'type="text", class="form-control" id="complemento" placeholder="Complemento de endereço"'); ?>
                            <?php if (!(form_error('bairro_evento')=='')) echo '<br>'; //ver isso aqui ?>
    
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-1 col-md-offset-2">
                        <div class="form-group <?php if (!(form_error('estado_evento')=='')) echo 'has-error'; ?>">
                            <?php echo form_label('UF', 'uf'); ?><br>
                            <?php echo form_dropdown('estado_evento', $estados, set_value('estado_evento')?set_value('estado_evento'):$estado_sel, 'type="text" min="2", class="form-control" id="uf" placeholder="Estado"'); ?>
                            <?php if (!(form_error('estado_evento')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        </div>  
                    </div>
                    <div class="col-md-4">
                        <div class="form-group <?php if (!(form_error('cidade_evento')=='')) echo 'has-error'; ?>">
                            <?php echo form_label('Cidade', 'cidade'); ?>
                            <?php echo form_dropdown('cidade_evento', $cidades, set_value('cidade_evento')?set_value('cidade_evento'):$cidade_sel ,'type="text", class="form-control" id="cidade" placeholder="Cidade"'); ?>
                            <span class="text-danger"><?php echo form_error('cidade_evento'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group <?php if (!(form_error('cep_evento')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('CEP', 'cep'); ?>             
                            <?php echo form_input('cep_evento', set_value('cep_evento')?set_value('cep_evento'):$cep_evento,'type="text", class="form-control" id="cep" placeholder="CEP" tipo="cep"'); ?>
                            <?php if (!(form_error('cep_evento')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('cep_evento'); ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="configuracao" class="tab-pane fade">                
                <br>
                <div class="row">
                    <div class="col-md-4  col-md-offset-2">
                        <div class="form-group <?php if (!(form_error('cor_principal_evento')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Cor Principal', 'cor_principal'); ?>             
                            <input type="color" name="cor_principal_evento" value="<?php echo set_value('cor_principal_evento')?set_value('cor_principal_evento'):$cor_principal_evento; ?>" id="cor_principal" class="form-control" placeholder="Cor Principal"/>
                            <?php if (!(form_error('cor_principal_evento')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('cor_principal_evento'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group <?php if (!(form_error('cor_secundaria_evento')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Cor Secundária', 'cor_secundaria'); ?>             
                            <input type="color" name="cor_secundaria_evento" value="<?php echo set_value('cor_secundaria_evento')?set_value('cor_secundaria_evento'):$cor_secundaria_evento; ?>" id="cor_secundaria" class="form-control" placeholder="Cor Secundária"/>
                            <?php if (!(form_error('cor_secundaria_evento')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('cor_secundaria_evento'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-md-offset-2">
                        <div class="form-group <?php if (!(form_error('titulo_imagem[0]')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Título da Imagem Obrigatória','titulo_imagem'); ?>                                    
                            <?php echo form_input('titulo_imagem[0]',set_value('titulo_imagem[0]')?set_value('titulo_imagem[0]'):$imagem[0]['titulo_imagem'], 'type="text", class="form-control" id="titulo_imagem" placeholder="Título da Imagem"'); ?>
                            <?php if (!(form_error('titulo_imagem[0]')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('titulo_imagem[0]'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group <?php if (!(form_error('descricao_imagem[0]')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Descrição da Imagem Obrigatória','descricao_imagem'); ?>                                    
                            <?php echo form_input('descricao_imagem[0]',set_value('descricao_imagem[0]')?set_value('descricao_imagem[0]'):$imagem[0]['descricao_imagem'], 'type="text", class="form-control" id="descricao_imagem" placeholder="Descrição da Imagem"'); ?>
                            <?php if (!(form_error('descricao_imagem[0]')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('descricao_imagem[0]'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group <?php if (!(form_error('imagem_0]')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Imagem Obrigatória','imagem'); ?>                                    
                            <?php echo form_upload('imagem_0',set_value('imagem_0')?set_value('imagem_0'):$imagem[0]['imagem'], 'type="text", class="form-control" id="imagem" placeholder="Imagem" accept="image/x-png,image/jpeg"'); ?>
                            <span class="text-danger"><?php echo form_error('imagem_0'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-md-offset-2">
                        <div class="form-group <?php if (!(form_error('titulo_imagem[1]')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Título da Imagem Adicional','titulo_imagem'); ?>                                    
                            <?php echo form_input('titulo_imagem[1]',set_value('titulo_imagem[1]')?set_value('titulo_imagem[1]'):$imagem[1]['titulo_imagem'], 'type="text", class="form-control" id="titulo_imagem" placeholder="Título da Imagem"'); ?>
                            <?php if (!(form_error('titulo_imagem[1]')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('titulo_imagem[1]'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group <?php if (!(form_error('descricao_imagem[1]')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Descrição da Imagem Adicional','descricao_imagem'); ?>                                    
                            <?php echo form_input('descricao_imagem[1]',set_value('descricao_imagem[1]')?set_value('descricao_imagem[1]'):$imagem[1]['descricao_imagem'], 'type="text", class="form-control" id="descricao_imagem" placeholder="Descrição da Imagem"'); ?>
                            <?php if (!(form_error('descricao_imagem[1]')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('descricao_imagem[1]'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group <?php if (!(form_error('imagem_1')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Imagem Adicional','imagem'); ?>                                    
                            <?php echo form_upload('imagem_1',set_value('imagem_1')?set_value('imagem_1'):$imagem[1]['imagem'], 'type="text", class="form-control" id="imagem" placeholder="Imagem" accept="image/x-png,image/jpeg"'); ?>
                            <span class="text-danger"><?php echo form_error('imagem_1'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-md-offset-2">
                        <div class="form-group <?php if (!(form_error('titulo_imagem[2]')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Título da Imagem Adicional','titulo_imagem'); ?>                                    
                            <?php echo form_input('titulo_imagem[2]',set_value('titulo_imagem[2]')?set_value('titulo_imagem[2]'):$imagem[2]['titulo_imagem'], 'type="text", class="form-control" id="titulo_imagem" placeholder="Título da Imagem"'); ?>
                            <?php if (!(form_error('titulo_imagem[2]')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('titulo_imagem[2]'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group <?php if (!(form_error('descricao_imagem[2]')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Descrição da Imagem Adicional','descricao_imagem'); ?>                                    
                            <?php echo form_input('descricao_imagem[2]',set_value('descricao_imagem[2]')?set_value('descricao_imagem[2]'):$imagem[2]['descricao_imagem'], 'type="text", class="form-control" id="descricao_imagem" placeholder="Descrição da Imagem"'); ?>
                            <?php if (!(form_error('descricao_imagem[2]')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('descricao_imagem[2]'); ?></span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group <?php if (!(form_error('imagem_2')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Imagem Adicional','imagem'); ?>                                    
                            <?php echo form_upload('imagem_2',set_value('imagem_2')?set_value('imagem_2'):$imagem[2]['imagem'], 'type="text", class="form-control" id="imagem" placeholder="Imagem" accept="image/x-png,image/jpeg"'); ?>
                            <span class="text-danger"><?php echo form_error('imagem_2'); ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1 col-md-offset-8">
            <div class="form-save-buttons">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="col-md-1">
            <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>
    </div>
</div>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<script  type="text/javascript">
    function readURL(input) {
    
      if (input.files && input.files[0]) {
        var reader = new FileReader();
    
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
    
        reader.readAsDataURL(input.files[0]);
      }
    }
    
    $("#imagem").change(function() {
      readURL(this);
    });

    function obterCidade(uf,cidade){
        jQuery.ajax({
                      type: "POST",
                      url: "<?php echo base_url(); ?>" + "index.php/usuario/buscaCidade",
                      dataType: 'json',
                      data: {uf: uf},
                      success: function(res) {
                        var row ='';      
                        for(var i in res.cidades){
                            if (res.cidades[i]!=cidade)
                                row+='<option  value = "'+res.cidades[i]+'">'+res.cidades[i]+'</option>';
                             else row+='<option  value = "'+res.cidades[i]+'" selected="select"">'+res.cidades[i]+'</option>';
                        }
                        $("[name=cidade_evento]").html(row);
                      }
             });
    }

    $(document).ready(function () {
        mascara();
        data();
        $("[name=estado_evento]").on('change',function(event) {
           event.preventDefault(); 
           var uf = $('option:selected', this).val();
           obterCidade(uf,'');
        });
    });
    $("#voltar").click(function(event){
        window.location.href = "<?php echo base_url(); ?>"+"index.php/evento/opcoes";  
    });
</script>

