<?php $this->template->menu($view) ?>
<div class='container'>
    <div class="row"> 
    	<div class="col-md-8 col-md-offset-2">
    		<div class="panel-group panel-default panel panel-info">
                <div class="panel-heading">
                	Destino da Manifestação
                	<a data-toggle="collapse" data-parent="#accordion" href="#collapse_destino">
                    	<span class="pull-right" >
                          	<i class="fa fa-chevron-up" aria-hidden="true"></i>
                      	</span>
                  	</a>
                </div>
                <div id="collapse_destino" class="panel-collapse collapse in">
	                <div class="panel-body">
	                    <div class="col-md-6 form-group">
		                    <div class="input-group">
		                        <span class="input-group-addon" id="user-addon" >Representante</span>
		                        <?php echo form_input('representantes_ouvidoria', set_value('representates_ouvidoria')?set_value('representates_ouvidoria'):$representates_ouvidoria ,'type="text", class="form-control" id="representantes" placeholder="Representantes" readonly'); ?>
		                    </div>
		                </div>
		                <div class="col-md-6 form-group">
		                    <div class="input-group <?php if (!(form_error('individuos_ouvidoria')=='')) echo 'has-error'; ?>">
		                        <span class="input-group-addon" id="user-addon" >Indivíduo</span>
		                        <?php echo form_input('individuos_ouvidoria', set_value('individuos_ouvidoria')?set_value('individuos_ouvidoria'):$individuos_ouvidoria ,'type="text", class="form-control" id="indiviuos" placeholder="Indivíduos" readonly'); ?>
		                    </div>
		                </div>
	                </div>
                </div>
            </div>
            <div class="panel-group panel-default panel panel-info">
                <div class="panel-heading">
                    Identificação do Remetente
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse_remetente">
                    	<span class="pull-right" >
                          	<i class="fa fa-chevron-up" aria-hidden="true"></i>
                      	</span>
                  	</a>
                </div>
                <div id="collapse_remetente" class="panel-collapse collapse in">
                	<div class="panel-body">
                        <div class="col-md-6 form-group">
		                    <div class="input-group">
		                        <span class="input-group-addon" id="user-addon" >Deseja se identificar?</span>
		                        <?php echo form_input('identificacao', set_value('identificacao')?set_value('identificacao'):$identificacao ,'class="form-control" id="identificacao" placeholder="Identificação" readonly'); ?>
		                    </div>
		                </div>
		                <div class="col-md-6 form-group">
		                    <div class="input-group">
		                        <span class="input-group-addon" id="user-addon" >Atividade</span>
		                        <?php echo form_input('atividade_remetente', set_value('atividade_remetente')?set_value('atividade_remetente'):$atividade_remetente ,'type="text", class="form-control" id="atividade" placeholder="Atividade" readonly'); ?>
		                    </div>
		                </div>
		                
		                <div class="col-md-12 form-group">
		                    <div class="input-group">
		                        <span class="input-group-addon" id="user-addon" >Remetente</span>
		                        <?php echo form_input('remetente_ouvidoria', set_value('remetente_ouvidoria')?set_value('remetente_ouvidoria'):$remetente_ouvidoria ,'type="text", class="form-control" id="remetente" placeholder="Remetente" readonly'); ?>
		                    </div>
		                </div>
	                </div>
                </div>
            </div>
            <div class="panel-group panel-default panel panel-info">
                <div class="panel-heading">
                    Corpo da Manifestaçao
                	<a data-toggle="collapse" data-parent="#accordion" href="#collapse_corpo">
                    	<span class="pull-right" >
                          	<i class="fa fa-chevron-up" aria-hidden="true"></i>
                      	</span>
                  	</a>
                </div>
                <div id="collapse_corpo" class="panel-collapse collapse in">
	                <div class="panel-body">
	                	<div class="col-md-6 form-group">
		                    <div class="input-group">
		                        <span class="input-group-addon" id="user-addon" >Tipo de Manifestação</span>
		                        <?php echo form_input('tipo_ouvidoria', set_value('tipo_ouvidoria')?set_value('tipo_ouvidoria'):$tipo_ouvidoria ,'type="text", class="form-control" id="tipo" placeholder="Tipo" readonly'); ?>
		                    </div>
		                </div>
	                	<div class="col-md-6 form-group">
		                    <div class="input-group <?php if (!(form_error('ambiente_ouvidorira')=='')) echo 'has-error'; ?>">
		                        <span class="input-group-addon" id="user-addon" >Ambiente da Manifestação</span>
		                        <?php echo form_input('ambiente_ouvidoria', set_value('ambiente_ouvidorira')?set_value('ambiente_ouvidorira'):$ambiente_ouvidorira ,'type="text", class="form-control" id="ambiente" placeholder="Ambiente" readonly'); ?>
		                    </div>
		                </div>
	                	<div class="col-md-6 form-group">
		                    <div class="input-group <?php if (!(form_error('refinacao_assunto')=='')) echo 'has-error'; ?>">
		                        <span class="input-group-addon" id="user-addon" >Assunto da Manifestação</span>
		                        <?php echo form_input('assunto_ouvidoria', set_value('assunto_ouvidorira')?set_value('assunto_ouvidorira'):$assunto_ouvidorira ,'type="text", class="form-control" id="assunto" placeholder="Assunto" readonly'); ?>
		                    </div>
		                </div>
		                <div class="col-md-6 form-group">
		                    <div class="input-group">
		                        <span class="input-group-addon" id="user-addon" >Data de Modificação</span>
		                        <?php echo form_input('data_modificacao', set_value('data_modificacao')?set_value('data_modificacao'):$data_modificacao ,'type="text", class="form-control" id="data" placeholder="Data de Modificação" readonly'); ?>
		                    </div>
		                </div>
	                	<div class="col-md-12 form-group <?php if (!(form_error('descricao')=='')) echo 'has-error has-feedback'; ?>">
	                        <?php echo form_textarea('descricao', set_value('descricao')?set_value('descricao'):$descricao, 'type="text", class="form-control" id="descricao" placeholder="Descrição" readonly'); ?> 
	                    </div>
	                </div>
               </div>
            </div>
            <div class="panel-group panel-default panel panel-info">
                <div class="panel-heading">
                    Resposta da Manifestaçao
                </div>
                <div class="panel-body">
                	<div class="col-md-12 form-group <?php if (!(form_error('resposta')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_textarea('resposta', set_value('resposta')?set_value('resposta'):$resposta, 'type="text", class="form-control" id="resposta" placeholder="Resposta" required'); ?> 
                        <?php if (!(form_error('resposta')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-9">
                <div class="form-save-buttons">
                    <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Enviar</button>&ensp;
                
                	<?php echo form_close(); ?>
                	<button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
            	</div>
            </div>
            
        </div>
	</div>
</div>