<?php $this->template->menu('inicio') ?>
<div class="container-fluid">
    <div class="row-content" >
        <div class="col-sm-3" >
            <aside role="complementary">
                <ul class="nav nav-pills nav-stacked navbar-default">
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem');?>"><i class="fa fa-envelope" aria-hidden="true"></i> Caixa de Entrada</a></li>
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem/novo'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i> Nova Mensagem</a></li>
                  <li role="presentation" class="active"><a href="#"><i class="fa fa-share" aria-hidden="true"></i> Enviados</a></li>
                  <?php if ($view>1){ ?>
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem/grupo'); ?>"><i class="fa fa-users" aria-hidden="true"></i> Grupos</a></li>
                  <?php } ?>
                </ul>
            </aside>
        </div>
        <div class="col-sm-9">
            <?php if (count($mensagens)>0){ ?>
            <table class="table">
              <thead>
                  <th class="col-md-3">Remetente</th>
                  <th class="col-md-7">Assunto</th>
              </thead>
              <tbody>
                <?php foreach ($mensagens as $mensagem): ?>
                    
                <tr class='clickable-row' style='cursor:pointer' data-href='<?php echo base_url('index.php/mensagem/ver/'.$mensagem['idmensagem']); ?>'>
                    <td>
                        <?php  if($mensagem['recado_visualizado_idrecado']<$mensagem['idrecado']){?><b><?php } ?>
                        <?php echo $mensagem['nome_usuario']; ?>
                        <?php  if($mensagem['recado_visualizado_idrecado']<$mensagem['idrecado']){?></b><?php } ?>
                    </td>
                    <td>
                        <?php  if($mensagem['recado_visualizado_idrecado']<$mensagem['idrecado']){?><b><?php } ?>
                        <?php echo $mensagem['assunto_mensagem']; ?>
                        <?php  if($mensagem['recado_visualizado_idrecado']<$mensagem['idrecado']){?></b><?php } ?>
                    </td>
                </tr>
                
                <?php endforeach ?>
              </tbody>
            </table>  
            <?php } ?> 
          </div>
        </div>
    </div>
    
</div>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<script type="text/javascript">
$(document).ready(function(){
    $('.clickable-row').click(function(){
        window.location = $(this).data('href');
    });
});

</script>