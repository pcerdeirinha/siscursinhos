<?php $this->template->menu($view) ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3><b>Lista de Eventos</b></h3>
            
            <table id="eventos" class="table table-hover">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Data</th> 
                        <th>Confirmações</th>             
                        <th>Opções</th>
                    </tr>   
                </thead>
                <?php foreach ($eventos as $key => $evento) { ?>
                <tr class="animated fadeInDown">
                    <td><?php echo $evento['nome_evento'];?></td>
                    <td><?php echo DateTime::createFromFormat('Y-m-d', $evento['data_inicio'])->format('d/m/Y'); ?></td>
                    <td><?php echo $evento['qtd']; ?></td>               
                    <td>
                        <a href="<?php echo base_url('index.php/evento/edita'); echo '/'.$key ?>" data-toggle="tooltip" data-placement="top" title="Editar Evento"><button type="button" class="btn btn-default"><i class="fa fa-pencil-square-o"></i></button></a>
                        &ensp;<a href="#" data-toggle="tooltip" data-placement="top" title="Remover Evento"><button type="button" class="btn btn-danger" onclick="remover_evento(<?php echo $key; ?>)"><i class="fa fa-trash"></i></button></a>
                        &ensp;<a href="#" data-toggle="tooltip" data-placement="top" title="Convidar Turmas"><button type="button" class="btn btn-primary" onclick="convidar_turmas(<?php echo $key; ?>)"><i class="fa fa-envelope" aria-hidden="true"></i></button></a>
                        &ensp;<a href="#" data-toggle="tooltip" data-placement="top" title="Verificar Frequência"><button type="button" class="btn btn-primary" onclick="frequencia(<?php echo $key; ?>)"><i class="fa fa-check-square-o" aria-hidden="true"></i></button></a>
                        <!--&ensp;<a href="<?php echo base_url('index.php/evento/painel'); echo '/'.$key ?>" data-toggle="tooltip" data-placement="top" title="Painel do Evento"><button type="button" class="btn btn-primary"><i class="fa fa-server" aria-hidden="true"></i></button></a>-->
                        &ensp;<a target="_blank" href="<?php echo base_url('index.php/evento/pagina'); echo '/'.$key ?>" data-toggle="tooltip" data-placement="top" title="Ir para Página do Evento"><button type="button" class="btn btn-primary"><i class="fa fa-external-link" aria-hidden="true"></i></button></a>
                    </td>
                </tr>
                <?php } ?>
            </table>                
        </div>
        <div class="modal fade" id="remove_modal" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title text-danger" id="myModalLabel" align="CENTER">Atenção <i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i></h3>
                    </div>
                    <div class="modal-body">
                        <p align="CENTER">Você está prestes a remover um Evento. </p> 
                        <p>Os seguintes itens relacionados também serão afetados:</p>
                        <ul>
                            <li>Todas as confirmações deste evento serão removidas.</li>
                            <li>Todas as informações deste evento serão removidas.</li>
                        </ul>
                        <p class="text-danger">As alterações acima citadas são irreversíveis.</p>
                        <h4 align="CENTER">Está certo disto?</h4>                               
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <button type="button" id="remove_evento_btn" class="btn btn-danger"  title="Sim, remover o evento." onclick="location.href='<?php echo base_url(); ?>index.php/evento/remove/'">Sim, remover o evento</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="turma_modal" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="nome_grade" align="CENTER">Convidar Turmas</h3>
                    </div>
                    <div class="modal-body">
                            <div class="row">       
                                <div class="col-md-8 col-md-offset-2">
                                    <?php echo form_hidden('id_evento_convidar'); ?>
                                    <div class="form-group pre-scrollable">
                                    <?php foreach ($turmas as $key => $turma): ?>
                                        <div class="checkbox">
                                            <label><input type="checkbox" id="turmas" name="turmas[]" value="<?php echo $key ?>"><?php echo $turma ?></label>
                                        </div>
                                    <?php endforeach ?> 
                                    </div>         
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="enviar_convites();">Convidar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="frequencia_modal" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="nome_grade" align="CENTER">Registrar Frequência</h3>
                    </div>
                    <div class="modal-body">
                            <div class="row">       
                                <div class="col-md-8 col-md-offset-2">
                                    <?php echo form_open('evento/frequencia'); ?>
                                    <?php echo form_hidden('id_evento_frequencia'); ?>
                                    <div class="pre-scrollable">
                                        <div id="tabela_alunos">
                                            
                                        </div>
                                    </div>         
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Registrar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>                          
        <div class="col-md-1 col-md-offset-9">
            <button class="btn btn-default" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>          
    </div>
</div>

<script type="text/javascript">

function remover_evento(id){
    $('#remove_evento_btn').attr('onclick',"location.href='<?php echo base_url(); ?>index.php/evento/remove/"+id+"'");
    $('#remove_modal').modal();    
}
    
function convidar_turmas (id) {
    $('[name=id_evento_convidar]').val(id);
    $('#turma_modal').modal();
}

function enviar_convites () {
    var id_evento = $('[name=id_evento_convidar]').val();
    var turmas= $('#turmas:checked').serializeArray();
    console.log(turmas);
    jQuery.ajax({
       type: "POST",
       url: "<?php echo base_url(); ?>" + "index.php/evento/convidar_turmas",
       dataType: 'json',
       data: {turmas,evento:id_evento},
       success: function(res) {
            if (res=="ok")
                mensagem('success','Turmas Convidadas com sucesso');
          }
       }); 
}

function frequencia (idevento) { 
    $('[name=id_evento_frequencia]').val(idevento);
     $("#tabela_alunos").html('');
     jQuery.ajax({
           type: "POST",
           url: "<?php echo base_url(); ?>" + "index.php/evento/busca_alunos",
           dataType: 'json',
           data: {idevento:idevento},
           success: function(res) {
                console.log(res);
                rows = '';
                rows = $('<table class="table table-hover" id="alunos">');
                rows.append('<thead><tr><th>Nome</th>\
                                        <th>Presente - Todos como <div class="btn-group" data-toggle="buttons">\
                                            <label class="btn btn-secondary active">\
                                                <input type="radio" name="todos" id="Sim" autocomplete="off" value ="Sim" checked> Sim\
                                            </label>\
                                            <label class="btn btn-secondary">\
                                                <input type="radio" name="todos" id="Nao" autocomplete="off" value = "Nao"> Não\
                                            </label>\
                                            </div>\
                                            </th></tr></thead><tbody>');
                if(res.err == "ok"){
                    for(var i in res.alunos){
                        var newRow = $('<tr class="animated fadeInDown">');
                        var cols = "";
                        cols +='<td style = "vertical-align:middle">'+res.alunos[i].nome_aluno+'</td>';
                        if (res.alunos[i].falta_aluno==0){                           
                        cols +='<td>\
                                   <div class="btn-group" data-toggle="buttons">\
                                        <label id="lbl_Sim" class="btn btn-secondary active">\
                                            <input type="radio" name="Presenca'+res.alunos[i].idusuario+'" id="Sim" autocomplete="off" value ="Sim" checked> Sim\
                                        </label>\
                                        <label id="lbl_Nao" class="btn btn-secondary">\
                                            <input type="radio" name="Presenca'+res.alunos[i].idusuario+'" id="Nao" autocomplete="off" value = "Nao"> Não\
                                        </label>\
                                   </div>\
                               </td>';
                        }
                        else {
                            cols +='<td>\
                                   <div class="btn-group" data-toggle="buttons">\
                                        <label id="lbl_Sim" class="btn btn-secondary">\
                                            <input type="radio" name="Presenca'+res.alunos[i].idusuario+'" id="Sim" autocomplete="off" value ="Sim"> Sim\
                                        </label>\
                                        <label id="lbl_Nao" class="btn btn-secondary active">\
                                            <input type="radio" name="Presenca'+res.alunos[i].idusuario+'" id="Nao" autocomplete="off" value = "Nao" checked> Não\
                                        </label>\
                                   </div>\
                               </td>';
                        }     
                        newRow.append(cols);                            
                        rows.append(newRow);                                                
                    }
                    cols+='</tbody></table>';
                    $("#tabela_alunos").html(rows);
                    $("#frequencia_modal").modal({show: true});
    
               }else{
                    $('[name=idAula]').val('');
                    mensagem('error',res.err);
                    $("#tabela_alunos").html('');
                    $("#ModalFrequencia").modal({show: false});
               }
           }
       }); 
    }
    $(document).on('change', 'input:radio[name="todos"]', function (event) {
        if ($('input:radio[name="todos"]:checked').val()=='Nao'){
            $('input#Sim').removeAttr("checked");
            $('input#Nao').attr("checked",true); 
            $('label#lbl_Nao').attr("class","btn btn-secondary active");
            $('label#lbl_Sim').attr("class","btn btn-secondary");
        }
        else{
            $('input#Nao').removeAttr("checked");
            $('input#Sim').attr("checked",true); 
            $('label#lbl_Sim').attr("class","btn btn-secondary active");
            $('label#lbl_Nao').attr("class","btn btn-secondary");
        }
        
    });

$(document).ready(function () {
    tabela('eventos');
    $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/evento/opcoes";  
    });
    
}); 
</script>

<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<?php if(isset($msg)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $msg;?>");</script>
<?php }?>

