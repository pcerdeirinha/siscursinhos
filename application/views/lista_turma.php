<?php $this->template->menu($view) ?>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			
			<h3><b>Lista de Turmas</b></h3>
			<div class="table-responsive">
			<table id='turmas' class="table table-hover">
				<thead>
					<tr>
						<th>Nome da Turma</th>
						<th>Curso</th>
						<th>Período</th>
						<th>Data Início</th>						
						<th>Opções</th>
					</tr>
				</thead>				
				<?php foreach ($turmas as $turma) {
				$modal = str_replace(' ', '', $turma['nome_turma']);?>
				<tr class="animated fadeInDown">
					<td><?php echo $turma['nome_turma'];?></td>
					<td><?php echo $cursos_drop[$turma['curso_idcurso']];?></td>
					<td><?php echo $turma['periodo_turma'];?></td>
					<td><?php $date = DateTime::createFromFormat('Y-m-d', $turma['data_inicio_turma']);
                              $data_inicio_turma =  $date->format('d/m/Y'); 
                              echo $data_inicio_turma;
                        ?>                              	
                    </td>					
					<td>
						<a href="edita/<?php echo $turma['idturma'] ?>" data-toggle="tooltip" data-placement="top" title="Editar"><button type="button" class="btn btn-default"><i class="fa fa-pencil-square-o"></i></button></a>
						&ensp;<a href="#" data-toggle="tooltip" data-placement="top" title="Remover"><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#<?php echo $modal; ?>"><i class="fa fa-trash"></i></button></a>						
					    &ensp;<button type="button" class="btn btn-primary" title="Alunos Matriculados" onclick="carregarAlunos(<?php echo $turma['idturma'] ?>,'<?php echo $turma['nome_turma']?>')"><i class="fa fa-info-circle"></i></button>                   
					</td>
				</tr>
				<div class="modal fade" id="<?php echo $modal;?>" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h3 class="modal-title text-danger" id="myModalLabel" align="CENTER">Atenção <i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i></h3>
							</div>
							<div class="modal-body">
								<p align="CENTER">Você está prestes a remover a Turma: </p> 
								<h4 align="CENTER"><?php echo $turma['nome_turma'];?></h4>
								<p>Os seguintes itens relacionados também serão afetados:</p>
								<ul>
									<li>As matrículas de alunos nesta turma serão canceladas.</li>
									<li>As ofertas de disciplinas de professores para esta turma serão canceladas.</li>
								</ul>
								<p class="text-danger">As alterações acima citadas são irreversíveis.</p>
								<h4 align="CENTER">Está certo disto?</h4>								
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
								<button type="button" class="btn btn-danger" onclick="location.href='<?php echo base_url(); ?>index.php/turma/remove/<?php echo $turma['idturma'] ?>'">Sim, remover a turma</button>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
			</table>	
			</div>
			<div class="modal fade" id="alunos_turma" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title" id="turma_alunos" align="CENTER">Alunos Matriculados</h3>
                        </div>
                        <div class="modal-body">
                            <div id="tabela_alunos">
                            </div>                             
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        </div>
                    </div>
                </div>
            </div>	
		</div>
		<div class="col-md-1 col-md-offset-9">
			<button class="btn btn-default" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
		</div>		
	</div>
</div>
<script type="text/javascript">
$(document).ready(function () {
	tabela('turmas',4,3);
    $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/turma";  
    });
}); 

function carregarAlunos(idTurma,nomeTurma) {
    $("#turma_alunos").html('Alunos Matriculados: '+nomeTurma);
    jQuery.ajax({
               type: "POST",
               url: "<?php echo base_url(); ?>" + "index.php/turma/getAlunosTurma",
               dataType: 'json',
               data: {idTurma:idTurma},
               success: function(res) {
                   if (res.alunos.nome_aluno.length>0){
                    var rows = $('<table id="alunos" align="center" class="table table-hover table-bordered">');
                    rows.append('<thead><tr><th style="text-align:center">Nome do Aluno</th><th style="text-align:center">CPF</th></tr></thead>');
                    for (var i in res.alunos.nome_aluno){
                        var cols = '';
                        cols += '<tr><td align="center">'+res.alunos.nome_aluno[i]+'</td>';
                        cols += '<td align="center">'+res.alunos.cpf_aluno[i]+'</td></tr>';
                        rows.append(cols);
                    }
                    rows.append('</table>');
                    }
                    else{
                        var rows = '<p align="CENTER">Não há alunos Matriculados</p>';
                    }
                    $("#tabela_alunos").html(rows);
                    $('#alunos_turma').modal('show')
                    
               }
       });
  
}
</script>

<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>


