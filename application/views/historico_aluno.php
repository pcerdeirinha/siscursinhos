<?php $this->template->menu($view); ?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<div class="container">
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		
	<?php 
	
	$i = 0;
	if (isset($simulados[$i]['disciplina_iddisciplina'])){
	?>
	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="headingDisciplina">
	      <h4 class="panel-title">
	        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDisciplina" aria-expanded="true" aria-controls="collapseOne">
	          Avaliações por Disciplina
	        </a>
	      </h4>
	    </div>
	    <div id="collapseDisciplina" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
	      <div class="panel-body">
	      	<?php 
	      		$disciplina = $simulados[$i]['disciplina_iddisciplina'];
				$nome_disciplina = $simulados[$i]['nome_adicional'];
	      		$avaliacao = '';
				$peso = '';
				$nota = '';
				$soma = 0;
				$div = 0;
	      		for ($i;$i<count($simulados);$i++){
	      			if (!isset($simulados[$i]['disciplina_iddisciplina']))
						break;
					else{
						if ($simulados[$i]['disciplina_iddisciplina']==$disciplina){
							$avaliacao.='<th>'.$simulados[$i]['descricao_simulado'].'</th>';
							$peso .='<td>'.number_format($simulados[$i]['peso_simulado'], 2, ',', '').'</td>';
							$prop = $simulados[$i]['valor_nota'] * 100 / $simulados[$i]['nota_maxima_simulado'];
							$nota .='<td>'.number_format($prop, 2, ',', '').'</td>';
							$soma += $simulados[$i]['peso_simulado']*$prop;
							$div += $simulados[$i]['peso_simulado'];
						}
						else {
							echo '<div class="table-responsive"><table class="table table-bordered"><thead><th>'.$nome_disciplina.'</th>'.$avaliacao.'<th>Média</th></thead>';
							echo '<tbody><tr><td>Peso</td>'.$peso.'<td></td></tr>';
							echo '<tr><td>Nota</td>'.$nota.'<th>'.number_format($soma/$div, 2, ',', '').'</th></tr></tbody></table></div>';
				      		$disciplina = $simulados[$i]['disciplina_iddisciplina'];
							$nome_disciplina = $simulados[$i]['nome_adicional'];
							$avaliacao='<th>'.$simulados[$i]['descricao_simulado'].'</th>';
							$peso ='<td>'.number_format($simulados[$i]['peso_simulado'], 2, ',', '').'</td>';
							$prop = $simulados[$i]['valor_nota'] * 100 / $simulados[$i]['nota_maxima_simulado'];
							$nota ='<td>'.number_format($prop, 2, ',', '').'</td>';
							$soma = $simulados[$i]['peso_simulado']*$prop;
							$div = $simulados[$i]['peso_simulado'];
						}
					}
				}
				echo '<div class="table-responsive"><table class="table table-bordered"><thead><th>'.$nome_disciplina.'</th>'.$avaliacao.'<th>Média</th></thead>';
				echo '<tbody><tr><th>Peso</th>'.$peso.'<td></td></tr>';
				echo '<tr><th>Nota</th>'.$nota.'<th>'.number_format($soma/$div, 2, ',', '').'</th></tr></tbody></table></div>';
	      	 ?>
	      </div>
	    </div>
	  </div>
	<?php
	}
	if (isset($simulados[$i]['area_idarea'])){
	?>	
		
	
	  
	  
	  
	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="headingArea">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseArea" aria-expanded="false" aria-controls="collapseTwo">
	          Avaliações por Área
	        </a>
	      </h4>
	    </div>
	    <div id="collapseArea" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
	      <div class="panel-body">
	      	<?php 
				$area = $simulados[$i]['area_idarea'];
				$nome_disciplina = $simulados[$i]['nome_adicional'];
	      		$avaliacao = '';
				$peso = '';
				$nota = '';
				$soma = 0;
				$div = 0;
	      		for ($i;$i<count($simulados);$i++){
	      			if (!isset($simulados[$i]['area_idarea']))
						break;
					else{
						if ($simulados[$i]['area_idarea']==$area){
							$avaliacao.='<th>'.$simulados[$i]['descricao_simulado'].'</th>';
							$peso .='<td>'.number_format($simulados[$i]['peso_simulado'], 2, ',', '').'</td>';
							$prop = $simulados[$i]['valor_nota'] * 100 / $simulados[$i]['nota_maxima_simulado'];
							$nota .='<td>'.number_format($prop, 2, ',', '').'</td>';
							$soma += $simulados[$i]['peso_simulado']*$prop;
							$div += $simulados[$i]['peso_simulado'];
						}
						else {
							echo '<div class="table-responsive"><table class="table table-bordered"><thead><th>'.$nome_disciplina.'</th>'.$avaliacao.'<th>Média</th></thead>';
							echo '<tbody><tr><td>Peso</td>'.$peso.'<td></td></tr>';
							echo '<tr><td>Nota</td>'.$nota.'<th>'.number_format($soma/$div, 2, ',', '').'</th></tr></tbody></table></div>';
							$area = $simulados[$i]['area_idarea'];
							$nome_disciplina = $simulados[$i]['nome_adicional'];
							$avaliacao='<th>'.$simulados[$i]['descricao_simulado'].'</th>';
							$peso ='<td>'.number_format($simulados[$i]['peso_simulado'], 2, ',', '').'</td>';
							$prop = $simulados[$i]['valor_nota'] * 100 / $simulados[$i]['nota_maxima_simulado'];
							$nota ='<td>'.number_format($prop, 2, ',', '').'</td>';
							$soma = $simulados[$i]['peso_simulado']*$prop;
							$div = $simulados[$i]['peso_simulado'];
						}
					}
				}
				echo '<div class="table-responsive"><table class="table table-bordered"><thead><th>'.$nome_disciplina.'</th>'.$avaliacao.'<th>Média</th></thead>';
				echo '<tbody><tr><th>Peso</th>'.$peso.'<td></td></tr>';
				echo '<tr><th>Nota</th>'.$nota.'<th>'.number_format($soma/$div, 2, ',', '').'</th></tr></tbody></table></div>';
	      	 ?>
	      </div>
	    </div>
	  </div>
	  
  	<?php
  	}
	if (isset($simulados[$i]['grande_area_idgrande_area'])){
 	?>
	  
	  
	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="headingGrandeArea">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseGrandeArea" aria-expanded="false" aria-controls="collapseThree">
	          Avaliações por Grande Área
	        </a>
	      </h4>
	    </div>
	    <div id="collapseGrandeArea" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
	      <div class="panel-body">
	      	<?php 
				$grande_area = $simulados[$i]['grande_area_idgrande_area'];
				$nome_disciplina = $simulados[$i]['nome_adicional'];
	      		$avaliacao = '';
				$peso = '';
				$nota = '';
				$soma = 0;
				$div = 0;
	      		for ($i;$i<count($simulados);$i++){
	      			if (!isset($simulados[$i]['grande_area_idgrande_area']))
						break;
					else{
						if ($simulados[$i]['grande_area_idgrande_area']==$grande_area){
							$avaliacao.='<th>'.$simulados[$i]['descricao_simulado'].'</th>';
							$peso .='<td>'.number_format($simulados[$i]['peso_simulado'], 2, ',', '').'</td>';
							$prop = $simulados[$i]['valor_nota'] * 100 / $simulados[$i]['nota_maxima_simulado'];
							$nota .='<td>'.number_format($prop, 2, ',', '').'</td>';
							$soma += $simulados[$i]['peso_simulado']*$prop;
							$div += $simulados[$i]['peso_simulado'];
						}
						else {
							echo '<div class="table-responsive"><table class="table table-bordered"><thead><th>'.$nome_disciplina.'</th>'.$avaliacao.'<th>Média</th></thead>';
							echo '<tbody><tr><td>Peso</td>'.$peso.'<td></td></tr>';
							echo '<tr><td>Nota</td>'.$nota.'<th>'.number_format($soma/$div, 2, ',', '').'</th></tr></tbody></table></div>';
							$grande_area = $simulados[$i]['grande_area_idgrande_area'];
							$nome_disciplina = $simulados[$i]['nome_adicional'];
							$avaliacao='<th>'.$simulados[$i]['descricao_simulado'].'</th>';
							$peso ='<td>'.number_format($simulados[$i]['peso_simulado'], 2, ',', '').'</td>';
							$prop = $simulados[$i]['valor_nota'] * 100 / $simulados[$i]['nota_maxima_simulado'];
							$nota ='<td>'.number_format($prop, 2, ',', '').'</td>';
							$soma = $simulados[$i]['peso_simulado']*$prop;
							$div = $simulados[$i]['peso_simulado'];
						}
					}
				}
				echo '<div class="table-responsive"><table class="table table-bordered"><thead><th>'.$nome_disciplina.'</th>'.$avaliacao.'<th>Média</th></thead>';
				echo '<tbody><tr><th>Peso</th>'.$peso.'<td></td></tr>';
				echo '<tr><th>Nota</th>'.$nota.'<th>'.number_format($soma/$div, 2, ',', '').'</th></tr></tbody></table></div>';
	      	 ?>
	      </div>
	    </div>
	  </div>
	  
	  
	  <?php 
	  }
	  if (isset($simulados[$i])){
	  ?>
	  
	  	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="headingGeral">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseGeral" aria-expanded="false" aria-controls="collapseThree">
	          Avaliações Gerais
	        </a>
	      </h4>
	    </div>
	    <div id="collapseGeral" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
	      <div class="panel-body">
	      	<?php 
				$nome_disciplina = 'Simulados Gerais';
	      		$avaliacao = '';
				$peso = '';
				$nota = '';
				$soma = 0;
				$div = 0;
	      		for ($i;$i<count($simulados);$i++){
					$avaliacao.='<th>'.$simulados[$i]['descricao_simulado'].'</th>';
					$peso .='<td>'.number_format($simulados[$i]['peso_simulado'], 2, ',', '').'</td>';
					$prop = $simulados[$i]['valor_nota'] * 100 / $simulados[$i]['nota_maxima_simulado'];
					$nota .='<td>'.number_format($prop, 2, ',', '').'</td>';
					$soma += $simulados[$i]['peso_simulado']*$prop;
					$div += $simulados[$i]['peso_simulado'];
				}
				echo '<div class="table-responsive"><table class="table table-bordered"><thead><th>'.$nome_disciplina.'</th>'.$avaliacao.'<th>Média</th></thead>';
				echo '<tbody><tr><th>Peso</th>'.$peso.'<td></td></tr>';
				echo '<tr><th>Nota</th>'.$nota.'<th>'.number_format($soma/$div, 2, ',', '').'</th></tr></tbody></table></div>';
	      	 ?>
	      	
	      </div>
	    </div>
	  </div>
	  <br><br><br>
	<p align="center">
		<button class="btn btn-default" id="voltar"><i class="fa fa-arrow-circle-left"></i> Voltar</button>
	</p>
	  <?php 
		}
	  ?>
	  
	</div>
</div>
<script>
		$(document).ready(function () {
		$("#voltar").click(function(event){
			window.location.href = "<?php echo base_url(); ?>"+"index.php/aluno"; 
		});
	}); 

    
</script>