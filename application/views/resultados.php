<?php $this->template->menu($view); ?>
<div class='container'>
    <div class="row">       
        <div class="col-md-12">
        <center>        
        <h3><b>Resultados</b></h3>                 
        </center>
        </div>      
        <div class="col-md-5">
            <div class="input-group">
                <span class="input-group-addon" id="cpf-addon">Parâmetro Analisado:</span>              
                <?php echo form_dropdown('id_parametro',$resultados,$result_sel,"class='form-control'"); ?>
            </div>
        </div>
        <div class="col-md-5 col-md-offset-1">
            <div class="input-group">
                <span class="input-group-addon" id="cpf-addon">Filtro:</span>              
                <?php echo form_dropdown('id_filtro',$filtros,null,"class='form-control'"); ?>
            </div>
        </div>
        <div class="col-md-5">
            <div class="input-group">
                <span class="input-group-addon" id="cpf-addon">Agrupamento:</span>              
                <?php echo form_dropdown('id_agrupamento',$agrupamentos,null,"class='form-control'"); ?>
            </div>
        </div>
        <div class="col-md-5 col-md-offset-1">
            <div class="input-group">
                <span class="input-group-addon" id="cpf-addon">Agrupamento Socioeconômico:</span>              
                <?php echo form_dropdown('id_socio',$socios,null,"class='form-control'"); ?>
            </div>
        </div>              
    </div>
    <div class="row">
        <br>
        <div class="col-md-4 col-md-offset-4">
            <div class="form-group"> 
                <button type="button" class="btn btn-primary btn-block" id="gerar_resultado"><i class="fa fa-search"></i> Gerar resultado</button>
                <button class="btn btn-default btn-block" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
            </div>          
        </div>                  
    </div>
    <div id="chart_div"></div>
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
    // Load the Visualization API and the corechart package.
    google.charts.load("current", {packages:['bar']}); 
    
    $( "[name=id_parametro]" ).change(function() {
        alert($( this ).val());
    });
    
                     
    $('#gerar_resultado').click(function() {
        jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/resultado/obter_resultado",
                  dataType: 'json',
                  data: {id_resultado: $( "[name=id_parametro]" ).val(), id_filtro: $( "[name=id_filtro]" ).val(), id_agrupamento: $( "[name=id_agrupamento]" ).val(), id_socio: $( "[name=id_socio]" ).val() },
                  success: function(res) {
                          console.log(res);
                              // Create the data table.
                            var k = 0;
                            
                            var data = new google.visualization.DataTable();
                            data.addColumn({type: 'string', label: 'Descrição'});
                            if ($( "[name=id_socio]" ).val()!='' && ($( "[name=id_filtro]" ).val()=='' || $( "[name=id_agrupamento]" ).val()=='')){
                                var indices_socio = [];
                                var nomes_socio = [];
                                for (var i in res){
                                    if (!indices_socio.includes(res[i]['id_socio_'+$( "[name=id_socio]" ).val()])){
                                        indices_socio.push(res[i]['id_socio_'+$( "[name=id_socio]" ).val()]);
                                        if (res[i]['msg_socio_'+$( "[name=id_socio]" ).val()]==null)
                                            nomes_socio.push('Não declarado');
                                        else{
                                            nomes_socio.push(res[i]['msg_socio_'+$( "[name=id_socio]" ).val()]);
                                        }
                                    }
                                } 
                                for (var i in indices_socio){
                                    data.addColumn({type:'number',label: nomes_socio[i]});
                                }                              
                            }
                            else if ($( "[name=id_socio]" ).val()!=''){
                                var indices_socio = [];
                                var nomes_socio = [];
                                for (var i in res){
                                    if (!indices_socio.includes(res[i]['id_socio_'+$( "[name=id_socio]" ).val()])){
                                        indices_socio.push(res[i]['id_socio_'+$( "[name=id_socio]" ).val()]);
                                        if (res[i]['msg_socio_'+$( "[name=id_socio]" ).val()]==null)
                                            nomes_socio.push('Não declarado');
                                        else{
                                            nomes_socio.push(res[i]['msg_socio_'+$( "[name=id_socio]" ).val()]);
                                        }
                                    }
                                }
                                var indices_filtro = [];
                                var nomes_filtro = [];
                                for(var i in res){
                                    if (!indices_filtro.includes(res[i].id_filtro)){
                                        indices_filtro.push(res[i].id_filtro);
                                        nomes_filtro.push(res[i].msg_filtro);
                                    }
                                }
                                    
                                for (var i in indices_filtro){ 
                                    for (var j in indices_socio){
                                        data.addColumn({type:'number',label: nomes_socio[j]+' - '+nomes_filtro[i]});
                                    }
                                }
                            }
                            else if ($( "[name=id_filtro]" ).val()=='' || $( "[name=id_agrupamento]" ).val()==''){
                                data.addColumn({type: 'number', label: 'Quantidade'});
                                data.addColumn({type: 'string', role: 'style'});
                            }else{
                                var indices_filtro = [];
                                var nomes_filtro = [];
                                for(var i in res){
                                    if (!indices_filtro.includes(res[i].id_filtro)){
                                        indices_filtro.push(res[i].id_filtro);
                                        nomes_filtro.push(res[i].msg_filtro);
                                    }
                                }
                                for (var i in indices_filtro){
                                    data.addColumn({type: 'number', label:nomes_filtro[i]});
                                }
                            }
                        
                            var colors = ['red','green','yelow','blue','purple','black','gray'];
                                
                            //var data = google.visualization.arrayToDataTable([['Descrição','Quantidade',{role:'style'}]]);
                            
                            if ($( "[name=id_socio]" ).val()!='' && ($( "[name=id_filtro]" ).val()=='' || $( "[name=id_agrupamento]" ).val()=='')){
                                if ($( "[name=id_filtro]" ).val()=='' && $( "[name=id_agrupamento]" ).val()==''){
                                    var linha = [res[0].msg];
                                    
                                    for(var i in indices_socio){
                                        var c = 0;
                                        for(var j in res){
                                           if (res[j]['id_socio_'+$( "[name=id_socio]" ).val()] == indices_socio[i]){
                                             linha.push(parseInt(res[j].qtd));                                       
                                             c++;
                                             break;
                                           }
                                        }
                                        if (c==0)linha.push(null);
                                    }
                                    data.addRow(linha);
                                }
                                else if ($( "[name=id_agrupamento]" ).val()!=''){
                                    var indices_agr = [];
                                    var nomes_agr = [];
                                    for (var i in res){
                                        if (!indices_agr.includes(res[i].id_agr)){
                                            indices_agr.push(res[i].id_agr);
                                            nomes_agr.push(res[i].msg_agr);
                                        }
                                    }
                                    for (var i in indices_agr){
                                        var linha = [nomes_agr[i]];
                                        for (var j in indices_socio){
                                            var c=0;
                                            for (var k in res){
                                                if ((res[k].id_agr == indices_agr[i]) && (res[k]['id_socio_'+$( "[name=id_socio]" ).val()] == indices_socio[j])){
                                                    linha.push(parseInt(res[k].qtd));
                                                    c++; break;
                                                }
                                            }
                                            if (c==0) linha.push(null);
                                        }
                                        data.addRow(linha);
                                    }    
                                }
                                else if ($( "[name=id_filtro]" ).val()!=''){
                                    var indices_filtro = [];
                                    var nomes_filtro = [];
                                    for (var i in res){
                                        if (!indices_filtro.includes(res[i].id_filtro)){
                                            indices_filtro.push(res[i].id_filtro);
                                            nomes_filtro.push(res[i].msg_filtro);
                                        }
                                    }
                                    for (var i in indices_filtro){
                                        var linha = [nomes_filtro[i]];
                                        for (var j in indices_socio){
                                            var c=0;
                                            for (var k in res){
                                                if ((res[k].id_filtro == indices_filtro[i]) && (res[k]['id_socio_'+$( "[name=id_socio]" ).val()] == indices_socio[j])){
                                                    linha.push(parseInt(res[k].qtd));
                                                    c++; break;
                                                }
                                            }
                                            if (c==0) linha.push(null);
                                        }
                                        data.addRow(linha);
                                    }    
                                }
                            }
                            else if ($( "[name=id_socio]" ).val()!=''){
          
                                var indices_agr = [];
                                var nomes_agr = [];
                                for (var i in res){
                                    if (!indices_agr.includes(res[i].id_agr)){
                                        indices_agr.push(res[i].id_agr);
                                        nomes_agr.push(res[i].msg_agr);
                                    }
                                }
                                for (var i in indices_agr){
                                    var linha = [nomes_agr[i]];
                                    for (var j in indices_filtro){
                                        for (var k in indices_socio){
                                            var c=0;
                                            for (var l in res){
                                                if ((res[l].id_agr==indices_agr[i])&&(res[l].id_filtro == indices_filtro[j]) && (res[l]['id_socio_'+$( "[name=id_socio]" ).val()] == indices_socio[k])){
                                                    linha.push(parseInt(res[l].qtd));
                                                    c++; break;
                                                }
                                            }
                                            if (c==0) linha.push(0);
                                        }
                                    }
                                    console.log(linha);
                                    data.addRow(linha);
                                }
                                var k=0;
                                var deletedColumnsIndex = [];
                                for (var i=1; i<data.getNumberOfColumns();i++){
                                    var c=0;
                                    for (var j=0; j<data.getNumberOfRows();j++){
                                        if (data.getValue(j, i)>0){
                                             c++; break;
                                        }
                                    }
                                    if (c==0){
                                        deletedColumnsIndex.push(k);
                                        data.removeColumn(i);
                                        i--;
                                    }
                                    k++;
                                }                                
                                
                            }
                            else if ($( "[name=id_filtro]" ).val()=='' || $( "[name=id_agrupamento]" ).val()==''){
                                for(var i in res){
                                	if ($( "[name=id_filtro]" ).val()!='')
                                    	data.addRow([res[i].msg_filtro,parseInt(res[i].qtd),colors[k++%7]]);
                                   	else
                                    	data.addRow([res[i].msg_agr,parseInt(res[i].qtd),colors[k++%7]]);
                                }
                            }
                            else{
                                var indices_agr = [];
                                var nomes_agr = [];
                                for (var i in res){
                                    if (!indices_agr.includes(res[i].id_agr)){
                                        indices_agr.push(res[i].id_agr);
                                        nomes_agr.push(res[i].msg_agr);
                                    }
                                }
                                
                                for (var i in indices_agr){
                                    var linha = [nomes_agr[i]];
                                    for (var j in indices_filtro){
                                        var contem = 0;
                                        for (var k in res){
                                            if (res[k].id_filtro == indices_filtro[j] && res[k].id_agr == indices_agr[i]){
                                                linha.push(parseInt(res[k].qtd));
                                                contem++; break;
                                            }
                                        }
                                        if (contem == 0){
                                            linha.push(null);
                                        } 
                                    }
                                    console.log(linha);
                                    data.addRow(linha);
                                }
                            }
                            
                            
                            console.log(data);
                           
                            // Set chart options
                            var options = {'title':res[0].titulo,
                                           'width':300 + k*100,
                                           'height':300,
                                           vAxis: {format: 'decimal'},
                                           chartArea: { width: "50%", height: "70%" },
                                           bars: 'vertical',
                                           series: {}
                                          };
                          if ($( "[name=id_socio]" ).val()!='') options.isStacked = true;
                          
                          if ($( "[name=id_socio]" ).val()!='' &&  $( "[name=id_filtro]" ).val()!='' && $( "[name=id_agrupamento]" ).val()!=''){
                              var n =0 ;
                              var colunaAtual = 0;
                              options.chartArea = {width: "90%", height: "70%"};
                              var maior = 0;
                              var num = 0;
                              var soma_filtro = [];
                              for (var i in indices_agr){
                                  for (var j in indices_filtro){
                                      var soma = 0;
                                      for (var k in res){
                                          if (res[k].id_filtro == indices_filtro[j] && res[k].id_agr == indices_agr[i]){
                                              soma+=parseInt(res[k].qtd);
                                          }
                                      }
                                      soma_filtro.push(soma);
                                      num++;
                                  }
                              }
                              options.width = 300 + num*100;
                              
                              console.log(soma_filtro);
                              for (var i in soma_filtro){
                                  if (soma_filtro[i]>maior) maior = soma_filtro[i];
                              }
                              options.vAxis =  {
                                  viewWindow: {
                                        max: maior
                                    }
                              }
                              for (var i in indices_filtro){
                                  for (var j in indices_socio){
                                          if (!deletedColumnsIndex.includes(n))
                                            options.series[colunaAtual++] = {targetAxisIndex: parseInt(i)};
                                           n++;
                                           
                                  }
                                  
                              }
                              console.log(options.series);
                          }
                          
                          var chart = new google.charts.Bar(document.getElementById('chart_div'));
                          chart.draw(data, google.charts.Bar.convertOptions(options));
                                    
                  }
        });
    });
    
    
    
    
</script>

