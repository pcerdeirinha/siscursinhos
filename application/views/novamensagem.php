<?php $this->template->menu('inicio') ?>
<div class="container-fluid">
    <div class="row-content" >
        <div class="col-sm-3" >
            <aside role="complementary">
                <ul class="nav nav-pills nav-stacked navbar-default">
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem'); ?>"><i class="fa fa-envelope" aria-hidden="true"></i> Caixa de Entrada</a></li>
                  <li role="presentation" class="active"><a href="#"><i class="fa fa-plus-square" aria-hidden="true"></i> Nova Mensagem</a></li>
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem/enviado'); ?>"><i class="fa fa-share" aria-hidden="true"></i> Enviados</a></li>
                  <?php if ($view>1){ ?>
                  <li role="presentation"><a href="<?php echo base_url('index.php/mensagem/grupo'); ?>"><i class="fa fa-users" aria-hidden="true"></i> Grupos</a></li>
                  <?php } ?>
                </ul>
            </aside>
        </div>
        <div class="col-sm-9">
            <?php echo form_open('mensagem/cria','','') ?>
            <div class="panel-group panel-default panel">
                <div class="panel-heading">
                    Destinatários
                </div>
                <div class="panel-body">
                    <div class="dropdown" id="drop">
                        <span id="btn-search" data-toggle="dropdown"></span>
                        <div class="input-group <?php if (!(form_error('usuarios')=='')) echo 'has-error has-feedback'; ?>">
                            <span class="input-group-addon" id="user-addon" >Usuários:</span>
                            <?php echo form_input('usuarios', set_value('usuarios')?set_value('usuarios'):$usuarios, 'type="text", class="form-control" id="nome" placeholder="Usuários" autocomplete="false"'); ?> 
                            <?php if (!(form_error('usuarios')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('usuarios'); ?></span>
                    
                        </div>
                        <ul class="dropdown-menu" id="drop_usuarios">
                        </ul>
                    </div> 
                    <div class="row">
                    <div id="usuarios" class="col-md-12"></div>
                    </div>  
                    <br>
                    <div class="input-group <?php if (!(form_error('grupos')=='')) echo 'has-error has-feedback'; ?>">
                        <span class="input-group-addon" id="user-addon">Grupos:</span>
                        <?php echo form_input('grupos', set_value('grupos')?set_value('grupos'):$grupos, 'type="text", class="form-control" id="grupos" placeholder="Grupos"  readonly'); ?> 
                        <span class="input-group-btn">
                                <button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#ModalGrupo" id="btnAdicionarGrupos">Adicionar Grupos</button>
                        </span>
                    </div>
                    <div class="row">
                        <div id="gruposDiv" class="col-md-12"></div>
                    </div>  
                </div>
            </div>
            <div class="panel-group panel-default panel">
                <div class="panel-heading">
                    Corpo   
                </div>
                <div class="panel-body">
                    <div class="form-group <?php if (!(form_error('assunto')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_input('assunto', set_value('assunto')?set_value('assunto'):$assunto, 'type="text", class="form-control" id="assunto" placeholder="Assunto"'); ?> 
                    </div>
                    <div class="form-group <?php if (!(form_error('mensagem')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_textarea('mensagem', set_value('mensagem')?set_value('mensagem'):$mensagem, 'type="text", class="form-control" id="mensagem" placeholder="Mensagem"'); ?> 
                        <?php if (!(form_error('mensagem')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('mensagem'); ?></span>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label><input type="checkbox" name="resposta[]" value="publica" checked=TRUE >Receber Respostas Públicas</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox" name="resposta[]" value="privada" checked=TRUE >Receber Respostas Privadas</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1 col-md-offset-10">
                <div class="form-save-buttons">
                    <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Enviar</button>&ensp;
                </div>
            </div>
            <?php echo form_close(); ?>
            <div class="col-md-1">
                <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
            </div>
        </div>
        <div class="modal fade" id="ModalGrupo" tabindex="-1" role="dialog" aria-labellby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel" align="CENTER">Selecionar Grupo</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group pre-scrollable">
                            <?php foreach ($grupos_padroes as $keyG => $grupo): ?>
                                <div class="checkbox">
                                    <label><input type="checkbox" class="<?php echo $grupo==null?'tipo':'grupo' ?>" id="chk<?php echo $keyG;?>" name="<?php echo $gruposNome[$keyG];?>" value="<?php echo $keyG;?>"><?php echo $gruposNome[$keyG]; ?></label>
                                </div>
                                <?php foreach ($grupo as $keyT => $tipo): ?>
                                    <div class="checkbox">
                                        <label><label><input type="checkbox" class="tipo" id="chk<?php echo $keyG;?>" name="<?php echo $gruposNome[$keyG].': '.$tipo ?>" value="<?php echo $keyG.'-'.$keyT;?>"><?php echo $tipo ?></label></label>
                                    </div>    
                                <?php endforeach ?>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <button type="button" onclick="adicionarGrupos();" data-dismiss="modal" class="btn btn-primary">Adicionar</button>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<script type="text/javascript">
var usuariosAdd = [];
var gruposAdd = [];

function adicionarGrupos() {
    var checked_array = document.getElementsByClassName("tipo");
    for (k=0;k<checked_array.length;k++){
        if (checked_array[k].checked==true){
            if (gruposAdd.indexOf(checked_array[k].value)==-1){
                var itens = $('#gruposDiv').html();
                itens += '<span class="label label-default" style="display: inline-block;" id="g'+checked_array[k].value+'">'+checked_array[k].name+'&nbsp;<a href="#" style="color: inherit;" onclick="fechar(\'g'+checked_array[k].value+'\');"><i class="fa fa-times" aria-hidden="true"></i></a></span>&nbsp'
                $('#gruposDiv').html(itens);
                
                $('<input>').attr({
                    type: 'hidden',
                    id: 'g'+checked_array[k].value ,
                    name: 'grupos[]',
                    value: checked_array[k].value
                }).appendTo('form');
                gruposAdd.push(checked_array[k].value);
            }
        }
    }
}

function buscaUsuario(nome,aberto) {
    event.preventDefault(); 
    jQuery.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>" + "index.php/usuario/buscaNomeEmail",
              dataType: 'json',
              data: {nome: nome, usuarios:usuariosAdd},
              success: function(res) {
                    if (res.err = 'ok'){
                        console.log(res);
                        var row = '';
                        var j=0;
                        for (var i in res.users)
                            row += '<li><a class="'+j++ +' drop_link"  href="#" onclick="adicionar(\''+res.users[i].nome_usuario+'\','+res.users[i].idusuario+')">'+res.users[i].nome_usuario+'<br><small>'+res.users[i].email_usuario+' - '+res.users[i].nome_cursinho_unidade+'</small></a></li>';
                        $('#drop_usuarios').html(row);
                        if (j!=0){
                                if (!$('#drop').hasClass('open')){
                                    $('#drop').addClass('open');
                                    $('#btn-search').attr('aria-expanded','true');
                                }
                        }else{
                            console.log('a');
                            $('#drop').attr('class','dropdown');
                            $('#btn-search').attr('aria-expanded','false');
                        }
                    }else{
                        $('#drop').attr('class','dropdown');
                        $('#btn-search').attr('aria-expanded','false');
                    }
              }
          });
}

function adicionar(nome,id){
    if (usuariosAdd.indexOf(id)==-1){
        var itens = $('#usuarios').html();
        itens += '<span class="label label-default" style="display: inline-block; "id='+id+'>'+nome+'&nbsp;<a href="#" style="color: inherit;" onclick="fechar('+id+')"><i class="fa fa-times" aria-hidden="true"></i></a></span>&nbsp'
        $('#usuarios').html(itens);
        
            $('<input>').attr({
                type: 'hidden',
                id: id,
                name: 'usuario[]',
                value: id
            }).appendTo('form');
        usuariosAdd.push(id);
        console.log(usuariosAdd)
        $('[name=usuarios]').val('');
    }
}


function fechar(id){
    $('#'+id).remove();
    $('#'+id).remove();
    if (String(id).charAt(0)=='g'){
        gruposAdd.splice(gruposAdd.indexOf(id), 1);
    }
    else {
        usuariosAdd.splice(usuariosAdd.indexOf(id),1);   
    }
}


$(document).on('focus',"a.drop_link", function() {
        $(this).parent().addClass('active');
    }).on('blur',"a.drop_link", function() {
        $(this).parent().removeClass('active');
    });

$(document).ready(function(){
    var aberto = false;
    $(".dropdown").on("show.bs.dropdown", function(event){
        aberto = true;
    });
    $(".dropdown").on("hide.bs.dropdown", function(event){
        aberto = false;
    });
    $( "input[type=checkbox].grupo" ).on( "click", function(){
        var id = $(this).attr('id');
         $('#'+ id+".tipo").prop('checked',this.checked)
    });
    $( "input[type=checkbox].tipo" ).on( "click", function(){
        var id = $(this).attr('id');
         if ($(this).prop("checked")==false){
            $('#'+ id+".grupo").prop('checked',false);
            
        }
    });
    
    $("[name=usuarios]").on('keyup',function(event){
        var nome = $('[name=usuarios]').val();
        if (event.keyCode==40)
            $(".0").trigger('focus');
        else{
            if(aberto==true && nome==''){
                $('#drop').remove('open');
                $('#btn-search').attr('aria-expanded','false');
            }
            if(nome!=''){
                buscaUsuario(nome,aberto);
            }
        }
    });
});

</script>