<?php $this->template->menu('inicio') ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">            
                <?php echo form_open('aluno/questionario'); ?>       
                <h3><b>Questionário Socioeconômico</b></h3>
                <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#socioEconomicos">Dados Socioeconômicos</a></li>                        
                </ul>                
        </div>
        <div id="socioEconomicos" class="tab-pane fade in active">
        <br>          
            <div class="col-md-12">                   
                <div class="col-md-2 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('sexo_aluno')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Sexo', 'sexo_aluno'); ?>
                        <?php echo form_dropdown('sexo_aluno',array(-1=>'Selecione',0=>'Feminino',1=>'Masculino',null=>'Outros'),set_value('sexo_aluno')==null?"-1":set_value('sexo_aluno'),'class="form-control" id="sexo" placeholder="Sexo"'); ?>
                        <span class="text-danger"><?php echo form_error('sexo_aluno'); ?></span>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group <?php if (!(form_error('outro_sexo_aluno')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Outro Sexo', 'outro_sexo_aluno'); ?>
                        <?php echo form_input('outro_sexo_aluno',set_value('outro_sexo_aluno') ,'type="text", class="form-control" id="outro_sexo" placeholder="Outro Sexo"'); ?>
                        <?php if (!(form_error('outro_sexo_aluno')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('outro_sexo_aluno'); ?></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group <?php if (!(form_error('etnia_aluno')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Em relação cor/etnia, você se considera', 'etnia_aluno'); ?>
                        <?php echo form_dropdown('etnia_aluno',$etnia,set_value('etnia_aluno')==null?-1:set_value('etnia_aluno'),'class="form-control" id="etnia" placeholder="Etnia"'); ?>
                        <span class="text-danger"><?php echo form_error('etnia_aluno'); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('realizacao_ensino_fund_aluno')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Realização do ensino fundamental', 'realizacao_ensino_fund_aluno'); ?>
                        <?php echo form_dropdown('realizacao_ensino_fund_aluno',$ensino,set_value('realizacao_ensino_fund_aluno')==null?-1:set_value('realizacao_ensino_fund_aluno'),'class="form-control" id="ensino_fundamental" placeholder="Ensino Fundamental"'); ?>
                        <span class="text-danger"><?php echo form_error('realizacao_ensino_fund_aluno'); ?></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group <?php if (!(form_error('realizacao_ensino_medio_aluno')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Realização do ensino médio', 'realizacao_ensino_medio_aluno'); ?>
                        <?php echo form_dropdown('realizacao_ensino_medio_aluno',$ensino,set_value('realizacao_ensino_medio_aluno')==null?-1:set_value('realizacao_ensino_medio_aluno'),'class="form-control" id="ensino_medio" placeholder="Ensino Medio"'); ?>
                        <span class="text-danger"><?php echo form_error('realizacao_ensino_medio_aluno'); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('ano_conclusao_em')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Ano de conclusão do ensino médio', 'ano_conclusao_em'); ?>
                        <?php echo form_dropdown('ano_conclusao_em',$conclusao,set_value('ano_conclusao_em')==null?-1:set_value('ano_conclusao_em'),'class="form-control" id="ensino_fundamental" placeholder="Ensino Fundamental"'); ?>
                        <span class="text-danger"><?php echo form_error('ano_conclusao_em'); ?></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group <?php if (!(form_error('escola_ensino_medio')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Nome da escola que cursa/concluiu o ensino médio', 'escola_ensino_medio'); ?>
                        <?php echo form_input('escola_ensino_medio',set_value('escola_ensino_medio') ,'type="text", class="form-control" id="escola_medio" placeholder="Nome da Escola"'); ?>
                        <?php if (!(form_error('escola_ensino_medio')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('escola_ensino_medio'); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('vez_cursinho_aluno')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Quanto ao cursinho pré-vestibular', 'vez_cursinho_aluno'); ?>
                        <?php echo form_dropdown('vez_cursinho_aluno',$vez,set_value('vez_cursinho_aluno')==null?-1:set_value('vez_cursinho_aluno'),'class="form-control" id="vez" placeholder="Vezes"'); ?>
                        <span class="text-danger"><?php echo form_error('vez_cursinho_aluno'); ?></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group <?php if (!(form_error('nome_cursinho_anterior_aluno')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Nome do cursinho, caso tenha cursado', 'nome_cursinho_anterior_aluno'); ?>
                        <?php echo form_input('nome_cursinho_anterior_aluno',set_value('nome_cursinho_anterior_aluno') ,'type="text", class="form-control" id="escola_cursinjo" placeholder="Nome do Cursinho"'); ?>
                        <?php if (!(form_error('escola_ensino_medio')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('nome_cursinho_anterior_aluno'); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('atividade_remunerada_aluno')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Exerce atividade remunerada', 'atividade_remunerada_aluno'); ?>
                        <?php echo form_dropdown('atividade_remunerada_aluno',$remunerada,set_value('atividade_remunerada_aluno')==null?-1:set_value('atividade_remunerada_aluno'),'class="form-control" id="atividade_remunerada_aluno"'); ?>
                        <span class="text-danger"><?php echo form_error('atividade_remunerada_aluno'); ?></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group <?php if (!(form_error('renda_familiar_aluno')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Qual é a sua renda familiar mensal?', 'renda_familiar_aluno'); ?>
                        <?php echo form_dropdown('renda_familiar_aluno',$renda,set_value('renda_familiar_aluno')==null?"-1":set_value('renda_familiar_aluno'),'class="form-control" id="renda_familiar_aluno"'); ?>
                        <span class="text-danger"><?php echo form_error('renda_familiar_aluno'); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Neste ano, que vestibulares você pretende prestar?'); ?>
                        <div class="checkbox">
                            <label><input type="checkbox" <?php if (in_array(1, $vestibular)) echo "checked"; ?> value="1" name="vestibular[]">UNESP</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox" <?php if (in_array(2, $vestibular)) echo "checked"; ?>  value="2" name="vestibular[]">FUVEST</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox" <?php if (in_array(3, $vestibular)) echo "checked"; ?>  value="3" name="vestibular[]">UNICAMP</label>
                        </div>
                        Outro
                        <div class="input-group">
                            <?php echo form_dropdown('vestibular_aluno',$vestibulares,set_value('vestibular_aluno')==null?0:set_value('vestibular_aluno'),'class="form-control" id="vestibular_aluno"'); ?>
                            <div class="input-group-btn">
                                    <button class="btn-secondary btn" type="button" id="btnNovaFaculdade" data-toggle="modal" data-target="#ModalCriaFaculdade">Novo Vestibular</button>
                           </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo form_label('Pretende prestar outros concursos?'); ?>
                        <div class="radio">
                            <label><input type="radio" name="concurso" value="01" <?php if (set_value('concurso_aluno')==1) echo "checked"; ?>>Sim</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="concurso" value="00" <?php if (set_value('concurso_aluno')==0) echo "checked"; ?>>Não</label>
                        </div>
                        <br>
                        <?php echo form_label('Caso a resposta seja afirmativa qual (is):', 'nome_concurso_aluno'); ?>
                        <?php echo form_input('nome_concurso_aluno',set_value('nome_concurso_aluno') ,'type="text", class="form-control" id="concurso_aluno" placeholder="Concursos"'); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('escolaridade_resp1_aluno')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Qual o grau de escolaridade da sua mãe?', 'escolaridade_resp1_aluno'); ?>
                        <?php echo form_dropdown('escolaridade_resp1_aluno',$escolaridade,set_value('escolaridade_resp1_aluno')==null?-1:set_value('escolaridade_resp1_aluno'),'class="form-control" id="escolaridade_resp1_aluno"'); ?>
                        <span class="text-danger"><?php echo form_error('escolaridade_resp1_aluno'); ?></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group <?php if (!(form_error('escolaridade_resp2_aluno')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Qual o grau de escolaridade do seu pai?', 'escolaridade_resp2_aluno'); ?>
                        <?php echo form_dropdown('escolaridade_resp2_aluno',$escolaridade,set_value('escolaridade_resp2_aluno')==null?-1:set_value('escolaridade_resp2_aluno'),'class="form-control" id="escolaridade_resp1_aluno"'); ?>
                        <span class="text-danger"><?php echo form_error('escolaridade_resp2_aluno'); ?></span>
                    </div>
                </div>
            </div>     
            <div class="col-md-12">
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Como você se informar sobre atualidades?'); ?>
                        <?php foreach ($info_atualidades as $key => $info): ?>
                                <div class="checkbox">
                                    <label><input type="checkbox" <?php if (in_array($info['idsocio_atualidades'], $info_atual)) echo "checked"; ?> value="<?php echo $info['idsocio_atualidades']; ?>" name="info_atual[]"><?php echo $info['descricao_info_atualidades']; ?></label>
                                </div>
                        <?php endforeach ?>
                        <?php echo form_label('Outros:', 'info_atualidades_aluno'); ?>
                        <?php echo form_input('info_atualidades_aluno',set_value('info_atualidades_aluno') ,'type="text", class="form-control" id="info_atualidades_aluno" placeholder="Outras Maneiras"'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo form_label('Quais são seus hábitos culturais mais frequentes?'); ?>
                        <?php foreach ($habito_cult as $key => $habito): ?>
                                <div class="checkbox">
                                    <label><input type="checkbox" <?php if (in_array($habito['idsocio_habito'], $habito_cultural)) echo "checked"; ?> value="<?php echo $habito['idsocio_habito']; ?>" name="habito_cultural[]"><?php echo $habito['descricao_habito']; ?></label>
                                </div>
                        <?php endforeach ?>
                        <?php echo form_label('Outros:', 'habitos_aluno'); ?>
                        <?php echo form_input('habitos_aluno',set_value('habitos_aluno') ,'type="text", class="form-control" id="habitos_aluno" placeholder="Outros Hábitos"'); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="form-group <?php if (!(form_error('organizacao_cultura_aluno')=='')) echo 'has-error'; ?>">
                    <?php echo form_label('Você faz parte de algum tipo de organização associativa de cultura?', 'organizacao_cultura_aluno'); ?>
                    <?php echo form_input('organizacao_cultura_aluno',set_value('organizacao_cultura_aluno') ,'type="text", class="form-control" id="organizacao_cultura_aluno" placeholder="Organizações Associativas de Cultura"'); ?>
                    <?php if (!(form_error('organizacao_cultura_aluno')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                    <span class="text-danger"><?php echo form_error('organizacao_cultura_aluno'); ?></span>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="ModalCriaFaculdade" tabindex="-1" role="dialog" aria-labellby="myModalLabel" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel" align="CENTER">Novo Vestibular</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <?php echo form_label('Nome do Vestibular', 'nome_faculdade'); ?> 
                            <?php echo form_input('nome_faculdade',set_value('nome_faculdade') , 'type="date", class="form-control" id="nome_faculdade" placeholder="Faculdade"'); ?> 
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Tipo da Faculdade', 'tipo_faculdade'); ?> 
                            <?php echo form_dropdown('tipo_faculdade',$tipos_faculdade,set_value('tipo_faculdade'), 'type="date", class="form-control" id="tipo_faculdade" placeholder="Tipo"'); ?> 
                        </div>
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                <input type="radio" name="escola" id="optionsRadios1" value="0" checked>
                                Particular
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                <input type="radio" name="escola" id="optionsRadios2" value="1">
                                Pública
                                </label>
                            </div>                   
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <button type="button" class="btn btn-primary" id="btnCriaFaculdade">Registrar</button>
                    </div>
                </div>
            </div>
        </div>
          <div class="col-md-1 col-md-offset-8">
            <div class="form-save-buttons">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="col-md-1">
            <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>
    </div>
</div>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<script type="text/javascript">

function criaFaculdade(){
    jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/vestibular/criaFaculdade",
                  dataType: 'json',
                  data: {nome_faculdade: $("#nome_faculdade").val(),tipo_faculdade: $("#tipo_faculdade option:selected").val(),publica:$('input[name=escola]:checked').val()},
                  success: function(res) {
                     console.log(res.err);
                    if (res.err == 'ok')
                        var row ='<option  value">Selection</option>';      
                        for(var i in res.faculdade_drop){
                                if (i>3){
                                    row+='<option  value = "'+i+'">'+res.faculdade_drop[i]+'</option>';
                                }
                            }
                         $("[name=vestibular_aluno]").html(row);
                         $('#ModalCriaFaculdade').modal('toggle');
                         
                  }
         });
}

$(document).ready(function () {
    $("#btnCriaFaculdade").click(function(event){
        event.preventDefault(); 
        criaFaculdade();
    });    
});
</script>        