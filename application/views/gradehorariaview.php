<?php
$this->template->menu($view);

?>
<div class="container">
	<h2>
		<center>
		<?php
		echo "$nome_turma";
		?>
		</center>
	</h2>
    <hr>
    <br>
	<?php
	$maxpos=4; //Estabelece o minimo de 4 faixas de horário para montar a tabela
	foreach ($horarios as $horario){ 
		if ($horario['pos_horario'] > $maxpos){ //checa os registros de horário para verificar qual a posição máxima de horário existente
			$maxpos=$horario['pos_horario'];
		}
	}
	$filled = false;
	?>
	<?php if($view>2){ 
	    echo form_open('gradehoraria/novo');
		echo form_hidden('id_turma', $id_turma); 
    } ?>
    <!-- Grades Horárias Alternativas -->
    <div class="col-md-6">
        <h3 id="nome_grade">
            <b>Grade Horária:</b> 
            <?php
            echo $grade_at;
            ?>
        </h3>
    </div>
    <div class="col-md-4 pull-right">
        <div class="form-group <?php if (!(form_error('nome_grade')=='')) echo 'has-error'; ?>">
            <?php echo form_label('Grade Horária Selecionada', 'nome_grade'); ?>
            <?php echo form_dropdown('nome_grade', $grades, $idgrade_at ,'type="text", class="form-control" id="nome_grade" placeholder="Grade Horária"'); ?>
            <span class="text-danger"><?php echo form_error('nome_grade'); ?></span>
        </div>
    </div>
	<div class="col-md-12 table-responsive">
		<table class="table table-bordered">
			<tr bgcolor="lightblue"><th><center>Segunda</center></th><th><center>Terça</center></th><th><center>Quarta</center></th>
				<th><center>Quinta</center></th><th><center>Sexta</center></th>
				<th><font color="gray"><center>Sábado</center></font></th>
			</tr>
			<tbody id="table_horarios">
			<?php 
			for ($tpos=1; $tpos <= $maxpos; $tpos++) { //percorrendo linhas da tabela
				?><tr><?php
				for ($tdia=1; $tdia < 7; $tdia=$tdia+1){ //percorrendo colunas da tabela
					?><td><h4><strong><center><?php
					foreach ($horarios as $idhorario => $horario){
						if ($horario['pos_horario'] == $tpos && $horario['dia_horario'] == $tdia){ //Se existe um horário para a atual posição da tabela, inseri-lo
							?><p style="font-size:20px;"> <?php 
							echo $horario['nome_disciplina'];?></p>
							<small style="font-size:15px;"><b><?php 
							echo $horario['nome_professor'];?></b></small><?php
							$filled = true;	//o campo foi preenchido com um horário existente
							if ($view>2)
								echo '<small style="font-size:12px;"><br><a href ="'.base_url().'index.php/gradehoraria/remove/'.$idhorario.'" data-toggle="tooltip" data-placement="top" title="Remover">Remover</a></small>';
						}
					}
					if (!$filled && $view>2){ //campo não preenchido com horário, inserir opção de novo horário
						?> </strong><h5><input type="checkbox" onchange="habilitarBotao();" name="novohorario[]" value="<?php echo 'rd'.$tpos.$tdia ?>" id="chk_novohorario"/> Selecionar <strong></h5><?php 
					}else if ($view<=2 && !$filled) {?>
					</strong><p class="lead">Vaga</p><strong>
					<?php	
					}
					$filled=false;
					?></center></h4></strong></td><?php
				}
				?></tr><?php
			}
				?>
			</tbody>
		</table>
	</div>
	<br>
	<?php if($view>2){ ?>
	<div class="col-md-12">
	   <button class="btn btn-primary" disabled="true" type="submit" title="Selecione uma posição na grade" id="novohorariobt"><i class="fa fa-plus"></i> Adicionar novo horário</button>
	<?php echo form_close(); ?> 
	   <button class="btn btn-success" type="button" title="Criar Nova Grade" id="novagrade" data-toggle="modal" data-target="#ModalCriaGrade"><i class="fa fa-plus"></i> Criar Grade Horária Alternativa</button>
	   <button class="btn" type="button" title="Alterar Nome da Grade Atual" id="editagrade" data-toggle="modal" data-target="#ModalEditaGrade"><i class="fa fa-cogs"></i> Editar Nome da Grade Atual</button>
	   <button class="btn btn-danger" type="button" title="Remover Grade Atual" id="removegrade" data-toggle="modal" data-target="#ModalRemoveGrade"><i class="fa fa-times"></i> Remover Grade Atual</button>
       <button class="btn btn-default pull-right" id="voltar"><i class="fa fa-arrow-circle-left"></i> Voltar</button>
    </div>
    
    <div class="modal fade" id="ModalCriaGrade" tabindex="-1" role="dialog" aria-labellby="myModalLabel" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="myModalLabel" align="CENTER">Criar Grade Horária Alternativa</h3>
                </div>
                <div class="modal-body">
                    <?php echo form_open('gradehoraria/cria_grade'); ?>
                        <?php echo form_hidden('id_turma',$id_turma); ?>
                        <div class="form_group">
                            <?php echo form_label('Nome da Grade Horária', 'nome_nova_grade'); ?>
                            <?php echo form_input('nome_nova_grade',set_value('nome_nova_grade')?set_value('nome_nova_grade'):$nome_nova_grade, 'type="text" min="2", class="form-control" id="nome_nova_grade" placeholder="Nome da Nova Grade Horária"'); ?>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    <button type="submit" class="btn btn-primary">Registrar</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="ModalEditaGrade" tabindex="-1" role="dialog" aria-labellby="myModalLabel" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="myModalLabel" align="CENTER">Editar Grade Horária</h3>
                </div>
                <div class="modal-body">
                    <?php echo form_open('gradehoraria/edita_grade'); ?>
                        <?php echo form_hidden('id_grade',$idgrade_at); ?>
                        <div class="form_group">
                            <?php echo form_label('Nome da Grade Horária', 'nome_nova_grade'); ?>
                            <?php echo form_input('nome_nova_grade',set_value('nome_nova_grade')?set_value('nome_nova_grade'):$nome_nova_grade, 'type="text" min="2", class="form-control" id="nome_nova_grade" placeholder="Novo Nome da Grade Horária"'); ?>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    <button type="submit" class="btn btn-primary">Registrar</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalRemoveGrade" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title text-danger" id="myModalLabel" align="CENTER">Atenção <i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i></h3>
                </div>
                <div class="modal-body">
                    <p align="CENTER">Você está prestes a remover uma Grade Horária: </p> 
                    <p>Os seguintes itens relacionados também serão afetados:</p>
                    <ul>
                        <li>Todas os horários já informados serão removidos</li>
                    </ul>
                    <p class="text-danger">As alterações acima citadas são irreversíveis.</p>
                    <h4 align="CENTER">Está certo disto?</h4>                               
                </div>
                <div class="modal-footer">
                    <?php echo form_open('gradehoraria/remove_grade'); ?>
                        <?php echo form_hidden('id_grade',$idgrade_at); ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <button type="submit" class="btn btn-danger"  id = "btn_del" >Sim, remover a Grade Horária</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
    
    <?php } ?>
    
    
	<script type="text/javascript">

	function habilitarBotao(){
	    if ($('input[name="novohorario[]"]:checked').length > 0){
		  document.getElementById("novohorariobt").disabled = false;
		  $('#novohorariobt').attr('title',"Clique para adicionar um novo horário");
		}
		else{
		  document.getElementById("novohorariobt").disabled = true;  
		}
	}
	
	function obterHorarios(id_grade){
	    jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/gradehoraria/obter_horarios",
                  dataType: 'json',
                  data: {idgrade_sel: id_grade},
                  success: function(res) {
                    var rows = '';
                    $('#nome_grade').html('<b>Grade Horária: </b>'+res.grade_at);
                    if (res.horario==null){
                        for (var i=1; i <= 4; i++) {
                            rows+='<tr>';
                            for (var j=1; j < 7; j++) {
                                rows+='<td><h4><strong><center>';
                                <?php if ($view>2){?>
                                    rows+= '</strong><h5><input type="checkbox" onchange="habilitarBotao();" name="novohorario[]" value="rd'+i+j+'" id="chk_novohorario"/> Selecionar <strong></h5>'
                                <?php }else{ ?>
                                    rows+='</strong><p class="lead">Vaga</p><strong>';
                                <?php } ?>  
                                rows+='</h4></strong></center></td>';
                            };
                            rows+='</tr>';
                        }
                    }
                    else{
                        var horarios = res.horario;
                        var max = 4;
                        if (Math.max(...Object.keys(horarios))>4) max = Math.max(...Object.keys(horarios)); 
                        for (var i=1; i <= max; i++) {
                            rows+='<tr>';
                            for (var j=1; j < 7; j++) {
                                rows+='<td><h4><strong><center>';
                                if (Object.keys(horarios).indexOf(String(i))!=-1 && Object.keys(horarios[i]).indexOf(String(j))!=-1){
                                       rows+= '<p style="font-size:20px;">'+horarios[i][j].nome_disciplina+'</p><small style="font-size:15px;"><b>'+horarios[i][j].nome_professor+'</b></small>';
                                       <?php if ($view>2){?>
                                           rows+='<small style="font-size:12px;"><br><a href ="<?php echo base_url();?>index.php/gradehoraria/remove/'+horarios[i][j].idhorario+'" data-toggle="tooltip" data-placement="top" title="Remover">Remover</a></small>';
                                       <?php } ?>      
                                }
                               else{
                                       <?php if ($view>2){?>
                                            rows+= '</strong><h5><input type="checkbox" onchange="habilitarBotao();" name="novohorario[]" value="rd'+i+j+'"/> Selecionar <strong></h5>'
                                       <?php }else{ ?>
                                           rows+='</strong><p class="lead">Vaga</p><strong>';
                                       <?php } ?>    
                               }
                                rows+='</h4></strong></center></td>';
                            };
                            rows+='</tr>';
                        };
                    }
                    $('#table_horarios').html(rows);
                  }
         });
	}

	$(document).ready(function () {
        $("[name=nome_grade]").change(function(event) {
           event.preventDefault(); 
           var id_grade = $('option:selected', this).val();
           obterHorarios(id_grade);
           $('[name=id_grade]').val(id_grade);
        });
    });
	$(document).ready(function () {
		$("#voltar").click(function(event){
			<?php if ($view>1) {?>
			window.location.href = "<?php echo base_url(); ?>"+"index.php/gradehoraria";  
			<?php }else{?>
			window.location.href = "<?php echo base_url(); ?>"+"index.php/aluno"; 
			<?php }?>
		});
	}); 

	</script>

	</div>