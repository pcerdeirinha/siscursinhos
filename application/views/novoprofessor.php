<?php $this->template->menu($view) ?>
<br>
<div class="container">
    <div class="row">        
        <div class="col-md-8 col-md-offset-2">            
                <h3><b>Novo professor-monitor</b></h3>
                <br>
                <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#dadosGerais">Dados Gerais</a></li>
                        <li><a data-toggle="tab" href="#endereco">Endereço</a></li>
                        <li><a data-toggle="tab" href="#academico">Dados acadêmicos</a></li>
                        <li><a data-toggle="tab" href="#projeto">Dados relativos ao projeto</a></li> 
                        <li><a data-toggle="tab" href="#bancario">Dados bancários</a></li>
                </ul>                
        </div>
        <?php echo form_open('professor/cria'); ?>
        <div class="tab-content">
            <div id="dadosGerais" class="tab-pane fade in active">                
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        <label for="nome">Nome</label>                                    
                        <?php echo form_input('nome_usuario', $nome_usuario, 'type="text", class="form-control" id="nome" placeholder="Nome"'); ?>
                    </div>
                </div>
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Email', 'email'); ?>
                        <?php echo form_input('email_usuario', $email_usuario, 'type="email", class="form-control" id="email" placeholder="Email"'); ?> 
                    </div>
                </div> 
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('RG', 'rg'); ?>        
                         <?php echo form_input('rg_usuario', $rg_usuario, 'type="text", class="form-control" id="rg" placeholder="RG"'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo form_label('CPF', 'cpf'); ?>
                        <?php echo form_input('cpf_usuario', $cpf_usuario, 'type="text", class="form-control" id="cpf" placeholder="CPF" tipo="cpf"'); ?>
                    </div>
                </div>
                <div class="col-md-5 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Data de Nascimento', 'data_nasc'); ?> 
                        <?php echo form_input('data_nascimento_usuario', $data_nascimento_usuario, 'type="date", class="form-control" id="data_nasc" placeholder="Data de nascimento" tipo="data"'); ?> 
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Telefone', 'telefone'); ?>
                        <?php echo form_input('telefone_usuario', $telefone_usuario, 'type="tel" min="8" max="10", class="form-control" id="telefone" placeholder="DDD + Telefone" tipo="telefone"'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo form_label('Celular', 'celular'); ?>
                        <?php echo form_input('celular_usuario', $celular_usuario, 'type="tel" min="8" max="10", class="form-control" id="telefone" placeholder="DDD + Telefone" tipo="celular"'); ?>
                    </div>
                </div>                                                  
            </div>
            <div id="endereco" class="tab-pane fade">
                <br>
                <div class="col-md-6 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Logradouro', 'logradouro'); ?>
                        <?php echo form_input('logradouro_usuario', $logradouro_usuario, 'type="text", class="form-control" id="logradouro" placeholder="Endereço"'); ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?php echo form_label('Numero', 'numero'); ?>
                        <?php echo form_input('numero_usuario', $numero_usuario, 'type="text", class="form-control" id="numero" placeholder="N." tipo="numero"'); ?>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Bairro', 'bairro'); ?>
                        <?php echo form_input('bairro_usuario', $bairro_usuario, 'type="text", class="form-control" id="bairro" placeholder="Bairro"'); ?>
                    </div>
                </div >
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo form_label('Complemento', 'complemento'); ?>
                        <?php echo form_input('complemento_endereco_usuario', $complemento_endereco_usuario, 'type="text", class="form-control" id="complemente" placeholder="Complemento de endereço"'); ?>
                    </div>
                </div> 
                <div class="col-md-1 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('UF', 'uf'); ?><br>
                        <?php echo form_dropdown('estado_usuario', $estados,'', 'type="text" min="2", class="form-control" id="uf" placeholder="Estado"'); ?>
                    </div>  
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo form_label('Cidade', 'cidade'); ?>
                        <?php echo form_dropdown('cidade_usuario', $cidades,'','type="text", class="form-control" id="cidade" placeholder="Cidade"'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?php echo form_label('CEP', 'cep'); ?>             
                        <?php echo form_input('cep_usuario', $cep_usuario,'type="text", class="form-control" id="cep" placeholder="CEP" tipo="cep"'); ?>
                    </div>
                </div>              
            </div>
            <div  id="academico" class="tab-pane fade">
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Curso', 'curso'); ?>
                        <?php echo form_input('curso_monitor', $curso_monitor, 'type="text", class="form-control" id="curso" placeholder="Curso"'); ?>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2 ">
                    <div class="form-group">
                        <?php echo form_label('Período do Curso', 'pecurso'); ?>
                        <?php echo form_input('periodo_curso_monitor', $periodo_curso_monitor, 'type="text", class="form-control" id="pecurso" placeholder="Nome"'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo form_label('Ano de Curso', 'anocurso'); ?>
                        <?php echo form_input('ano_curso_monitor', $ano_curso_monitor, 'type="text", class="form-control" id="anocurso" placeholder="Ano"'); ?>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2 ">
                    <div class="form-group">
                        <?php echo form_label('RA', 'ra'); ?>
                        <?php echo form_input('ra_monitor', $ra_monitor, 'type="text", class="form-control" id="ra" placeholder="RA"'); ?>
                    </div>
                </div>
            </div>
            <div id="projeto" class="tab-pane fade">
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Função', 'funcao'); ?>
                        <?php echo form_input('funcao_monitor', $funcao_monitor, 'type="text", class="form-control" id="funcao" placeholder="Função (disciplina)"'); ?>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Data de Admissão', 'admissao'); ?>
                        <?php echo form_input('data_admissao_monitor', $data_admissao_monitor, 'type="date", class="form-control" id="admissao" placeholder="Data de admissão" tipo="data"'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="checkbox">                        
                      <label>
                        <?php echo form_checkbox('monitor_voluntario', 1, false, 'type="checkbox" id="monitor_voluntario" title="monitor_voluntario"'); ?>                        
                        Monitor Voluntário
                      </label>
                    </div>
                </div>
            </div>
            <div id="bancario" class="tab-pane fade">
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group">                        
                        <?php echo form_label('Instituição Financeira', 'bancno'); ?>
                        <?php echo form_input('banco_monitor', $banco_monitor, 'type="text", class="form-control" id="bancno" placeholder="Nome do banco"'); ?>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">
                        <?php echo form_label('Agencia', 'agencia'); ?>
                        <?php echo form_input('agencia_monitor', $agencia_monitor, 'type="text", class="form-control" id="agencia" placeholder="Número da agência"'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo form_label('Conta', 'conta'); ?>
                        <?php echo form_input('conta_monitor', $conta_monitor, 'type="text", class="form-control" id="conta" placeholder="Número da conta"'); ?>
                    </div>
                </div>                
                <?php echo form_hidden('unidade_idunidade', $unidade['idunidade']); ?>
            </div>            
        </div>
        <div class="col-md-1 col-md-offset-8">
            <div class="form-save-buttons">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="col-md-1">
            <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div> 
    </div>
</div>
<script type="text/javascript">
$(document).ready(function () {
    mascara();
    data();
    $('input[type=text]').addClass('uppercase');
    $("[name=estado_usuario]").change(function(event) {
       event.preventDefault(); 
       var uf = $('option:selected', this).val();
        jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/usuario/buscaCidade",
                  dataType: 'json',
                  data: {uf: uf},
                  success: function(res) {
                    var row ='';      
                    for(var i in res.cidades){
                        row+='<option>'+res.cidades[i]+'</option>';
                    }
                    $("[name=cidade_usuario]").html(row);
                  }
              });      
       
    });
    $("#voltar").click(function(event){
        window.location.href = "<?php echo base_url(); ?>"+"index.php/professor/opcoes";  
    });
}); 
</script>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
