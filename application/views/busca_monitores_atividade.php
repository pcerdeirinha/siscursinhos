<?php $this->template->menu($view) ?>
<div class='container'>
    <div class="row">       
        <div class="col-md-8 col-md-offset-2">
        <center>        
        <h3><b>Busca de Monitores</b></h3>                   
        </center>
        </div>      
        <div class="col-md-3 col-md-offset-2">
            <div class="input-group">
                <span class="input-group-addon" id="cpf-addon">CPF:</span>              
                <input type="text" class="form-control" id="cpf" placeholder="CPF" name="cpf" tipo="cpf">
            </div>
        </div>
        <div class="col-md-5">
            <div class="input-group">
                <span class="input-group-addon" id="nome-addon">Nome:</span>                
                <input type="text" class="form-control" id="nome" placeholder="Nome" name="nome">
            </div>
        </div>              
    </div>
    <div class="row">
        <br>
        <div class="col-md-4 col-md-offset-4">
            <div class="form-group"> 
                <button type="submit" class="btn btn-primary btn-block" id="buscar"><i class="fa fa-search"></i> Buscar</button>
                <button class="btn btn-default btn-block" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
            </div>          
        </div>                  
    </div>
    <div class="row">       
        <div class="col-md-12">
            <div id="tabela_funcionarios" class="table-responsive">         
            </div>          
        </div>
        <div class="row">
        <div class="modal fade" id="ModalAtividade" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel" align="CENTER">Modificar Atividade</h3>
                    </div>
                    <div class="modal-body">
                        <div class="row">       
                            <?php echo form_open('monitor/mudarAtividade'); ?>
                            <?php echo form_hidden('id_monitor'); ?>
                            <div class="col-md-12">
                                <div class="form-group <?php if (!(form_error('atividade_monitor')=='')) echo 'has-error has-feedback'; ?>">                        
                                    <?php echo form_label('Atividade', 'atividade_monitor'); ?>
                                    <?php echo form_dropdown('func',array(2 => 'Monitor', 3 => 'Secretário', 4 => 'Coordenador Discente'), atividade_monitor_sel, 'type="text" min="2", class="form-control" id="periodo" placeholder="Período"'); ?>                               
                                    <?php if (!(form_error('atividade_monitor')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                    <span class="text-danger"><?php echo form_error('atividade_monitor'); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <button type="submit" class="btn btn-primary">Registrar</button>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>
<script type="text/javascript">
function resetar_senha(id){
    jQuery.ajax({
               type: "POST",
               url: "<?php echo base_url(); ?>" + "index.php/usuario/reset_senha_usuario",
               dataType: 'json',
               data: {id_usuario:id},
               success: function(res) {
                    mensagem('success',"Senha resetada com sucesso!");
               }
    });
    
}

function abrir_reset(id){
    $("#btn_reset_senha").attr("onclick","resetar_senha("+id+")");
    $("#reset_modal").modal();
}

function abrir_atividade(id){
    $("[name=id_monitor]").val(id);
    $("#ModalAtividade").modal();
}



$(document).ready(function () {
    mascara();
    $('[data-toggle="tooltip"]').tooltip();     
    $("#buscar").click(function(event){
        event.preventDefault();                
        var cpf = $("input[type=text][name=cpf]").val();
        var nome = $("input[type=text][name=nome]").val();
        jQuery.ajax({
               type: "POST",
               url: "<?php echo base_url(); ?>" + "index.php/monitor/buscar_monitores",
               dataType: 'json',
               data: {cpf: cpf, nome:nome},
               success: function(res) {                             
                var rows = $('<table id="monitores" class="table table-hover">');
                rows.append('<thead><tr><th>Nome</th><th>Atividade</th><th>Email</th><th>Opções</th></tr></thead>');
                if(res.err == "ok"){                   
                        for(var i in res.userfunc){
                            var modal = '';
                            var bolsa = "<?php echo base_url(); ?>" + "index.php/monitor/bolsas/"+res.userfunc[i].idusuario;
                            var newRow = $('<tr class="animated fadeInDown">');
                            var cols = "";
                            if (res.userfunc[i].nome_social_usuario)
                                 cols +='<td>'+res.userfunc[i].nome_social_usuario+'</td>';
                            else cols +='<td>'+res.userfunc[i].nome_usuario+'</td>';
                            atividade = (res.userfunc[i].tipo_usuario==4)?'Coordenador(a) Discente':((res.userfunc[i].tipo_usuario==3)?'Secretário(a)':'Monitor(a)');
                            cols +='<td>'+atividade+'</td>';
                            cols +='<td>'+res.userfunc[i].email_usuario+'</td>';
                            cols += '<td><a href="'+bolsa+'" data-toggle="tooltip" data-placement="top" title="Período de Atividade"><button type="button" class="btn btn-success"><i class="fa fa-calendar" aria-hidden="true"></i></button></a>&ensp;';
                            cols+= '&ensp;<button type="button" class="btn btn-default" title = "Modificar Atividade" onclick="abrir_atividade('+res.userfunc[i].idusuario+');"><i class="fa fa-expand" aria-hidden="true"></i></button>';
                            cols += '</td>';
                            newRow.append(cols);                            
                            rows.append(newRow);                            
                            $("#modals").append(modal); 
                        }
                        $("#tabela_funcionarios").html(rows);
                        tabela('funcionarios'); 
                                            

                   }else{
                    mensagem('error',res.err);
                    $("#tabela_funcionarios").html('');
                    $("#modals").html('');
                   }
                
               }
           });  
    });
    $("#voltar").click(function(event){
        window.location.href = "<?php echo base_url(); ?>"+"<?php echo $voltar;?>";  
    });
});
</script>