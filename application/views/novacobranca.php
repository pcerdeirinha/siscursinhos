<?php $this->template->menu('inicio') ?>
<div class="container-fluid">
        <div class="col-md-8 col-md-offset-2">
            <?php echo form_open('ouvidoria/cria','','') ?>
            <?php if($idouvidoria!=null) 
				echo form_hidden('idmanifestacao',$idouvidoria); ?>
            <div class="panel-group panel-default panel">
                <div class="panel-heading">
                    Destino da Manifestação
                </div>
                <div class="panel-body">
                    <div class="col-md-6 form-group">
	                    <div class="input-group <?php if (!(form_error('representates_ouvidoria')=='')) echo 'has-error'; ?>">
	                        <span class="input-group-addon" id="user-addon" >Representante</span>
	                        <?php echo form_dropdown('representantes_ouvidoria', $representantes, set_value('representates_ouvidoria')?set_value('representates_ouvidoria'):$representates_ouvidoria ,'type="text", class="form-control" id="representantes" placeholder="Representantes" required'); ?>
	                    </div>
	                    <span class="text-danger"><?php echo form_error('representates_ouvidoria'); ?></span>
	                </div>
	                <div class="col-md-6 form-group">
	                    <div class="input-group <?php if (!(form_error('individuos_ouvidoria')=='')) echo 'has-error'; ?>">
	                        <span class="input-group-addon" id="user-addon" >Indivíduo</span>
	                        <?php if($representates_ouvidoria==5) $habilitado = ''; else $habilitado = 'disabled';
	                        echo form_dropdown('individuos_ouvidoria', $individuos, set_value('individuos_ouvidoria')?set_value('individuos_ouvidoria'):$individuos_ouvidoria ,'type="text", class="form-control" id="indiviuos" placeholder="Indivíduos"'.$habilitado); ?>
	                    </div>
	                    <span class="text-danger"><?php echo form_error('individuos_ouvidoria'); ?></span>
	                </div>
                </div>
            </div>
            <div class="panel-group panel-default panel">
                <div class="panel-heading">
                    Identificação
                </div>
                <div class="panel-body">
                    <div class="col-md-12 form-group">
	                    <div class="input-group <?php if (!(form_error('identificacao')=='')) echo 'has-error'; ?>">
	                        <span class="input-group-addon" id="user-addon" >Deseja se identificar?</span>
	                        <?php echo form_dropdown('identificacao', array(0=>'Sim.', 1=>'Não, apenas desejo informar minha atividade.', 2=>'Não, desejo anonimato.'), set_value('identificacao')?set_value('identificacao'):$identificacao ,'class="form-control" id="identificacao" placeholder="Identificação" required'); ?>
	                    </div>
	                    <span class="text-danger"><?php echo form_error('identificacao'); ?></span>
	                </div>
                </div>
            </div>
            
            
            <div class="panel-group panel-default panel">
                <div class="panel-heading">
                    Corpo da Manifestaçao
                </div>
                <div class="panel-body">
                	<div class="col-md-6 form-group">
	                    <div class="input-group <?php if (!(form_error('tipo_ouvidoria')=='')) echo 'has-error'; ?>">
	                        <span class="input-group-addon" id="user-addon" >Tipo de Manifestação</span>
	                        <?php echo form_dropdown('tipo_ouvidoria',$tipos, set_value('tipo_ouvidoria')?set_value('tipo_ouvidoria'):$tipo_ouvidoria ,'type="text", class="form-control" id="tipo" placeholder="Tipo" required'); ?>
	                    </div>
	                    <span class="text-danger"><?php echo form_error('tipo_ouvidoria'); ?></span>
	                </div>
                	<div class="col-md-6 form-group">
	                    <div class="input-group <?php if (!(form_error('ambiente_ouvidorira')=='')) echo 'has-error'; ?>">
	                        <span class="input-group-addon" id="user-addon" >Ambiente da Manifestação</span>
	                        <?php echo form_dropdown('ambiente_ouvidoria',$ambientes, set_value('ambiente_ouvidorira')?set_value('ambiente_ouvidorira'):$ambiente_ouvidorira ,'type="text", class="form-control" id="ambiente" placeholder="Ambiente"'); ?>
                        </div>
                        <span class="text-danger"><?php echo form_error('ambiente_ouvidorira'); ?></span>
	                </div>
                	<div class="col-md-12 form-group">
	                    <div class="input-group <?php if (!(form_error('refinacao_assunto')=='')) echo 'has-error'; ?>">
	                        <span class="input-group-addon" id="user-addon" >Assunto da Manifestação</span>
	                        <?php echo form_dropdown('assunto_ouvidoria',$assuntos, set_value('assunto_ouvidorira')?set_value('assunto_ouvidorira'):$assunto_ouvidorira ,'type="text", class="form-control" id="assunto" placeholder="Assunto"'); ?>
	                    </div>
	                    <span class="text-danger"><?php echo form_error('assunto_ouvidorira'); ?></span>
	                </div>
                	<div class="col-md-12 form-group <?php if (!(form_error('descricao')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_textarea('descricao', set_value('descricao')?set_value('descricao'):$descricao, 'type="text", class="form-control" id="descricao" placeholder="Descrição" required'); ?> 
                        <?php if (!(form_error('descricao')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                    </div>
                    <span class="text-danger"><?php echo form_error('descricao'); ?></span>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-9">
                <div class="form-save-buttons">
                    <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Enviar</button>&ensp;
                
                	<?php echo form_close(); ?>
                	<button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
            	</div>
            </div>
            
        </div>   
    </div>
</div>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<script type="text/javascript">

$(document).ready(function () {
    $("[name=representantes_ouvidoria]").on('change',function(event) {
       event.preventDefault(); 
       if ($('option:selected', this).val()==5){
       		$("[name=individuos_ouvidoria]").prop('disabled',false);
       	}
       else{
       		$('[name=individuos_ouvidoria]').val(null);
       		$("[name=individuos_ouvidoria]").prop('disabled',true);
       	}
    });
    
    
    $("[name=ambiente_ouvidoria]").on('change',function(event){
    	idambiente = $('option:selected', this).val();
    	console.log(idambiente);
    	jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/ouvidoria/get_assuntos",
                  dataType: 'json',
                  data: {idambiente: idambiente},
                  success: function(res) {
                    console.log(res);
                    var row ='';      
                    for(var i in res.assuntos){
                        row+='<option value="' + i + '">'+res.assuntos[i]+'</option>';                        
                    }
                    $("[name=assunto_ouvidoria]").html(row);
                  }
        });
    	
    });
    
    $("#voltar").click(function(event){
        window.location.href = "<?php echo base_url(); ?>"+"index.php/inicio";  
    });
    
});

</script>