<?php $this->template->menu($view); ?>
<div>	

    <center>
        <h3>Turmas</h3>
        <br><br>
        <table style="border: 0px; border-collapse: collapse;">
            <tr>
                <?php  

                ?>
                <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="150px">
                    <a href="turma/novo" class="btn btn-default">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                        <!-- <img src="../images/coorddoc-mod-ico.png" border="0" alt="SGC - Curso: Novo" title="SGC - Curso: Novo" width="70"/><br /> -->
                        <!--<b>SGC</b><br /> -->
                        Nova Turma
                    </a>
                </td>

                <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="150px">
                    <a href="turma/lista" class="btn btn-primary">
                        <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                        Pesquisa de Turmas
                    </a>
                </td>
                <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="150px">
                    <a href="turma/consolidarTurmas" class="btn btn-primary">
                        <span class="fa fa-calendar-check-o" aria-hidden="true"></span> 
                        <!--<span class="fa fa-calendar-check-o" aria-hidden="true"></span>-->
                        Consolidar Turma
                    </a>
                </td>
                <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="150px">
                    <a href="turma/exibirUniao" class="btn btn-primary">
                        <span class="fa fa-cubes" aria-hidden="true"></span> 
                        <!--<span class="fa fa-calendar-check-o" aria-hidden="true"></span>-->
                        Unir Turmas
                    </a>
                </td>
            </tr><tr>
            

        </tr>   
    </table>
</center>

</div>