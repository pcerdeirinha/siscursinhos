<?php $this->template->menu($view) ?>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="col-md-12">
                <h3><b>Novo Horário de aula</b></h3>
            </div>
                <br>
                <?php echo form_open('gradehoraria/cria'); ?>  
                <?php echo form_hidden('id_grade',set_value('id_grade')?set_value('id_grade'):$id_grade) ?> 
                <?php foreach ($tpos as $key => $t): ?>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="form-group">                  
                            <?php echo form_label('Oferta', 'disciplina_oferta'); ?>
                            <?php echo form_dropdown('disciplina_oferta['.$key.']',$disciplina_oferta, $disciplina_oferta_sel[$key], 'type="text" min="2", class="form-control" id="periodo" placeholder="Nome do Monitor"'); ?>                               
                        </div>
                    </div>   
                    <div class="col-md-3">    
                        <div class="form-group <?php if (!(form_error('pos_horario['.$key.']')=='')) echo 'has-error has-feedback'; ?>">
                            <?php echo form_label('Posição da Aula', 'pos_horario'); ?>
                            <?php echo form_input('pos_horario['.$key.']',$t,'tipo="numero" class="form-control"') ?>
                            <?php if (!(form_error('pos_horario['.$key.']')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                            <span class="text-danger"><?php echo form_error('pos_horario['.$key.']'); ?></span>
                        </div>
                    </div>  
                    <div class="col-md-3">
                        <div class="form-group">                  
                            <?php echo form_label('Dia da Semana', 'dia_horario['.$key.']'); ?>
                            <?php echo form_dropdown('dia_horario['.$key.']',$dias_semana,$tdia[$key], 'type="text" min="2", class="form-control" id="dias" placeholder=Dia da Semana"'); ?>                               
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
            <div class="col-md-1 col-md-offset-8">
                <div class="form-save-buttons" >
                    <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
                </div>
           </div>
           <div class="col-md-1">
                    <button class="btn btn-default"  type="button" onclick="window.location.href='<?php echo base_url('index.php/gradehoraria/exibir/'.$id_grade)  ?>'" id="voltar">Voltar</button>
            </div>
    </div>
</div>
<?php echo form_close(); ?> 
<script type="text/javascript">
$(document).ready(function () {
    mascara();
}); 
</script>