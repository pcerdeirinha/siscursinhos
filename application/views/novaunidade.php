<?php $this->template->menu($view) ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">            
                <?php echo form_open('unidade/cria'); ?>      
                <?php if(isset($id)||(set_value('id')!='')){/*Então é Update*/?>
                            <h3><b>Edita Cursinho</b></h3>
                            <br>
                            <?php echo form_hidden('id', $id?$id:set_value('id')); 
                        }else{ ?>
                            <h3><b>Novo Cursinho</b></h3>                    <br>
                <?php } ?> 
                <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#dadosGerais">Dados Gerais</a></li>                          
                </ul>                
        </div>
                
        <div class="tab-content">
            <div id="dadosGerais" class="tab-pane fade in active">                
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('nome_unidade')=='')) echo 'has-error has-feedback'; ?>">
                        <label for="nome">Nome</label>                                    
                        <?php echo form_input('nome_unidade', set_value('nome_unidade')?set_value('nome_unidade'):$nome_unidade, 'type="text", class="form-control" id="nome" placeholder="Nome"'); ?>
                        <?php if (!(form_error('nome_unidade')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('nome_unidade'); ?></span>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('cidade_unidade')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Cidade', 'cidade_unidade'); ?>
                        <?php echo form_dropdown('cidade_unidade', $cidades, set_value('cidade_unidade')?set_value('cidade_unidade'):$cidade_unidade ,'type="text", class="form-control" id="cidade" placeholder="Cidade"'); ?>
                        <span class="text-danger"><?php echo form_error('cidade_unidade'); ?></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group <?php if (!(form_error('faculdade_unidade')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Faculdade', 'faculdade_unidade'); ?>
                        <?php echo form_dropdown('faculdade_unidade', $faculdades, set_value('faculdade_unidade')?set_value('faculdade_unidade'):$faculdade_unidade ,'type="text", class="form-control" id="cidade" placeholder="Cidade"'); ?>
                        <span class="text-danger"><?php echo form_error('faculdade_unidade'); ?></span>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="col-md-1 col-md-offset-8">
            <div class="form-save-buttons">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="col-md-1">
            <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>
    </div>
</div>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<script  type="text/javascript">
    
    $("#voltar").click(function(event){
        window.location.href = "<?php echo base_url(); ?>"+"index.php/unidade/opcoes";  
    });
</script>

