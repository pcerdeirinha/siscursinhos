<?php $this->template->menu($view) ?>

<div class="container">
    <?php echo form_open('simulado/cria'); ?>
    <?php echo form_hidden('forma',($professor==false)?"geral":"disciplina"); ?> 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">   
            <h3><b>Novo Simulado</b></h3>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">                        
                        <?php echo form_label('Curso:', 'cursos'); ?><br>
                        <?php echo form_dropdown('cursos', $cursos_drop, $curso_sel,'class="form-control" id ="cursos"'); ?>
                         
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">                  
                        <?php echo form_label('Turma <i class="fa fa-circle-o-notch fa-spin fa-fw" id="load_turma" style="display:none;"></i>', 'idTurma'); ?><br>
                        <?php echo form_dropdown('idTurma', $turmas_drop, $turma_sel,'class="form-control" id ="turmas"'); ?>
                    </div>
                </div>
                <div class="col-md-6 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('descricao_simulado')=='')) echo 'has-error has-feedback'; ?>">
                        <label for="descricao_simulado">Descrição</label>                                    
                        <?php echo form_input('descricao_simulado', set_value('descricao_simulado')?set_value('descricao_simulado'):$descricao_simulado, 'type="text", class="form-control" id="descricao" placeholder="Descricao"'); ?>
                        <?php if (!(form_error('descricao_simulado')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('descricao_simulado'); ?></span>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group <?php if (!(form_error('peso_simulado')=='')) echo 'has-error has-feedback'; ?>">
                        <label for="peso_simulado">Peso</label>                                    
                        <?php echo form_input('peso_simulado', set_value('peso_simulado')?set_value('peso_simulado'):$peso_simulado, 'type="text", class="form-control" id="peso" placeholder="Peso" tipo = "real"'); ?>
                        <?php if (!(form_error('peso_simulado')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('peso_simulado'); ?></span>
                    </div>
                </div>
                <div class="col-md-5 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('data_simulado')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Data do Simulado', 'data_simulado'); ?> 
                        <?php echo form_input('data_simulado', set_value('data_simulado')?set_value('data_simulado'):$data__simulado, 'type="date", class="form-control" id="data_aula" placeholder="Data da Aula" tipo="data"'); ?> 
                        <?php if (!(form_error('data_simulado')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('data_simulado'); ?></span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group <?php if (!(form_error('nota_simulado')=='')) echo 'has-error has-feedback'; ?>">
                        <label for="nota_simulado">Nota Máxima</label>                                    
                        <?php echo form_input('nota_simulado', set_value('nota_simulado')?set_value('nota_simulado'):$nota_simulado, 'type="text", class="form-control" id="nota" placeholder="Nota" tipo = "real"'); ?>
                        <?php if (!(form_error('nota_simulado')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('nota_simulado'); ?></span>
                    </div>
                </div>
                <div class="col-md-12">
                    <ul class="nav nav-tabs">
                        <li <?php if ($professor==false) echo 'class="active"'?>><a <?php if ($professor==false) echo 'data-toggle="tab" id="geral"'; else echo 'id="disciplina"'?> href="#Tgeral">Simulado Geral</a></li>
                        <li><a <?php if ($professor==false) echo 'data-toggle="tab" id="frente"'; else echo 'id="disciplina"' ?> href="#TgrandeArea">Simulado por Grande Área</a></li>
                        <li><a <?php if ($professor==false) echo 'data-toggle="tab" id="area"'; else echo 'id="disciplina"' ?> href="#Tarea">Simulado por Área</a></li> 
                        <li <?php if ($professor==true) echo 'class="active"'?>><a data-toggle="tab" href="#Tdisciplina" id="disciplina">Simulado por Disciplina</a></li>                           
                    </ul>                
                </div>
        </div>
        <?php if ($professor==false){  ?>        
        <div class="tab-content">
            <div id="Tgeral" class="tab-pane <?php if ($professor==false) echo 'fade in active"'?>"> 
                <div class="col-md-6 col-md-offset-3">
                    <div class='form-group'>
                        <br>
                        <p class="text-justify">Simulado que avalia todas as disciplinas do aluno. O resultado deste tipo de simulado interfere na nota de todas as disciplinas do aluno.</p>
                    </div>
                </div>
            </div>
            <div id="TgrandeArea" class="tab-pane fade">
                <br>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">                        
                        <?php echo form_label('Grande Área', 'nome_frente_frente');//frente=grande_area(modificação) ?>
                        <?php echo form_dropdown('nome_frente_frente',$nome_frente, set_value('nome_frente_frente')?set_value('nome_frente_frente'):$nome_frente_frente_sel, 'type="text" min="2", class="form-control" id="frente" placeholder="Frente"'); ?>                               
                    </div>
                </div>
            </div>
            <div id="Tarea" class="tab-pane fade">
                <br>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">                        
                        <?php echo form_label('Grande Área', 'nome_frente_area');//frente=grande_area(modificação) ?>
                        <?php echo form_dropdown('nome_frente_area',$nome_frente, set_value('nome_frente_area')?set_value('nome_frente_area'):$nome_frente_area_sel, 'type="text" min="2", class="form-control" id="frente" placeholder="Frente"'); ?>                               
                    </div>
                </div>
                <br>
                <div class="col-md-4">
                    <div class="form-group">                        
                        <?php echo form_label('Área', 'nome_area_area'); ?>
                        <?php echo form_dropdown('nome_area_area',$nome_area, set_value('nome_area_area')?set_value('nome_area_area'):$nome_area_area_sel, 'type="text" min="2", class="form-control" id="area" placeholder="Area"'); ?>                               
                    </div>
                </div>
            </div>
            <?php } ?>               
            <div id="Tdisciplina" class="tab-pane <?php if ($professor==true) echo 'fade in active"'?>">
                <br>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">                        
                        <?php echo form_label('Grande Área', 'nome_frente_disciplina');//frente=grande_area(modificação) ?>
                        <?php echo form_dropdown('nome_frente_disciplina',$nome_frente_disciplina, set_value('nome_frente_disciplina')?set_value('nome_frente_disciplina'):$nome_frente_disciplina_sel, 'type="text" min="2", class="form-control" id="frente" placeholder="Frente"'); ?>                               
                    </div>
                </div>
                <br>
                <div class="col-md-4">
                    <div class="form-group">                        
                        <?php echo form_label('Área', 'nome_area_disciplina'); ?>
                        <?php echo form_dropdown('nome_area_disciplina',$nome_area, set_value('nome_area_disciplina')?set_value('nome_area_disciplina'):$nome_area_disciplina_sel, 'type="text" min="2", class="form-control" id="area" placeholder="Area"'); ?>                               
                    </div>
                </div>
               <div class="col-md-4 col-md-offset-2">
                    <div class="form-group">                        
                        <?php echo form_label('Disciplina', 'nome_disciplina_disciplina'); ?>
                        <?php echo form_dropdown('nome_disciplina_disciplina',$nome_disciplina, set_value('nome_disciplina_disciplina')?set_value('nome_disciplina_disciplina'):$nome_disciplina_disciplina_sel, 'type="text" min="2", class="form-control" id="disciplina" placeholder="Disciplina"'); ?>                               
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1 col-md-offset-8">
            <div class="form-save-buttons">
                <button class="btn btn-primary" data-toggle="modal" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
            </div>
        </div>
        <div class="col-md-1">
            <button class="btn btn-default" onclick="window.location.href='<?php echo base_url('index.php/simulado')  ?>'" id="voltar" type="button" ><i class="fa fa-reply"></i> Voltar</button>
        </div>
    </div>
</div>

<script>
function getTurmasByCurso () {    
    var curso_sel = $('#cursos option:selected').val();
    $("#load_turma").css('display','inline-block');
    jQuery.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "index.php/gradehoraria/buscaTurma",
          dataType: 'json',
          data: {cursos: curso_sel},
          success: function(res) {
             console.log(res.turmas);
            var row ='';      
            for(var i in res.turmas){
              row+='<option value="'+res.turmas[i].idturma+'">'+
              res.turmas[i].nome_turma+", "+res.turmas[i].periodo_turma+'</option>';
          }
          $("#load_turma").css('display','none');
          $("[name=idTurma]").html(row);
      }
    });
}

function carregarAreasTotais () {
        var idFrente = $('option:selected', "[name=nome_frente_area]").val();
        jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/disciplina/obterAreas",
                  dataType: 'json',
                  data: {idFrente: idFrente},
                  success: function(res) {
                    console.log(res.areas);
                    var row ='';      
                    for(var i in res.areas){
                       row+='<option value="' + i + '">'+res.areas[i]+'</option>';
                                             
                    }
                    $("[name=nome_area_area]").html(row);
                  }
              });
}

function carregarAreasDisponiveis () {
        var idFrente = $('option:selected', "[name=nome_frente_disciplina]").val();
        jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/disciplina/obterAreasDisp",
                  dataType: 'json',
                  data: {idFrente: idFrente, idTurma:$("[name=idTurma]").val() },
                  success: function(res) {
                    console.log(res.areas);
                    var row ='';      
                    for(var i in res.areas){
                       row+='<option value="' + i + '">'+res.areas[i]+'</option>';
                                             
                    }
                    $("[name=nome_area_disciplina]").html(row);
                     carregarDisciplinasDisponiveis();
                  }
              });

}


function carregarDisciplinasDisponiveis () {
        var idArea = $('option:selected', "[name=nome_area_disciplina]").val();
        jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/disciplina/obterDisciplinasDisp",
                  dataType: 'json',
                  data: {idArea: idArea, idTurma:$("[name=idTurma]").val() },
                  success: function(res) {
                    console.log(res.disciplinas);
                    var row ='';      
                    for(var i in res.disciplinas){
                       row+='<option value="' + i + '">'+res.disciplinas[i]+'</option>';
                                             
                    }
                    $("[name=nome_disciplina_disciplina]").html(row);
                  }
              });
}

    $(document).ready(function(){
        $(".nav-tabs a").click(function(){
           $('[name=forma]').val($(this).attr('id')); 
        });
    
    carregarAreasTotais();
    carregarAreasDisponiveis();
         
        
        
    $("[name=nome_frente_area]").change(function(event) {
        event.preventDefault(); 
        carregarAreasTotais();
    }); 
    
    $("[name=nome_frente_disciplina]").change(function(event) {
        event.preventDefault(); 
        carregarAreasDisponiveis();
    }); 
    
    $("[name=nome_area_disciplina]").change(function(event) {
        event.preventDefault(); 
        carregarDisciplinasDisponiveis();
    }); 
    $("[name=cursos]").on("change", getTurmasByCurso);

            
        mascara();
        
        data();
    });

    
    
</script>