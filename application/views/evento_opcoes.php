<?php $this->template->menu($view); ?>
<div>   

    <center>
        <h3>Evento</h3>
        <br><br>
        <table style="border: 0px; border-collapse: collapse;">
            <tr>
                <?php if($view>3){?>
                <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="150px">
                    <a href="<?php echo base_url('index.php/evento/novo')?>" class="btn btn-default" title="Novo Evento">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                        <!-- <img src="../images/coorddoc-mod-ico.png" border="0" alt="SGC - Curso: Novo" title="SGC - Curso: Novo" width="70"/><br /> -->
                        <!--<b>SGC</b><br /> -->
                        Novo Evento
                    </a>
                </td>
                <?php } ?>
                <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="150px">
                    <a href="<?php echo base_url('index.php/evento/busca')?>" class="btn btn-primary" title="Eventos da Unidade">
                        <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                        Eventos da Unidade
                    </a>
                </td>
                <td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="150px">
                    <a href="<?php echo base_url('index.php/evento/calendario')?>" class="btn btn-primary" title="Calendário de Eventos">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        Calendário de Eventos
                    </a>
                </td>
                
                <!--td style="border: 2px dotted #E2EBEE; padding: 5px; text-align: center;" width="150px">
                    <a href="<?php echo base_url('index.php/aluno/matricula')?>" class="btn btn-primary">
                        <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                        Matricular Alunos
                    </a>
                </td-->


            </tr><tr>

        </tr>
    </table>
</center>

</div>