<?php $this->template->menu($view) ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3><b>Lista de Departamentos da Faculdade: <?php echo $faculdade['nome_faculdade'] ?></b></h3>
            <?php echo form_open('departamento/cria'); ?>
            <?php echo form_hidden('idfaculdade', set_value('idfaculdade')?set_value('idfaculdade'):$faculdade['idfaculdade']); ?>
                <div class="row">
                <div class="col-md-6 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('nome_departamento')=='')) echo 'has-error has-feedback'; ?>">                                  
                        <div class="input-group">
                            <span class="input-group-addon" id="nome-addon">Nome:</span>              
                            <?php echo form_input('nome_departamento', set_value('nome_departamento')?set_value('nome_departamento'):$nome_departamento, 'type="text", class="form-control" id="nome" placeholder="Nome"'); ?>
                        </div>
                        <?php if (!(form_error('nome_departamento')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('nome_departamento'); ?></span>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-save-buttons">
                        <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Adicionar</button>
                    </div>
                </div>
                </div>
            <?php echo form_close(); ?>
            <table id="faculdades" class="table table-hover">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Opções</th>
                    </tr>   
                </thead>
                <?php foreach ($departamentos as $departamento) { ?>
                <tr class="animated fadeInDown">
                    <td><?php echo $departamento['nome_departamento'];?></td>
                    <td>
                        <a href="<?php echo base_url('index.php/departamento/remove'); echo '/'.$departamento['iddepartamento'] ?>" title="Remover"><button type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button></a>
                     </td>
                </tr>
                <?php } ?>
            </table>    
        </div>
        <div class="col-md-1 col-md-offset-9">
            <button class="btn btn-default" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>          
    </div>
</div>

<script type="text/javascript">


$(document).ready(function () {
    tabela('faculdades');
    $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/faculdade/busca";  
    });
}); 
</script>

<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<?php if(isset($msg)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $msg;?>");</script>
<?php }?>

