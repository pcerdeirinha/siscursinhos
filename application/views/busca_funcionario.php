<?php $this->template->menu($view) ?>
<div class='container'>
	<div class="row">		
		<div class="col-md-8 col-md-offset-2">
		<center>		
		<h3><b><?php echo $page_title;?></b></h3>					
		</center>
		</div>		
		<div class="col-md-3 col-md-offset-2">
			<div class="input-group">
				<span class="input-group-addon" id="cpf-addon">CPF:</span>			    
			    <input type="text" class="form-control" id="cpf" placeholder="CPF" name="cpf" tipo="cpf">
			</div>
		</div>
		<div class="col-md-5">
			<div class="input-group">
				<span class="input-group-addon" id="nome-addon">Nome:</span>			   	
			   	<input type="text" class="form-control" id="nome" placeholder="Nome" name="nome">
			</div>
		</div>				
	</div>
	<div class="row">
		<br>
		<div class="col-md-4 col-md-offset-4">
			<div class="form-group"> 
				<button type="submit" class="btn btn-primary btn-block" id="buscar"><i class="fa fa-search"></i> Buscar</button>
				<button class="btn btn-default btn-block" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
			</div>			
		</div>					
	</div>
	<div class="row">		
		<div class="col-md-12">
			<div id="tabela_funcionarios" class="table-responsive">			
			</div>			
		</div>
		<div id="modals"></div>
		<div class="modal fade" id="reset_modal" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title text-danger" id="myModalLabel" align="CENTER">Atenção <i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i></h3>
					</div>
					<div class="modal-body">
						<p align="CENTER">Você está prestes a resetar a senha de um Monitor! </p> 
						<p>Com esta alteração acontecerão as seguintes ações:</p>
						<ul>
							<li>O monitor não poderá acessar o sistema por sua antiga senha.</li>
							<li>O monitor receberá em seu email uma nova senha.</li>
						</ul>
						<p class="text-danger">As alterações acima citadas são irreversíveis.</p>
						<h4 align="CENTER">Está certo disto?</h4>								
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
						<button type="button" class="btn btn-danger" id="btn_reset_senha" onclick="" data-dismiss="modal" aria-label="Close">Sim, resetar a Senha!</button>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
        <div class="modal fade" id="ModalAtividade" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel" align="CENTER">Modificar Atividade</h3>
                    </div>
                    <div class="modal-body">
                        <div class="row">       
                            <?php echo form_open('monitor/mudarAtividade'); ?>
                            <?php echo form_hidden('id_monitor'); ?>
                            <div class="col-md-12">
                                <div class="form-group <?php if (!(form_error('atividade_monitor')=='')) echo 'has-error has-feedback'; ?>">                        
                                    <?php echo form_label('Atividade', 'atividade_monitor'); ?>
                                    <?php echo form_dropdown('func',array(2 => 'Monitor', 3 => 'Secretário', 4 => 'Coordenador Discente'), atividade_monitor_sel, 'type="text" min="2", class="form-control" id="periodo" placeholder="Período"'); ?>                               
                                    <?php if (!(form_error('atividade_monitor')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                                    <span class="text-danger"><?php echo form_error('atividade_monitor'); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                        <button type="submit" class="btn btn-primary">Registrar</button>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
	</div>	
</div>
<script type="text/javascript">
function resetar_senha(id){
	jQuery.ajax({
	           type: "POST",
	           url: "<?php echo base_url(); ?>" + "index.php/usuario/reset_senha_usuario",
	           dataType: 'json',
	           data: {id_usuario:id},
	           success: function(res) {
	           		mensagem('success',"Senha resetada com sucesso!");
	           }
    });
	
}

function abrir_reset(id){
	$("#btn_reset_senha").attr("onclick","resetar_senha("+id+")");
	$("#reset_modal").modal();
}

function abrir_atividade(id){
    $("[name=id_monitor]").val(id);
    $("#ModalAtividade").modal();
}



$(document).ready(function () {
	mascara();
	$('[data-toggle="tooltip"]').tooltip();		
	$("#buscar").click(function(event){
	    event.preventDefault();	               
	    var cpf = $("input[type=text][name=cpf]").val();
	    var nome = $("input[type=text][name=nome]").val();
	    var tipo = '<?php echo $tipoFuncionario;?>';
	    var usuario;
	    switch(tipo) {
            case '1':
                usuario = "aluno";
                break;
            case '2':
                usuario = "professor";
                break;
            case '3':
                usuario = "secretario";
                break;
            case '4':
                usuario = "coordenador";
                break;
            case '5':
                usuario = "docente";
                break;
        }
	    jQuery.ajax({
	           type: "POST",
	           url: "<?php echo base_url(); ?>" + "index.php/usuario/busca",
	           dataType: 'json',
	           data: {cpf: cpf, nome:nome, tipo:tipo},
	           success: function(res) {	            	           	
	           	var rows = $('<table id="funcionarios" class="table table-hover">');
	           	<?php if($view>2){?>
	           		rows.append('<thead><tr><th>Nome</th><th>CPF</th><th>Email</th><th>Opções</th></tr></thead>');
	           	<?php } else {?>
	           		rows.append('<thead><tr><th>Nome</th><th>CPF</th><th>Email</th></tr></thead>');
	           	<?php } ?>
				if(res.err == "ok"){	               
	               		for(var i in res.userfunc){
	               			var modal = '';
	               			var name_modal = '#'+res.userfunc[i].idusuario;
	               			<?php if($view>2){?>
	               				var editar = "<?php echo base_url(); ?>" + "index.php/"+usuario+"/edita/"+res.userfunc[i].idusuario;
	               			<?php }?>
	               			var remover = "<?php echo base_url(); ?>" + "index.php/usuario/remove/"+res.userfunc[i].idusuario;
	               			var bolsa = "<?php echo base_url(); ?>" + "index.php/monitor/bolsas/"+res.userfunc[i].idusuario;
	               			var newRow = $('<tr class="animated fadeInDown">');
	               			var cols = "";
	               			if (res.userfunc[i].nome_social_usuario!=null)
	               			     cols +='<td>'+res.userfunc[i].nome_social_usuario+'</td>';
	               			else cols +='<td>'+res.userfunc[i].nome_usuario+'</td>';
	               			cols +='<td>'+res.userfunc[i].cpf_usuario+'</td>';
	               			cols +='<td>'+res.userfunc[i].email_usuario+'</td>';
	               			<?php if($view>2){?>
		               			modal = '<div class="modal fade" id="'+res.userfunc[i].idusuario+'" tabindex="-1" role="dialog" aria-labellby="myModalLabel">';
		               			modal+='<div class="modal-dialog" role="document"><div class="modal-content">';
		               			modal+='<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3 class="modal-title text-danger" id="myModalLabel" align="CENTER">Atenção <i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i></h3></div>';
		               			modal+='<div class="modal-body"><p align="CENTER">Você está prestes a remover o <?php echo $monitor;?>: </p><h4 align="CENTER">'+res.userfunc[i].nome_usuario+'</h4><p>Os seguintes itens relacionados também serão afetados:</p><ul><li>Todas as ofertas de disciplinas relacionadas a este monitor serão canceladas, caso este monitor ser um professor-monitor.</li></ul><p class="text-danger">As alterações acima citadas são irreversíveis.</p><h4 align="CENTER">Está certo disto?</h4></div>';
		               			cols += '<td><a href="'+editar+'" data-toggle="tooltip" data-placement="top" title="Editar"><button type="button" class="btn btn-default"><i class="fa fa-pencil-square-o"></i></button></a>&ensp;';
		               			<?php if($view>3){?>
    		               			if (tipo<5){
    		               			    cols += '<a href="'+bolsa+'" data-toggle="tooltip" data-placement="top" title="Período de Atividade"><button type="button" class="btn btn-success"><i class="fa fa-calendar" aria-hidden="true"></i></button></a>&ensp;';
	               		                }
	               		        <?php }?>
		               			cols += '<a href="#" data-toggle="tooltip" data-placement="top" title="Remover"><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#'+res.userfunc[i].idusuario+'"><i class="fa fa-trash"></i></button></a>&ensp;<button type="button" class="btn btn-default" title = "Resetar Senha" onclick="abrir_reset('+res.userfunc[i].idusuario+');"><i class="fa fa-key" aria-hidden="true"></i></button>';
		               			<?php  if($view>3){?>
		               			   cols+= '&ensp;<button type="button" class="btn btn-default" title = "Modificar Atividade" onclick="abrir_atividade('+res.userfunc[i].idusuario+');"><i class="fa fa-expand" aria-hidden="true"></i></button>';
		               			<?php }?>
		               			cols += '</td>';
		               			
		               			modal+='<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button><button type="button" class="btn btn-danger"  onclick=location.href="'+remover+'">Sim, remover o <?php echo $monitor;?></button></div></div></div></div>';
	               			<?php }?>
	               			newRow.append(cols);	               			
	               			rows.append(newRow);	               			
	               			$("#modals").append(modal); 
	               		}
	               		$("#tabela_funcionarios").html(rows);
	               		tabela('funcionarios'); 
	               		             		

	               }else{
	               	mensagem('error',res.err);
	               	$("#tabela_funcionarios").html('');
	               	$("#modals").html('');
	               }
				
	           }
	       });  
	});
	$("#voltar").click(function(event){
    	window.location.href = "<?php echo base_url(); ?>"+"<?php echo $voltar;?>";  
	});
});
</script>