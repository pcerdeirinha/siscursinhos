<?php $this->template->menu($view) ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            
            <h3><b>Lista de Relatórios</b></h3>
            
            <table id='relatorios' class="table table-hover">
                <thead>
                    <tr>
                        <th>Nome do Relatório</th>
                        <th>Opções</th>
                    </tr>
                </thead>         
                <tr class="animated fadeInDown">
                    <td>Relatório de Frequência por Disciplina</td>
                    <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalFrequenciaDisciplina" title="Relatório de Frequência por Disciplina"><i class="fa fa-file-text"></i></button>
                    </td>
                </tr>
                <tr class="animated fadeInDown">
                    <td>Relatório de Alunos por Turma</td>
                    <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalAlunosTurma" title="Relatório de Alunos por Turma"><i class="fa fa-file-text"></i></button>
                    </td>
                </tr>
               <tr class="animated fadeInDown">
               	<td>Relatório de Grade Horária das Turmas</td>
                    <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalGradeHoraria" title="Relatório de Grade Horária das Turmas"><i class="fa fa-file-text"></i></button>
                    </td>
                </tr>
            </table>
                   
            <div class="modal fade" id="ModalFrequenciaDisciplina" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title" id="myModalLabel" align="CENTER">Selecione a Turma</h3>
                        </div>
                        <div class="modal-body">
                            <?php echo form_open('aula/relatorio_frequencia_disciplina'); ?>
                            
                            <?php echo form_label('Curso', 'nome_curso'); ?>
                            <?php echo form_dropdown('nome_curso',$nome_curso, set_value('nome_curso')?set_value('nome_curso'):$nome_curso_sel, 'type="text" min="2", class="form-control" id="curso" placeholder="Curso"'); ?>                               
                            
                            <?php echo form_label('Turma', 'nome_turma'); ?>
                            <?php echo form_dropdown('nome_turma',$nome_turma, set_value('nome_turma')?set_value('nome_turma'):$nome_turma_sel, 'type="text" min="2", class="form-control" id="turma" placeholder="Turma"'); ?>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                            <button type="submit" value="pdf" class="btn btn-primary" name="btnRelatorio">Gerar Relatório PDF</button>
                            <button type="submit" value="csv" class="btn btn-primary" name="btnRelatorio">Gerar Relatório CSV</button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="ModalAlunosTurma" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title" id="myModalLabel" align="CENTER">Selecione a Turma</h3>
                        </div>
                        <div class="modal-body">
                            <?php echo form_open('turma/relatorio_aluno_turma','target="_blank"'); ?>
                            
                            <?php echo form_label('Curso', 'nome_curso'); ?>
                            <?php echo form_dropdown('nome_curso',$nome_curso, set_value('nome_curso')?set_value('nome_curso'):$nome_curso_sel, 'type="text" min="2", class="form-control" id="curso" placeholder="Curso"'); ?>                               
                            
                            <?php echo form_label('Turma', 'nome_turma'); ?>
                            <?php echo form_dropdown('nome_turma',$nome_turma, set_value('nome_turma')?set_value('nome_turma'):$nome_turma_sel, 'type="text" min="2", class="form-control" id="turma" placeholder="Turma"'); ?>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                            <button type="submit" value="pdf" class="btn btn-primary" name="btnRelatorio">Gerar Relatório PDF</button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="modal fade" id="ModalGradeHoraria" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title" id="myModalLabel" align="CENTER">Selecione a Turma</h3>
                        </div>
                        <div class="modal-body">
                            <?php echo form_open('gradehoraria/relatorio'); ?>
                            
                            <?php echo form_label('Curso', 'nome_curso'); ?>
                            <?php echo form_dropdown('nome_curso',$nome_curso, set_value('nome_curso')?set_value('nome_curso'):$nome_curso_sel, 'type="text" min="2", class="form-control" id="curso" placeholder="Curso"'); ?>                               
                            
                            <?php echo form_label('Turma', 'nome_turma'); ?>
                            <?php echo form_dropdown('nome_turma',$nome_turma, set_value('nome_turma')?set_value('nome_turma'):$nome_turma_sel, 'type="text" min="2", class="form-control" id="turma" placeholder="Turma"'); ?>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                            <button type="submit" value="pdf" class="btn btn-primary" name="btnRelatorio">Gerar Relatório PDF</button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
                    
        </div>
        <div class="col-md-1 col-md-offset-9">
            <button class="btn btn-default" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>      
    </div>
</div>
<script type="text/javascript">

function carregarDisciplinas() {
  var idTurma = $('option:selected', "[name=nome_turma]").val();   
        jQuery.ajax({
               type: "POST",
               url: "<?php echo base_url(); ?>" + "index.php/oferta_disciplina/getOfertasTurma",
               dataType: 'json',
               data: {idTurma:idTurma},
               success: function(res) {
                   console.log(res.disciplinas);
                    if(res.err == "ok"){         
                        var row ='';          
                        for(var i in res.disciplinas){
                           row+='<option value="' + res.disciplinas[i].id_disciplina + '">'+res.disciplinas[i].nome_disciplina+'</option>';
                        }
                        $("[name=disciplina_oferta]").html(row);

                   }else{
                    $("[name=disciplina_oferta]").html('');
                   }
               }
           });

}

$(document).ready(function () {
    tabela('turmas');
    $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/turma";  
    });
    
    var idCurso= $('option:selected', "[name=nome_curso]").val();
    jQuery.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>" + "index.php/turma/getTurmaCurso",
              dataType: 'json',
              data: {idcurso: idCurso},
              success: function(res) {
                console.log(res.turmas);
                var row =''; 
                if (res.err = 'ok'){
                    for(var i in res.turmas){
                       row+='<option value="' + i + '">'+res.turmas[i]+'</option>';
                                             
                    }
                }
                $("[name=nome_turma]").html(row);
                carregarDisciplinas();
              }
          });
          
          
                 
          
   $("[name=nome_curso]").change(function(event) {
        event.preventDefault(); 
        var idCurso= $('option:selected', "[name=nome_curso]").val();
        jQuery.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>" + "index.php/turma/getTurmaCurso",
                  dataType: 'json',
                  data: {idcurso: idCurso},
                  success: function(res) {
                    console.log(res.turmas);
                    var row ='';      
                    for(var i in res.turmas){
                       row+='<option value="' + i + '">'+res.turmas[i]+'</option>';
                                             
                    }
                    $("[name=nome_turma]").html(row);
                    carregarDisciplinas();
                  }
              });
    }); 
    
      $("[name=nome_turma]").change(function(event) {
        event.preventDefault(); 
        carregarDisciplinas();
       });

        
       
}); 
</script>

<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>


