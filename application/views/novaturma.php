<?php $this->template->menu($view) ?>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">            
        <?php echo form_open('turma/cria'); ?>
            <?php if((isset($idturma))||(set_value('idturma')!='')){/*Então é Update*/?>
                    <h3><b>Edita Turma</b></h3>
                    <?php echo form_hidden('idturma', $idturma); 
                }else{ ?>
                    <h3><b>Nova Turma</b></h3>
            <?php } ?>
            <br>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#dadosTurma">Dados da Turma</a></li>
            </ul>
        </div>
        <div class="tab-content">
            <div id="dadosTurma" class="tab-pane fade in active">
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('nome_turma')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Nome da Turma', 'nom_turma'); ?>
                        <?php echo form_input('nome_turma', set_value('nome_turma')?set_value('nome_turma'):$nome_turma, 'type="text", class="form-control" id="nome" placeholder="Nome da Turma"'); ?> 
                        <?php if (!(form_error('nome_turma')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('nome_turma'); ?></span>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('periodo_turma')=='')) echo 'has-error has-feedback'; ?>">                        
                        <?php echo form_label('Período', 'an_curso'); ?>
                        <?php echo form_dropdown('periodo_turma',set_value('periodo_turma')?set_value('periodo_turma'):$periodo_turma, $periodo_sel, 'type="text" min="2", class="form-control" id="periodo" placeholder="Período"'); ?>                               
                        <?php if (!(form_error('periodo_turma')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('periodo_turma'); ?></span>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group <?php if (!(form_error('sala_turma')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Sala', 'sala'); ?>
                        <?php echo form_input('sala_turma', set_value('sala_turma')?set_value('sala_turma'):$sala_turma, 'type="text", class="form-control" id="sala" placeholder="Sala"'); ?>        
                        <?php if (!(form_error('sala_turma')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('sala_turma'); ?></span>
                    </div>
                </div>
                <div class="col-md-2">
                    <div  class="form-group <?php if (!(form_error('num_vagas')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Numero de Vagas', 'vagas'); ?>
                        <?php echo form_input('num_vagas', set_value('num_vagas')?set_value('num_vagas'):$num_vagas, 'class="form-control" id="Num. Vagas" placeholder="Vagas" tipo="numero"'); ?>
                        <?php if (!(form_error('num_vagas')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('num_vagas'); ?></span>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('curso')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Curso', 'cursolbl'); ?><br>
                        <?php echo form_dropdown('curso',$cursos_drop, set_value('curso')?set_value('curso'):$curso_sel, 'type="text" min="2", class="form-control" id="curso" placeholder="Curso"'); ?>
                        <?php if (!(form_error('curso')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('curso'); ?></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group <?php if (!(form_error('data_inicio_turma')=='')) echo 'has-error has-feedback'; ?>">
                        <?php echo form_label('Data Inicial da Turma', 'turmabl'); ?>
                        <?php echo form_input('data_inicio_turma',set_value('data_inicio_turma')?set_value('data_inicio_turma'):$data_inicio_turma, 'type="text" min="2", class="form-control" id="ano" placeholder="Data de início" tipo = "data"'); ?>                        
                        <?php if (!(form_error('data_inicio_turma')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('data_inicio_turma'); ?></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1 col-md-offset-8">
            <div class="form-save-buttons">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
            </div>
        </div> 
        <?php echo form_close(); ?>
        <div class="col-md-1">
            <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>
    </div>
</div>

<script type="text/javascript">
console.log('')
$(document).ready(function () {
    $("#voltar").click(function(event){
        window.location.href = "<?php echo base_url(); ?>"+"index.php/turma";  
    });
}); 
</script>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<script type="text/javascript">
    mascara();
    data();
</script>
