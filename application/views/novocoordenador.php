<?php $this->template->menu($view) ?>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-sm-7">
                <h3><b>Novo Coordenador</b></h3>
                <br>
                <?php echo form_open('coordenador/cria'); ?>   
                <div class="form-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">Dados pessoais</div>
                        <div class="panel-body">
                            <label for="nome">Nome</label>
                            <?php echo form_input('nome_usuario', $nome_usuario, 'type="text", class="form-control" id="nome" placeholder="Nome"'); ?> 
                            <br>
                            
                            <?php echo form_label('Email', 'email'); ?>
                            <?php echo form_input('email_usuario', $email_usuario, 'type="email", class="form-control" id="email" placeholder="Email"'); ?>          
                            <br>
                            <?php echo form_label('RG', 'rg'); ?>        
                            <?php echo form_input('rg_usuario', $rg_usuario, 'type="text", class="form-control" id="rg" placeholder="RG"'); ?>
                            <br>
                            <?php echo form_label('CPF', 'cpf'); ?>
                            <?php echo form_input('cpf_usuario', $cpf_usuario, 'type="text", class="form-control" id="cpf" placeholder="CPF"'); ?>
                            <br>
                            <?php echo form_label('Data de Nascimento', 'data_nasc'); ?> 
                            <?php echo form_input('data_nascimento_usuario', $data_nascimento_usuario, 'type="date", class="form-control" id="data_nasc" placeholder="Data de nascimento"'); ?> 
                            <br>
                            <?php echo form_label('Logradouro', 'logradouro'); ?>
                            <?php echo form_input('logradouro_usuario', $logradouro_usuario, 'type="text", class="form-control" id="logradouro" placeholder="Endereço"'); ?>
                            <br>
                            <?php echo form_label('Bairro', 'bairro'); ?>
                            <?php echo form_input('bairro_usuario', $bairro_usuario, 'type="text", class="form-control" id="bairro" placeholder="Bairro"'); ?>
                            <br>
                            <?php echo form_label('Complemento', 'complemento'); ?>
                            <?php echo form_input('complemento_endereco_usuario', $complemento_endereco_usuario, 'type="text", class="form-control" id="complemente" placeholder="Complemento de endereço"'); ?> 
                            <br>
                            <?php echo form_label('CEP', 'cep'); ?>             
                            <?php echo form_input('cep_usuario', $cep_usuario, 'type="text", class="form-control" id="cep" placeholder="CEP"'); ?>
                            <br>
                            <?php echo form_label('Cidade', 'cidade'); ?>
                            <?php echo form_input('cidade_usuario', $cidade_usuario, 'type="text", class="form-control" id="cidade" placeholder="Cidade"'); ?>
                            <br>
                            <?php echo form_label('UF', 'uf'); ?>
                            <?php echo form_input('estado_usuario', $estado_usuario, 'type="text" min="2", class="form-control" id="uf" placeholder="Estado"'); ?> 
                            <br>
                            <?php echo form_label('Telefone', 'telefone'); ?>
                            <?php echo form_input('telefone_usuario', $telefone_usuario, 'type="tel" min="8" max="10", class="form-control" id="telefone" placeholder="DDD + Telefone"'); ?>
                            <br>
                            <?php echo form_label('Celular', 'celular'); ?>
                            <?php echo form_input('celular_usuario', $celular_usuario, 'type="tel" min="8" max="10", class="form-control" id="telefone" placeholder="DDD + Telefone"'); ?>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Dados acadêmicos</div>
                        <div class="panel-body">
                            <?php echo form_label('Curso', 'curso'); ?>
                            <?php echo form_input('curso_monitor', $curso_monitor, 'type="text", class="form-control" id="curso" placeholder="Curso"'); ?>
                            <br>
                            <?php echo form_label('Período do Curso', 'pecurso'); ?>
                            <?php echo form_input('periodo_curso_monitor', $periodo_curso_monitor, 'type="text", class="form-control" id="pecurso" placeholder="Nome"'); ?>
                            <br>
                            <?php echo form_label('Ano de Curso', 'anocurso'); ?>
                            <?php echo form_input('ano_curso_monitor', $ano_curso_monitor, 'type="text", class="form-control" id="anocurso" placeholder="Ano"'); ?>
                            <br>
                            <?php echo form_label('RA', 'ra'); ?>
                            <?php echo form_input('ra_monitor', $ra_monitor, 'type="text", class="form-control" id="ra" placeholder="RA"'); ?>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Dados relativos ao projeto</div>
                        <div class="panel-body">
                            <?php echo form_label('Função', 'funcao'); ?>
                            <?php echo form_input('funcao_monitor', $funcao_monitor, 'type="text", class="form-control" id="funcao" placeholder="Função"'); ?>
                            <br>
                            <?php echo form_label('Data de Admissão', 'admissao'); ?>
                            <?php echo form_input('data_admissao_monitor', $data_admissao_monitor, 'type="date", class="form-control" id="admissao" placeholder="Data de admissão"'); ?>
                            <br>
                            <?php echo form_label('Monitor Voluntário: ', 'mv'); ?>
                            &ensp;
                            <?php echo form_checkbox('monitor_voluntario', 1, false, 'type="checkbox" id="monitor_voluntario" title="monitor_voluntario"'); ?>
                            <br>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Dados bancários</div>
                        <div class="panel-body">
                            <?php echo form_label('Instituição Financeira', 'bancno'); ?>
                            <?php echo form_input('banco_monitor', $banco_monitor, 'type="text", class="form-control" id="bancno" placeholder="Nome do banco"'); ?>
                            <br>
                            <?php echo form_label('Agencia', 'agencia'); ?>
                            <?php echo form_input('agencia_monitor', $agencia_monitor, 'type="text", class="form-control" id="agencia" placeholder="Número da agência"'); ?>
                            <br>
                            <?php echo form_label('Conta', 'conta'); ?>
                            <?php echo form_input('conta_monitor', $conta_monitor, 'type="text", class="form-control" id="conta" placeholder="Número da conta"'); ?>
                        </div>
                    </div>
                    <?php echo form_hidden('unidade_idunidade', $unidade['idunidade']); ?>
                    <div class="form-save-buttons">
                        <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
                        &ensp;&ensp;
                        <?php// echo anchor('inicio', 'Voltar'); ?>
                        <button class="btn btn-default" href="#" id="voltar">Voltar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function () {
    $("#voltar").click(function(event){
        window.location.href = "<?php echo base_url(coordenador/opcoes); ?>"; 
    });
}); 
</script>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<?php echo form_close(); ?> 