<?php $this->template->menu($view) ?>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<center>
			<h3><b>Lista de Disciplinas</b></h3>			
			<table id="disciplinas" class="table table-hover">
				<thead>
					<tr>
						<th>Nome da Disciplina</th>
						<th>Área</th>
						<th>Curso</th>
						
						<th>Opções</th>
					</tr>
				</thead>				
				<?php foreach ($disciplinas as $disciplina) { ?>
				<tr class="animated fadeInDown">
					<td><?php echo $disciplina['nome_disciplina']; ?> </td>
					<td><?php echo $disciplina['area_disciplina'];?></td>
					
					<td><?php echo $cursos_drop[$disciplina['disciplina_idcurso']];?></td>
					<td>
						<a href="edita/<?php echo $disciplina['iddisciplina'] ?>" data-toggle="tooltip" data-placement="top" title="Editar"><button type="button" class="btn btn-default"><i class="fa fa-pencil-square-o"></i></button></a>
						&ensp;<a href="remove/<?php echo $disciplina['iddisciplina'] ?>" data-toggle="tooltip" data-placement="top" title="Remover"><button type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button></a>
					</td>
				</tr>
				<?php } ?>
			</table>
						
			</center>			
		</div>
		<div class="col-md-1 col-md-offset-9">
			<button class="btn btn-default" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
		</div>		
	</div>
</div>
<script type="text/javascript">
$(document).ready(function () {
	tabela('disciplinas');
    $("#voltar").click(function(event){
            window.location.href = "<?php echo base_url(); ?>"+"index.php/disciplina";  
    });
}); 
</script>

<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>

<?php if(isset($msg)){?>
    <script type="text/javascript">mensagem('success',"<?php echo $msg;?>");</script>
<?php }?>


