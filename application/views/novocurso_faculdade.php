<?php $this->template->menu($view) ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">            
                <?php echo form_open('curso_faculdade/cria'); ?>      
                <?php if(isset($id)||(set_value('id')!='')){/*Então é Update*/?>
                            <h3><b>Edita Curso</b></h3>
                            <br>
                            <?php echo form_hidden('id', $id?$id:set_value('id')); 
                        }else{ ?>
                            <h3><b>Novo Curso</b></h3>                    <br>
                <?php } ?> 
                <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#dadosGerais">Dados Gerais</a></li>                          
                </ul>                
        </div>
                
        <div class="tab-content">
            <div id="dadosGerais" class="tab-pane fade in active">                
                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group <?php if (!(form_error('nome_curso')=='')) echo 'has-error has-feedback'; ?>">
                        <label for="nome">Nome</label>                                    
                        <?php echo form_input('nome_curso', set_value('nome_curso')?set_value('nome_curso'):$nome_curso, 'type="text", class="form-control" id="nome" placeholder="Nome"'); ?>
                        <?php if (!(form_error('nome_curso')=='')) echo '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'; ?>
                        <span class="text-danger"><?php echo form_error('nome_curso'); ?></span>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-2"">
                    <div class="form-group <?php if (!(form_error('departamento_curso')=='')) echo 'has-error'; ?>">
                        <?php echo form_label('Departamento', 'departamento_curso'); ?>
                        <?php echo form_dropdown('departamento_curso', $departamentos, set_value('departamento_curso')?set_value('departamento_curso'):$departamento_curso ,'type="text", class="form-control" id="departamento" placeholder="Departamento"'); ?>
                        <span class="text-danger"><?php echo form_error('departamento_curso'); ?></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo form_label('Periodos', 'periodo_curso');echo '<br>' ?>
                        <?php foreach ($periodos as $periodo): ?>
                            <?php echo form_checkbox('periodo['.$periodo['idperiodo'].']',$periodo['idperiodo'],($periodo['checked']||set_value('periodo['.$periodo['idperiodo'].']'))?true:false,'');echo $periodo['nome_periodo'].'<br>'; ?>
                        <?php endforeach ?>                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1 col-md-offset-8">
            <div class="form-save-buttons">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o"></i> Registrar</button>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="col-md-1">
            <button class="btn btn-default" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
        </div>
    </div>
</div>
<?php if(isset($err)){?>
    <script type="text/javascript">mensagem('error',"<?php echo $err;?>");</script>
<?php }?>
<script  type="text/javascript">
    
    $("#voltar").click(function(event){
        window.location.href = "<?php echo base_url(); ?>"+"index.php/unidade/opcoes";  
    });
</script>

