<?php $this->template->menu($view) ?>
<div class='container'>
	<div class="row">		
		<div class="col-md-8 col-md-offset-2">
		<center>		
		<h3><b>Busca de Alunos</b></h3>					
		</center>
		</div>		
		<div class="col-md-3 col-md-offset-2">
			<div class="input-group">
				<span class="input-group-addon" id="cpf-addon">CPF:</span>			    
			    <input type="text" class="form-control" id="cpf" placeholder="CPF" name="cpf" tipo="cpf">
			</div>
		</div>
		<div class="col-md-5">
			<div class="input-group">
				<span class="input-group-addon" id="nome-addon">Nome:</span>			   	
			   	<input type="text" class="form-control" id="nome" placeholder="Nome" name="nome">
			</div>
		</div>				
	</div>
	<div class="row">
		<br>
		<div class="col-md-4 col-md-offset-4">
			<div class="form-group"> 
				<button type="submit" class="btn btn-primary btn-block" id="buscar"><i class="fa fa-search"></i> Buscar</button>
				<button class="btn btn-default btn-block" href="#" id="voltar"><i class="fa fa-reply"></i> Voltar</button>
			</div>			
		</div>					
	</div>
	<div class="row">		
		<div class="col-md-12">
			<div id="tabela_alunos" class="table-responsive">			
			</div>			
		</div>
		<div id='modals'></div>
		
		<div class="modal fade" id="reset_modal" tabindex="-1" role="dialog" aria-labellby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title text-danger" id="myModalLabel" align="CENTER">Atenção <i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i></h3>
					</div>
					<div class="modal-body">
						<p align="CENTER">Você está prestes a resetar a senha de um Aluno! </p> 
						<p>Com esta alteração acontecerão as seguintes ações:</p>
						<ul>
							<li>O aluno não poderá acessar o sistema por sua antiga senha.</li>
							<li>O aluno receberá em seu email uma nova senha.</li>
						</ul>
						<p class="text-danger">As alterações acima citadas são irreversíveis.</p>
						<h4 align="CENTER">Está certo disto?</h4>								
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
						<button type="button" class="btn btn-danger" id="btn_reset_senha" onclick="" data-dismiss="modal" aria-label="Close">Sim, resetar a Senha!</button>
					</div>
				</div>
			</div>
		</div>
		
	</div>	
</div>
<script type="text/javascript">
function resetar_senha(id){
	jQuery.ajax({
	           type: "POST",
	           url: "<?php echo base_url(); ?>" + "index.php/usuario/reset_senha_usuario",
	           dataType: 'json',
	           data: {id_usuario:id},
	           success: function(res) {
	           		mensagem('success',"Senha resetada com sucesso!");
	           }
    });
	
}

function abrir_reset(id){
	$("#btn_reset_senha").attr("onclick","resetar_senha("+id+")");
	$("#reset_modal").modal();
}


$(document).ready(function () {
	mascara();
	$('[data-toggle="tooltip"]').tooltip();		
	$("#buscar").click(function(event){
	    event.preventDefault();	               
	    var cpf = $("input[type=text][name=cpf]").val();
	    var nome = $("input[type=text][name=nome]").val(); 
	    jQuery.ajax({
	           type: "POST",
	           url: "<?php echo base_url(); ?>" + "index.php/aluno/busca",
	           dataType: 'json',
	           data: {cpf: cpf, nome:nome},
	           success: function(res) {
	           	var rows = $('<table id="alunos" class="table table-hover table-bordered">');
	           	rows.append('<thead><tr><th>Nome</th>\
				<th>CPF</th>\
				<th>Status da Matrícula</th>\
				<th>Curso</th>\
				<th>Turma</th>\
				<th>Opções</th></tr></thead>');
	           	if(res.err == "ok"){	               
	               		for(var i in res.useralunos){
	               			var name_modal = '#'+res.useralunos[i].idusario;
	               			var modal = '<div class="modal fade" id="'+res.useralunos[i].idusuario+'" tabindex="-1" role="dialog" aria-labellby="myModalLabel">';
	               			modal+='<div class="modal-dialog" role="document"><div class="modal-content">';
	               			modal+='<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3 class="modal-title text-danger" id="myModalLabel" align="CENTER">Atenção <i class="fa fa-exclamation-triangle animated tada infinite" aria-hidden="true"></i></h3></div>';
	               			modal+='<div class="modal-body"><p align="CENTER">Você está prestes a remover o Aluno: </p><h4 align="CENTER">'+res.useralunos[i].nome_usuario+'</h4><p>Os seguintes itens relacionados também serão afetados:</p><ul><li>A matrícula ativa para este aluno será cancelada, se ele possuir.</li></ul><p class="text-danger">As alterações acima citadas são irreversíveis.</p><h4 align="CENTER">Está certo disto?</h4></div>';
	               			var editar = "<?php echo base_url(); ?>" + "index.php/aluno/edita/"+res.useralunos[i].idusuario;
	               			var remover = "<?php echo base_url(); ?>" + "index.php/aluno/remove/"+res.useralunos[i].idusuario;
	               			var matricular = "<?php echo base_url(); ?>" + "index.php/aluno/matricula/"+res.alunos[res.useralunos[i].idusuario]+"/aluno";
	               			var aprovar = "<?php echo base_url(); ?>" + "index.php/aprovacao/novo/"+res.alunos[res.useralunos[i].idusuario];
	               			
	               			var newRow = $('<tr class="animated fadeInDown">');
	               			var cols = "";
	               			if (res.useralunos[i].nome_social_usuario!=null)
	               			     cols +='<td>'+res.useralunos[i].nome_social_usuario+'</td>';
	               			else cols +='<td>'+res.useralunos[i].nome_usuario+'</td>';
	               			cols +='<td>'+res.useralunos[i].cpf_usuario+'</td>';
	               			cols +='<td>'+res.matricula[res.useralunos[i].idusuario]+'</td>';
	               			cols +='<td>'+res.curso[res.useralunos[i].idusuario]+'</td>';
	               			cols +='<td>'+res.turma[res.useralunos[i].idusuario]+'</td>';	               			
	               			cols += '<td><a href="'+editar+'" data-toggle="tooltip" data-placement="top" title="Editar"><button type="button" class="btn btn-default"><i class="fa fa-pencil-square-o"></i></button></a>&ensp;<a href="#" data-toggle="tooltip" data-placement="top" title="Remover"><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#'+res.useralunos[i].idusuario+'"><i class="fa fa-trash"></i></button></a>&ensp;<a href="'+matricular+'" data-toggle="tooltip" data-placement="top" title="Informações da matrícula"><button type="button" class="btn btn-primary"><i class="fa fa-info-circle" aria-hidden="true"></i></button></a>&ensp;<a href="'+aprovar+'" data-toggle="tooltip" data-placement="top" title="Aprovação"><button type="button" class="btn btn-success"><i class="fa fa-trophy"></i></button></a>&ensp;<button type="button" class="btn btn-default" title= "Resetar Senha" onclick="abrir_reset('+res.useralunos[i].idusuario+');"><i class="fa fa-key" aria-hidden="true"></i></button></td>';
	               			
	               			modal+='<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button><button type="button" class="btn btn-danger"  onclick=location.href="'+remover+'">Sim, remover o Aluno</button></div></div></div></div>';
	               			newRow.append(cols);	               			
	               			rows.append(newRow);
	               			$("#modals").append(modal); 	               			               		
	               		}
	               		$("#tabela_alunos").html(rows);
	               		tabela('alunos',5);               		

	               }else{
	               	mensagem('error',res.err);
	               	$("#tabela_alunos").html('');
	               	$("#modals").html('');
	               }
				
	           }
	       });  
	});
	$("#voltar").click(function(event){
    	window.location.href = "<?php echo base_url(); ?>"+"index.php/aluno/opcoes";  
	});
});
</script>