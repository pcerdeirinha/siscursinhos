<?php
class Departamento_model extends CI_Model {
	
	public function get($faculdade){
		$this->db->where('depto_idfaculdade', $faculdade);
		$get = $this->db->get('departamento');
		if($get->num_rows > 0) return $get->result_array();
	    return array();
	}

	public function getDep($id){
		$this->db->where('iddepartamento', $id);
		$get = $this->db->get('departamento');
		if($get->num_rows > 0) return $get->row_array();
	    return array();
	}
	
    public function create($data)
    {
        return $this->db->insert('departamento', $data); 
    }
    
    public function delete($id)
    {
        $this->db->where('iddepartamento', $id);
        $this->db->delete('departamento');
    }
    
}
?>