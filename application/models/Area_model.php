<?php
class Area_model extends CI_Model {
    
    public function getArea($idArea)
    {
        $this->db->select('*');
        $this->db->from('area');
        $this->db->where('idarea',$idArea);
        return $this->db->get()->row_array();
    }    
    
    
    public function getAreas()
    {
        $this->db->select('*');
        $this->db->from('area');
        $this->db->order_by('grande_area_idgrande_area,nome_area','ASC');
        return $this->db->get()->result_array();
    }
        
    public function getGrandeArea($idgrande_area)
    {
        $this->db->select('*');
        $this->db->from('grande_area');
        $this->db->where('idgrande_area',$idgrande_area);
        return $this->db->get()->row_array();        
    }
    
    public function getGrandesAreas()
    {
        $this->db->select('*');
        $this->db->from('grande_area');
        return $this->db->get()->result_array();        
    }
    
    public function getAreasGrande($idgrande_area)
    {
        $this->db->select('*');
        $this->db->from('area');
        $this->db->where('grande_area_idgrande_area',$idgrande_area);
        return $this->db->get()->result_array();
    }
    
    public function getAreasGrandeDisp($idgrande_area,$idcurso)
    {
        $this->db->select('*');
        $this->db->from('area');
        $this->db->where('grande_area_idgrande_area',$idgrande_area);
        $this->db->join('disciplina','area_disciplina = idarea');
        $this->db->where('disciplina.status = 1');
        $this->db->where('disciplina.disciplina_idcurso',$idcurso);
        return $this->db->get()->result_array();
    }
        
}
?>
    