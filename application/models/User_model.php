<?php
class User_model extends CI_Model {
	private $salt = 'r4nd0m';
	public function get($id = false)
	{
	    if ($id) $this->db->where('idusuario', $id);
	    $this->db->order_by('nome_usuario', 'asc');
	    $get = $this->db->get('usuario');
	    if($id) return $get->row_array();
	    if($get->num_rows > 0) return $get->result_array();
	    return array();
	}
    
    public function getUser($cpf)
    {
        $this->db->where('cpf_usuario', $cpf);
        $get = $this->db->get('usuario');
        if($get->num_rows > 0) return $get->result_array();
        return null;
        
    }
    
    
    public function getUserValid($cpf,$unidade)
    {
        $this->db->select('*')
                 ->from('usuario')
                ->where('usuario.cpf_usuario', $cpf)
                ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario','left')
                ->group_start()
                ->where('unidade_usuario.unidade_idunidade !=', $unidade)
                ->or_where('usuario.status',0)
                ->group_end();
        //$get = $this->db->get_compiled_select();
        $get = $this->db->get();
        if($get->num_rows > 0) return $get->result_array();
        return null;
        //return $get;
    }
    
    public function getUserEmail($email)
    {
        $this->db->where('email_usuario', $email);
        $get = $this->db->get('usuario');
        if($get->num_rows > 0) return $get->row_array();
        return null;
        
    }
    
    public function getDocentesFaculdade($idfaculdade){
        $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->join('unidade','unidade.idunidade = unidade_usuario.unidade_idunidade')
                 ->where('unidade.unidade_idfaculdade',$idfaculdade)
                 ->where('usuario.status',1)
                 ->where('unidade_usuario.tipo_usuario',5);
        return $this->db->get()->result_array();
    }

	public function validate($email, $password){
	    //$this->db->where('email_usuario', $email)->where('senha_usuario', $password);//sha1($password.$this->salt));
	    $this->db->where('email_usuario', $email)->where('senha_usuario', sha1($password.$this->salt));
	    $get = $this->db->get('usuario');
	 
	    if($get->num_rows > 0) return $get->row_array(); //se achou user o numero de coluna vai ser maior que zero
		else {
			$this->db->where('cpf_usuario', $email)->where('senha_usuario', sha1($password.$this->salt));
		    $get = $this->db->get('usuario');
		 
		    if($get->num_rows > 0) return $get->row_array();
				else return 0;
		}
	}
    
    public function unidades_usuario($id_user)
    {
        $this->db->select('*')
                 ->from('unidade_usuario')
                 ->join('unidade','unidade_usuario.unidade_idunidade = unidade.idunidade')
                 ->where('unidade_usuario.usuario_idusuario',$id_user);
        $unidade_usuario = $this->db->get()->result_array();
        return $unidade_usuario;
    }
    
    public function userEstaNaUnidade($id_user,$id_unidade)
    {
        $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('usuario.idusuario',$id_user)
                 ->where('unidade_usuario.unidade_idunidade',$id_unidade);
        $get = $this->db->get();
        if ($get->num_rows > 0) return $get->row_array();
        else return FALSE;
                 
    }
    
	public function existeEmail($userEmail){
		$query = $this->db->get_where("usuario",array("email_usuario"=>$userEmail));
			if ($query->num_rows() > 0) {
   				return true;
   			}else return false;
	}

	public function create($data)
	{
		$data['senha_usuario'] = sha1($data['senha_usuario'].$this->salt);
		$this->db->insert('usuario', $data);
		$id = $this->db->insert_id();
	    return  $id;

	}

	public function getAlunos($unidade){
	    
        $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('unidade_usuario.unidade_idunidade',$unidade)
                 ->where('unidade_usuario.tipo_usuario',1)
                 ->where('usuario.status',1)
                 ->order_by('usuario.nome_usuario','asc');
	    $get = $this->db->get();
	    if($get->num_rows > 0) return $get->result_array();
	    return array();
	}

    public function getMonitoresUnidade($idUnidade){
        $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('unidade_usuario.unidade_idunidade',$unidade)
                 ->where('unidade_usuario.tipo_usuario >',1)
                 ->where('unidade_usuario.tipo_usuario <',5)
                 ->where('usuario.status',1)
                 ->order_by('usuario.nome_usuario','asc');
        return $this->db->get()->result_array();
    }

	public function getUsuariosCPF($cpf, $unidade, $tipo){
	    $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('unidade_usuario.tipo_usuario',$tipo)
                 ->where('unidade_usuario.unidade_idunidade',$unidade)
                 ->where('usuario.cpf_usuario',$cpf)
                 ->where('usuario.status',1)
                 ->order_by('usuario.nome_usuario',asc);
		$get = $this->db->get();
		if($get->num_rows > 0) return $get->result_array();
		return array();
	}

	public function getUsuariosNome($nome, $unidade, $tipo){
	           $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('unidade_usuario.tipo_usuario',$tipo)
                 ->where('unidade_usuario.unidade_idunidade',$unidade)
                 ->like('usuario.nome_usuario',$nome)
                 ->where('usuario.status',1)
                 ->order_by('usuario.nome_usuario',asc);
		$get = $this->db->get();
		if($get->num_rows > 0) return $get->result_array();
		return array();
	}


    public function getUsuariosNomeEmail($nome,$unidade,$users,$nivel){
        $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->join('unidade','unidade_usuario.unidade_idunidade = unidade.idunidade');
                 if($nivel==4){
                     $this->db->group_start()
                              ->where('unidade_usuario.tipo_usuario',4)
                              ->or_where('unidade_usuario.unidade_idunidade',$unidade)
                              ->group_end();
                 }
                 else if($nivel==5){
                     $this->db->group_start()
                              ->where('unidade_usuario.tipo_usuario',5)
                              ->or_where('unidade_usuario.unidade_idunidade',$unidade)
                              ->group_end();
                 }
                 else if ($nivel!=6){
                     $this->db->where('unidade_usuario.unidade_idunidade',$unidade);
                 }
                 $this->db->group_start()
                 ->like('usuario.nome_usuario',$nome)
                 ->or_like('usuario.email_usuario',$nome)
                 ->group_end()
                 ->where('usuario.status',1)
                 ->where_not_in('usuario.idusuario',$users)
                 ->limit(5)
                 ->order_by('usuario.nome_usuario','ASC')
                 ->group_by('usuario.idusuario');
        return $this->db->get()->result_array();
    }

	public function getUsuariosNomeCPF($nome,$cpf, $unidade, $tipo){
        $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('unidade_usuario.tipo_usuario',$tipo)
                 ->where('unidade_usuario.unidade_idunidade',$unidade)
                 ->where('usuario.cpf_usuario',$cpf)
                  ->like('usuario.nome_usuario',$nome)
                 ->where('usuario.status',1)
                 ->order_by('usuario.nome_usuario',asc);
		$get = $this->db->get();
		if($get->num_rows > 0) return $get->result_array();
		return array();

	}

    public function createUnidadeUsuario($data)
    {
        $this->db->replace('unidade_usuario',$data);
        return null;        
    }
    
    public function removeUnidadeUsuario($data)
    {
        $this->db->delete('unidade_usuario',$data);
        return null;
    }

	public function getFuncionariosTipo($unidade,$tipo){
	    $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('unidade_usuario.unidade_idunidade',$unidade)
                 ->where('unidade_usuario.tipo_usuario',$tipo)
                 ->where('usuario.status',1)
                 ->order_by('nome_usuario','asc');
		$get = $this->db->get();
		if($get->num_rows > 0) return $get->result_array();
		return array();
	}

	public function update($id, $data){
		if(isset($data['senha_usuario']))
			$data['senha_usuario'] = sha1($data['senha_usuario'].$this->salt);
		$this->db->where('idusuario', $id);
		$update = $this->db->update('usuario', $data);
		return $id;
	}
    
    
    public function updateCPF($data){
        if(isset($data['senha_usuario']))
            $data['senha_usuario'] = sha1($data['senha_usuario'].$this->salt);
        $this->db->where('cpf_usuario', $data['cpf_usuario']);
        $this->db->update('usuario', $data);
        
        return $this->db->select('idusuario')->where('cpf_usuario', $data['cpf_usuario'])->get('usuario')->row()->idusuario;
    }
    
    public function delete($idusuario){
        $this->db->where('idusuario', $idusuario);
        $this->db->set('status',0);
        $this->db->update('usuario');
    }
    
    public function delete_unidade($idser,$idunidade)
    {
        
        $this->db->where('usuario_idusuario',$idser)
                 ->where('unidade_idunidade',$idunidade)
                 ->delete('unidade_usuario');   
    }

    public function getAllDocentes()
    {
       $this->db->select('*')
                ->from('usuario')
                ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                ->where('unidade_usuario.tipo_usuario',5)
                ->where('usuario.status', 1)
                ->order_by('usuario.nome_usuario', 'asc')
                ->group_by('usuario.idusuario');
        $get = $this->db->get();
        if($get->num_rows > 0) return $get->result_array();
        return array();   
    }
    
    public function monitorNaUnidade($iduser, $idunidade)
    {
        $this->db->select('*')
                 ->from('unidade_usuario')
                 ->where('unidade_idunidade',$idunidade)
                 ->where('usuario_idusuario',$iduser);
       return $this->db->get()->row_array();
    }

    public function getAllUsersUnidade($idUnidade){
        $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('unidade_usuario.unidade_idunidade',$idUnidade)
                 ->where('unidade_usuario.tipo_usuario <',5);
        return ($this->db->get()->result_array());
    }
    
    public function getAllUsers(){
        $this->db->select('*')
                 ->from('usuario')
                 ->where('status',1);
       return $this->db->get()->result_array();
    }

    public function getAllSupervisores(){
        $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('unidade_usuario.tipo_usuario',6);
       return $this->db->get()->result_array();
    }
    
    public function removeUnidadesUser($iduser)
    {
        $this->db->delete('unidade_usuario',array('usuario_idusuario' => $iduser));        
    }
}