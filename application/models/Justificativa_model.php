<?php
class Justificativa_model extends CI_Model {

	public function get($id){
		if($id)$this->db->where('idjustificativa', $id);	    
	    $get = $this->db->get('justificativa');
	    if($id) return $get->row_array();
	    if($get->num_rows > 0) return $get->result_array();
	    return array();
	}

	public function getJustMatricula(){
		$this->db->where('idjustificativa >', 9);		
	    $get = $this->db->get('justificativa');	    
	    if($get->num_rows > 0) return $get->result_array();
	    return array();
	}

	
}