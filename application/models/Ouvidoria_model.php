<?php
class Ouvidoria_model extends CI_Model {
	
		public function get_manifestacao($id_manifestacao)
		{
			$this->db->select('*')
					 ->from('ouvidoria_manifestacao')
					 ->where('ouvidoria_manifestacao.idmanifestacao',$id_manifestacao)
					 ->join('ouvidoria_assunto','ouvidoria_assunto.idassunto = ouvidoria_manifestacao.ouvidoria_assunto_idassunto','left');
					 
			return $this->db->get()->row_array();
			
		}
		
		public function get_representantes($nivel)
		{
			$this->db->select('*')
					 ->from('ouvidoria_representante')
					 ->where($nivel.' >=nivel_minimo_representante');
			//return $this->db->get_compiled_select();
			return $this->db->get()->result_array();
		}
		
		public function get_tipos()
		{
			$this->db->select('*')
					 ->from('ouvidoria_tipo');
			return $this->db->get()->result_array();
		}
		
		public function get_ambientes($nivel)
		{
			$this->db->select('*')
					 ->from('ouvidoria_ambiente')
					 ->where($nivel.' >=nivel_minimo_ambiente');
			//return $this->db->get_compiled_select();
			return $this->db->get()->result_array();
		}
		
		public function get_assuntos($idambiente)
		{
			$this->db->select('*')
					 ->from('ouvidoria_assunto')
					 ->where('ouvidoria_ambiente_idambiente',$idambiente);
			//return $this->db->get_compiled_select();
			return $this->db->get()->result_array();
		}
		
		
		public function get_manifestacoes_enviadas($iduser)
		{
			$this->db->select('*')
					 ->from('ouvidoria_manifestacao')
					 ->where('ouvidoria_manifestacao.usuario_idusuario_remetente',$iduser)
					 ->join('ouvidoria_assunto','ouvidoria_assunto.idassunto = ouvidoria_manifestacao.ouvidoria_assunto_idassunto','left')
					 ->join('ouvidoria_representante','ouvidoria_representante.idrepresentante = ouvidoria_manifestacao.ouvidoria_representante_idrepresentante')
					 ->join('ouvidoria_tipo','ouvidoria_tipo.idtipo = ouvidoria_manifestacao.ouvidoria_tipo_idtipo');
			return $this->db->get()->result_array();
			//return $this->db->get_compiled_select();	 
		}
		
		public function create_manifestacao($data)
		{
			$this->db->insert('ouvidoria_manifestacao',$data);
		}
		
		public function update_manifestacao($id,$data)
		{
			$this->db->where('idmanifestacao', $id);
			$this->db->update('ouvidoria_manifestacao',$data);
		}
		
}
?>
	