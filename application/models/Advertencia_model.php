<?php 
class Advertencia_model extends CI_Model {

    public function get($idadvertencia)
    {
        $this->db->where('idadvertencia',$idadvertencia);
        return $this->db->get('advertencia')->row_array();
    }

	public function getAdvertencias($usuario){
		$this->db->where('usuario_idusuario', $usuario);
		$get = $this->db->get('advertencia');
	   	if($get->num_rows > 0) return $get->result_array();
	   	return array();
	}

	public function getAdvAll($idunidade){
		$this->db->where('usuario_unidade_idunidade', $idunidade);
		$get = $this->db->get('advertencia');
		if($get->num_rows > 0) return $get->result_array();
		return array();
	}

	public function create($data){
		return $this->db->insert('advertencia', $data); 
	}

	public function update($id,$data){
		$this->db->where('idadvertencia', $id);
		$update = $this->db->update('advertencia', $data);
		return $update;
	}
	public function delete($idadvertencia){
		$this->db->where('idadvertencia', $idadvertencia);
		$this->db->delete('advertencia');
	}
}


 ?>