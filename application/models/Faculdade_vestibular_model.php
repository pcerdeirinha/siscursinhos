<?php
class Faculdade_vestibular_model extends CI_Model {

	public function create($data)
	{
		return $this->db->insert('faculdade_vestibular', $data);
	}

	public function getFaculdade($id){
		$this->db->where('idFaculdade', $id);
		$get = $this->db->get('faculdade_vestibular');
		if($get->num_rows > 0) return $get->row_array();
	    return array();
	}

	public function getFaculdades()	{
	    $get = $this->db->get('faculdade_vestibular');
	    if($get->num_rows > 0) return $get->result_array();
	    return array();
	}
    

	
}
?>