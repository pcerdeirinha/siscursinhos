<?php
class Disciplina_model extends CI_Model {

    public function getDisciplinasCurso($curso)
    {
        $this->db->where('disciplina_idcurso', $curso);
        $this->db->order_by('iddisciplina', 'asc');
        $get = $this->db->get('disciplina');
        if($get->num_rows > 0) return $get->result_array();
        return array();

    }
    
    public function getDisciplinasUnidade($unidade) 
    {
        $this->db->select('disciplina.iddisciplina,disciplina.nome_disciplina,disciplina.area_disciplina,disciplina.disciplina_idcurso');
        //$this->db->from('disciplina');
        $this->db->join('curso', 'curso.idcurso = disciplina.disciplina_idcurso');
        $this->db->where('curso.unidade_idunidade', $unidade);
        $this->db->where('disciplina.status',1);
        $get = $this->db->get('disciplina');
        if($get->num_rows > 0) return $get->result_array();
        return array();

    }
    
    public function getDisciplinasArea($area,$curso)
    {
        $this->db->where('disciplina_idcurso', $curso);
        $this->db->where('area_disciplina', $area);
        $this->db->where('status', 1);
        $get = $this->db->get('disciplina');
        if($get->num_rows > 0) return $get->result_array();
        return array();

    }
    

    public function getDisciplina($iddisciplina){
        $this->db->where('iddisciplina', $iddisciplina);
        $get = $this->db->get('disciplina');
        return $get->row_array();
    }

    public function create($data){
    	return $this->db->insert('disciplina', $data); 
    }

    public function update($id,$data){
        $this->db->where('iddisciplina', $id);
        $update = $this->db->update('disciplina', $data);
        return $update;
    }



	public function delete($iddisciplina){
		$this->db->where('iddisciplina', $iddisciplina);
        $this->db->set('status',0);
		$this->db->update('disciplina');
	}
}