<?php
class Resultado_model extends CI_Model {
    public function get_resultados()
    {
        return $this->db->get('resultado')->result_array();
    }
    
    public function get_filtros($id_resultado)
    {
        $this->db->select('*')
                 ->from('resultado_filtro')
                 ->where('resultado_idresultado',$id_resultado);
        return $this->db->get()->result_array();
    }
    
    public function get_agrupamentos($id_resultado)
    {
        $this->db->select('*')
                 ->from('resultado_agrupamento')
                 ->where('resultado_idresultado',$id_resultado);
        return $this->db->get()->result_array();
    }
    
    public function get_socios($id_resultado)
    {
        $this->db->select('*')
                 ->from('resultado_socio')
                 ->where('resultado_idresultado',$id_resultado);
        return $this->db->get()->result_array();
    }
    
    public function gerar_resultado($id_resultado,$id_filtro,$id_agrupamento,$ids_socio, $id_unidade, $ano)
    {
        
        $this->db->select('*')
                 ->from('resultado_select_join')
                 ->where('resultado_select_join.resultado_select_idresultado_select',$id_resultado);
        $join_sel = $this->db->get()->result_array();
         //return $join_sel; 
        $this->db->select('*')
                 ->from('resultado_select')
                 ->where('resultado_select.resultado_idresultado',$id_resultado);
                 
        if ($id_filtro!=null){
            $this->db->join('resultado_select_filtro','resultado_select_filtro.resultado_filtro_idresultado_filtro = '.$id_filtro);
        }

        $sel = $this->db->get()->row_array();
        
        if ($id_agrupamento!=null){
            $this->db->select('*')
                     ->from('resultado_select_agrupamento')
                     ->join('resultado_select_join_agrupamento','resultado_select_agrupamento.resultado_agrupamento_idresultado_agrupamento = resultado_select_join_agrupamento.resultado_select_agrupamento_idresultado')
                     ->where('resultado_select_agrupamento.resultado_agrupamento_idresultado_agrupamento',$id_agrupamento);
            $agr = $this->db->get()->result_array();   
            $this->db->select('*')
                     ->from('resultado_select_agrupamento')
                     ->where('resultado_select_agrupamento.resultado_agrupamento_idresultado_agrupamento',$id_agrupamento);
            $agr_sel = $this->db->get()->row_array();
			//return $this->db->get_compiled_select();
			//return $agr_sel;
        }
		
        
        if ($ids_socio!=null){
            $this->db->select('*')
                     ->from('resultado_socio_resultado_select')
                     ->where('resultado_idresultado', $id_resultado);
            $join_socio = $this->db->get()->row_array();
            $joins_socio = array();
            foreach ($ids_socio as $id_socio){
                $this->db->select('*')
                         ->from('resultado_select_socio')
                         ->where('resultado_socio_idresultado_socio',$id_socio);
                array_push($joins_socio, $this->db->get()->row_array());
            }
            //return $joins_socio;
        } 
        
        $gb = '';
        $sel_args = $sel['resultado_selects'];
        
        $sel['resultado_select_where'] = str_replace(':ano', $ano, $sel['resultado_select_where']);
        $sel['resultado_select_where'] = str_replace(':unidade', $id_unidade, $sel['resultado_select_where']);
        
        $this->db->from($sel['resultado_select_from'])
                 ->where($sel['resultado_select_where']);
                 
        foreach ($join_sel as $join) {
            $this->db->join($join['resultado_select_join_join'],$join['resultado_select_join_on']);
        }
        if ($id_filtro!=null){
            $this->db->join($sel['resultado_select_filtro_join'],$sel['resultado_select_filtro_on']);
            $gb .= $gb==''?$sel['resultado_select_filtro_group_by']:','.$sel['resultado_select_filtro_group_by'];
            $sel_args.=','.$sel['resultado_select_filtro_select_add'];
        } 
        
        if ($id_agrupamento!=null){
            $sel_args.=','.$agr_sel['resultado_select_agrupamento_select_add'];
            if ($agr_sel['resultado_select_agrupamento_where_add']!=null){
                $this->db->where($agr_sel['resultado_select_agrupamento_where_add']);
            }
            $gb .= $gb==''?$agr_sel['resultado_select_agrupamento_group_by']:','.$agr_sel['resultado_select_agrupamento_group_by'];
            foreach ($agr as $group) {
                $this->db->join($group['resultado_select_join_agrupamento_join'],$group['resultado_select_join_agrupamento_on_join']);
            }
        }

        if ($ids_socio!=null){
            $this->db->join('aluno',$join_socio['resultado_socio_resultado_select_on_join']);
            foreach ($joins_socio as $socio) {
                if ($socio['resultado_select_socio_left_join']!=null)
                    $this->db->join($socio['resultado_select_socio_left_join'], $socio['resultado_select_socio_on_left_join'],'left');
                $sel_args.=','.$socio['resultado_select_socio_select'];
                $gb .= $gb==''?$socio['resultado_select_socio_group_by']:','.$socio['resultado_select_socio_group_by'];
            }
        }

        
        $this->db->select($sel_args);
        $this->db->group_by($gb);
        return $this->db->get()->result_array();
        //return $this->db->get_compiled_select();         
                 
    }
}
?>