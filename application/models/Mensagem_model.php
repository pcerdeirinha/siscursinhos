<?php
class Mensagem_model extends CI_Model {
    public function obterUltimasMensagensUsuario($id_user){
        $this->db->select('*')
                 ->from('recado_mensagem')
                 ->order_by('idrecado','DESC');
        
        $query = $this->db->get_compiled_select();
        
        $this->db->select('idrecado')
                 ->from("($query) as a")
                 ->group_by('mensagem_idmensagem');
        $query = $this->db->get_compiled_select();
            
        $this->db->select('mensagem.idmensagem,mensagem.assunto_mensagem,usuario.nome_usuario')
                ->from('mensagem')
                ->join('destinatario_mensagem','mensagem.idmensagem = destinatario_mensagem.mensagem_idmensagem')
                ->join('recado_mensagem','recado_mensagem.mensagem_idmensagem = mensagem.idmensagem')
                ->join('usuario','usuario.idusuario = recado_mensagem.remetente_idusuario')
                ->where('destinatario_mensagem.usuario_idusuario',$id_user)
                ->where("recado_mensagem.idrecado in ($query)")
                ->where('destinatario_mensagem.status',1)
                ->order_by('recado_mensagem.idrecado','DESC');
        return $this->db->get()->result_array();
    }

    public function obterUltimasMensagensEnviadas($id_user){
        $this->db->select('*')
                 ->from('recado_mensagem')
                 ->order_by('idrecado','DESC');
        
        $query = $this->db->get_compiled_select();
        
        $this->db->select('idrecado')
                 ->from("($query) as a")
                 ->group_by('mensagem_idmensagem');
        $query = $this->db->get_compiled_select();
            
        $this->db->select('*')
                ->from('mensagem')
                ->join('recado_mensagem','recado_mensagem.mensagem_idmensagem = mensagem.idmensagem','left')
                ->join('resposta_recado','resposta_recado.recado_idrecado = recado_mensagem.idrecado','left')
                ->join('usuario','usuario.idusuario = recado_mensagem.remetente_idusuario')
                ->group_start()
                ->where('recado_mensagem.remetente_idusuario',$id_user)
                ->or_where('resposta_recado.remetente_idusuario',$id_user)
                ->group_end()
                ->where("recado_mensagem.idrecado in ($query)")
                ->order_by('recado_mensagem.idrecado','DESC');
        return $this->db->get()->result_array();
    }

    public function novaMensagem($data){
        $this->db->insert('mensagem',$data);
        return $this->db->insert_id();
    }
    
    public function novoRecado($data){
        $this->db->insert('recado_mensagem',$data);
        return $this->db->insert_id();
    }
    
    public function novaRespostaRecado($data){
        $this->db->insert('resposta_recado',$data);
        return $this->db->insert_id();    
    }
    
    public function novoDestinatario($data){
        $orig_db_debug = $this->db->db_debug;

        $this->db->db_debug = FALSE;
        $this->db->insert('destinatario_mensagem',$data);
        
        $this->db->db_debug=$orig_db_debug;
        return $this->db->insert_id();
    }
    
    public function getAllRecados($id_mensagem, $id_user){
        $this->db->select('*')
                 ->from('recado_mensagem')
                 ->join('destinatario_mensagem','destinatario_mensagem.mensagem_idmensagem = recado_mensagem.mensagem_idmensagem')
                 ->join('usuario','recado_mensagem.remetente_idusuario = usuario.idusuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->join('mensagem','mensagem.idmensagem = recado_mensagem.mensagem_idmensagem')
                 ->where('recado_mensagem.mensagem_idmensagem',$id_mensagem)
                 ->group_start()
                 ->where('destinatario_mensagem.usuario_idusuario',$id_user)
                 ->or_where('recado_mensagem.remetente_idusuario',$id_user)
                 ->group_end()
                 ->group_by('recado_mensagem.idrecado');
        //echo $this->db->get_compiled_select();
        return $this->db->get()->result_array();
    }
    
    public function getAllRespostas($id_recado,$id_user){
        $this->db->select('*')
                 ->from('resposta_recado')
                 ->join('recado_mensagem','resposta_recado.recado_idrecado = recado_mensagem.idrecado')
                 ->join('usuario','resposta_recado.remetente_idusuario = usuario.idusuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('resposta_recado.recado_idrecado',$id_recado)
                 ->group_start()
                 ->where('resposta_recado.publica_resposta',1)
                 ->or_where('recado_mensagem.remetente_idusuario',$id_user)
                 ->or_where('resposta_recado.remetente_idusuario',$id_user)
                 ->group_end()
                 ->group_by('resposta_recado.idresposta_recado');
        return ($this->db->get()->result_array());
    }
    
    public function getPrimeiroRecado($id_mensagem, $id_user){
        $this->db->select('*')
                 ->from('recado_mensagem')
                 ->join('mensagem','mensagem.idmensagem = recado_mensagem.mensagem_idmensagem')
                 ->join('destinatario_mensagem','destinatario_mensagem.mensagem_idmensagem = recado_mensagem.mensagem_idmensagem')
                 ->join('usuario','recado_mensagem.remetente_idusuario = usuario.idusuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('recado_mensagem.mensagem_idmensagem',$id_mensagem)
                 ->where('destinatario_mensagem.usuario_idusuario',$id_user)
                 ->order_by('recado_mensagem.idrecado','ASC')
                 ->limit(1);
        return $this->db->get()->row_array();
    }

    public function getRecado($id_recado,$id_user){
        $this->db->select('*')
                 ->from('recado_mensagem')
                 ->join('destinatario_mensagem','destinatario_mensagem.mensagem_idmensagem = recado_mensagem.mensagem_idmensagem')
                 ->where('recado_mensagem.idrecado',$id_recado)
                 ->group_start()
                 ->where('destinatario_mensagem.usuario_idusuario',$id_user)
                 ->or_where('recado_mensagem.remetente_idusuario',$id_user)
                 ->group_end();
        return $this->db->get()->row_array();
    }

    
    public function updateRecadoVisualizado($id_mensagem, $id_user, $data){
        $this->db->where('mensagem_idmensagem',$id_mensagem)
                 ->where('usuario_idusuario',$id_user)
                 ->update('destinatario_mensagem',$data);
    }
    
    public function exibirMensagens($id_mensagem){
        $this->db->where('mensagem_idmensagem',$id_mensagem)
                 ->set('status',1)
                 ->update('destinatario_mensagem');
        
    }

    public function obterNumeroRecadosNaoLidos($id_user){
       $this->db->select('COUNT(`idmensagem`) as count,mensagem.idmensagem')
                 ->from('mensagem')
                 ->join('destinatario_mensagem','destinatario_mensagem.mensagem_idmensagem = mensagem.idmensagem')
                 ->join('recado_mensagem','recado_mensagem.mensagem_idmensagem = mensagem.idmensagem')
                 ->where('destinatario_mensagem.usuario_idusuario',$id_user)
                 ->where('destinatario_mensagem.status',1)
                 ->group_start()
                 ->where('destinatario_mensagem.recado_visualizado_idrecado',NULL)
                 ->or_where('recado_mensagem.idrecado > destinatario_mensagem.recado_visualizado_idrecado')
                 ->group_end()
                 ->group_by('mensagem.idmensagem');      
        //echo $this->db->get_compiled_select();
        return $this->db->get()->result_array();
    }

    public function obterNumeroRespostasNaoLidas($id_user){
       $this->db->select('COUNT(`idmensagem`) as count,mensagem.idmensagem')
                 ->from('mensagem')
                 ->join('destinatario_mensagem','destinatario_mensagem.mensagem_idmensagem = mensagem.idmensagem')
                 ->join('recado_mensagem','recado_mensagem.mensagem_idmensagem = mensagem.idmensagem')
                 ->join('resposta_recado','recado_mensagem.idrecado = resposta_recado.recado_idrecado')
                 ->where('destinatario_mensagem.usuario_idusuario',$id_user)
                 ->where('destinatario_mensagem.status',1)
                 ->group_start()
                 ->where('destinatario_mensagem.resposta_visualizada_idresposta',NULL)
                 ->or_where('resposta_recado.idresposta_recado > destinatario_mensagem.resposta_visualizada_idresposta')
                 ->group_end()
                 ->group_start()
                 ->where('resposta_recado.publica_resposta',1)
                 ->or_where('recado_mensagem.remetente_idusuario',$id_user)
                 ->group_end()
                 ->group_by('mensagem.idmensagem');      
        //echo $this->db->get_compiled_select();
        return $this->db->get()->result_array();
    }

    public function obterMensagensUsuarios($data,$id_user){
        $this->db->select('*')
                 ->from('recado_mensagem')
                 ->order_by('idrecado','DESC');
        
        $query = $this->db->get_compiled_select();
        
        $this->db->select('idrecado')
                 ->from("($query) as a")
                 ->group_by('mensagem_idmensagem');
        $query = $this->db->get_compiled_select();
         
            
        $this->db->select('a.mensagem_idmensagem as idmensagem,mensagem.assunto_mensagem,usuario.nome_usuario')
                 ->from('destinatario_mensagem as a')
                 ->join('destinatario_mensagem as b','a.mensagem_idmensagem = b.mensagem_idmensagem')
                 ->join('recado_mensagem','a.mensagem_idmensagem = recado_mensagem.mensagem_idmensagem')
                 ->join('resposta_recado','recado_mensagem.idrecado = resposta_recado.recado_idrecado','left')
                 ->join('mensagem','a.mensagem_idmensagem = mensagem.idmensagem')
                 ->join('usuario','recado_mensagem.remetente_idusuario = usuario.idusuario')
                 ->group_start()
                 ->where_in('a.usuario_idusuario',$data)
                 ->or_where_in('recado_mensagem.remetente_idusuario',$data)
                 ->group_end()
//                 ->group_start()
                 ->where('b.usuario_idusuario',$id_user)
//                 ->or_where('recado_mensagem.remetente_idusuario',$id_user)
//                 ->group_end()
                 ->where('b.status',1)
                 ->where('a.status',1)
                 ->where("recado_mensagem.idrecado in ($query)")
                 ->order_by('recado_mensagem.idrecado DESC')
                 ->group_by('a.mensagem_idmensagem');
        return $this->db->get()->result_array();
    }

    public function obterMembrosMensagem($id_mensagem){
        $this->db->select('usuario.nome_usuario')
                 ->from('destinatario_mensagem')
                 ->join('usuario','usuario.idusuario = destinatario_mensagem.usuario_idusuario')
                 ->where('destinatario_mensagem.mensagem_idmensagem',$id_mensagem);
        return $this->db->get()->result_array();
    }


    public function desativa_destinatario($data){
        $this->db->where($data)
                 ->set('status',0)
                 ->update('destinatario_mensagem');
    }

/*
    public function obterTodasMensagens($id_user){
            $this->db->select('mensagem.idmensagem,mensagem.assunto_mensagem,usuario.nome_usuario,0 as count')
                 ->from('mensagem')
                 ->join('destinatario_mensagem','destinatario_mensagem.mensagem_idmensagem = mensagem.idmensagem')
                 ->join('recado_mensagem','recado_mensagem.mensagem_idmensagem = mensagem.idmensagem')
                 ->join('usuario','usuario.idusuario = recado_mensagem.remetente_idusuario')
                 ->where('destinatario_mensagem.usuario_idusuario',$id_user)
                 ->where('destinatario_mensagem.status',1)
                 ->group_by('mensagem.idmensagem')
                 ->order_by('mensagem.idmensagem','DESC');   
        return $this->db->get()->result_array();
    }

 * 
 */
}