<?php 
class Bolsa_model extends CI_Model {
	public function create($data)
	{
		$this->db->insert('bolsa',$data);
	}	
    
    public function get($id_monitor)
    {
        $this->db->select('*');
        $this->db->from('bolsa');
        $this->db->where('monitor_idusuario',$id_monitor);
        $this->db->order_by('data_inicio_bolsa');
        return $this->db->get()->result_array();
    }
    
    public function get_bolsa($id_bolsa)
    {
        $this->db->select('*');
        $this->db->from('bolsa');
        $this->db->where('id_bolsa',$id_bolsa);
        return $this->db->get()->row_array();
    }
    


    public function get_interseccao($data1,$data2,$id_monitor)
    {
        $this->db->select('*')
                 ->from('bolsa')
                 ->where('monitor_idusuario',$id_monitor)
                 ->group_start()
                    ->where("data_inicio_bolsa BETWEEN '$data1' AND '$data2'")
                    ->or_where("data_fim_bolsa BETWEEN '$data1' AND '$data2'")
                    ->or_where("'$data1' BETWEEN data_inicio_bolsa AND data_fim_bolsa")
                    ->or_where("'$data2' BETWEEN data_inicio_bolsa AND data_fim_bolsa")
                 ->group_end();
        //echo $this->db->get_compiled_select();         
        return count($this->db->get()->row_array());      
    }
    
    public function delete($id_bolsa)
    {
        $this->db->where('id_bolsa',$id_bolsa);
        $this->db->delete('bolsa');
    }
    
    public function update($data)
    {
        $this->db->where('id_bolsa',$data['id_bolsa']);
        $this->db->update('bolsa',$data);
    }
}
?>
	