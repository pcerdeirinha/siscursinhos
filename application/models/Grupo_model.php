<?php 
class Grupo_model extends CI_Model {
    public function getGruposPadroes($nivel){
        $this->db->select('*')
                 ->from('grupo')
                 ->join('grupo_padrao','grupo.idgrupo = grupo_padrao.grupo_idgrupo')
                 ->where('grupo_padrao.nivel_minimo <=',$nivel)
                 ->where('grupo_padrao.nivel_maximo >=',$nivel)
                 ->join('obtencao_grupo','grupo_padrao.grupo_idgrupo = obtencao_grupo.grupo_padrao_idgrupo');
        //echo $this->db->get_compiled_select();
        return $this->db->get()->result_array();
    }
    
    public function getGruposPersonalizados($id_user){
        $this->db->select('*')
                 ->from('grupo')
                 ->join('grupo_personalizado','grupo.idgrupo = grupo_personalizado.grupo_idgrupo')
                 ->where('grupo_personalizado.usuario_idusuario',$id_user);
        return $this->db->get()->result_array();
    }
    
    public function getGrupoArmazenamento($idgrupo){
        $this->db->select('*')
                 ->from('armazenamento_grupo')
                 ->where('grupo_padrao_idgrupo',$idgrupo);
        return $this->db->get()->row_array();
    }
    
    public function getGruposPadraoInArray($array_grupo,$nivel){
        $this->db->select('grupo_idgrupo')
                 ->from('grupo_padrao')
                 ->where('grupo_padrao.nivel_minimo <=',$nivel)
                 ->where('grupo_padrao.nivel_maximo >=',$nivel)
                 ->where_in('grupo_idgrupo',$array_grupo);
        return (array_intersect($array_grupo, $this->db->get()->row_array()));
    }
    
    public function getGruposPersonalizadoInArray($array_grupo,$id_user){
        $this->db->select('grupo_idgrupo')
                 ->from('grupo_personalizado')
                 ->where_in('grupo_idgrupo',$array_grupo)
                 ->where('grupo_personalizado.usuario_idusuario',$id_user);
        return (array_intersect($array_grupo, $this->db->get()->row_array()));
    }

    public function getUsersInGrupoPersonalizado($idgrupo){
        $this->db->select('grupo_usuario.usuario_idusuario,usuario.nome_usuario,usuario.idusuario')
                 ->from('grupo_usuario')
                 ->join('usuario','usuario.idusuario = grupo_usuario.usuario_idusuario')
                 ->where('grupo_personalizado_idgrupo',$idgrupo);
        return $this->db->get()->result_array();
    }

    public function remove_usuario($data){
        $this->db->delete('grupo_usuario',$data);
    }
    
    public function cria_grupo($data){
        $this->db->insert('grupo',$data);
        return $this->db->insert_id();
    }
    
    public function cria_grupo_personalizado($data){
        $this->db->insert('grupo_personalizado',$data);
        return $this->db->insert_id();
    }
    
    public function adicionaMembro($data){
        $this->db->insert('grupo_usuario',$data);
        return $this->db->insert_id();
    }
    
    public function removerGrupo($idgrupo){
        $this->db->where('idgrupo',$idgrupo);
        $this->db->delete('grupo');
    }
    
}