<?php
class Matricula_model extends CI_Model {

	public function get($id){
		if($id)$this->db->where('idmatricula', $id);
	    $this->db->order_by('data_matricula', 'asc');
	    $get = $this->db->get('matricula');
	    if($id) return $get->row_array();
	    if($get->num_rows > 0) return $get->result_array();
	    return array();
	}

	public function getMatriculasAluno($idusuario){
		$this->db->where('matricula_idaluno', $idusuario);
		$this->db->order_by('data_matricula', 'asc');
	    $get = $this->db->get('matricula');
	    if($get->num_rows > 0) return $get->result_array();
	    return array();
	}

	public function getUltimaMatricula($idusuario){
		$this->db->where('matricula_idaluno', $idusuario);
		$this->db->order_by('idmatricula', 'desc');
	    $get = $this->db->get('matricula');
	    if($get->num_rows > 0) return $get->result_array();
	    return array();	
	}

	public function create($data){
		return $this->db->insert('matricula', $data); 
	}

	public function update($idmatricula, $data){
		$this->db->where('idmatricula', $idmatricula);
		$update = $this->db->update('matricula', $data);
		return $update;
	}

	public function getMatriculaAtiva($idusuario){
		$this->db->where('matricula_idaluno', $idusuario);
		$this->db->where('status', 1);
		$get = $this->db->get('matricula');
		if($get->num_rows > 0) return $get->result_array();
		return array();		
	}

	public function matriculaAtiva($idusuario){
		$this->db->where('matricula_idaluno', $idusuario);
		$this->db->where('status', 1);
	    $get = $this->db->get('matricula');
	    if ($get->num_rows() > 0) {
   			return true;
   		}else{
   			return false;	
   		} 
	}

	public function qtdMatriculas($idturma){
		$this->db->select('COUNT(*) as total');
		$this->db->where('matricula_idturma',$idturma);
		$this->db->where('status', 1);
		return $this->db->get('matricula')->row()->total;		
	}
}