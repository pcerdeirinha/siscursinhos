<?php 
class Frequencia_model extends CI_Model {

    public function adicionarFalta($data)
    {
        return $this->db->replace('frequencia',$data);
    }   
    
    public function removerFalta($data)
    {
        return $this->db->delete('frequencia',$data);
    }
    
    public function possuiFalta($data)
    {
        $freq = $this->db->get_where('frequencia',$data);
        
        if ($freq->num_rows > 0) return true;
        return false;
    }
    
    public function getFrequenciAula($id_aula)
    {
        $this->db->select('*')
                 ->from('frequencia')
                 ->where('frequencia.aula_idaula',$id_aula);
                 
        return $this->db->get()->result_array();
    }
    
    public function removerFaltasAula($id_aula){
        return $this->db->delete('frequencia',array('aula_idaula'=>$id_aula));
    }      
        
}

 ?>
    