<?php
class Oferta_model extends CI_Model {
	public function get($disc_mon_tur){
		$this->db->where('id_oferta', $disc_mon_tur);
		$get = $this->db->get('oferta_disciplina');
		if($get->num_rows > 0) return $get->row_array();
	    return array();
	}

	public function getTurma($idturma){
        $this->db->where('idturma', $idturma);
        $get = $this->db->get('turma');
        return $get->row_array();
    }

	public function getMonitor($idmonitor){
		$this->db->where('usuario_idusuario', $idmonitor);
		$get = $this->db->get('monitor');
		return $get->row_array();
	}

	public function getDisciplina($iddisciplina){
		$this->db->where('iddisciplina', $iddisciplina);
		$get = $this->db->get('disciplina');
		return $get->row_array();	
	}

    

    public function getDiscTur($idturma){ // Pega as disciplinas de uma determinada turma
        $this->db->select('*');
        $this->db->from('disciplina');
        $this->db->join('oferta_disciplina', 'oferta_disciplina.iddisciplina = disciplina.iddisciplina');
        $this->db->where('oferta_disciplina.idturma', $idturma);
        $this->db->where('oferta_disciplina.status', 1);
        $get = $this->db->get('disciplina');
        if($get->num_rows > 0) return $get->result_array();
        return array();
    }
    
    public function getOferTur($idturma){
        $this->db->select('*');
        $this->db->from('oferta_disciplina');
        $this->db->join('disciplina','disciplina.iddisciplina = oferta_disciplina.iddisciplina');
        $this->db->where('disciplina.status = 1');
        $this->db->where('oferta_disciplina.idturma', $idturma);
        $this->db->where('oferta_disciplina.status', 1);
        return ($this->db->get()->result_array());   
    }
    
    public function getDiscLiv($idTurma)
    {
        $ofertas = $this->getOferTur($idTurma);
        foreach ($ofertas as $oferta) {
            $lista_disciplinas[] = $oferta['iddisciplina'];
        }  
        
        $this->db->select('*');
        $this->db->from('turma');
        $this->db->where('idturma',$idTurma);
        $idcurso = $this->db->get()->row()->curso_idcurso;
        
        $this->db->select('*');
        $this->db->from('disciplina');
        $this->db->where('disciplina_idcurso',$idcurso);
        $this->db->where('status',1);
        $this->db->where_not_in('iddisciplina',$lista_disciplinas);
        
        return $this->db->get()->result_array();
        
        
        
        
    }

    public function getDiscMon($idmonitor){ // Pega as disciplinas de um determinado professor
        $this->db->select('*');
        $this->db->from('disciplina');
        $this->db->join('oferta_disciplina', 'oferta_disciplina.iddisciplina = disciplina.iddisciplina');
        $this->db->where('oferta_disciplina.monitor_usuario', $idmonitor);
        $this->db->where('oferta_disciplina.status', 1);
        $get = $this->db->get('disciplina');
        if($get->num_rows > 0) return $get->result_array();
        return array();
    }


	public function getOferMon($idmonitor){ // Pega as ofertas de um determinado professor
		$this->db->where('monitor_idusuario', $idmonitor);
        $this->db->where('status', 1);
	    $get = $this->db->get('oferta_disciplina');
	    if($get->num_rows > 0) return $get->result_array();
	    return array();
	}

    public function getOferMonUni($idmonitor,$idunidade){ //pega as ofertas de um professor em uma unidade determinada
        $this->db->select('*')
                 ->from('oferta_disciplina')
                 ->join('usuario','oferta_disciplina.monitor_idusuario = usuario.idusuario')
                 ->join('turma','oferta_disciplina.idturma = turma.idturma')
                 ->where('usuario.idusuario',$idmonitor)
                 ->where('turma.curso_unidade_idunidade',$idunidade);
        $get = $this->db->get();
        if($get->num_rows > 0) return $get->result_array();
        return array();
    }
    
    public function getOferUni($idUnidade)//Pega todas as ofertas de uma Unidade
    {
        $this->db->select('*');
        $this->db->from('oferta_disciplina');
        $this->db->join('turma','turma.idturma = oferta_disciplina.idturma');
        $this->db->where('turma.curso_unidade_idunidade',$idUnidade);
        $this->db->where('oferta_disciplina.status', 1);
        
        return $this->db->get()->result_array();  
    }

    public function UnidPossuiOfer($idUnidade,$idOferta)
    {
        $this->db->select('COUNT(*) as total');
        $this->db->from('oferta_disciplina');
        $this->db->where('oferta_disciplina.id_oferta',$idOferta);
        $this->db->join('turma','turma.idturma = oferta_disciplina.idturma');
        $this->db->where('turma.curso_unidade_idunidade',$idUnidade);
        
        
        return $this->db->get()->row()->total;
    }
    
    public function possuiMatricula($data)
    {
        
        $this->db->select('COUNT(*) as total');
        return $this->db->get_where('oferta_disciplina',$data)->row()->total;
    }
    
	public function create($data){
        if ($data['status']==1){
            $time = strtotime($data['data_inicio_oferta'].'-1 days');
            $dia_fim = date("Y-m-d", $time);
            $this->db->set('status',0);
            $this->db->set('data_fim_oferta',$dia_fim);
            $this->db->where('idturma',$data['idturma']);
            $this->db->where('iddisciplina',$data['iddisciplina']);
            $this->db->where('data_fim_oferta IS NULL');
            $this->db->update('oferta_disciplina');
        }       
        return $this->db->insert('oferta_disciplina', $data); 
    }

    public function getWhere($data)
    {
        return $this->db->get_where('oferta_disciplina',$data)->row_array();
    }

    public function delete($id_oferta,$data){
		$this->db->where('id_oferta', $id_oferta);
		$this->db->update('oferta_disciplina',$data);
	}
}
?>