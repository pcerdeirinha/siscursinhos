<?php
class Cursofaculdade_model extends CI_Model {
	
	public function get($dep){
		$this->db->where('curso_iddepartamento', $dep);
		$get = $this->db->get('curso_faculdade');
		if($get->num_rows > 0) return $get->result_array();
	    return array();
	}

    public function getFaculdade($id){
        $this->db->select('*')
                 ->from('curso_faculdade')
                 ->join('departamento','departamento.iddepartamento = curso_faculdade.curso_iddepartamento')
                 ->where('departamento.depto_idfaculdade',$id);
        $get = $this->db->get();
        if($get->num_rows > 0) return $get->result_array();
        return array();
    }

	public function getCurso($curso){
		$this->db->where('idcurso_faculdade', $curso);
		$get = $this->db->get('curso_faculdade');
		if($get->num_rows > 0) return $get->row_array();
	    return array();
	}
    
    public function getPeriodos()
    {
        return $this->db->get('periodo')->result_array();
    }
	
    
    public function create($sql_data)
    {
        $this->db->insert('curso_faculdade',$sql_data);
        return $this->db->insert_id();   
    }
    
    public function createPeriodo($data)
    {
        return $this->db->insert('periodo_curso_faculdade',$data);
    }
}
?>