<?php
class Aprovacao_model extends CI_Model {

	public function get($id){
		if($id)$this->db->where('idaprovacao', $id);
	    $this->db->order_by('data_aprovacao', 'asc');
	    $get = $this->db->get('aprovacao');
	    if($id) return $get->row_array();
	    if($get->num_rows > 0) return $get->result_array();
	    return array();
	}

	public function getAprovacoesAluno($idusuario){
		$this->db->where('aluno_usuario_idusuario', $idusuario);
		$this->db->where('status',1);
		$this->db->order_by('data_aprovacao', 'asc');
	    $get = $this->db->get('aprovacao');
	    if($get->num_rows > 0) return $get->result_array();
	    return array();
	}	

	public function create($data){
		return $this->db->insert('aprovacao', $data); 
	}

	public function delete($id){
	    $this->db->set('status',0);
		$this->db->where('idaprovacao', $id);
		$this->db->update('aprovacao');
	}
    
	
}
?>