<?php
class Aluno_model extends CI_Model {

	public function create($data){
		return $this->db->insert('aluno', $data); 
	}

	public function turmaTemAluno($idturma){
		$this->db->where('turma_idturma', $idturma);
        $get = $this->db->get('aluno');
        if($get->num_rows > 0) return true;
        else false;
	}

	public function getAlunos($idunidade){
	    
        $this->db->select('*')
                 ->from('aluno')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = aluno.usuario_idusuario')
                 ->where('unidade_usuario.unidade_idunidade',$idunidade)
	             ->order_by('aluno.usuario_idusuario', 'asc');
	    $get = $this->db->get();
	    if($get->num_rows > 0) return $get->result_array();
	    return array();
	}
    
    /*
     * 
     * getAluno($id);
     * Método capaz de obter os dados de um Aluno a partir de seu respectivo id;
     * 
     */
    public function getAluno($id){
        $this->db->where('usuario_idusuario', $id);
        $get = $this->db->get('aluno');
        if($get->num_rows > 0) return $get->row_array();
        return array();
    }
    
    
    public function update($id, $data){
        $this->db->where('usuario_idusuario', $id);
        $update = $this->db->update('aluno', $data);
        return $update;
    }
    /*
     * 
     * replace($data);
     * Método capaz de atualizar dar um replace(deletar e criar) num $data específico;
     * 
     */
    
    public function replace($data){
        $this->db->replace('aluno', $data);
    }
    
    
    public function obterAlunoTurma($idTurma,$data_matricula)
    {
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->join('matricula','matricula.matricula_idaluno = usuario.idusuario ');
        
        $this->db->where('matricula.status = 1');
        $this->db->where('matricula.data_matricula <=',$data_matricula);
        $this->db->where('matricula.matricula_idturma = ',$idTurma);
        $this->db->where('usuario.status',1);
        $this->db->order_by('usuario.nome_usuario');
        return ($this->db->get()->result_array());
    }
    
    public function getAlunosTurma($idTurma)
    {
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->join('matricula','matricula.matricula_idaluno = usuario.idusuario ');
        
        $this->db->where('matricula.status = 1');
        $this->db->where('matricula.matricula_idturma = ',$idTurma);
        $this->db->where('usuario.status',1);
        $this->db->order_by('usuario.nome_usuario');
        return ($this->db->get()->result_array());
    }
    
        public function obterAlunoTurmaTrans($idTurma,$data_trancamento)
    {
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->join('matricula','matricula.matricula_idaluno = usuario.idusuario ');
        
        $this->db->where('matricula.status > 1');
        $this->db->where('matricula.data_matricula <=',$data_trancamento);
        $this->db->where('matricula.data_trancamento >=',$data_trancamento);
        $this->db->where('matricula.matricula_idturma = ',$idTurma);
        $this->db->where('usuario.status',1);
        return ($this->db->get()->result_array());
    }
	
	public function getConcluintes()
	{
		$this->db->select('*');
		$this->db->from('aluno');
		$this->db->where('ano_conclusao_em <=',date('Y'));
		return ($this->db->get()->result_array());
	}

    public function getAlunosMonitor($idMonitor){
        $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('unidade_usuario.tipo_usuario',1)
                 ->join('matricula','matricula.matricula_idaluno = usuario.idusuario')
                 ->where('matricula.status',1)
                 ->join('oferta_disciplina','oferta_disciplina.idturma = matricula.matricula_idturma')
                 ->where('oferta_disciplina',$idMonitor);
        return $this->db->get()->result_array();
    }
    
    public function getAlunosCurso($idCurso){
        $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('unidade_usuario.tipo_usuario',1)
                 ->join('matricula','matricula.matricula_idaluno = usuario.idusuario')
                 ->where('matricula.status',1)
                 ->join('turma','turma.idturma = matricula.matricula_idturma')
                 ->where('turma.curso_idcurso',$idCurso);
        return $this->db->get()->result_array();
    }

    public function getAllAlunos(){
        $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('unidade_usuario.tipo_usuario',1);
       return $this->db->get()->result_array();
    }
    
}