<?php  
class Socio_model extends CI_Model {
    public function getAll(){
        $data['etnia'] = $this->db->get('socio_etnia')->result_array();
        $data['ensino'] = $this->db->get('socio_ensino')->result_array();
        $data['escolaridade'] = $this->db->get('socio_escolaridade_resp')->result_array();
        $data['remunerada'] = $this->db->get('socio_atividade_remunerada')->result_array();
        $data['habito_cult'] = $this->db->get('socio_habito')->result_array();
        $data['info_atualidades'] = $this->db->get('socio_info_atualidades')->result_array();
        $data['vez'] = $this->db->get('socio_vezes_cursinho')->result_array();
        return $data;
    }
    
    public function limpaAtualidades($id_user){
        $this->db->where('aluno_idusuario',$id_user);
        $this->db->delete('socio_atua_aluno');
    }
    
    public function criaAtualidade($data){
        $this->db->insert('socio_atua_aluno',$data);
    }
    public function obterAtualidade($id_user){
        $this->db->select('atualidade_idatualidade')
                 ->from('socio_atua_aluno')
                 ->where('aluno_idusuario',$id_user);
        return $this->db->get()->result_array();
    }
    
    public function limpaPretensoes($id_user){
        $this->db->where('aluno_idaluno',$id_user);
        $this->db->delete('pretensao_vestibular');
    } 
    public function criaPretensao($data){
        $this->db->insert('pretensao_vestibular',$data);
    }
    public function obterPretensao($id_user){
        $this->db->select('faculdade_vestibular_idfaculdade')
                 ->from('pretensao_vestibular')
                 ->where('aluno_idaluno',$id_user);
        return $this->db->get()->result_array();
    }

    public function limpaHabitos($id_user){
        $this->db->where('aluno_idusuario',$id_user);
        $this->db->delete('socio_habito_aluno');
    } 
    public function criaHabito($data){
        $this->db->insert('socio_habito_aluno',$data);
    }
    public function obterHabito($id_user){
        $this->db->select('habito_idhabito')
                 ->from('socio_habito_aluno')
                 ->where('aluno_idusuario',$id_user);
        return $this->db->get()->result_array();
    }

}
