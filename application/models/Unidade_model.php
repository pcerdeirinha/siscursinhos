<?php
class Unidade_model extends CI_Model {
	public function get($unidade){
		$this->db->where('idunidade', $unidade);
		$get = $this->db->get('unidade');
		if($get->num_rows > 0) return $get->row_array();
	    return array();
	}
    
    public function update($id,$data)
    {
        $this->db->where('idunidade', $id);
        $update = $this->db->update('unidade', $data);
        return $update;    
    }
    
    public function create($data)
    {
        $this->db->insert('unidade', $data); 
        return $this->db->insert_id();
    }
    
    public function getUnidades()
    {
        $this->db->select('*')
                 ->from('unidade')
                 ->order_by('unidade.nome_cursinho_unidade')
                 ->where('unidade.idunidade !=',5);
        $get = $this->db->get();
        return $get->result_array();
    }
    
    public function getUnidadesFaculdade($idfaculdade)
    {
        $this->db->where('unidade_idfaculdade',$idfaculdade);
        $get = $this->db->get('unidade');
        return $get->result_array();
    }
    
    
    public function delete($id)
    {
        $this->db->where('idunidade', $id);
        $this->db->delete('unidade');
    }
}
?>