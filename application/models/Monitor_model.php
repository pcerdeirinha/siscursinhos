<?php
class Monitor_model extends CI_Model {

	public function create($data)
	{
		return $this->db->insert('monitor', $data);
	}

	public function getMonitor($id){
		$this->db->where('usuario_idusuario', $id);
		$get = $this->db->get('monitor');
		if($get->num_rows > 0) return $get->row_array();
	    return array();
	}
    
    public function getMonitoresComOfertas($unidade)
    {
        $this->db->select('*')
                 ->from('usuario')
                 ->join('oferta_disciplina','oferta_disciplina.monitor_idusuario = usuario.idusuario')
                 ->join('turma','turma.idturma = oferta_disciplina.idturma')
                 ->where('turma.curso_unidade_idunidade',$unidade)
                 ->where('oferta_disciplina.status',1)
                 ->group_by('usuario.idusuario');
                 
        return $this->db->get()->result_array();
    }
    

	public function update($id,$data){
		$this->db->where('usuario_idusuario', $id);
		$update = $this->db->update('monitor', $data);
		return $update;
	}
    
    public function getCoordenadoresUnidade($idUnidade){
        $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('unidade_usuario.unidade_idunidade',$idUnidade)
                 ->where('unidade_usuario.tipo_usuario',4);
        return $this->db->get()->result_array();
    }
    
    public function getAllBolsistas(){
        $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('unidade_usuario.tipo_usuario >',1)
                 ->where('unidade_usuario.tipo_usuario <',5)
                 ->group_by('usuario.idusuario');
       return $this->db->get()->result_array();
    }
	
	public function getMonitoresAluno($idaluno)
	{
		$this->db->select('*')
                 ->from('usuario')
                 ->join('oferta_disciplina','oferta_disciplina.monitor_idusuario = usuario.idusuario')
                 ->join('matricula','matricula.matricula_idturma = oferta_disciplica.idturma')
                 ->where('matricula.matricula_idaluno',$idaluno)
				 ->group_by('usuario.idusuario');
       return $this->db->get()->result_array();
	}

	public function replace($data){
        $this->db->replace('monitor', $data);
    }
}