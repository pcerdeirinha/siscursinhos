<?php
class Curso_vestibular_model extends CI_Model {

	public function create($data)
	{
		return $this->db->insert('curso_vestibular', $data);
	}

	public function getCurso($id){
		$this->db->where('idCurso', $id);
		$get = $this->db->get('curso_vestibular');
		if($get->num_rows > 0) return $get->row_array();
	    return array();
	}
    
    public function getCursos()	{
		$this->db->order_by('nomeCurso', 'asc');
	    $get = $this->db->get('curso_vestibular');
	    if($get->num_rows > 0) return $get->result_array();
	    return array();
	}

	
}
?>