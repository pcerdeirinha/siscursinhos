<?php
class Turma_model extends CI_Model {

    public function get($curso)
    {
        $this->db->where('curso_idcurso', $curso);
        $this->db->where('status',1);
        $this->db->order_by('idturma', 'asc');
        $get = $this->db->get('turma');
        if($get->num_rows > 0) return $get->result_array();
        return array();
    }
    
    public function getTurma($idturma){
        $this->db->where('idturma', $idturma);
        $get = $this->db->get('turma');
        return $get->row_array();
    }

    public function create($data){
    	$this->db->insert('turma', $data); 
        return $this->db->insert_id();
    }

    public function cursoTemTurma($curso_idcurso){//verifica se um curso tem pelo menos uma turma
    	$this->db->where('curso_idcurso', $curso_idcurso);
        $get = $this->db->get('turma');
        if($get->num_rows > 0) return true;
        else false;
    }

    public function getTurmas($unidade)	{
		$this->db->where('curso_unidade_idunidade', $unidade);
        $this->db->where('status',1);
	    $this->db->order_by('nome_turma', 'asc');
	    $get = $this->db->get('turma');
	    if($get->num_rows > 0) return $get->result_array();
	    return array();
	}
    
   public function getTurmaCurso($idcurso){
        $this->db->where('curso_idcurso',$idcurso);
        $this->db->where('status',1);
        return($this->db->get('turma')->result_array());
    }

    public function UnidadePossuiTurma($idTurma,$idUnidade)
    {
        $this->db->select('COUNT(*) as total');
        $this->db->from('turma');
        $this->db->where('turma.idturma',$idTurma);
        $this->db->join('curso','curso.idcurso = turma.curso_idcurso');
        $this->db->where('curso.unidade_idunidade',$idUnidade);
        return $this->db->get()->row()->total;
    }
    
    public function getTurmasMonitor($idMonitor){
        $this->db->select('*')
                 ->from('turma')
                 ->join('oferta_disciplina','oferta_disciplina.idturma = turma.idturma')
                 ->where('oferta_disciplina.monitor_idusuario',$idMonitor);
        return $this->db->get()->result_array();                    
    }
    
    public function getTurmasUnidade($idUnidade)
    {
        $this->db->select('*')
                  ->from('turma')
                  ->join('curso','curso.idcurso = turma.curso_idcurso')
                  ->where('curso.unidade_idunidade',$idUnidade);
        return $this->db->get()->result_array();
    }

    public function update($id,$data){
        $this->db->where('idturma', $id);
        $update = $this->db->update('turma', $data);
        return $update;
    }

	public function delete($idturma){
		$this->db->where('idturma', $idturma);
        $this->db->set('status',0);
		$this->db->update('turma');
	}
}