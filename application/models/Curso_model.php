<?php
class Curso_model extends CI_Model {

	public function get($id){
		$this->db->where('idcurso', $id);
	    $get = $this->db->get('curso');
	   	return $get->row_array();
	}

	public function getCursos($unidade)
	{
		$this->db->where('unidade_idunidade', $unidade);
        $this->db->where('status',1);
	    $this->db->order_by('idcurso', 'asc');
	    $get = $this->db->get('curso');
	    if($get->num_rows > 0) return $get->result_array();
	    return array();
	}
	
			public function getAllCursos($unidade)
		{
			$this->db->where('unidade_idunidade', $unidade);	        
		    $this->db->order_by('idcurso', 'asc');
		    $get = $this->db->get('curso');
		    if($get->num_rows > 0) return $get->result_array();
		    return array();
		}

	public function create($data){
		return $this->db->insert('curso', $data); 
	}

	public function update($id,$data){
		$this->db->where('idcurso', $id);
		$update = $this->db->update('curso', $data);
		return $update;
	}
	public function delete($idcurso){
	    $this->db->set('status',0);
		$this->db->where('idcurso', $idcurso);
		$this->db->update('curso');
	}
    
    public function UnidadeContemCurso($idCurso,$idUnidade)
    {
        $this->db->select('COUNT(*) as total');
        $this->db->where('unidade_idunidade',$idUnidade);
        $this->db->where('idcurso',$idCurso);
        return $this->db->get('curso')->row()->total;
    }

	public function cursoturma($unidade){
		$this->db->select('turma.idturma,turma.curso_idcurso,turma.nome_turma,turma.data_inicio_turma,curso.nome_curso');
		$this->db->from('turma');
		$this->db->join('curso', 'curso.idcurso = turma.curso_idcurso');
		$this->db->where('curso.unidade_idunidade', $unidade);
	    $get = $this->db->get();
	    if($get->num_rows > 0) return $get->result_array();
	    return array();
	}
    
   public function getCursosUnidade($idUnidade){
       $this->db->select('*')
                ->from('curso')
                ->where('unidade_idunidade',$idUnidade);
       return $this->db->get()->result_array();
   }
}