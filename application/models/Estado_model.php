<?php
class Estado_model extends CI_Model {
	public function get(){
		$get = $this->db->get('estado');
		if($get->num_rows > 0) return $get->result_array();
	}
		
    public function getUF($id){
        $this->db->select('uf');
        $this->db->where('id',$id);
         
        return $this->db->get('estado')->row()->uf;
       
    }    
    /*
     * getID($uf);
     * Obtém o id de um estado a partir de seu UF
     * 
     */
    public function getID($uf){
        $this->db->select('id');
        $this->db->where('uf',$uf);
         
        return $this->db->get('estado')->row()->id;
       
    } 
}
?>