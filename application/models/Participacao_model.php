<?php
class Participacao_model extends CI_Model {
    public function get_participante_interno($id_usuario)
    {
        $this->db->select('*')
                 ->from('participante_interno')
                 ->where('usuario_idusuario',$id_usuario);
        return $this->db->get()->row_array();
    }
    
    public function get_participante_externo($email_usuario)
    {
        $this->db->select('*')
                 ->from('participante_externo')
                 ->where('email_participante',$email_usuario);
        return $this->db->get()->row_array();
    }
    
    public function inserir_participacao($sql)
    {
        $this->db->insert('participacao',$sql);
    }
    
    public function inserir_participante_interno($sql)
    {
        $this->db->insert('participante_interno',$sql);
    }
    
    public function inserir_participante_externo($sql)
    {
        $this->db->insert('participante_externo',$sql);
    }
    
    public function inserir_participante()
    {
        $this->db->insert('participante',array('idparticipante'=>null));
        return $this->db->insert_id();
                
    }
    
    public function participacao_confirmada($id_user,$id_evento)
    {
        $this->db->select('count(*) as total')
                 ->from('participacao')
                 ->join('participante_interno','participante_interno.participante_idparticipante = participacao.participante_idparticipante')
                 ->where('participante_interno.usuario_idusuario',$id_user)
                 ->where('participacao.evento_idevento',$id_evento);
        return $this->db->get()->row()->total;
    }
        
    public function remover_participacao($sql)
    {
        $this->db->delete('participacao',$sql);
    }
    
    public function qtd_participacoes($id_evento)
    {
        $this->db->select('count(*) as total')
                 ->from('participacao')
                 ->where('evento_idevento',$id_evento);
        return $this->db->get()->row()->total;
    }
}
?>
