<?php
class Horario_model extends CI_Model {
	



	public function get($idgrade_horaria){
	    $this->db->select('*');
        $this->db->from('horario');
        $this->db->where('grade_horaria_idgrade',$idgrade_horaria);
	    $this->db->order_by('pos_horario', 'asc');
		$get = $this->db->get();
		if($get->num_rows > 0) return $get->result_array();
	    return array();

	}

    public function getGrade_Horaria($idturma){
        $this->db->select('*')
                 ->from('grade_horaria')
                 ->where('turma_idturma',$idturma);
        return $this->db->get()->result_array();    
    }
    
    public function getGrade ($idgrade){
        $this->db->select('*')
                 ->from('grade_horaria')
                 ->where('idgrade_horaria',$idgrade);
        return $this->db->get()->row_array();    
    }
	
	public function getHorario($idhorario)
	{
		$this->db->select('*');
        $this->db->from('horario');
		$this->db->where('idhorario',$idhorario);
		return ($this->db->get()->row_array());
	}
    
    public function getHorario_array($data)
    {
        $this->db->select('*');
        $this->db->from('horario');
        $this->db->where($data);
        return ($this->db->get()->row_array());
    }

	public function create($data){
		return $this->db->insert('horario', $data); 
	}
    
    public function createGrade_Horaria($data){
        $this->db->insert('grade_horaria',$data);
        return $this->db->insert_id();
    }
    
    public function updateGrade_Horaria($id,$data){
        $this->db->where('idgrade_horaria', $id);
        $update = $this->db->update('grade_horaria', $data);
        return $update;
    }
    

	public function update($id,$data){
		$this->db->where('idhorario', $id);
		$update = $this->db->update('horario', $data);
		return $update;
	}
	public function delete($idhorario){
		$this->db->where('idhorario', $idhorario);
		$this->db->delete('horario');
	}

    public function deleteGrade_Horaria($id){
        $this->db->where('idgrade_horaria', $id);
        $this->db->delete('grade_horaria');
    }
	
	
	public function horarioLivreTurma($idgrade,$pos,$dia)
	{
		$this->db->select('*');
		$this->db->from('horario');
		$this->db->where('horario.pos_horario',$pos);
		$this->db->where('horario.dia_horario',$dia);
		$this->db->where('grade_horaria_idgrade',$idgrade);
		$get = $this->db->get();
		if ($get->num_rows > 0) return FALSE;
		return TRUE;
	}

	public function horarioLivreProfessor($idoferta,$pos,$dia)
	{
		
		$this->db->select('monitor_idusuario');
		$this->db->from('oferta_disciplina');	
		$this->db->where('id_oferta',$idoferta);
		$idProf = $this->db->get()->row()->monitor_idusuario;
        
        $this->db->select('turma.periodo_turma');
        $this->db->from('turma');
        $this->db->join('oferta_disciplina','oferta_disciplina.idturma = turma.idturma');
        $this->db->where('oferta_disciplina.id_oferta',$idoferta);
        $periodo = $this->db->get()->row()->periodo_turma;
        
		$this->db->select('*');
		$this->db->from('horario');
		$this->db->where('horario.pos_horario',$pos);
		$this->db->where('horario.dia_horario',$dia);
		$this->db->join('oferta_disciplina','oferta_disciplina.id_oferta=horario.id_oferta');
		$this->db->where('oferta_disciplina.monitor_idusuario',$idProf);
        $this->db->join('turma','turma.idturma = oferta_disciplina.idturma');
        $this->db->where('turma.periodo_turma',$periodo);
		
		$get = $this->db->get();
		if ($get->num_rows > 0) return FALSE;
		return TRUE;
	}

}
?>