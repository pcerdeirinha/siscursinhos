<?php  
class Simulado_model extends CI_Model {
    
    
    public function get($idsimulado)
    {
        $this->db->where('idsimulado',$idsimulado);
        return $this->db->get('simulado')->row_array();
    }
    
    
    public function create($data)
    {
       $this->db->insert('simulado', $data); 
       return $this->db->insert_id();
       //return $this->db->get_where('simulado',$data)->row()->idsimulado;
    }
    
    public function adicionarNota($data)
    {
        return $this->db->insert('nota', $data);
    }
    
    public function getSimuladosTurma($idTurma)
    {
        $this->db->where('turma_idturma',$idTurma);
        return $this->db->get('simulado')->result_array();
    }
    
    
    public function obterAlunoSimulado($idSimulado)
    {
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->join('nota','usuario.idusuario = nota.aluno_idaluno');
        $this->db->where('nota.simulado_idsimulado',$idSimulado);
        return $this->db->get()->result_array();   
    }
    
    public function update($ids,$data)
    {
        $this->db->where('simulado_idsimulado',$ids['idsimulado']);
        $this->db->where('aluno_idaluno',$ids['idaluno']);
        $this->db->update('nota', $data);   
    }

    public function updateSimulado($id,$data){
        $this->db->where('idsimulado',$id);
        $this->db->update('simulado',$data);
    }

    public function replace($data)
    {
        $this->db->replace('nota', $data);   
    }
    
    public function delete($idsimulado)
    {
        $this->db->where('idsimulado', $idsimulado);
        return $this->db->delete('simulado');
    }
	
	public function getSimuladoTurmaAluno($idaluno)
	{
		$this->db->select('*');
		$this->db->from('simulado');
		$this->db->join('nota','simulado.idsimulado = nota.simulado_idsimulado');
		$this->db->where('nota.aluno_idaluno',$idaluno);
        $this->db->where('nota.valor_nota IS NOT NULL');
		$this->db->join('turma','turma.idturma=simulado.turma_idturma');
        $this->db->where('turma.status',1);
		$this->db->order_by('simulado.disciplina_iddisciplina DESC, simulado.area_idarea DESC, simulado.grande_area_idgrande_area DESC, simulado.data_simulado ASC, simulado.descricao_simulado ASC');
		return ($this->db->get()->result_array());
	}
	
}