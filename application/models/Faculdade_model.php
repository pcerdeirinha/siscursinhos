<?php
class Faculdade_model extends CI_Model {
	
	public function get($faculdade){
		$this->db->where('idfaculdade', $faculdade);
		$get = $this->db->get('faculdade');
		if($get->num_rows > 0) return $get->row_array();
	    return array();
	}
    
    public function update($id,$data)
    {
        $this->db->where('idfaculdade', $id);
        $update = $this->db->update('faculdade', $data);
        return $update;    
    }
    
    public function create($data)
    {
        return $this->db->insert('faculdade', $data); 
    }
    
    public function getFaculdades()
    {
        $this->db->select('*')
                 ->from('faculdade')
                 ->order_by('faculdade.nome_faculdade')
                 ->where('faculdade.idfaculdade !=',5);
        $get = $this->db->get();
        return $get->result_array();
    }
    
    public function getFaculdadeCidade($nome_cidade){
        $this->db->select('*')
                 ->from('faculdade')
                 ->where('faculdade.cidade_faculdade',$nome_cidade);
        return $this->db->get()->result_array();
    }
    
    public function delete($id)
    {
        $this->db->where('idfaculdade', $id);
        $this->db->delete('faculdade');
    }

}
?>