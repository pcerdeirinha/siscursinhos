<?php
class Cidade_model extends CI_Model {
	public function get($uf){
		$this->db->where('estado', $uf);
		$get = $this->db->get('cidade');
		if($get->num_rows > 0) return $get->result_array();
	    return array();
	}
}
?>