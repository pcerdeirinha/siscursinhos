<?php 
class Aula_model extends CI_Model {

    public function get($idAula)
    {
        $this->db->where('idaula',$idAula);
        return $this->db->get('aula')->row_array();
    }

    public function getAulasOfer($idOferta)
    {
        $this->db->where('aula_id_oferta',$idOferta);
        return $this->db->get('aula')->result_array();	
    }
    
    public function getAulasTurmaDisc($idTurma,$idDisciplina)
    {
        $this->db->select('*');
        $this->db->from('aula');
        $this->db->join('oferta_disciplina','oferta_disciplina.id_oferta = aula.aula_id_oferta');
        $this->db->where('oferta_disciplina.iddisciplina',$idDisciplina);
        $this->db->where('oferta_disciplina.idturma',$idTurma);
        
        return $this->db->get()->result_array();
    }
    
    
    public function create($data)
    {
        return $this->db->insert('aula', $data); 
    }
    
    public function update($idaula,$data)
    {
        $this->db->where('idaula', $idaula);
        $update = $this->db->update('aula', $data);
        return $update;
    }
    
    public function UnidadeContemOferta($idOferta,$idUnidade)
    {
        $this->db->select('COUNT(*) as total');
        $this->db->from('oferta_disciplina');
        $this->db->where('oferta_disciplina.id_oferta',$idOferta);
        $this->db->join('turma','turma.idturma = oferta_disciplina.idturma');
        $this->db->where('turma.curso_unidade_idunidade',$idUnidade);
        $total = $this->db->get()->row()->total;
        return ($total == 0) ? FALSE : TRUE;
    }

        
    public function getAulasFrequentadasOfertaDia($id_oferta,$data_aula){
        $this->db->select('idturma')
                 ->from('oferta_disciplina')
                 ->where('oferta_disciplina.id_oferta',$id_oferta);
        $query = $this->db->get()->row_array();
            
        $this->db->select('aula.idaula,disciplina.nome_disciplina,usuario.nome_usuario')
                 ->from('aula')
                 ->where('aula.data_aula',$data_aula)
                 ->join('oferta_disciplina','oferta_disciplina.id_oferta = aula.aula_id_oferta')
                 ->join('disciplina','disciplina.iddisciplina = oferta_disciplina.iddisciplina')
                 ->join('usuario','usuario.idusuario = oferta_disciplina.monitor_idusuario')
                 ->where('oferta_disciplina.idturma',$query['idturma'])
                 ->where('aula.frequencia_lancada',1);
       
       return $this->db->get()->result_array();
    }    
    
    public function delete($idaula)
    {
        $this->db->where('idaula', $idaula);
        return $this->db->delete('aula');
    }
    
        public function possuiFalta($data)
    {
        $freq = $this->db->get_where('frequencia',$data);
        
        if ($freq->num_rows > 0) return true;
        return false;
    }  
}

 ?>