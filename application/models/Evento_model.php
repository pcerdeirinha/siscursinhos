<?php
class Evento_model extends CI_Model {
   public function get($id)
   {
       $this->db->select('*')
                 ->from('evento')
                 ->where('idevento',$id);
        return($this->db->get()->row_array());
   }
        
    public function getAll($id_unidade)
    {
        $this->db->select('*')
                 ->from('evento');
        if (isset($id_unidade))
            $this->db->where('unidade_idunidade',$id_unidade);         
        return($this->db->get()->result_array());
    }
    
    public function get_imagens($id_evento)
    {
        $this->db->select('*')
                 ->from('imagem_evento')
                 ->where('evento_idevento',$id_evento);
        return ($this->db->get()->result_array());
    }
    
    public function get_imagem($id)
    {
        $this->db->select('*')
                 ->from('imagem_evento')
                 ->where('idimagem_evento',$id);
        return ($this->db->get()->row_array());
        
    }
    
    public function get_pagina($id_evento)
    {
        $this->db->select('*')
                 ->from('pagina_evento')
                 ->where('evento_idevento',$id_evento);
        return ($this->db->get()->row_array());
    }
    
    public function convidar_turma($array)
    {
        $this->db->replace('turma_evento',$array);
    }
    
    public function get_turmas_convidadas($id_evento)
    {
        $this->db->select('*')
                 ->from('turma_evento')
                 ->where('evento_idevento',$id_evento);
        return ($this->db->get()->result_array());
    }
    
    public function get_frequencia($id_evento)
    {
        $this->db->select('*')
                 ->from('frequencia_evento')
                 ->where('evento_idevento',$id_evento);
        return ($this->db->get()->result_array());
    }
    
    public function inserir_imagem($data)
    {
        $this->db->insert('imagem_evento',$data);
    }
    
    public function inserir_evento($data)
    {
        $this->db->insert('evento',$data);
        return $this->db->insert_id();
    }

    public function adicionar_falta($sql)
    {
        $this->db->replace('frequencia_evento',$sql);
    }
    
    public function update_evento($id,$data)
    {
        $this->db->where('idevento',$id);
        $this->db->update('evento',$data);
    }
    
    public function update_pagina($id,$data)
    {
        $this->db->where('evento_idevento',$id);
        $this->db->update('pagina_evento',$data);
    }
    
    public function update_imagem($id,$data)
    {
        $this->db->where('idimagem_evento',$id);
        $this->db->update('imagem_evento',$data);
    }
    
    public function remover_falta($sql)
    {
        $this->db->delete('frequencia_evento',$sql);
    }
    
    public function inserir_pagina($data)
    {
        $this->db->insert('pagina_evento',$data);
    }
    
    public function remove_evento($id_evento)
    {
        $this->db->where('idevento',$id_evento);
        $this->db->delete('evento');
    }
    
    public function remover_imagem($id_imagem)
    {
        $this->db->where('idimagem_evento',$id_imagem);
        $this->db->delete('imagem_evento');
    }
 
}
?>