<?php
class Docente_model extends CI_Model {

	public function create($data)
	{
		return $this->db->insert('docente', $data); 

	}
    
   public function replace($data){
        $this->db->replace('docente', $data);
    }
   
    public function update($id,$data){
        $this->db->where('usuario_idusuario', $id);
        $update = $this->db->update('docente', $data);
        return $update;
    }
    
    
    public function getDocente($id){
        $this->db->where('usuario_idusuario', $id);
        $get = $this->db->get('docente');
        if($get->num_rows > 0) return $get->row_array();
        return array();
    }
    
    
    public function getDocentesUnidade($idUnidade){
        $this->db->select('*')
                 ->from('usuario')
                 ->join('unidade_usuario','unidade_usuario.usuario_idusuario = usuario.idusuario')
                 ->where('unidade_usuario.unidade_idunidade',$idUnidade)
                 ->where('unidade_usuario.tipo_usuario',5);
        return $this->db->get()->result_array();         
    }

    
}