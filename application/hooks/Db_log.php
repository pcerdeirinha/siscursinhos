<?php 
// Name of Class as mentioned in $hook['post_controller]
class Db_log {
 
    function __construct() {
       // Anything except exit() :P
    }
 
    // Name of function same as mentioned in Hooks Config
    function logQueries() {
 
        $CI = & get_instance();
 
        $times = $CI->db->query_times;                   // Get execution time of all the queries executed by controller
        foreach ($CI->db->queries as $key => $query) {
            if ((strpos($query, 'imagem_evento')===FALSE)&&((strpos($query, 'SELECT')===FALSE)&&((!(strpos($query, 'ci_sessions')===FALSE)&&(strpos($query, 'DELETE')===FALSE))||(strpos($query, 'ci_sessions')===FALSE)))){ 
                $sql_log['query_log'] = $query;
                $sql_log['time_log'] = $times[$key]; // Generating SQL file alongwith execution time
                $sql_log['usuario_idusuario'] = $CI->session->userdata('user');
                $sql_log['data_hora_log'] = date("Y-m-d H:i:s");
                $CI->db->insert('log_execucao',$sql_log);  
            }
        }
 
    }
 
}

?>